<?php
    /**
     * Базовый класс
     *
     * @package Core
     */

class MSCore
{
    private static $_includePaths = array();
    private static $_pages = array();
    private static $_db = null;
    private static $_site = null;
    private static $_urls = null;
    private static $_modules = null;
    private static $_forms = null;
    private static $_settings = null;

    private static $_systemClassMap = array(
        'BreadCrumbs' => 'system/BreadCrumbs.php',
        'DBEngine' => 'system/DBEngine.php',
        'FormConfigs' => 'system/FormConfigs.php',
        'ImageToolbox' => 'vendor/ImageToolbox.php',
        'Minify_HTML' => 'min/lib/Minify/HTML.php',
        'ModuleControls' => 'system/ModuleControls.php',
        'ModuleDirector' => 'system/ModuleDirector.php',
        'MSCatalogControl' => 'system/MSCatalogControl.php',
        'MSFiles' => 'system/MSFiles.php',
        'MSError' => 'system/MSError.php',
        'MSModuleController' => 'system/MSModuleController.php',
        'MSCustomPage' => 'system/MSCustomPage.php',
        'MSPage' => 'system/MSPage.php',
        'MSSettings' => 'system/MSSettings.php',
        'MSSite' => 'system/MSSite.php',
        'MSTable' => 'system/MSTable.php',
        'MSTapeControl' => 'system/MSTapeControl.php',
        'MSUser' => 'system/MSUser.php',
        'MSConfig' => 'system/MSConfig.php',
        'MSNestedSets' => 'system/MSNestedSets.php',
        'PagesTree' => 'system/PagesTree.php',
        'qqFileUploader' => 'vendor/qqFileUploader.php',
        'Redirects' => 'system/Redirects.php',
        'SendMail' => 'system/SendMail.php',
        'StaticFiles' => 'system/StaticFiles.php',
        'Tabs' => 'system/Tabs.php',
        'UrlsAndPaths' => 'system/UrlsAndPaths.php',
        'Xml2Array' => 'system/Xml2Array.php',
        'Validator' => 'vendor/Validator.php',
        'MSRegistry' => 'system/MSRegistry.php',
        'MSEvent' => 'system/MSEvent.php',
        'MSSession' => 'system/MSSession.php',
        'MSCookie' => 'system/MSCookie.php',
        'Widget' => 'system/Widget.php',
        'MSGCF' => 'system/MSGCF.php',
        'MSJs' => 'system/MSJs.php',
        'EnStemmer' => 'vendor/Stemmer/EnStemmer.php',
        'RuStemmer' => 'vendor/Stemmer/RuStemmer.php',
        'MSSeeder' => 'system/MSSeeder.php'
    );

    /**
     * Экземпляр класса управления сайтом.
     *
     * @return MSSite
     */
    public static function site()
    {
        if (is_null(static::$_site))
        {
            static::$_site = new MSSite();
        }

        return static::$_site;
    }

    /**
     * Подключение к базе данных.
     *
     * При первом обращении создает экземпляр класса работы с базой данных,
     * при дальнейших вызовах возвращает существующее подключение.
     *
     * @return DBEngine
     */
    public static function db()
    {
        if (is_null(static::$_db))
        {
            static::$_db = new DBEngine('mysql', CONFIG_DBHOST, CONFIG_DBUSER, CONFIG_DBPASS, CONFIG_DBNAME);
        }

        return static::$_db;
    }

    /**
     * Экземпляр объекта «страница».
     *
     * При первом обращении передается массив с параметрами страницы
     *
     * @see UrlsAndPaths::processCurrentUrl()
     *
     * @param string $url
     *
     * @return MSCustomPage
     */
    public static function page($url = null)
    {
        if($url === null)
        {
            $url = MSCore::urls()->requestUri;
        }

        if (empty(static::$_pages[$url]))
        {
            $pageData = MSCore::urls()->processCurrentUrl($url);
            static::$_pages[$url] = new MSCustomPage($pageData);
        }

        return static::$_pages[$url];
    }

    /**
     * @param string $moduleName
     * @param int $pathId
     *
     * @return MSSettings
     */
    public static function settings($moduleName = 'settings', $pathId = 0)
    {
        $settingsKey = $moduleName . '_' . $pathId;

        if(!isset(static::$_settings[$settingsKey]))
        {
            static::$_settings[$settingsKey] = new MSSettings(array(
                'moduleName' => $moduleName,
                'pathId' => $pathId
            ));
        }

        return static::$_settings[$settingsKey];
    }

    /**
     * @return UrlsAndPaths
     */
    public static function urls()
    {
        if(is_null(static::$_urls))
        {
            static::$_urls = new UrlsAndPaths();
        }

        return static::$_urls;
    }

    /**
     * @return ModuleDirector
     */
    public static function modules()
    {
        if(is_null(static::$_modules))
        {
            static::$_modules = new ModuleDirector();
        }

        return static::$_modules;
    }

    /**
     * @return FormConfigs
     */
    public static function forms()
    {
        if(is_null(static::$_forms))
        {
            static::$_forms = new FormConfigs();
        }

        return static::$_forms;
    }

    /**
     * Метод автоматической загрузки классов.
     *
     * @param string $className имя класса
     *
     * @return bool
     */
    public static function autoLoad($className)
    {
        if (isset(self::$_systemClassMap[$className]))
        {
            /** @noinspection PhpIncludeInspection */
            require_once(self::$_systemClassMap[$className]);
            if(method_exists($className,'__initStatic')) {
                $className::__initStatic();
            }
            return true;
        }
        else
        {
            $includePaths = array(
                CORE_DIR . DS . 'classes' . DS . 'api',
                CORE_DIR . DS . 'classes' . DS . 'base',
                CORE_DIR . DS . 'classes' . DS . 'models',
                CORE_DIR . DS . 'classes' . DS . 'helpers',
                CORE_DIR . DS . 'classes' . DS . 'widgets',
                CORE_DIR . DS . 'classes' . DS . 'interfaces',
                CORE_DIR . DS . 'classes' . DS . 'custom',
                CORE_DIR . DS . 'classes' . DS . 'services',
                CORE_DIR . DS . 'classes',
            );

            $includePaths = array_merge($includePaths, self::$_includePaths);
            foreach ($includePaths as $includePath)
            {
                $classPath = $includePath . DS . $className . '.php';
                if (file_exists($classPath))
                {
                    /** @noinspection PhpIncludeInspection */
                    require_once($classPath);
                    if(method_exists($className,'__initStatic')) {
                        $className::__initStatic();
                    }
                    return true;
                }
            }
        }

        return false;
    }
}

spl_autoload_register(array('MSCore', 'autoLoad'));