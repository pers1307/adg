<?php
/**
 * ApiCall.php
 * Контроллер для обработки ajax запросов от "Обратный звонок"
 */

class ApiCallback extends MSBaseApi
{
    /** @var array */
    protected $_errorMessages = [
        1001 => 'System error',
        1004 => 'Not found'
    ];

	public function callAction() {

	    $data = array_intersect_key($_POST, array_flip(['name', 'phone', 'email', 'full', 'comment', 'page', 'confirmation']));

		$data = array_map(function($value) {
			return MSCore::db()->pre($value);
		}, $data);

        if (!empty($data['name'])) {
            $this->errorAction(1001);
        }

		// Validate data
		$validator = new Validator($data);
		$validator->rule('required', 'phone')->message('Поле не заполнено');
		$validator->rule('required', 'email')->message('Поле не заполнено');
		$validator->rule('required', 'full')->message('Поле не заполнено');
		$validator->rule('required', 'confirmation')->message('Поле не заполнено');

		$validator->rule('phone', 'phone')->message('Некорректный номер телефона');
		$validator->rule('email', 'email')->message('Некорректный Email адрес');

		if($validator->validate()) {

			if($recipients = getMailer(1)) {

                $this->addData([
                    'content' => getTextMod(8),
                ]);

                $host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'];

                //From page
                $data['page'] = $host . DS . path($data['page']);

				$SendMail = new SendMail();
				$SendMail->init();
				$SendMail->setEncType("base64");
				$SendMail->setFrom('no-reply@'.DOMAIN, DOMAIN);
				$subject = 'Обратная связь - '. DOMAIN;
				$message = template('common/email/callback', $data);
				$SendMail->setSubject($subject);
				$SendMail->setMessage($message);
				$SendMail->setTo(implode(',', $recipients));

                $folder = MSFiles::getUploadFolder();
                $MSFiles = new MSFiles();
                $result = $MSFiles->uploadFile($folder, [
                    'allowedExtensions' => ['png', 'jpg', 'jpeg', 'doc', 'docx', 'txt', 'pdf', 'xls', 'xlsx'],
                    'sizeLimit' => 5 * 1024 * 1024,
                    'inputName' => 'uFile',
                ]);

                if (!empty($result['success'])) {
                    $sourceFile = $folder . DS . $result['uploadName'];
                    $SendMail->setFiles([$host . MSFiles::getFileRootPath($sourceFile)]);
                }

				$SendMail->send();
			}

		} else {
			$errors = $validator->errors();
			foreach($errors as $_name => $_error) {
				if(is_array($_error)) {
					$errors[$_name] = reset($_error);
				}
			}
			$this->errorAction(1001, 'Некорректно заполненные поля', array('errors' => $errors));
		}
	}

	/*public function orderAction() {
		$data = array_intersect_key($_POST, array_flip(['name', 'phone', 'departure', 'destination', 'date', 'email', 'confirmation']));

		$data = array_map(function($value) {
			return MSCore::db()->pre($value);
		}, $data);

		// Validate data
		$validator = new Validator($data);
		$validator->rule('required', 'phone')->message('Поле не заполнено');
		$validator->rule('required', 'name')->message('Поле не заполнено');
		$validator->rule('required', 'departure')->message('Поле не заполнено');
		$validator->rule('required', 'destination')->message('Поле не заполнено');
		$validator->rule('required', 'date')->message('Поле не заполнено');
		$validator->rule('required', 'email')->message('Поле не заполнено');
		$validator->rule('required', 'confirmation')->message('Поле не заполнено');

		$validator->rule('phone', 'phone')->message('Некорректный номер телефона');
		$validator->rule('date', 'date')->message('Некорректная дата');

		if($validator->validate()) {
			$this->addData([
				'content' => '<div class="success_message">Ваше сообщение<br/>успешно отправлено</div>',
				'callFunc' => 'order'
			]);

			if($recipients = getMailer(2)) {

				$SendMail = new SendMail();
				$SendMail->init();
				$SendMail->setEncType("base64");
				$SendMail->setFrom('no-reply@'.DOMAIN, DOMAIN);
				$subject = 'Заявка на обратный звонок '. DOMAIN;
				$message = template('email/order', $data);
				$SendMail->setSubject($subject);
				$SendMail->setMessage($message);
				$SendMail->setTo($recipients);
				$SendMail->send();
			}

		} else {
			$errors = $validator->errors();
			foreach($errors as $_name => $_error) {
				if(is_array($_error)) {
					$errors[$_name] = reset($_error);
				}
			}
			$this->errorAction(1001, 'Некорректно заполненные поля', array('errors' => $errors));
		}
	}*/
}