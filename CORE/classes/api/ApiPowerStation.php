<?php
/**
 * ApiCall.php
 * Контроллер для обработки ajax запросов от "Обратный звонок"
 */

class ApiPowerStation extends MSBaseApi
{
    /** @var array */
    protected $_errorMessages = [
        1001 => 'System error',
        1004 => 'Not found'
    ];

    public function calcAction()
    {
        $data = array_intersect_key($_POST, array_flip(['itemId', 'options']));

        // Validate data
        $validator = new Validator($data);
        $validator->rule('required', 'phone')->message('Required field');

        $query = new PreparePriceTable('{power_stations_items}');
        $query->prepareDefault = false;
        $query->setFilter('`id`='.(int)$data['itemId'], 'primary');
        $query->setFilter('`active`=1', 'active');
        $item = $query->getItem();

        if (empty($item)) {
            $this->errorAction(1004, 'Item not found');
        }

        if (!empty($_POST['priceForApi'])) {
            $item['priceForCalc'] = $_POST['priceForApi'];
        }

        if (empty($data['options'])) {
            $data['options'] = [];
        }

        $price = new CalculatePrice($item);

        $options = array_keys($data['options']);

        $query = PowerStationModel::pricesQuery($item);
        if ($options) {
            $query->setFilter('`oep`.`id` IN (' . implode(',', $options) . ')', 'options');
            $price->setAdditionalItems($query->getItems());
        }

        $this->addData([
            'success' => true,
            'content' => template('catalog/power_stations/priceInfo', [
                'price' => $price->calculate(),
                'adjustment' => $price->getAdjustment()
            ]),
            'options' => $options,
        ]);
    }


    public function orderAction()
    {
        $data = array_intersect_key($_POST, array_flip([
            'name',
            'phone',
            'email',
            'full',
            'address',
            'page',
            'item',
            'options',
            'confirmation'
        ]));

        $data = array_map(function($value) {
            return is_array($value) ? $value : MSCore::db()->pre($value);
        }, $data);

        if (!empty($data['name'])) {
            $this->errorAction(1001);
        }

        // Validate data
        $validator = new Validator($data);
        $validator->rule('required', 'phone')->message('Поле не заполнено');
        $validator->rule('required', 'email')->message('Поле не заполнено');
        $validator->rule('required', 'full')->message('Поле не заполнено');
        $validator->rule('required', 'item')->message('Поле не заполнено');
        $validator->rule('required', 'confirmation')->message('Поле не заполнено');

        $validator->rule('phone', 'phone')->message('Некорректный номер телефона');
        $validator->rule('email', 'email')->message('Некорректный Email адрес');

        if($validator->validate()) {

            if ($recipients = getMailer(2)) {

                $host = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . '://' . $_SERVER['HTTP_HOST'];
                list($itemId, $parent) = explode(':', $data['item']);

                $model = new PowerStationModel([
                        'pathId' => POWER_STATION_PATH_ID,
                        'onlyHaveItems' => true,
                        'articlesTreeFields' => ['*'],
                        'itemsOrder' => '`order` ASC',
                        'pagination' => false
                    ]
                );
                $model->_currentArticle = $model->getArticleById($parent);
                $model->setItemKey($itemId);
                $itemData = $model->getItem(null, false);

                if (empty($model->_currentArticle) || empty($itemData['item'])) {
                    $this->errorAction(1004, 'Item not found');
                }

                $itemData['item']['itemLink'] = $host . $itemData['item']['itemLink'];
                $itemData['item']['parent'] = $model->_currentArticle;
                $data['item'] = $itemData['item'];

                if (!empty($_POST['priceForCalc'])) {
                    $itemData['item']['priceForCalc'] = $data['priceForCalc'] = $_POST['priceForCalc'];
                }

                $data['price'] = $price = new CalculatePrice($itemData['item']);

                $options = isset($data['options']) ? array_keys($data['options']) : null;

                $query = PowerStationModel::pricesQuery($itemData['item']);
                if ($options) {
                    $query->setFilter('`oep`.`id` IN (' . implode(',', $options) . ')', 'options');
                    $optionItems = $query->getItems();
                    $price->setAdditionalItems($optionItems);
                }

                //From page
                if (!empty($data['page'])) {
                    $data['page'] = $host . DS . path($data['page']);
                } else {
                    $data['page'] = $host . DS;
                }



                $SendMail = new SendMail();
                $SendMail->init();
                $SendMail->setEncType("base64");
                $SendMail->setFrom('no-reply@'.DOMAIN, DOMAIN);
                $subject = 'Новый заказ - '. DOMAIN;
                $message = template('common/email/order', $data);
                $SendMail->setSubject($subject);
                $SendMail->setMessage($message);
                $SendMail->setTo(implode(',', $recipients));
                $SendMail->send();

                $this->addData([
                    'content' => getTextMod(8),
                ]);
            }
        } else {
            $errors = $validator->errors();
            foreach($errors as $_name => $_error) {
                if(is_array($_error)) {
                    $errors[$_name] = reset($_error);
                }
            }
            $this->errorAction(1001, 'Некорректно заполненные поля', array('errors' => $errors));
        }
    }
}