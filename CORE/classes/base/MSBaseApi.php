<?php

/**
 * Базовый модуль Api
 */

/** @noinspection PhpUndefinedClassInspection */
class MSBaseApi {
    /**
     * @var array
     */
    protected $_outputResult = array(
        'status' => array(
            'code' => 200,
            'message' => 'OK'
        ),
        'data' => array(),
    );

    /**
     * @var string
     */
    protected $_outputFormat = 'json';

    /**
     * @var array
     */
    protected $_defaultErrorMessages = array(
        200 => 'OK',
        201  => 'Created',
        304  => 'Not Modified',
        400 => 'Bad Request',
        401  => 'Unauthorized',
        403  => 'Forbidden',
        404  => 'Not Found',
        500 => 'Internal server error',
    );

    /**
     * @var array
     */
    protected $_errorMessages = array();

    /**
     * @var null
     */
    protected $_action = null;

    /**
     *  Error handler
     */
    protected function _error() { }

    /**
     *  Success handler
     */
    protected function _success() { }

    /**
     * Execute api call
     */
    public function execute() {
        $this->before();
        $this->{$this->_action.'Action'}();
        $this->after();
    }

    /**
     * Set error status
     * @param $code
     * @param $message
     * @param array $data
     */
    public function errorAction($code, $message = NULL, $data = array()){
        $this->_outputResult['status']['code'] = $code;
        if(!is_null($message)) {
            $this->_outputResult['status']['message'] = $message;
        } else {
            $this->_outputResult['status']['message'] = (isset($this->_errorMessages[$code])) ?
                $this->_errorMessages[$code] : 'Unknown error';
        }

        if($data) $this->addData($data);

        // Break function call
        $this->_error();
        $this->after();
        exit;

    }

    /**
     * Set success status
     * @param array $data
     */
    public function successAction($data = array()){
        $this->_outputResult['status']['code'] = 200;
        $this->_outputResult['status']['message'] = $this->_errorMessages[200];

        if($data) $this->addData($data);

        // Break function call
        $this->_success();
        $this->after();
        exit;
    }

    /**
     * Before handler
     */
    public function before() {
        // Get error messages
        foreach($this->_defaultErrorMessages as $_code => $_message) {
            if(!isset($this->_errorMessages[$_code])) {
                $this->_errorMessages[$_code] = $_message;
            }
        }

        if(!method_exists($this, $this->_action.'Action')) {
            $this->errorAction(400, NULL);
        }
    }
    
    /**
     *  After handler
     */
    public function after() {
        switch ($this->_outputFormat) {
            case 'array':
                debug($this->_outputResult);
                break;

            case 'json':
            default:
                header('Content-Type:application/json; charset=' . SITE_ENCODING);
                echo json_encode($this->_outputResult);
                break;
        }
    }

    /**
     * @return null
     */
    public function getAction() {
        return $this->_action;
    }

    /**
     * @param null $action
     * @return $this
     */
    public function setAction($action) {
        $this->_action = $action;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getData(){
        return $this->_outputResult['data'];
    }

    /**
     * @param $data
     * @return $this
     */
    public function setData($data){
        if(is_array($data)) {
            $this->_outputResult['data'] = $data;
        }
        return $this;
    }

    /**
     * @param $data
     * @return $this
     */
    public function addData($data) {
        if ( is_array($data) ) {
            foreach($data as $_key => $_value) {
                $this->_outputResult['data'][$_key] = $_value;
            }
        }
        return $this;
    }
}