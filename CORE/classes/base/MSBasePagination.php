<?php

/**
 *
 * Pagination
 *
 * Класс для построения постраничной навигации
 *
 * @example
 * $pages = new Pagination($count, $npage, '/'.$page['path'].'/%link%/');
 *
 * $pages->number_center = 3; // количество страниц вокруг активной страницы
 * $pages->number_left_right = 2; // количество выводимых страниц справа и слева от разделителя
 * $pages->onpage = 20; // кол-во выводимых на страницу элементов
 * $pages->show_forth_back = false; // отображать или не отображать стрелки
 *
 * echo $pages; //вывод
 *
 * @package Core.base
 *
 */
class MSBasePagination extends MSBaseComponent
{
    public $_currentPage,
        $_countItems,
        $_afterBefore = true,
        $_pages = array(),
        $_cache;

    public $onPage = 10,
        $numberCenter = 2,
        $numberLeftRight = 1,
        $showForthBack = true,
        $linkPref = 'page',
        $autoTitle = true,
        $linkMask = '',
        $forwardLabel = '<span>вперед</span> &raquo;',
        $backLabel = '&laquo; <span>назад</span>',
        $separatorLabel = '...',
        $needFilters = true,
        $filtersString = null,
        $_countPages;

    public function __construct($options = array(), $count_items, $current_page)
    {
        parent::__construct($options);

        $this->_countItems = $count_items;
        $this->_currentPage = $current_page;

        if (!is_null($this->_currentPage) && $this->_currentPage <= 1)
        {
            Page404();
        }
        if (!$this->_currentPage)
        {
            $this->_currentPage = 1;
        }

        if($this->needFilters && !empty($_GET)) {
            $this->filtersString = '?'.http_build_query($_GET);
        }

    }

    public function __toString()
    {
        return $this->render();
    }

    public function getCurrentPage() {
        return $this->_currentPage;
    }

	public function getPages() {
		$this->_pages();
		return $this->_pages;
	}

	public function getNextPage() {
		$pages = $this->pages;
		$nextKey = null;
		if ($pages) {
			array_pop($pages);
			foreach($pages as $key => $page) {
				if ($page->type == 'current') {
					$nextKey = $key + 1;
					break;
				}
			}
		}
		if ($nextKey !== null && isset($pages[$nextKey])) {
			return $pages[$nextKey];
		}
		return null;
	}

    private function _pages()
    {

        $this->_countPages = ceil($this->_countItems / $this->onPage);
        if ($this->_countItems && $this->_currentPage > $this->_countPages)
        {
            Page404();
        }

        if ($this->_currentPage > 1 && $this->_afterBefore && $this->showForthBack)
        {
            $this->addPage('link')->setClass('left')->setValue($this->backLabel)->setUrl($this->Link($this->_currentPage - 1));
        }

        for ($i = 1; $i <= $this->_countPages; $i++)
        {
            if ($this->_countPages > ($this->numberLeftRight * 2 + $this->numberCenter * 2 + 1))
            {
                if ($this->_currentPage < $this->_countPages - $this->numberLeftRight - $this->numberCenter && $i == $this->_countPages - $this->numberLeftRight)
                {
                    $this->addPage('sep')->setClass('page-nav__item_type_separator')->setValue($this->separatorLabel);
                }

                if ($i < $this->numberLeftRight + 1 || $i > $this->_countPages - $this->numberLeftRight || $i == $this->_currentPage || ($i >= $this->_currentPage - $this->numberCenter && $i < $this->_currentPage) || ($i <= $this->_currentPage + $this->numberCenter && $i > $this->_currentPage))
                {
                    if ($i == $this->_currentPage)
                    {
                        $this->addPage('current')->setClass('page-nav__item_current_true')->setValue($i);
                    }
                    else
                    {
                        $this->addPage('link')->setUrl($this->Link($i))->setClass('link')->setValue($i);
                    }
                }

                if ($this->_currentPage > $this->numberLeftRight + $this->numberCenter + 1 && $i == $this->numberLeftRight)
                {
                    $this->addPage('sep')->setClass('page-nav__item_type_separator')->setValue($this->separatorLabel);
                }
            }
            else
            {
                if ($i == $this->_currentPage)
                {
                    $this->addPage('current')->setClass('page-nav__item_current_true')->setValue($i);
                }
                else
                {
                    $this->addPage('link')->setUrl($this->Link($i))->setClass('link')->setValue($i);
                }
            }
        }

        if ($this->_currentPage != $this->_countPages && $this->_afterBefore && $this->showForthBack)
        {
            $this->addPage('link')->setClass('right')->setValue($this->forwardLabel)->setUrl($this->Link($this->_currentPage + 1));
        }
    }

    private function addPage($type)
    {
        return $this->_pages[] = new PaginationPage($type, $this->filtersString);
    }

    private function Link($i)
    {
        return $i == 1 ? str_replace('/%link%/', '/', $this->linkMask) : str_replace('%link%', $this->linkPref . $i, $this->linkMask);
    }

    public function getLimit()
    {
        $lim_start = $this->_currentPage > 1 ? ($this->_currentPage - 1) * $this->onPage : 0;

        return 'LIMIT ' . $lim_start . ', ' . $this->onPage;
    }

    private function render()
    {
        if ($this->_cache)
        {
            return $this->_cache;
        }

        $this->_pages();

        if ($this->_countPages <= 1)
        {
            return '';
        }

        return $this->_cache = template('common/pagination', array('pages' => $this->_pages));
    }

    /**
     * Установка текущей страницы
     * @param int $page
     */
    public function setCurrentPage($page)
    {
        $this->_currentPage = $page;
    }

}
