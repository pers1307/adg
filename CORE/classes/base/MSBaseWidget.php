<?php

/**
 * Class MSBaseWidget
 */
class MSBaseWidget implements InterfaceWidget {
    /**
     * Конфиг виджета
     *
     * @var array
     */
    public $config = array();

    /**
     * Имя виджета строчными буквами
     *
     * @var null
     */
    public $name = null;

    /**
     * Вьюха для рендеринга
     *
     * @var null
     */
    public $view = null;

    /**
     * Массив параметров, которые попадут во вьюху
     *
     * @var array
     */
    protected $_params = array();

    /**
     * Указание на то, следует ли кэшировать экземпляр виджета
     *
     * @var bool
     */
    public $_cache = false;

    /**
     * Конструктор
     *
     * @param $name
     * @param $config
     * @param $view
     */
    public function __construct($name, $config, $view) {

        $this->name = $name;
        $this->view = 'widgets/'. $this->name . '/'. $view;
        $this->config = $config;
    }

    /**
     * @param $view
     * @return string
     */
    protected function _render($view) {
        $view = $view ?: $this->view;
        return template($view, array('config' => $this->config, 'params' => $this->_params));
    }

    /**
     * @param null $view
     * @return string
     */
    public function render($view = null) {
        return $this->_render($view);
    }
}