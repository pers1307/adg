<?php

/**
 * Class PaginationPage
 * @package Core.base
 */
class PaginationPage
{

	public $url, $class, $value, $type, $getString, $filtersString;

	public function __construct($type, $filtersString)
	{

		$this->type = $type;
		$this->filtersString = $filtersString;

	}

	/**
	 * @param string $url
	 *
	 * @return PaginationPage $this
	 */
	public function setUrl($url)
	{

		$this->url = $url . $this->filtersString;

		return $this;

	}

	/**
	 * @param string $className
	 *
	 * @return PaginationPage $this
	 */
	public function setClass($className)
	{

		$this->class = $className;

		return $this;

	}

	/**
	 * @param mixed $value
	 *
	 * @return PaginationPage $this
	 */
	public function setValue($value)
	{

		$this->value = $value;

		return $this;

	}
}