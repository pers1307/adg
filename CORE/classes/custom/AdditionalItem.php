<?php

class AdditionalItem
{
    /**
     * @var float
     */
    public $price;

    /**
     * @var float
     */
    public $defaultPrice;

    /**
     * @var bool
     */
    public $individual;

    /**
     * @var string
     */
    public $title;

    public function __construct($config = array())
    {
        foreach ($config as $key => $value) {
            if (property_exists($this, $key)) {
                $this->{$key} = $value;
            }
        }
    }
}