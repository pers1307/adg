<?php

class CalculatePrice
{
    /**
     * @var array
     */
    protected $item;

    /**
     * @var AdditionalItem[]
     */
    protected $additional = [];

    /**
     * If there is individual price;
     * @var bool
     */
    protected $needAdjustment = false;

    /**
     * CalculatePrice constructor.
     * @param $item
     * @throws Exception
     */
    public function __construct($item)
    {
        $this->item = $item;

        if (!$this->item) {
            throw new Exception('Item not set');
        }
    }

    /**
     * @return float
     */
    public function basePrice()
    {
        return $this->item['price'];
    }

    /**
     * @return float|int
     */
    public function calculate()
    {
        if (empty($this->item['priceForCalc'])) {
            return $this->basePrice() + $this->calcAdditional();
        } else {
            return $this->item['priceForCalc'] + $this->calcAdditional();
        }

    }

    /**
     * @param array $items
     */
    public function setAdditionalItems($items)
    {
        foreach ($items as $item) {
            $this->setAdditionalItem($item);
        }
    }

    /**
     * @param array $item
     */
    public function setAdditionalItem($item)
    {
        $this->additional[] = new AdditionalItem($item);
    }

    /**
     * @return array
     */
    public function getAdditionalItems()
    {
        return $this->additional;
    }

    /**
     * @return bool
     */
    public function getAdjustment()
    {
        return $this->needAdjustment;
    }

    /**
     * @return int
     */
    protected function calcAdditional()
    {
        $result = 0;

        if ($this->additional) {
            foreach ($this->additional as $item) {
                $result += CalculatePrice::processPrice($item);

                //set adjustment
                if ($item->individual) {
                    $this->needAdjustment = true;
                }
            }
        }

        return $result;
    }

    /**
     * @param AdditionalItem $data
     * @return int
     */
    public static function processPrice($data)
    {
        if ($data->individual) {
            return 0;
        } else {
            return $data->price > 0 ? $data->price : ($data->defaultPrice > 0 ? $data->defaultPrice : 0);
        }
    }
}