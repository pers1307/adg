<?php

class Currency
{
    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $isoName;

    /**
     * @var int
     */
    public $isoCode;

    public function __construct($name, $code)
    {
        $this->setName($name);
        $this->setIsoName($name);
        $this->isoCode = $code;
    }

    protected function setName($name)
    {
        $this->name = strtolower($name);
    }

    protected function setIsoName($name)
    {
        $name = explode('_', $name);
        $this->isoName = isset($name[1]) ? $name[1] : '' ;
    }
}