<?php
/**
 * Created by PhpStorm.
 * User: xhooch
 * Date: 12.11.18
 * Time: 18:30
 */

class PreparePriceTable extends MSTable
{
    public $prepareDefault = true;

    public function getItems()
    {
        $items = parent::getItems();

        $currencies = CurrencyHelper::getCurrencies();

        foreach ($items as $key => &$item) {
            $item['price'] = $this->preparePrice($item);

            if ($this->prepareDefault) {
                foreach ($currencies as $currency) {
                    $currName = strtolower($currency->isoName);
                    $item['price_' . $currName] = $item[$currName . 'PriceDefault'];
                    unset($item[$currName . 'PriceDefault']);
                }

                $item['active_currency'] = $item['ActiveCurrencyDefault'];
                $item['defaultPrice'] = $this->preparePrice($item);
            }

            foreach ($currencies as $currency) {
                $currName = strtolower($currency->isoName);
                unset($item['price_' . $currName]);
            }
            unset($item['active_currency']);
        }

        return $items;
    }

    public function getItem()
    {
        $item = parent::getItem();
        $item['price'] = $this->preparePrice($item);

        $currencies = CurrencyHelper::getCurrencies();
        foreach ($currencies as $currency) {
            $currName = strtolower($currency->isoName);
            unset($item['price_' . $currName]);
        }
        unset($item['active_currency']);

        return $item;
    }

    protected function preparePrice($item)
    {
        $activeCurrency = $item['active_currency'] ? $item['active_currency'] : CurrencyHelper::CURRENCY_RUB;
        $currency = CurrencyHelper::getCurrencyByCode($activeCurrency);
        return CurrencyHelper::convert($item['price_' . strtolower($currency->isoName)], $currency->isoCode);
    }
}