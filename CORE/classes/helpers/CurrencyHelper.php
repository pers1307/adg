<?php

class CurrencyHelper
{
    const CURRENCY_RUB = 643;
    const CURRENCY_EUR = 840;
    const CURRENCY_USD = 978;
    const CURRENCY_JPY = 392;

    private static $rates = null;

    protected static $currencies = [];

    public static function convert($value, $from = self::CURRENCY_USD, $to = self::CURRENCY_RUB)
    {

        if ($from == $to) {
            return $value;
        }

        $result = 0;

        if (static::$rates === null) {
            static::$rates = static::getRates();
        }

        if (isset(static::$rates[$from])) {
            $result = $value * static::$rates[$from];
        }

        return $result;
    }

    public static function getCurrencies()
    {
        $object = new ReflectionClass(static::class);
        $constKeys = $object->getConstants();
        foreach ($constKeys as $key => $value) {
            static::$currencies[$key] = new Currency($key, $value);
        }

        return static::$currencies;
    }

    public static function getCurrencyByCode($code)
    {
        $result = null;
        if (empty(static::$currencies)) {
            static::$currencies = static::getCurrencies();
        }

        foreach (static::$currencies as $currency) {
            if ($currency->isoCode == $code) {
                $result = $currency;
                break;
            }
        }
        return $result;
    }

    private static function getRates()
    {
        $result = [];
        $constKeys = array_keys(static::getCurrencies());
        array_walk($constKeys, function (&$k) {
            $k = strtolower($k);
        });

        $table = new MSTable('{settings}');
        $table
            ->setFields(['value', 'key'])
            ->setFilter('`path_id` = 0', 'pathId')
            ->setFilter('`module_id` = 494', 'moduleId')
            ->setFilter('`key` IN ("' . implode('","', $constKeys) .'")', 'keys')
            ->indexBy('key');

        $rates = $table->getItemsCol();
        foreach (static::getCurrencies() as $key => $value) {
            if (isset($rates[$value->name])) {
                $result[$value->isoCode] = $rates[$value->name];
            }
        }

        return $result;
    }

    /**
     * modules config
     * @return array
     */
    public static function currenciesConfig()
    {
        $currencies = static::getCurrencies();

        $config = [
            [
                'type' => 'divider'
            ],
            [
                'type' => 'section',
                'caption' => 'Цены',
                'output' => 'open',
            ],
        ];

        /**
         * @var Currency $currency
         */

        foreach ($currencies as $currency) {
            $config['price_' . strtolower($currency->isoName)] = [
                'type' => 'string',
                'caption' => $currency->isoName,
                'in_list' => 0,
                'value' => '',
            ];
        }

        $config['active_currency'] = [
            'type' => 'select',
            'caption' => 'Валюта',
            'in_list' => 0,
            'value' => '',
            'values' => array_flip(array_map(function ($k) {
                return $k->isoCode;
            }, $currencies)),
        ];

        $config[] = [
            'type' => 'section',
            'output' => 'close',
        ];
        $config[] = ['type' => 'divider'];

        return $config;
    }
}