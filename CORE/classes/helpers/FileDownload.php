<?php

class FileDownload
{
    const ICON_PATH = '/DESIGN/SITE/images/file_icons/';
    const MODULE_NAME = 'download_files';

    private static $settings;

    public static function questionnaire()
    {
        static::setSettings();
        return static::getFileById(static::$settings->questionnaireId);
    }

    public static function presentation()
    {
        static::setSettings();
        return static::getFileById(static::$settings->presentationId);
    }

    public static function getFileById($id)
    {
        $query = new MSTable('{download_files}');
        $query->setFields([
            'title',
            'file'
        ]);
        $query->setFilter(['`id` = ' . (int)$id], 'qId');

        $data = $query->getItem();
        $data['file'] = static::fileData($data['file']);

        return $data;
    }

    public static function fileData($data)
    {
        $fileData = current(unserialize($data));
        $pathInfo = pathinfo($fileData['path']);
        $fileData['extension'] = $pathInfo['extension'];

        $icons = [];
        if (!isset($icons[$fileData['extension']])) {
            $icon = static::ICON_PATH . $fileData['extension'] . '.png';
            if (!file_exists(DOC_ROOT . $icon)) {
                $icon = static::ICON_PATH . 'default.png';
            }
            $icons[$fileData['extension']] = $icon;
        } else {
            $icon = $icons[$fileData['extension']];
        }

        $fileData['icon'] = $icon;
        $fileData['fileSize'] = str_replace(' ', '', $fileData['fileSize']);
        return $fileData;
    }

    private static function setSettings()
    {
        static::$settings = MSCore::settings(static::MODULE_NAME);
    }
}