<?php

/**
 * Simple ekaburu metrika helper
 */
class HelperMetrika {
    protected static $_apikey;
    protected static $_host;
    protected static $_users;

    /**
     * @return mixed
     */
    public static function getApiKey() {
        if (is_null(static::$_apikey)) {
            static::$_apikey = MSCore::db()->getOne(
                'SELECT `value` FROM `' . PRFX . 'settings` ' .
                'WHERE `path_id`=0 AND `module_id`=494 AND `key`=\'metrikakey\''
            );
        }
        return static::$_apikey;
    }

    /**
     * @return string
     */
    public static function getHost() {
        if (is_null(static::$_host)) {
            static::$_host = DOMAIN;
        }
        return static::$_host;
    }


    /**
     * @return mixed|string
     */
    public static function getCode() {
        $host = static::getHost();
        if (empty($host)) return '';


        $code = '';
        $apikey = static::getApiKey();
        $params = array(
            'url' => $host,
        );

        if (!empty($apikey)) $params['apikey'] = $apikey;

        $url = 'http://ekaburu.ru/api/metrika/?'.http_build_query($params);
        $content = curlRequest($url);
        if ($content) {
            $content = @json_decode($content, true);
        }
        $content = (array)$content;
        if (!empty($content)) {
            $apikey = !empty($content['apikey']) ? htmlspecialchars($content['apikey']) : '';
            if (!empty($apikey)) {
                static::$_apikey = $apikey;
                MSCore::db()->replace(PRFX . 'settings', array(
                    'path_id' => 0,
                    'module_id' => 494,
                    'key' => 'metrikakey',
                    'value' => $apikey
                ));
            }
            $code = !empty($content['code']) ? $content['code'] : '';
            $code = preg_replace('/(?:^\s+)|(?:\s+$)/suiU', '', $code);
        }
        return $code;
    }

    /**
     * @return array
     */
    public static function getUsers() {
        $host = static::getHost();
        $apikey = static::getApiKey();
        if (empty($host) || empty($apikey)) {
            static::$_users = array();
        }

        if(is_null(static::$_users)) {
            $params = array(
                'do' => 'grants',
                'url' => $host,
                'apikey' => $apikey
            );
            $url = 'http://ekaburu.ru/api/metrika/?' . http_build_query($params);
            $content = curlRequest($url);
            if ($content) {
                $content = @json_decode($content, true);
            }
            $content = (array)$content;
            if(!empty($content['grants']) && is_array($content['grants'])) {
                $users = $content['grants'];
                static::$_users = array();
                foreach ($users as $user) {
                    static::$_users[mb_strtolower($user['user_login'])] = $user;
                }
            }
        }
        return static::$_users;
    }

    public static function delegateUser($login) {
        $result = array(
            'error' => '',
            'login' => $login
        );

        $host = static::getHost();
        $apikey = static::getApiKey();
        $users = static::getUsers();
        $errors = array(
            'empty_host' => 'Домен сайта не получен. Обратитесь в поддержку компании',
            'empty_api' => 'Токен не получен. Обратитесь в поддержку компании',
            'user_exists' => 'Пользователь с таким логином уже делегирован.'
        );

        if (empty($host)) {
            $result['error'] = $errors['empty_host'];
        } elseif(empty($apikey)) {
            $result['error'] = $errors['empty_api'];
        } elseif(!empty($users[mb_strtolower($login)])) {
            $result['error'] = $errors['user_exists'];
        } else {
            $params = array(
                'do' => 'delegate',
                'url' => $host,
                'apikey' => $apikey,
                'yandexlogin' => $login
            );
            $url = 'http://ekaburu.ru/api/metrika/?' . http_build_query($params);
            $content = curlRequest($url);
            if ($content) {
                $content = @json_decode($content, true);
            }
            $content = (array) $content;
            if(!empty($content['error'])) {
                $result['error'] = $content['error'];
            } else {
                $users[$login] = array('user_login' => $login);
                static::$_users = $users;
            }
        }
        return $result;
    }
}