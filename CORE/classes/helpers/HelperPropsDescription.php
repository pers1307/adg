<?php

class HelperPropsDescription extends MSBaseComponent {

    public static $descriptions = [
        'economy' => ['Расход топлива в час при 100% нагрузке'],
        'service' => [
            'Средняя для Российского рынка стоимость основных запасных частей, подлежащих замене, по данным выборки из 20 поставщиков, включая ADG Energy',
            'Средняя стоимость обеспечения установки рекомендованными марками топлива и масла на 1 час работы',
        ],
        'accessibility' => [
            'Общее кол-во сервисных центров, авторизованных на ремонт двигателей определенной марки, в городах России.',
            'Общее кол-во Российских поставщиков авторизованных на поставку оригинальных запасных частей.',
            'Минимальное расстояние от места эксплуатации до ближайшей станции технического обслуживания по данным выборки из 200 клиентов завода ADG Energy.',
            'Наличие особенных требований к квалификации обслуживающего персонала в инструкции по эксплуатации.',
        ],
    ];

    /**
     * @param string $prop
     * @return array
     */
    public static function getDescriptionByProp($prop)
    {
        if (isset(self::$descriptions[$prop])) {

            return self::convertToList(
                self::$descriptions[$prop]
            );
        }

        return [];
    }

    protected static function convertToList($list)
    {
        $result = '<ul>';
        foreach ($list as $item) {
            $result .= "<li>$item</li>";
        }
        $result .= '</ul>';

        return $result;
    }
}