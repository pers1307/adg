<?php

class HelperSeo extends MSBaseComponent {

	public static $data;

	public static function addCanonical($url = null) {
		if ($url === null) {
			if (isset(static::$data['canonical'])) {
				$url = static::$data['canonical'];
			}
		}
		MSCore::page()->canonical = $url;
	}

	public static function addAllSeo($data) {
		static::$data = $data;
		static::addTitlePage();
        static::addNameGenerator();
		static::addTitleH1();
		static::addKeywords();
		static::addDescription();
		static::addAutoCorrectionValues();
        static::addAltForImage();
	}

	public static function addAltForImage()
    {
        if (isset(static::$data['name'])) {
            MSCore::page()->alt_for_image = "Детальные фотографии дизельной электростанции " . static::$data['name'];
        }

        if (isset(static::$data['variant']['name']) && isset(static::$data['name_article']['name'])) {

            MSCore::page()->alt_for_image = "Детальные фотографии дизельной электростанции " . static::$data['name_article']['name'] . " " . (getVariant(static::$data['variant']['name']));
        }

    }

	public static function addTitlePage() {
		if (static::$data['title_page']) {
			MSCore::page()->title_page = static::$data['title_page'];
			MSCore::page()->titleCustomPage = static::$data['title_page'];
		}
	}

    public static function addNameGenerator() {
        if (isset(static::$data['name'])) {
            MSCore::page()->name_generator = static::$data['name'];
        }
        if (isset(static::$data['name_article']['name']) && isset(static::$data['variant']['name'])) {
            MSCore::page()->name_generator = static::$data['name_article']['name'];
            MSCore::page()->variant_generator = getVariant(static::$data['variant']['name']);
        }
    }

	public static function addTitleH1() {
		if (!empty(static::$data['title_h1'])) {
			MSCore::page()->title_h1 = MSCore::page()->header = static::$data['title_h1'];
		} elseif (!empty(static::$data['name'])) {
			MSCore::page()->title_h1 = MSCore::page()->header = static::$data['name'];
		} elseif (!empty(static::$data['title'])) {
			MSCore::page()->title_h1 = MSCore::page()->header = static::$data['title'];
		}
	}

	public static function addKeywords() {
		if (static::$data['meta_keywords']) {
			MSCore::page()->meta_keywords = static::$data['meta_keywords'];
			MSCore::page()->metaCustomKeywords = static::$data['meta_keywords'];
		}
	}

	public static function addDescription() {
		if (static::$data['meta_description']) {
			MSCore::page()->meta_description = static::$data['meta_description'];
			MSCore::page()->metaCustomDescription = static::$data['meta_description'];
		}
	}
	public static function addAutoCorrectionValues() {
		if (!empty(static::$data['title'])) {
			MSCore::page()->itemName = static::$data['title'];
		}
		if (!empty(static::$data['name'])) {
			MSCore::page()->categoryName = static::$data['name'];
		}
	}
}