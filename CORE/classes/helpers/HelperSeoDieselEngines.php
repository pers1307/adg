<?php

class HelperSeoDieselEngines extends MSBaseComponent {

    /**
     * @param array $article
     * @return array
     */
    public static function transform($article)
    {
        $article = self::transformTitle($article);
        $article = self::transformDescription($article);
        $article = self::transformKeyWords($article);

        return $article;
    }

    /**
     * @param array $article
     * @return array
     */
    public static function transformTitle($article) {
        if ($article['title_page'] === "") {
            $article['title_page'] = "Дизельные двигатели " . $article['name'] . " для электростанций в Екатеринбурге";
        }

        return $article;
    }

    /**
     * @param array $article
     * @return array
     */
    public static function transformDescription($article) {
        if ($article['meta_description'] === "") {
            $article['meta_description'] = "Продажа дизельных двигателей " . $article['name'] . " для дизельных генераторов с доставкой по России и странам СНГ. Уточняйте подробности по телефону 8 800 775 65 10 - Звонок по России бесплатный";
        }

        return $article;
    }

    /**
     * @param array $article
     * @return array
     */
    public static function transformKeyWords($article) {
        if ($article['meta_keywords'] === "") {
            $article['meta_keywords'] = "Дизельные двигатели " . $article['name'] . ", продажа, характеристики, цена, купить";
        }

        return $article;
    }

}