<?php

class HelperSeoPowerStationItem extends MSBaseComponent {

    public static function generateH1($parentArticle, $article, $item)
    {
        return 'Дизельная электростанция ADG-ENERGY '
            . $article['name']
            . ' ' . $parentArticle['name']
            . ' ' . $item['variant']['titleGenerator'];
    }
}