<?php

class Html
{
    public static function stars($value)
    {
        $result = '';
        for($i = 1; $i <= $value; $i++) {
            $result .= '<span class="glyphicon glyphicon-star">★</span>';
        }

        return $result;
    }
}