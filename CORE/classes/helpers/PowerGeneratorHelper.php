<?php

class PowerGeneratorHelper
{
    const CHASSIS_CODE = 'traktornyy-polupritsep-shassi';

    public static function getPrice($item, $itemTypes, $options)
    {
        if ($item['variant']['id'] == 1) {
            // Открытый тип
            $openType =
                array_values(
                    array_filter($itemTypes, function ($value) {
                        return $value['title'] == 1;
                    })
                );

            if (
                !empty($openType[0])
            ) {
                return $openType[0]['price'];
            }

            return $item['price'];
        } else if ($item['variant']['id'] == 2) {
            // Передвижной тип
            $allOptions = [];

            foreach ($options as $option) {
                $allOptions = array_merge($allOptions, $option['options']);
            }

            $chassis =
                array_values(
                    array_filter($allOptions, function ($value) {
                        return $value['code'] === self::CHASSIS_CODE;
                    })
                );

            $coverType =
                array_values(
                    array_filter($itemTypes, function ($value) {
                        return $value['title'] == 3;
                    })
                );

            if (
                !empty($coverType[0])
                && !empty($chassis[0])
            ) {
                return $coverType[0]['price']
                    + $chassis[0]['price'];
            }

            return $item['price'];
        } else if ($item['variant']['id'] == 3) {
            // В кожухе
            $coverType =
                array_values(
                    array_filter($itemTypes, function ($value) {
                        return $value['title'] == 3;
                    })
                );

            if (!empty($coverType[0])) {
                return $coverType[0]['price'];
            }

            return $item['price'];
        } else if ($item['variant']['id'] == 4) {
            // В контейнере
            $openType =
                array_values(
                    array_filter($itemTypes, function ($value) {
                        return $value['title'] == 1;
                    })
                );

            $containerType =
                array_values(
                    array_filter($itemTypes, function ($value) {
                        return $value['title'] == 4;
                    })
            );

            if (!empty($openType[0]) && !empty($containerType[0])) {
                return $openType[0]['price'] + $containerType[0]['price'];
            }

            return $item['price'];
        }

        return 0;
    }

    /**
     * @param array $options
     *
     * @return array
     */
    public static function deleteChassisCodeFromOptions($options)
    {
        $copyOptions = $options;

        foreach ($copyOptions as $keyCopyOption => $copyOption) {
            if (!empty($copyOption['options'])) {
                foreach ($copyOption['options'] as $keyCopyOptionItem => $copyOptionItem) {
                    if ($copyOptionItem['code'] == self::CHASSIS_CODE) {
                        unset($options[$keyCopyOption]['options'][$keyCopyOptionItem]);

                        if (empty($options[$keyCopyOption]['options'])) {
                            unset($options[$keyCopyOption]);
                        }
                    }
                }
            }
        }

        return $options;
    }
}