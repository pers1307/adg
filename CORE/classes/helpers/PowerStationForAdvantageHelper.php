<?php

class PowerStationForAdvantageHelper
{
    /**
     * @param array $item
     * @param string $nameGenerator
     * @return string
     */
    public static function transformAltForImg($item , $nameGenerator, $variant)
    {
        if (isset($variant)) {
            $alt = $nameGenerator . " " . $variant . ": " . $item['title'];
        } else {
            $alt = $nameGenerator . ": " . $item['title'];
        }

        return $alt;
    }
}