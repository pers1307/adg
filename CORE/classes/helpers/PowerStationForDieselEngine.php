<?php

class PowerStationForDieselEngine
{
    /**
     * @param array $item
     * @param array $article
     * @param array $articles
     * @param string $phoneStock
     * @return array
     */
    public static function transform($item, $article, $articles, $phoneStock)
    {
        $item = self::transformTitle($item, $article);
        $item = self::transformDescription($item, $article, $articles, $phoneStock);
        $item = self::transformKeywords($item, $article);

        return $item;
    }

    /**
     * @param array $item
     * @param array $article
     * @return array
     */
    public static function transformTitle($item, $article)
    {
        if ($item['title_h1'] === "") {
            $item['title_h1'] = "Дизельные генераторы с двигателем " . $item['title'];
        }

        if ($item['title_page'] === "") {
            $item['title_page'] = "Дизельные электростанции с двигателем " . $item['title'];
        }

        return $item;
    }

    /**
     * @param array $item
     * @param array $article
     * @param array $articles
     * @return array
     */
    public static function transformDescription($item, $article, $articles, $phoneStock)
    {
        if ($item['meta_description'] === "") {
            $item['meta_description'] = "На базе дизельного двигателя " . $item['title'] . " собираются дизельные электростанции: ";
            foreach ($articles as $value) {
                $item['meta_description'] .= $value['name'] . " ";
            }
            $item['meta_description'] .= ". Подробности по телефону " . $phoneStock;
        }


        return $item;
    }

    /**
     * @param array $item
     * @param array $article
     * @return array
     */
    public static function transformKeywords($item, $article)
    {
        if ($item['meta_keywords'] === "") {
            $item['meta_keywords'] = " Дизельные генераторы на базе двигателя " . $item['title'] . ", цена, купить, екатеринбург";
        }

        return $item;
    }


}