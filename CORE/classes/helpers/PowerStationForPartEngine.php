<?php

class PowerStationForPartEngine
{
    /**
     * @param array $item
     * @param array $article
     * @param string $phoneStock
     * @return array
     */
    public static function transform($item, $article, $phoneStock)
    {
        $item = self::transformTitle($item, $article);
        $item = self::transformDescription($item, $article, $phoneStock);
        $item = self::transformKeywords($item, $article);

        return $item;
    }

    /**
     * @param array $item
     * @param array $article
     * @return array
     */
    public static function transformTitle($item, $article)
    {
        if ($item['title_h1'] === "") {
            $item['title_h1'] = " Запчасти для дизельного двигателя " . $item['title'];
        }

        if ($item['title_page'] === "") {
            $item['title_page'] = "Запчасти для дизельного двигателя " . $item['title'] . " купить в Екатеринбурге";
        }

        return $item;
    }

    /**
     * @param array $item
     * @param array $article
     * @return array
     */
    public static function transformDescription($item, $article, $phoneStock)
    {
        if ($item['meta_description'] === "") {
            $item['meta_description'] = "ADG Energy предлагает купить запчасти для дизельного двигателя " . $item['title'] . "  по низкой цене в Екатеринбурге. Весь список запасных частей к двигателю " . $item['title'] . " указано на сайте. Доставка по России и странам СНГ. Подробности по телефону " . $phoneStock . ".";
        }

        return $item;
    }

    /**
     * @param array $item
     * @param array $article
     * @return array
     */
    public static function transformKeywords($item, $article)
    {
        if ($item['meta_keywords'] === "") {
            $item['meta_keywords'] = " Запчасти для дизельного двигателя " . $item['title'] . ", ремкомплект, цена, купить, екатеринбург";
        }

        return $item;
    }

}