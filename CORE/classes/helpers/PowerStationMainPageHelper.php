<?php

class PowerStationMainPageHelper
{
    /**
     * @param $article
     * @param PowerStationModel $powerStationModel
     * @param string $phoneStock
     * @return array
     *
     */
   public static function transformMainPage($article, $powerStationModel, $phoneStock)
   {
        $article = self::transformTitle($article);
        $article = self::transformDescription($article, $phoneStock);
        $article = self::transformKeyword($article);

        return $article;
   }

    /**
     * @param array $article
     * @return array
     */
   public static function transformTitle($article)
   {
       if ($article['title_page'] === "") {
           $article['title_page'] = "Дизельные электростанции (ДЭС) " . $article['name'] . " | Цены на дизельные генераторы (ДГУ) " . $article['name'];
       }

       return $article;
   }

    /**
     * @param array $article
     * @return array
     */
   public static function transformDescription($article, $phoneStock)
   {
       if ($article['meta_description'] === "") {
           $article['meta_description'] =  "Завод ADG-ENERGY предлагает купить дизельный генератор " . $article['name'] . " напрямую от производителя. Все виды вариантов исполнения.  Дополнительное оборудование. Доставка во все города России и страны СНГ. О наличии электростанций " . $article['name'] . " Вы можете узнать по телефону " . $phoneStock;
       }

       return $article;
   }

    /**
     * @param array $article
     * @return array
     */
    public static function transformKeyword($article)
    {
        if ($article['meta_keywords'] === "") {
            $article['meta_keywords'] =  "Дизельный генератор " . $article['name'] . ", электростанция, дгу, дэс, купить, цена, производитель, екатеринбург";
        }

        return $article;
    }

    public static function transformAltForImg($article) {

        $alt = "Дизельная эдектростанция (ДЭС) " . $article['name'];

        return $alt;
    }

    public static function transformAltForImgInVariants($article,$variants, $namePowerStation) {

        $alt = "Дизельная эдектростанция (ДЭС) " . $namePowerStation . " " .  (getVariant($variants[$article['title']]['name']));

        return $alt;
    }


}