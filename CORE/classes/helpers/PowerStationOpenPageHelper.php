<?php

class PowerStationOpenPageHelper
{
    /**
     * @param array $item
     * @param array $article
     * @param PowerStationModel $powerStationModel
     * @param string $phoneStock
     * @return array
     *
     */
   public static function transformOpenPage($item, $article, $powerStationModel, $phoneStock)
   {
       if ($item['item']['variant']['id'] === "2") {
           $item['item'] = self::trunsfotmTitleForMoveVarian($item['item'], $article);
       } else {
           $item['item'] = self::transformTitle($item['item'], $article);
       }

        $item['item'] = self::transformDescription($item['item'], $article, $phoneStock);
        $item['item'] = self::transformKeyword($item['item'], $article, $powerStationModel);

        return $item;
   }


    /**
     * @param array $item
     * @param array $article
     * @return array
     */
   public static function transformTitle($item, $article)
   {
       if ($item['title_page'] === "") {
           $item['title_page'] =  "Дизельный генератор " . $article['name'] . "  " . (getVariant($item['variant']['name']));
       }
       return $item;
   }

    /**
     * @param array $item
     * @param array $article
     * @return array
     */
    public static function transformDescription($item, $article, $phoneStock)
    {
        if ($item['meta_description'] === "") {
            $item['meta_description'] =  "Завод ADG-ENERGY предлагает купить дизельный генератор " .
                $article['name'] . " " . $item['variant']['name'] . " напрямую от производителя. Установка дополнительного оборудования. Доставка во все города России и страны СНГ.  
            Подробнее о электростанции ". $article['fullName'] . " " . (getVariant($item['variant']['name'])) . " .Вы можете узнать по телефону " . $phoneStock;
        }

        return $item;
    }

    /**
     * @param array $item
     * @param array $article
     * @param PowerStationModel $powerStationModel
     * @return array
     */
    public static function transformKeyword($item, $article, $powerStationModel)
    {

        $parentArticle = $powerStationModel->getArticleById($article['parent']);
        if ($item['meta_keywords'] === "") {
            $item['meta_keywords'] =  "Дизельный генератор ADG-ENERGY " . $article['name'] .  " , " . (getVariant($item['variant']['name'])) . " , " . $parentArticle['name'] . ", электростанция, дгу, дэс, купить, цена, производитель, екатеринбург";
        }

        return $item;
    }

    /**
     * @param array $item
     * @param array $article
     * @return array
     */
    public static function trunsfotmTitleForMoveVarian($item, $article)
    {
        if ($item['title_page'] === "") {
            $item['title_page'] =  "Варианты передвижения дизельного генератора " . $article['name'];
        }

        return $item;
    }

}