<?php

class PowerStationTitleHelper
{
    /**
     * @param array $article
     * @param PowerStationModel $powerStationModal
     * @param string $phoneStock
     * @return array
     */
    public static function transform($article, $powerStationModal, $phoneStock)
    {
        $article = self::transformTitleH1($article, $powerStationModal);
        $article = self::transformTitle($article);
        $article = self::transformDescription($article, $powerStationModal, $phoneStock);
        $article = self::transformKeyWord($article, $powerStationModal);

        return $article;
    }

    /**
     * @param array $article
     * @param PowerStationModel $powerStationModal
     * @return array
     */
   public static function transformTitleH1($article, $powerStationModal)
   {
        $parentArticle = $powerStationModal->getArticleById($article['parent']);
        if ($article['title_h1'] === "") {
            $article['title_h1'] = "Дизельный геренатор (электростанция) ADG-ENERGY " . $article['name'] . " - " . $parentArticle['name'];
        }

        return $article;
   }

    /**
     * @param array $_item
     * @param array $article
     * @param array $variants
     * @param array $item
     * @return array
     */
   public static function transformAltForImage($_item, $article, $variants, $item)
   {
       $_item['alt'] = "Дизельная электростанция (генератор) ADG-ENERGY " . $item['name'] . " " . $article['name'] . " " .  (getVariant($variants[$_item['title']]['name']));

       return $_item;
   }

    /**
     * @param array $article
     * @return array
     */
   public static function transformTitle($article)
   {
       if ($article['title_page'] === "") {
           $article['title_page'] .= ": Цена, Характеристики, Исполнение, Комплектация";
       }

       return $article;
   }

    /**
     * @param array $article
     * @param PowerStationModel $powerStationModal
     * @return array
     */
   public static function transformDescription($article, $powerStationModal, $phoneStock)
   {
       $parentArticle = $powerStationModal->getArticleById($article['parent']);
       if ($article['meta_description'] === "") {
           $article['meta_description'] .= "Завод ADG-ENERGY предлагает купить дизельный генератор " . $article['name'] .
               " напрямую от производителя.Все виды вариантов исполнения. Дополнительное оборудование.
       Доставка во все города России и страны СНГ. Подробнее о электростанции " . $article['name'] . " " . $parentArticle['name'] . " Вы можете узнать по телефону " . $phoneStock;
       }


       return $article;
   }

    /**
     * @param array $article
     * @param array $powerStationModal
     * @return array
     */
    public static function transformKeyWord($article, $powerStationModal)
    {
        $parentArticle = $powerStationModal->getArticleById($article['parent']);
        if ($article['meta_keywords'] === "") {
            $article['meta_keywords'] .= "Дизельный генератор ADG-ENERGY " . $article['name'] . " " . $parentArticle['name'] . ", электростанция, дгу, дэс, купить, цена, производитель, екатеринбург";
        }

        return $article;
    }
}