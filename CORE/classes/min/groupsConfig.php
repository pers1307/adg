<?php
/**
 * Groups configuration for default Minify implementation
 * @package Minify
 */

/** 
 * You may wish to use the Minify URI Builder app to suggest
 * changes. http://yourdomain/min/builder/
 *
 * See http://code.google.com/p/minify/wiki/CustomSource for other ideas
 **/

defined('DOC_ROOT') or require_once(dirname(__FILE__) . '/../../../config.php');

return $GLOBALS['staticFiles'];