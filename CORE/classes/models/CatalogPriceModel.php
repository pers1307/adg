<?php

class CatalogPriceModel extends MSBaseCatalog
{

    public function getItems($fields = null, $recursive = true)
    {
        $items = parent::getItems($fields, $recursive);

        foreach ($items['items'] as $key => $item) {
            $items['items'][$key]['price'] = $this->preparePrice($item);
        }

        return $items;
    }

    public function getItem($fields = null)
    {
        $item = parent::getItem($fields);
        $item['item']['price'] = $this->preparePrice($item['item']);

        return $item;
    }

    protected function preparePrice($item)
    {
        $activeCurrency = isset($item['active_currency']) ? $item['active_currency'] : CurrencyHelper::CURRENCY_RUB;
        $currency = CurrencyHelper::getCurrencyByCode($activeCurrency);

        if (isset($item['price_' . strtolower($currency->isoName)])) {
            return CurrencyHelper::convert($item['price_' . strtolower($currency->isoName)], $currency->isoCode);
        }

        return 0;
    }
}