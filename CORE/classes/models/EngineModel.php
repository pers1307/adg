<?php

class EngineModel extends OptionalEquipmentModel
{
    const MODULE_NAME = 'engines';
    const PARTS_URL   = 'zapchasti';

    /**
     * Переменная показывает находимся мы на странице запчастей или нет
     * @var bool
     */
    public $isParts   = false;

    public
        $itemsTableName = '{engines_items}',
        $articlesTableName = '{engines_articles}';

    protected $_articleRequiredFields = array('id', 'parent', 'code', 'name', 'level', 'image');

    /**
     * EngineModel constructor.
     * @param array $config
     */
    public function __construct(array $config = array())
    {
        if (!empty($config['vars'])) {
            foreach ($config['vars'] as $key => $var) {
                if ($var == EngineModel::PARTS_URL) {
                    unset($config['vars'][$key]);
                    $this->isParts = true;
                }
            }
        }

        parent::__construct($config);
    }

    public function getCurrentItem()
    {
        return $this->_currentItem;
    }

    public static function getProperties($item)
    {
        $config = MSCore::modules()->getModuleConfig(static::MODULE_NAME);

        $properties = [];
        foreach ($config['tables']['items']['config'] as $key => $value) {
            if (isset($value['group'])) {
                if (is_numeric($key)) {
                    $value['value'] = isset($item['properties'][$value['name']]) ? $item['properties'][$value['name']] : null;
                } else {
                    $value['value'] = isset($item[$key]) ? $item[$key] : null;
                }
                $properties[$value['group']][$key] = $value;
            }
        }

        return $properties;
    }

    public function getItem($fields = null)
    {
        $item = parent::getItem($fields);
        if ($item) {
            $item['item']['properties'] = json_decode($item['item']['properties'], true);
            $item['item']['itemLink'] = $this->path . '/' . $this->articleId2path[$item['item']['parent']] . '/' . ($item['item'][$this->itemKeyUniqueName] ? $item['item'][$this->itemKeyUniqueName] : $item['item'][$this->itemKeyPrimaryName]) . '/';
        }

        return $item;
    }

    /**
     * @param int $id
     *
     * @return array|null
     */
    public function getItemById($id)
    {
        $query = new MSTable($this->itemsTableName);
        $query->setFields(['*']);
        $query->setFilter('`id` = ' . $id);

        $result = $query->getItem();

        if (!empty($result)) {
            $result['brand'] = $this->getArticleById($result['parent']);
        }

        return $result;
    }

    /**
     * @param array $item
     *
     * @return array
     */
    public function getTabs($item)
    {
        $tabs = [
            0 => [
                'title'  => 'Характеристики',
                'url'    => $item['itemLink'],
                'active' =>
                    ($_SERVER['REQUEST_URI'] == $item['itemLink']) ? true : false
            ],
            1 => [
                'title'  => 'Запчасти',
                'url'    => $item['itemLink'] . EngineModel::PARTS_URL  . '/',
                'active' =>
                    ($_SERVER['REQUEST_URI'] == $item['itemLink'] . EngineModel::PARTS_URL  . '/') ? true : false
            ],
            2 => [
                'title'  => 'Генераторы с двигателем ' . $item['title'],
                'url'    => $item['itemLink'] . '?generators=1',
                'active' =>
                    ($_SERVER['REQUEST_URI'] == $item['itemLink'] . '?generators=1') ? true : false
            ],
        ];

        return $tabs;
    }

    /**
     * @param array  $item
     * @param object $settings
     *
     * @return mixed
     */
    public function replaceMetaForParts($item, $settings)
    {
        if (!empty($settings->parts_title_h1)) {
            $item['title_h1'] = str_replace(
                '%name%',
                $item['title'],
                $settings->parts_title_h1
            );
        }

        if (!empty($settings->parts_title_page)) {
            $item['title_page'] = str_replace(
                '%name%',
                $item['title'],
                $settings->parts_title_page
            );
        }

        if (!empty($settings->parts_meta_description)) {
            $item['meta_description'] = str_replace(
                '%name%',
                $item['title'],
                $settings->parts_meta_description
            );
        }

        if (!empty($settings->parts_meta_keywords)) {
            $item['meta_keywords'] = str_replace(
                '%name%',
                $item['title'],
                $settings->parts_meta_keywords
            );
        }

        return $item;
    }
}