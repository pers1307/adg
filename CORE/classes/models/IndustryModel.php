<?php

/**
 * Class IndustryModel
 *
 *
 * @property-read array $currentItem
 * @property-read array $items
 */
class IndustryModel extends MSBaseTape
{
    const LEFT_MENU_SHOT_LIMIT = 3;

    public function getCurrentItem()
    {
        return $this->_currentItem;
    }
}