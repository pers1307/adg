<?php

class ModelDuplex
{
    /**
     * @var array
     */
    public static $values = [
        '0' => 'Нет',
        '1' => 'Да',
    ];

    /**
     * @param string $value
     * @return int
     */
    public static function getKeyByValue($value) {
        $result = array_flip(static::$values);

        return $result[$value];
    }
}