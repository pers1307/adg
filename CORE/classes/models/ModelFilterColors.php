<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     */

    class ModelFilterColors
    {
        public $itemsTableName = '{filter_colors}';

        /**
         * @return array
         */
        public function getAll()
        {
            $query = new MSTable($this->itemsTableName);
            $query->setFields(['id', 'title']);
            $query->setOrder('`order` ASC');

            return $query->getItems();
        }

        /**
         * @return array
         */
        public function getFormalAll()
        {
            $items = $this->getAll();
            return $this->format($items);
        }

        /**
         * @param array $items
         * @return array
         */
        public function format($items)
        {
            $result = [];

            foreach ($items as $item) {
                $result[$item['id']] = $item['title'];
            }

            return $result;
        }
    }