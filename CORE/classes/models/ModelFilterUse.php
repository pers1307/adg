<?php
    /**
     * @author Andruha <andruha@mediasite.ru>
     * @copyright Mediasite LLC (http://www.mediasite.ru/)
     */

    class ModelFilterUse
    {
        public $itemsTableName = '{filter_use}';

        /**
         * @return array
         */
        public function getAll()
        {
            $query = new MSTable($this->itemsTableName);
            $query->setFields(['id', 'title']);
            $query->setOrder('`order` ASC');

            return $query->getItems();
        }

        /**
         * @return array
         */
        public function getFormalAll()
        {
            $items = $this->getAll();
            return $this->format($items);
        }

        /**
         * @param array $items
         * @return array
         */
        public function format($items)
        {
            $result = [];

            foreach ($items as $item) {
                $result[$item['id']] = $item['title'];
            }

            return $result;
        }

        /**
         * @param string $ids
         *
         * @return array
         */
        public function getItemsByIds($ids)
        {
            if (empty($ids)) {
                return [];
            }

            $query = new MSTable($this->itemsTableName);
            $query->setFields(['title']);
            $query->setFilter('`id` IN (' . $ids . ')');
            $query->setOrder('`order` ASC');

            return $query->getItems();
        }
    }