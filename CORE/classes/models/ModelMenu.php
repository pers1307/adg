<?php

class ModelMenu {
    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}

    protected static $_cache = array();

    public static function getMainMenu($path_id = 1, $parent = null, $www_type = 2, $sort = 'order') {

        $cacheId = md5($path_id.$parent.$www_type.$sort);

        $menuItems = array();
        $sectionList = MSCore::db()->getAll(
            'SELECT `title_menu`, `path`, `path_id`, `parent` FROM `' . PRFX . 'www` WHERE `visible` = 1 ' .
            'AND `parent` = ' . $path_id . ' AND `www_type` = ' . $www_type . ' ORDER BY `'.$sort.'`'
        );

        if (!$sectionList && !empty($parent) && $parent != 1) {
            $sectionList = MSCore::db()->getAll(
                'SELECT `title_menu`, `path`, `path_id`, `parent` FROM `' . PRFX . 'www` WHERE `visible` = 1 ' .
                'AND `parent` = ' . $parent . ' AND `www_type` = ' . $www_type . ' ORDER BY `'.$sort.'`'
            );
        }

        if ($sectionList) {
            $childsIds = array();
            $childsList = array();

            foreach ($sectionList as $_section) {
                $childsIds[] = $_section['path_id'];
            }

            if ($childsIds) {
                $itemsList = MSCore::db()->getAll(
                    'SELECT `title_menu`, `path`, `path_id`, `parent` FROM `' . PRFX . 'www` WHERE `visible` = 1 ' .
                    'AND `www_type` = ' . $www_type . ' AND parent IN (' . implode(',', $childsIds) . ') ORDER BY `'.$sort.'`'
                );

                foreach ($itemsList as $_child) {
                    if (empty($_child['title_menu'])) continue;

                    $item = array();
                    $item['id'] = $_child['path_id'];
                    $item['parent'] = $_child['parent'];
                    $item['title'] = $_child['title_menu'];
                    $item['path'] = '/' . $_child['path'] . '/';
                    $item['current'] = (strpos(MSCore::page()->path, $_child['path']) === 0) ? true : false;
                    $item['here'] = ($item['current'] === true && !MSCore::urls()->vars && (MSCore::page()->path == $_child['path'])) ? true : false;

                    $childsList[$item['parent']][] = $item;
                }
                unset($itemsList);
                unset($childsIds);
            }

            foreach ($sectionList as $_section) {
                if (empty($_section['title_menu'])) continue;

                $item = [];
                $item['id'] = $_section['path_id'];
                $item['parent'] = $_section['parent'];
                $item['title'] = $_section['title_menu'];
                $item['path'] = '/' . $_section['path'] . '/';
                $item['current'] = (strpos(MSCore::page()->path, $_section['path']) === 0) ? true : false;
                $item['here'] = ($item['current'] === true && !MSCore::urls()->vars && (MSCore::page()->path == $_section['path'])) ? true : false;
                $item['childs'] = (isset($childsList[$item['id']])) ? $childsList[$item['id']] : array();

                $menuItems[] = $item;
            }
            unset($sectionList);
        }

        static::$_cache[$cacheId] = $menuItems;
        return $menuItems;
    }

    public static function getMainMenuCached($path_id = 1, $parent = null, $www_type = 2, $sort = 'order') {
        $cacheId = md5($path_id.$parent.$www_type.$sort);
        if(empty(static::$_cache[$cacheId])) {
            static::$_cache = static::getMainMenu($path_id, $parent, $www_type, $sort);
        }
        return static::$_cache[$cacheId];
    }

    public static function getConcreteLink($pathId) {
        $menu = static::getMainMenuCached();
        $concrete = array();
        foreach($menu as $_item) {
            if($_item['id'] == $pathId) {
                $concrete = $_item;
                break;
            }
        }
        unset($menu);
        return $concrete;
    }

    public static function stack(&$gotMenu, $codes) {
        array_walk($gotMenu, function(&$value) use ($codes) {
            if(isset($codes[$value['id']])) {
                if(is_array($codes[$value['id']])) {
                    $value = array_merge($value, $codes[$value['id']]);
                } else {
                    $value['path'] .= $codes[$value['id']];
                }
            }
            if(!empty($value['childs'])) {
                return static::stack($value['childs'], $codes);
            }
        });

        return $gotMenu;
    }
}