<?php

class ModelPage
{
    public $itemsTableName = '{www}';

    /**
     * @param int $type
     * @return array
     */
    public function getItemsByType($type)
    {
        $query = new MSTable($this->itemsTableName);
        $query->setFields(['*']);
        $query->setFilter("`www_type` = '" . $type . "'");
        $query->setOrder('`order` ASC');

        return $query->getItems();
    }
}