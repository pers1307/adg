<?php

class ModelSpareParts extends MSBaseCatalog
{
    public
        $itemsTableName = '{spare_parts_items}',
        $articlesTableName = '{spare_parts_articles}';

    public $available = [
        0 => 'Под заказ',
        1 => 'В наличии',
    ];

    /**
     * @return array
     */
    public function getFormatParts()
    {
        $query = new MSTable($this->itemsTableName);
        $query->setFields(['*']);
        $query->setFilter('`active` = 1');

        $items = $query->getItems();

        $result = [];

        foreach ($this->_articles as $article) {
            foreach ($items as $item) {
                if ($item['parent'] == $article['id']) {
                    $result[$article['name']][] = $item;
                }
            }
        }

        return $result;
    }

    public function formatAvailable($int)
    {
        return $this->available[$int];
    }
}
