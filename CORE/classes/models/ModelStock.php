<?php

class ModelStock
{
    /**
     * @var array
     */
    public static $values = [
        0 => 'Пусто',
        1 => 'В наличии',
        2 => 'Под заказ',
    ];

    /**
     * @param string $value
     * @return int
     */
    public static function getKeyByValue($value) {
        $result = array_flip(static::$values);

        return $result[$value];
    }
}