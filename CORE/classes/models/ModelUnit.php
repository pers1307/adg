<?php

class ModelUnit
{
    /**
     * @var array
     */
    public static $units = [
        '1' => 'руб./кв. м.',
        '2' => 'руб./шт.',
        '3' => 'руб./пог. м.',
    ];

    /**
     * @param string $value
     * @return int
     */
    public static function getKeyByValue($value) {
        $result = array_flip(static::$units);

        return $result[$value];
    }
}