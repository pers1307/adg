<?php

class OptionalEquipmentModel extends CatalogPriceModel
{
    public
        $itemsTableName = '{optional_equipment_items}',
        $articlesTableName = '{optional_equipment_articles}';

    public function getIsCatalogIndexPage()
    {
        $vars = $this->_vars;
        $countVars = count($vars);
        return !$countVars || ($countVars == 1 && $this->getCurrentPage(array_pop($vars)));
    }

    /**
     * @param null $fields
     * @param bool|array $withItems
     *
     * @return array|bool|null
     */
    public function getArticle($fields = null, $withItems = true)
    {
        if ($this->_currentArticle === null)
        {
            if (count($this->_vars))
            {
                $lastParam = null;

                if (
                    ($articleId = array_search(implode('/', $this->checkArticleRedirect($this->_vars)), $this->articleId2path)) !== false ||
                    (($lastParam = array_pop($this->_vars)) && $articleId = array_search(implode('/', $this->checkArticleRedirect($this->_vars, $lastParam)), $this->articleId2path)) !== false
                )
                {
                    if ($lastParam !== null && (($this->pagination !== null && !$this->getCurrentPage($lastParam)) || $this->pagination === null))
                    {
                        $this->_itemKey = $lastParam;
                    }

                    $out = array(
                        'model' => $this,
                        'article' => null
                    );

                    $this->articlesTable->setFields($fields !== null ? $fields : array('*'));
                    $this->articlesTable->setOrder();
                    $this->articlesTable->setFilter('`a`.`id` = ' . $articleId, 'id');

                    $this->_currentArticle = $this->articlesTable->getItem();

                    if (empty($this->_currentArticle))
                    {
                        return false;
                    }
                    else
                    {
                        $out['article'] = $this->_currentArticle;

                        $lastPath = '';
                        foreach ($this->_vars as $var)
                        {
                            $lastPath .= ($lastPath ? '/' : '') . $var;
                            $articleId = array_search($lastPath, $this->articleId2path);

                            $this->_id2article[$articleId]['selected'] = true;

                            if ($this->autoBreadCrumbs)
                            {
                                MSCore::page()->breadCrumbs->addLevel($this->path . '/' . $this->articleId2path[$articleId] . '/', $this->articleId2name[$articleId]);
                            }

                            if ($this->autoTitle)
                            {
                                MSCore::page()->title_page = $this->articleId2name[$articleId] . ' &mdash; ' . MSCore::page()->title_page;
                            }
                        }

                        if ($withItems !== false)
                        {
                            $itemsOptions = array(
                                'fields'    => null,
                                'recursive' => true,
                                'withLimit' => true,
                            );
                            !is_array($withItems) or $itemsOptions = array_merge($itemsOptions, $withItems);

                            $out += $this->getItems($itemsOptions['fields'], $itemsOptions['recursive'], $itemsOptions['withLimit']);
                        }

                        return $out;
                    }
                }
                else
                {
                    return false;
                }
            }

            if($this->onlyHaveActiveItems)
            {
                $this->itemsTable->setFilter('`i`.`active` = 1', 'countActive');
            }
        }

        return $this->_currentArticle;
    }

    /**
     * @param null $fields
     * @param bool $recursive
     * @param bool $withLimit
     *
     * @return array
     */
    public function getItems($fields = null, $recursive = true, $withLimit = true)
    {
        $limit = null;
        $out = array(
            'model' => $this,
            'items' => null,
            'pagination' => null
        );

        $this->itemsTable->setFields($fields !== null ? $fields : array('*'));
        $this->itemsTable->setOrder($this->itemsOrder);

        if ($recursive) {
            $this->articlesTable->unsetFilter('id');
            $this->articlesTable->setFields(array('`a`.`id`'));
            $this->articlesTable->setFilter('`a`.`parents_visible` = 1', 'parents_visible');
            $this->articlesTable->setFilter('`a`.`lft` >= ' . $this->_currentArticle['lft'] . ' AND `a`.`rgt` <= ' . $this->_currentArticle['rgt'], 'children');

            $this->itemsTable->setFilter('`i`.`parent` in (' . $this->articlesTable->makeSql() . ')', 'parent');
        } else {
            if(!empty($this->_currentArticle['id'])) {
                $this->itemsTable->setFilter('`i`.`parent` = ' . $this->_currentArticle['id'], 'parent');
            }
        }

        if ($this->pagination) {
            is_array($this->pagination) or $this->pagination = array();

            !empty($this->pagination['linkMask']) or $this->pagination['linkMask'] = $this->path . '/' . $this->articleId2path[$this->_currentArticle['id']] . '/%link%/';
            !empty($this->pagination['autoTitle']) or $this->pagination['autoTitle'] = $this->autoTitle;

            $Pagination = new MSBasePagination(
                $this->pagination,
                $this->itemsTable->count,
                $this->currentPage
            );

            $limit = $Pagination->getLimit();
            $out['pagination'] = $Pagination;
        }

        if ($withLimit) {
            $items = $this->itemsTable->setLimit($limit)->getItems();
        } else {
            $items = $this->itemsTable->getItems();
        }

        foreach ($items as &$item) {
            $item['itemLink'] = $this->path . '/' . $this->articleId2path[$item['parent']] . '/' . ($item[$this->itemKeyUniqueName] ? $item[$this->itemKeyUniqueName] : $item[$this->itemKeyPrimaryName]) . '/';
        }
        unset($item);

        $out['items'] = $items;

        return $out;
    }
}