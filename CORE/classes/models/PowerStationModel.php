<?php

class PowerStationModel extends CatalogPriceModel
{
    const MODULE_NAME = 'power_stations';

    public
        $itemsTableName = '{power_stations_items}',
        $articlesTableName = '{power_stations_articles}';

    public function getArticle($fields = null, $withItems = true, $bread = '')
    {
        $article = parent::getArticle($fields, $withItems);

        if ($article !== false) {
            $fields = ['characteristics', 'control', 'electrical', 'generator', 'dimensions', 'information'];
            foreach ($fields as $field) {
                if (!empty($article['article'][$field])) {
                    $article['article'][$field] = json_decode($article['article'][$field], true);
                }
            }

            if (!empty($article['article']['engine'])) {
                $query = new MSTable('{engines_items}');
                $query->setFilter('`active` = 1', 'activeKey');
                $query->setFilter('`id`=' . (int)$article['article']['engine'], 'relKey');
                if ($engine = $query->getItem()) {
                    $engine['properties'] = json_decode($engine['properties'], 1);
                    $article['article']['engine'] = $engine;
                }
            }

            $article['article']['fullName'] = null;
            if ($article['article']['parent'] > 0) {
                $parent = $this->getArticleById($article['article']['parent']);
                $article['article']['fullName'] = $article['article']['name'] . ' ' . $parent['name'];
            }

            $article['article']['industries'] = $this->getIndustries();
            $article['article']['equipment'] = $this->getEquipment();

            $this->_currentArticle = $article['article'];
        }

        return $article;
    }

    public function initCharacteristics()
    {
        $configModule = MSCore::modules()->getModuleConfig(static::MODULE_NAME);
        foreach ($configModule['tables']['articles']['config'] as $cKey => $cValue) {
            if (!empty($cValue['in'])) {

                if (isset($this->_currentArticle[$cValue['in']][$cValue['name']])) {
                    $cValue['value'] = $this->_currentArticle[$cValue['in']][$cValue['name']];
                }
                $this->_currentArticle[$cValue['in']][$cValue['name']] = $cValue;
            }
        }

        return $this->_currentArticle;
    }

    public static function getMinPriceFromItems($items)
    {
        $price = 0;
        foreach ($items as $item) {
            if ($item['price'] > 0 && (($price > 0 && $item['price'] < $price) || $price == 0)) {
                $price = $item['price'];
            }
        }

        return $price;
    }

    /**
     * @param array $items
     *
     * @return int
     */
    public static function getOpenTypePrice($items)
    {
        $price = 0;
        foreach ($items as $item) {
            if ($item['title'] == 1) {
                $price = $item['price'];
            }
        }

        return $price;
    }

    /**
     * @param null $fields
     * @param bool $checkRedirects
     *
     * @return array|bool|string
     */
    public function getItem($fields = null, $checkRedirects = true)
    {
        if ($this->_itemKey)
        {
            $keyCondition = $this->getKeyCondition($this->_itemKey);
            if ($keyCondition === false)
            {
                return $keyCondition;
            }

            $out = array(
                'model' => $this,
                'item' => null
            );

            $this->itemsTable->setFields($fields !== null ? $fields : array('*'));
            $this->itemsTable->setFilter($keyCondition, 'keyFilter');
            $this->itemsTable->setFilter('`i`.`parent` = ' . $this->_currentArticle['id'], 'parent');
            $this->itemsTable->setOrder();

            $this->_currentItem = $this->itemsTable->getItem();

            if (empty($this->_currentItem)) {
                return false;
            } else {
                $this->_currentItem['itemLink'] = $this->path . '/' . $this->articleId2path[$this->_currentItem['parent']] . '/' . ($this->_currentItem[$this->itemKeyUniqueName] ? $this->_currentItem[$this->itemKeyUniqueName] : $this->_currentItem[$this->itemKeyPrimaryName]) . '/';
                $variants = VariantModel::getVariants();
                $variant = $variants[$this->_currentItem['title']];

                if (!empty($this->_currentArticle['fullName'])) {
                    $this->_currentItem['title'] = $this->_currentArticle['fullName'] . ' ' . mb_strtolower($variant['name'], 'utf-8');
                } else {
                    $this->_currentItem['title'] = $this->_currentArticle['name'] . ' ' . mb_strtolower($variant['name'], 'utf-8');
                }

                $this->_currentItem['variant'] = $variant;
                $this->_currentItem['price'] = $this->preparePrice($this->_currentItem);

                $out['item'] = $checkRedirects ? $this->checkItemRedirect($this->_currentItem) : $this->_currentItem;

                if ($this->autoBreadCrumbs) {
                    MSCore::page()->breadCrumbs->addLevel('', $variant['titleGenerator']);
                }

                if ($this->autoTitle) {
                    MSCore::page()->title_page = $this->_currentItem['title'] . ' &mdash; ' . MSCore::page()->title_page;
                }

                return $out;
            }
        }

        return false;
    }

    public function getId2article()
    {
        return $this->_id2article;
    }

    /**
     * @param array $item
     *
     * @return array
     */
    public function getOEPrices($item = [])
    {
        $result = null;

        if (
            !empty($this->_currentItem)
            ||!empty($item)
        ) {
            if (!empty($item)) {
                $query = static::pricesQuery($item);
            } else {
                $query = static::pricesQuery($this->_currentItem);
            }

            if ($items = $query->getItems()) {
                foreach ($items as $item) {
                    if (!isset($result[$item['parent']])) {
                        $result[$item['parent']] = [
                            'title' => $item['parentName'],
                            'text' => '',
                            'options' => [],
                        ];
                    }
                    $result[$item['parent']]['options'][] = $item;
                }
            }
        }

        return $result;
    }

    /**
     * @param $item
     * @return MSTable
     */
    public static function pricesQuery($item)
    {
        $fields = [
            '`oep`.*',
            '`oei`.`id` as itemId',
            '`oei`.`title` as title',
            '`oei`.`image` as image',
            '`oei`.`active_currency` as ActiveCurrencyDefault',
            '`oei`.`announce` as text',
            '`oei`.`code`',
            '`oea`.`id` as parent',
            '`oea`.`code` as parentCode',
            '`oea`.`name` as parentName',
        ];
        $currencies = CurrencyHelper::getCurrencies();
        foreach ($currencies as $currency) {
            $currName = strtolower($currency->isoName);
            $fields[] = '`oei`.`price_' . $currName . '` as ' . $currName . 'PriceDefault';
        }
        unset($currencies, $currency, $currName);

        $query = new PreparePriceTable('{optional_eq_prices} oep');
        $query->setJoin('{optional_equipment_items} oei', 'INNER', '`oei`.`id`=`oep`.`title`');
        $query->setJoin('{optional_equipment_articles} oea', 'INNER', '`oei`.`parent`=`oea`.`id`');
        $query->setFilter('`oep`.`item_id` = '.(int)$item['id'], 'keyItem');
        $query->setFilter('`oep`.`active`=1 AND `oei`.`active`=1 AND `oea`.`visible`=1', 'keyActive');
        $query->setOrder(['`oea`.`order`','`oep`.`order`']);
        $query->setFields($fields);

        return $query;
    }

    /**
     * @param $value
     */
    public function setItemKey($value)
    {
        $this->_itemKey = $value;
    }

    /**
     * @param string $ids
     *
     * @return string
     */
    public function sortArticlesByIds($ids)
    {
        if (empty($ids)) {
            return $ids;
        }

        $query = new MSTable($this->articlesTableName);

        $query
            ->setFields(['mp_power_stations_articles.id'])
            ->setJoin($this->articlesTableName . ' parentArticle', 'INNER', 'mp_power_stations_articles.`parent` = parentArticle.`id`')
            ->setFilter('mp_power_stations_articles.`id` IN (' . $ids . ')')
            ->setOrder('parentArticle.`order`, mp_power_stations_articles.`order`');
        $ids = $query->getItemsCol();
        $ids = implode(',', $ids);

        return $ids;
    }

    /**
     * @param EngineModel $engineModel
     * @param int         $filterEngineId
     *
     * @return array
     */
    public function getArticlesFormat($engineModel, $engines = [], $filterEngineId = 0)
    {
        $result = [];

        foreach ($this->fullTree as $article) {
            if (empty($article['children'])) {
                continue;
            }

            if (!is_null($article['generator'])) {
                $article['generator'] = $engineModel->getItemById($article['generator']);
            }

            foreach ($article['children'] as $key => &$item) {
                $this->_currentArticle = $item;
                $itemsData = $this->getItems(null, false);
                $item['items']  = $itemsData['items'];
                $item['parent'] = $this->getArticleById($item['parent']);

                if (!empty($item['engine'])) {
                    $item['engine'] = $engineModel->getItemById($item['engine']);
                }

                if (!empty($filterEngineId)) {
                    if ($item['engine']['id'] != $filterEngineId) {
                        continue;
                    }
                }

	            if (0 != count($engines)) {
		            if (!in_array($item['engine']['brand']['id'], $engines)) {
			            continue;
		            }
	            }

                $result[] = $item;
            }
        }

        return $result;
    }

    /**
     * @param EngineModel $engineModel
     *
     * @return array
     */

    /**
     * @param EngineModel $engineModel
     * @param string $articleIds
     *
     * @return array
     */
    public function getArticlesFormatToIndustry($engineModel, $articleIds)
    {
        $result = [];

        if (!empty($articleIds)) {
            $articleIds = $this->sortArticlesByIds($articleIds);
            $articleIds = explode(',', $articleIds);

            foreach ($articleIds as $articleId) {
                $article = $this->getArticleById($articleId);

                $this->_currentArticle = $article;
                $itemsData = $this->getItems(null, false);
                $article['items']  = $itemsData['items'];
                $article['parent'] = $this->getArticleById($article['parent']);

                if (!empty($article['engine'])) {
                    $article['engine'] = $engineModel->getItemById($article['engine']);
                }

                if (!empty($filterEngineId)) {
                    if ($article['engine']['id'] != $filterEngineId) {
                        continue;
                    }
                }

                $result[] = $article;
            }
        }

        return $result;
    }

    public function getPaginationForArticle($countArticle, $engineId = false)
    {
        $currentUrl = preg_replace('/\/page\d+$/suiU','',MSCore::urls()->current);

        $items = $this->getItems(['*'], false);

        /** @var MSBasePagination $pagination */
        $pagination = $items['pagination'];

        $pagination->linkMask = '/' . $currentUrl . '/%link%/';

        $pagination->_countPages = ceil($countArticle / $pagination->onPage);
        $pagination->_countItems = $countArticle;

        return $pagination;
    }

    /**
     * @param array            $articles
     * @param MSBasePagination $pagination
     *
     * @return array
     */
    public function filterArticleForPagination($articles, $pagination)
    {
        $result = [];

        for ($index = 0; $index < $pagination->onPage; ++$index) {
            if ($pagination->_currentPage == 1) {
                $position = $index;
            } else {
                $position = (($pagination->_currentPage - 1) * $pagination->onPage) + $index;
            }

            if (isset($articles[$position])) {
                $result[] = $articles[$position];
            } else {
                break;
            }
        }

        return $result;
    }

    public function allowPagesForIndexPage()
    {
        $vars = MSCore::urls()->vars;
        $firstSegment = reset($vars);
        $lastSegment = end($vars);
        $currentUrl = preg_replace('/\/page\d+$/suiU','',MSCore::urls()->current);

        if($lastSegment && preg_match('/^page([0-9]+)$/', $lastSegment, $match)) {
            $this->currentPage = $match[1];
            array_pop($vars);
            $this->_vars = [];
        }
    }

    /**
     * @return mixed
     */
    protected function getIndustries()
    {
        $query = new MSTable('{industry}');
        $query
            ->setFields(['id', 'title', 'code'])
            ->setFilter('CONCAT(",",list,",") LIKE "%,' . (int)$this->_currentArticle['id'] . ',%"', 'keyInList')
            ->setFilter('`active`=1', 'keyActive')
            ->setOrder('order');

        return $query->getItems();
    }

    protected function getEquipment()
    {
        $query = new MSTable('{power_stations_equipment}');
        $query
            ->setFilter('`path_id`=' . (int)$this->pathId, 'itemkey')
            ->setFilter('`item_id`=' . (int)$this->_currentArticle['id'], 'itemkey')
            ->setFilter('`active`=1', 'keyActive')
            ->setOrder('order');

        return $query->getItems();
    }
}