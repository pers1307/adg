<?php

/**
 * Class VariantModel
 */
class VariantModel extends MSBaseCatalog
{
    const MOVEMENT = 2;

    public
        $itemsTableName = '{variants_items}',
        $articlesTableName = '{variants_articles}';

    public function getItems($fields = null, $recursive = true)
    {
        $out = parent::getItems($fields, $recursive);

        if (!empty($out['items'])) {
            $query = new MSTable('{variants_item_list}');
            $query->setFilter('`active` = 1', 'activeKey');
            foreach ($out['items'] as &$item) {
                $query->setFilter('`item_id` = ' . (int)$item['id'], 'relationKey');
                $item['options'] = $query->getItems();
            }
        }

        return $out;
    }

    public static function getVariants($withItems = true)
    {
        $articles = [];
        $model = new static([
            'articlesTreeFields' => ['*'],
            'itemsOrder' => '`order` ASC',
        ]);
        foreach ($model->articles as $article) {
            $articles[$article['id']] = $article;
            if ($withItems) {
                $model->_currentArticle = $article;
                $items = $model->getItems(null, false);
                $articles[$article['id']]['items'] = $items['items'];
            }
        }

        return $articles;
    }
}