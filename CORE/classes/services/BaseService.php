<?php

/**
 * Базовый класс для всех сервисов
 * Class BaseService
 * @package common\services
 */
class BaseService
{
    /**
     * @var null|BaseService
     */
    protected static $instance = null;

    /**
     * @return null|BaseService
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }
}
