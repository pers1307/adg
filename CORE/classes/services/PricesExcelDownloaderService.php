<?php
require_once VENDOR_DIR  . '/phpoffice/phpexcel/Classes/PHPExcel.php';

class PricesExcelDownloaderService extends BaseService implements InterfacePriceService
{
    private $activeSheetIndex = 0;

    /**
     * @param string $pathToFile
     */
    public function processFile($pathToFile)
    {
        $this->startCreateFile($pathToFile);
    }

    /**
     * @param string $pathToFile
     */
    public function startCreateFile($pathToFile)
    {
        $xlsInstance = new \PHPExcel();
        $pagesWithCatalog = array(
            "variants" => "Варианты исполнения",
            "chassi" => "Передвижной тип"
        );

        foreach ($pagesWithCatalog as $page) {
            $xlsInstance = $this->generateListsByPage($page, $xlsInstance);
        }

        $this->saveFile($pathToFile, $xlsInstance);
    }

    /**
     * @param array $page
     * @param PHPExcel $xlsInstance
     * @return PHPExcel
     */
    protected function generateListsByPage($page, $xlsInstance)
    {
        if ($page === "Варианты исполнения") {
            $xlsInstance = $this->generateListsVariantsWithItemsByPage($page, $xlsInstance);
        } else {
            $xlsInstance = $this->generateListsChassiWithItemsByPage($page, $xlsInstance);
        }

        return $xlsInstance;
    }

    /**
     * @param array $page
     * @param PHPExcel $xlsInstance
     * @return mixed
     * @throws PHPExcel_Exception
     */
    protected function generateListsVariantsWithItemsByPage($page, $xlsInstance)
    {
        $activeSheetIndex = $this->getActiveSheetIndex();

        if ($activeSheetIndex == 0) {
            $xlsInstance->setActiveSheetIndex($activeSheetIndex);
            $sheet = $xlsInstance->getActiveSheet();
        } else {
            $sheet = $xlsInstance->createSheet($activeSheetIndex);
        }

        // Подписываем лист
        $sheet->setTitle($page);

        /**
         * todo: зарефакторить под DI
         */
        $items = $this->getItemsVariantsWithArticlesByPathId();

        $this->setHeaderVariantsListsWithItems($sheet);

        foreach ($items as $key => $item) {
            $sheet->setCellValueByColumnAndRow(0, $key + 2, $item['id']);
            $sheet->setCellValueByColumnAndRow(1, $key + 2, $item['article']);
            $sheet->setCellValueByColumnAndRow(2, $key + 2, $item['station']);
            $sheet->setCellValueByColumnAndRow(3, $key + 2, $item['name']);
            $sheet->setCellValueByColumnAndRow(4, $key + 2, $item['price_rub']);
            $sheet->setCellValueByColumnAndRow(5, $key + 2, $item['price_usd']);
            $sheet->setCellValueByColumnAndRow(6, $key + 2, $item['price_eur']);
            $sheet->setCellValueByColumnAndRow(7, $key + 2, $item['price_jpy']);
            $sheet->setCellValueByColumnAndRow(8, $key + 2, $item['active_currency']);
            $sheet->setCellValueByColumnAndRow(9, $key + 2, $item['options']);
        }
        $var = 1;
        // Применяем выравнивание
        for ($indexColumn = 0; $indexColumn < 20; ++$indexColumn) {
            for ($indexRow = 2; $indexRow < count($items) + 2; ++$indexRow) {
                $sheet->getStyleByColumnAndRow($indexColumn, $indexRow)->getAlignment()->
                setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

                $var = $this->setColorForVariant($sheet, $indexRow, $var);
            }
        }

        return $xlsInstance;
    }

    /**
     * @param array $page
     * @param PHPExcel $xlsInstance
     * @return mixed
     * @throws PHPExcel_Exception
     */
    protected function generateListsChassiWithItemsByPage($page, $xlsInstance)
    {
        $activeSheetIndex = $this->getActiveSheetIndex();

        if ($activeSheetIndex == 0) {
            $xlsInstance->setActiveSheetIndex($activeSheetIndex);
            $sheet = $xlsInstance->getActiveSheet();
        } else {
            $sheet = $xlsInstance->createSheet($activeSheetIndex);
        }

        // Подписываем лист
        $sheet->setTitle($page);

        /**
         * todo: зарефакторить под DI
         */
        $items = $this->getItemsChassiWithArticlesByPathId();

        $this->setHeaderChassiListsWithItems($sheet);

        foreach ($items as $key => $item) {
            $sheet->setCellValueByColumnAndRow(0, $key + 2, $item['id']);
            $sheet->setCellValueByColumnAndRow(1, $key + 2, $item['article']);
            $sheet->setCellValueByColumnAndRow(2, $key + 2, $item['station']);
            $sheet->setCellValueByColumnAndRow(3, $key + 2, $item['name']);
            $sheet->setCellValueByColumnAndRow(4, $key + 2, $item['price_rub']);
        }

        // Применяем выравнивание
        for ($indexColumn = 0; $indexColumn < 10; ++$indexColumn) {
            for ($indexRow = 2; $indexRow < count($items) + 2; ++$indexRow) {
                $sheet->getStyleByColumnAndRow($indexColumn, $indexRow)->getAlignment()->
                setHorizontal(\PHPExcel_Style_Alignment::HORIZONTAL_RIGHT);

            }
        }

        return $xlsInstance;
    }


    /**
     * @param string $pathToFile
     * @param PHPExcel $xlsInstance
     * @throws PHPExcel_Writer_Exception
     */
    protected function saveFile($pathToFile, $xlsInstance)
    {
        $objWriter = new \PHPExcel_Writer_Excel5($xlsInstance);
        $objWriter->save($pathToFile);
    }

    /**
     * @return int
     */
    private function getActiveSheetIndex()
    {
        $forReturn = $this->activeSheetIndex;

        ++$this->activeSheetIndex;

        return $forReturn;
    }

    /**
     * @param int $pathId
     * @return array
     */
    public function getItemsVariantsWithArticlesByPathId()
    {
//        optional.title options

        $items = MSCore::db()->getAll("
             SELECT
		      power_station.id,
		      power_station.article,
		      power_station.station,
		      power_station.name,
		      power_station.price_rub,
		      power_station.price_usd,
		      power_station.price_eur,
		      power_station.price_jpy,
		      power_station.active_currency,
		      optional.title options
		    FROM (
		      SELECT
		      mp_power_stations_items.id,
		      psa2.name as article,
		      psa.name as station,
		      mp_variants_articles.name,
		      mp_power_stations_items.price_rub,
		      mp_power_stations_items.price_usd,
		      mp_power_stations_items.price_eur,
		      mp_power_stations_items.price_jpy,
		      mp_power_stations_items.active_currency
		    FROM mp_power_stations_items
		    JOIN mp_power_stations_articles psa ON mp_power_stations_items.parent = psa.id
		    JOIN mp_power_stations_articles psa2 ON psa.parent = psa2.id
		    JOIN mp_variants_articles ON mp_power_stations_items.title = mp_variants_articles.id
		    ) as power_station
		    LEFT JOIN (
		      SELECT
		      mp_optional_eq_prices.id,
		      mp_optional_eq_prices.item_id,
		      mp_optional_equipment_items.title
		    FROM mp_optional_eq_prices
		    JOIN mp_optional_equipment_items ON mp_optional_eq_prices.title = mp_optional_equipment_items.id
		    WHERE mp_optional_equipment_items.title LIKE '%шкаф%'
		    ) as optional
		    ON power_station.id = optional.item_id
		    ORDER BY power_station.id;
        ");

        foreach ($items as &$item) {
            if (!empty($item['active_currency'])) {
                $item['active_currency'] =
                    (CurrencyHelper::getCurrencyByCode($item['active_currency']))->isoName;
            }
        }

        return $items;
    }

    public function getItemsChassiWithArticlesByPathId()
    {
        $items = MSCore::db()->getAll('
            SELECT
              mp_optional_eq_prices.id,
              psa2.name as article,
              psa.name as station,
              mp_variants_articles.name,
              mp_optional_eq_prices.price_rub,
              mp_optional_eq_prices.price_usd,
              mp_optional_eq_prices.price_eur,
              mp_optional_eq_prices.price_jpy,
              mp_optional_eq_prices.active_currency
            FROM mp_power_stations_items
            JOIN mp_power_stations_articles psa ON mp_power_stations_items.parent = psa.id
            JOIN mp_power_stations_articles psa2 ON psa.parent = psa2.id
            JOIN mp_variants_articles ON mp_power_stations_items.title = mp_variants_articles.id
            JOIN mp_optional_eq_prices ON mp_optional_eq_prices.item_id = mp_power_stations_items.id
            WHERE mp_optional_eq_prices.title = \'101\' AND mp_power_stations_items.title = \'2\'
            ORDER BY article ASC
        ');

        foreach ($items as &$item) {
            $item['active_currency'] =
                (CurrencyHelper::getCurrencyByCode($item['active_currency']))->isoName;
        }

        return $items;
    }


    /**
     * @param PHPExcel_Worksheet $sheet
     */
    protected function setHeaderVariantsListsWithItems($sheet)
    {
        $this->setPramForHeader(
            $sheet,
            "A",
            "A1",
            'id',
            20
        );

        $this->setPramForHeader(
            $sheet,
            "B",
            "B1",
            'Мощность',
            20
        );
        $this->setPramForHeader(
            $sheet,
            "C",
            "C1",
            'Генератор',
            30
        );
        $this->setPramForHeader(
            $sheet,
            "D",
            "D1",
            'Модификация',
            30
        );

        $this->setPramForHeader(
            $sheet,
            "E",
            "E1",
            'Цена в руб',
            20
        );
        $this->setPramForHeader(
            $sheet,
            "F",
            "F1",
            'Цена в usd',
            20
        );
        $this->setPramForHeader(
            $sheet,
            "G",
            "G1",
            'Цена в eur',
            20
        );

        $this->setPramForHeader(
            $sheet,
            "H",
            "H1",
            'Цена в jpy',
            20
        );
        $this->setPramForHeader(
            $sheet,
            "I",
            "I1",
            'Выбранная валюта',
            20
        );
        $this->setPramForHeader(
            $sheet,
            "J",
            "J1",
            'Шкаф',
            20
        );
    }

    protected function setHeaderChassiListsWithItems($sheet)
    {
        $this->setPramForHeader(
            $sheet,
            "A",
            "A1",
            'id',
            20
        );

        $this->setPramForHeader(
            $sheet,
            "B",
            "B1",
            'Мощность',
            20
        );
        $this->setPramForHeader(
            $sheet,
            "C",
            "C1",
            'Генератор',
            30
        );
        $this->setPramForHeader(
            $sheet,
            "D",
            "D1",
            'Модификация',
            30
        );

        $this->setPramForHeader(
            $sheet,
            "E",
            "E1",
            'Цена в руб',
            20
        );


    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @param string $colon
     * @param string $cell
     * @param string $nameColon
     * @param int    $width
     */
    protected function setPramForHeader($sheet, $colon, $cell, $nameColon, $width)
    {
        $sheet->setCellValue($cell, $nameColon);
        $sheet->getStyle($cell)->getFill()->getStartColor()->setRGB('EEEEEE');
        $sheet->getStyle($cell)->getAlignment()->setHorizontal(
            \PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
        $sheet->getColumnDimension($colon)->setWidth($width);
    }

    private function setColorForVariant($sheet, $indexRow, $var){
        if ($var <= 4) {
            $sheet
                ->getStyle('D' . $indexRow)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('F08080');
        } else if ($var > 4 && $var <= 8) {
            $sheet
                ->getStyle('D' . $indexRow)
                ->getFill()
                ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
                ->getStartColor()
                ->setARGB('7FFF00');
            if ($var === 8) {
                $var = 0;
            }

        }
        return ++$var;
    }

}