<?php

/**
 * Class PricesExcelManagerService
 */
class PricesExcelManagerService {
    /**
     * @var PricesExcelUploaderService
     */
    protected $priceUploadService;

    /**
     * @var PricesExcelDownloaderService
     */
    protected $priceDownloadService;

    /**
     * PricesExcelManagerService constructor.
     *
     * @param $priceUploadService
     * @param $priceDownloadService
     */
    public function __construct(
        InterfacePriceService $priceUploadService,
        InterfacePriceService $priceDownloadService
    ) {
        $this->priceUploadService   = $priceUploadService;
        $this->priceDownloadService = $priceDownloadService;
    }

    /**
     * @param string $pathToFile
     * @throws PHPExcel_Reader_Exception
     */
    public function uploadExcelFile($pathToFile)
    {
        $this->priceUploadService->processFile($pathToFile);
    }

    /**
     * @param string $pathToFile
     */
    public function downloadExcelFile($pathToFile)
    {
        $this->priceDownloadService->processFile($pathToFile);
    }
}