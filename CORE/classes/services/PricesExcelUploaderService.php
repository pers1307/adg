<?php

class PricesExcelUploaderService extends BaseService implements InterfacePriceService
{
    /**
     * @param string $pathToFile
     * @throws PHPExcel_Reader_Exception
     */
    public function processFile($pathToFile)
    {
        $this->startUploadFile($pathToFile);
    }

    /**
     * @param string $pathToFile
     * @throws PHPExcel_Reader_Exception
     */
    public function startUploadFile($pathToFile)
    {
        /** @var PHPExcel $xlsInstance */
        $xlsInstance = \PHPExcel_IOFactory::load($pathToFile);
        $allSheets = $xlsInstance->getAllSheets();

        foreach (array_reverse($allSheets) as $sheet) {
            if ($sheet->getTitle() === "Варианты исполнения") {
                $this->parseSheetVariants($sheet);
            } else if ($sheet->getTitle() === "Передвижной тип") {
                $this->parseSheetChassi($sheet);
            }
        }
    }


    protected function parseSheetVariants($sheet)
    {
        $arrayWithDataFromSheet = $this->convertSheetToArray($sheet);
        $arrayWithDataFromSheet = $this->convertArrayFromSheetToArrayForUpdateVariants($arrayWithDataFromSheet);
        $this->insertUpdateArrayForUpdateVariants($arrayWithDataFromSheet);
    }

    protected function parseSheetChassi($sheet)
    {
        $arrayWithDataFromSheet = $this->convertSheetToArray($sheet);
        $arrayWithDataFromSheet = $this->convertArrayFromSheetToArrayForUpdateChassi($arrayWithDataFromSheet);
        $this->insertUpdateArrayForUpdateChassi($arrayWithDataFromSheet);
    }

    /**
     * @param PHPExcel_Worksheet $sheet
     * @return array
     */
    protected function convertSheetToArray($sheet)
    {
        //этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
        $array = [];
        //получим итератор строки и пройдемся по нему циклом
        foreach($sheet->getRowIterator() as $row) {
            //получим итератор ячеек текущей строки
            $cellIterator = $row->getCellIterator();
            //пройдемся циклом по ячейкам строки
            //этот массив будет содержать значения каждой отдельной строки
            $item = array();
            foreach($cellIterator as $cell){
                //заносим значения ячеек одной строки в отдельный массив
                array_push($item, iconv('utf-8', 'cp1251', $cell->getCalculatedValue()));
                array_push($item, $cell->getCalculatedValue());
            }
            //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
            array_push($array, $item);
        }

        return $array;
    }

    /**
     * @param array $arrayWithDataFromSheet
     * @return array
     */
    protected function convertArrayFromSheetToArrayForUpdateVariants($arrayWithDataFromSheet)
    {
        // Удаление заголовка
        unset($arrayWithDataFromSheet[0]);

        foreach ($arrayWithDataFromSheet as $item) {
            if ($this->findNullVariants($item)){
                continue;
            }
            $resultItem = [];

            $resultItem['id'] = $item[1];
            $resultItem['article']  = $item[3];
            $resultItem['station'] = $item[5];
            $resultItem['name']            = $item[7];
            $resultItem['price_rub']       = $item[9];
            $resultItem['price_usd']       = $item[11];
            $resultItem['price_eur']       = $item[13];
            $resultItem['price_jpy']       = $item[15];
            $resultItem['active_currency'] = $item[17];
            $resultItem['options']         = $item[19];

            $result[] = $resultItem;
        }

        return $result;
    }

    protected function convertArrayFromSheetToArrayForUpdateChassi($arrayWithDataFromSheet)
    {
        // Удаление заголовка
        unset($arrayWithDataFromSheet[0]);

        foreach ($arrayWithDataFromSheet as $item) {

            if ($this->findNullChassi($item)){
                continue;
            }

            $resultItem = [];

            $resultItem['id'] = $item[1];
            $resultItem['article']  = $item[3];
            $resultItem['station'] = $item[5];
            $resultItem['name'] = $item[7];
            $resultItem['price_rub'] = $item[9];

            $result[] = $resultItem;
        }

        return $result;
    }

    /**
     * @param array $arrayWithDataFromSheet
     */
    protected function insertUpdateArrayForUpdateVariants($arrayWithDataFromSheet)
    {
        foreach ($arrayWithDataFromSheet as $item) {
            $id = $this->getIdPowerStation($item['station'], $item['name']);

            MSCore::db()->update(
                PRFX . 'power_stations_items',
                [
                    'price_rub' => $item['price_rub'],
                    'price_usd' => $item['price_usd'],
                    'price_eur' => $item['price_eur'],
                    'price_jpy' => $item['price_jpy'],
                ],
                '`id` = ' . $id
            );

	        if(!empty($item['options'])) {
		        $prevOptionTitle = MSCore::db()->getOne("
	                SELECT `mp_optional_eq_prices`.`title` FROM `mp_optional_eq_prices`
	                JOIN `mp_optional_equipment_items`
		            ON `mp_optional_eq_prices`.`title` = `mp_optional_equipment_items`.`id`
	                WHERE `mp_optional_eq_prices`.`item_id` = ".$item['id']." AND
	                `mp_optional_equipment_items`.`title` LIKE '%шкаф%'
	            ");

		        $newOptionTitle = MSCore::db()->getOne("
	                SELECT `id`
                    FROM `mp_optional_equipment_items`
                    WHERE `title` LIKE '%".$item['options']."%'
	            ");

		        //Если шкаф назначен на генератор, просто изменяем запись
		        if(!empty($prevOptionTitle)) {
			        MSCore::db()->update(
				        PRFX . 'optional_eq_prices',
				        [
					        'title' => (int)$newOptionTitle
				        ],
				        '`title` = "'.(int)$prevOptionTitle.'" AND `item_id` = '.(int)$item['id']
			        );
		        } else {
		        	/*
		        	Если шкаф не назначен, вставляем новую запись
		        	сначала достаём path_id и минимальный order из таблицы
		        	*/
		        	$insertData = MSCore::db()->getRow("
			            SELECT `path_id`, MIN(`order`) 
			            AS min_order
			            FROM `mp_optional_eq_prices` 
			            WHERE `item_id` = ".$item['id']." 
			            GROUP BY `path_id`
			        ");

			        MSCore::db()->insert(
				        PRFX . 'optional_eq_prices',
				        [
					        'item_id' => (int)$item['id'],
					        'title'   => (int)$newOptionTitle,
					        'path_id' => (int)$insertData['path_id'],
					        'order'   => ($insertData['min_order'] - 1),
					        'text'    => ''
				        ]
			        );
		        }
	        } else {
	        	/*
	        	Здесь отработается обратный вариант - если был шкаф прикреплён
	        	к генератору, но мы хотим его убрать.
	        	*/
		        $isThereOption = MSCore::db()->getOne("
	                SELECT `mp_optional_eq_prices`.`title` FROM `mp_optional_eq_prices`
	                JOIN `mp_optional_equipment_items`
		            ON `mp_optional_eq_prices`.`title` = `mp_optional_equipment_items`.`id`
	                WHERE `mp_optional_eq_prices`.`item_id` = ".$item['id']." AND
	                `mp_optional_equipment_items`.`title` LIKE '%шкаф%'
	            ");

		        if(!empty($isThereOption)) {
					MSCore::db()->delete(
						PRFX . 'optional_eq_prices',
						"`item_id` = ".(int)$item['id']." AND `title` = ".(int)$isThereOption
					);
		        }
	        }

            if (!empty($item['options']) && empty($item['id'])) {
                $id = MSCore::db()->getOne('
                    SELECT id
                    FROM mp_optional_equipment_items
                    WHERE `title` LIKE "%' . $item['options'] . '%"
                ');

                if (!empty($id)) {
                    MSCore::db()->update(
                        PRFX . 'optional_eq_prices',
                        [
                            'title' => $id
                        ],
                        '`title` = "82" AND `item_id` = ' . $item['id']
                    );
                }
            }

        }
    }

    protected function insertUpdateArrayForUpdateChassi($arrayWithDataFromSheet)
    {
        foreach ($arrayWithDataFromSheet as $item) {

            if (!empty($item['id'])) {
                MSCore::db()->update(
                    PRFX . 'optional_eq_prices',
                    [
                        'price_rub' => $item['price_rub']
                    ],
                    '`id` = ' . $item['id']
                );
            } else {
                /**
                 * по названию найти передвижной тип и его шасси
                 */
                if ($item['station'] !== null) {
                    $idOptionalEqPrices = MSCore::db()->getOne('
                        SELECT mp_optional_eq_prices.id
                        FROM mp_power_stations_articles
                        JOIN mp_power_stations_items ON mp_power_stations_items.parent = mp_power_stations_articles.id
                        JOIN mp_optional_eq_prices ON mp_optional_eq_prices.item_id = mp_power_stations_items.id
                        WHERE
                          mp_power_stations_articles.name LIKE "' . $item['station'] . '"
                          AND mp_power_stations_items.code = \'peredvezhnoj\'
                    ');

                    MSCore::db()->update(
                        PRFX . 'optional_eq_prices',
                        [
                            'price_rub' => $item['price_rub']
                        ],
                        '`id` = ' . $idOptionalEqPrices
                    );
                }
            }

        }
    }



    /**
     * @param $nameStation Название генератора
     * @param $item_name
     * @return mixed
     */
    public function getIdPowerStation($nameStation, $item_name)
    {
        $typeMap = [
            'Открытый тип' => 1,
            'В кожухе' => 3,
            'В контейнере' => 4,
            'Передвижной тип' => 2,
        ];

        $idPowerStation = MSCore::db()->getOne('
            SELECT id
            FROM mp_power_stations_articles
            WHERE `name` LIKE "%' . $nameStation . '%"
        ');

        $idModification = MSCore::db()->getOne('
            SELECT id
            FROM mp_power_stations_items
            WHERE `title` = "' . $typeMap[$item_name] . '"
            AND parent = ' . $idPowerStation . '
        ');

        if (!empty($idModification)) {
            return $idModification;
        }
    }

    private function findNullVariants($item){
        $count = 0;
        for ($i = 4; $i <= 7; $i++) {
            if (is_null($item[$i])) {
                $count++;
            }
        }

        if ($count === 4) {
            return true;
        }

        return false;

    }

    private function findNullChassi($item){

        if (is_null($item[4])) {
            return true;
        }

        return false;
    }

}