<?php
/**
 * Класс для построения "хлебных крошек"
 *
 * Пример использования:
 * <pre>
 * $breadCrumbs = new breadCrumbs();
 * $breadCrumbs->fastGet( $page['path_id'] ); //загрузка пути
 * //предыдущие два действия по умл. выполняются при загрузке CMS
 * $breadCrumbs->level(1)->setActive(0); //делаем не активым первый уровень
 * $breadCrumbs->level(1)->setTitle('Название'); //меняем заголовок первого уровня
 * $breadCrumbs->level(1)->setUrl('/test/'); //меняем урл первого уровня
 * $breadCrumbs->remove(2); //удаляем второй уровень
 * $breadCrumbs->addLevel('/url/', 'Название'); //добавляем в конец еще один уровень
 * $breadCrumbs->addLevel('/url/', 'Название', array('after', 0)); //добавляем перед нулевым (по умл. Главная) новый уровень
 * echo $breadCrumbs; //вывод
 * </pre>
 *
 * @package Core.system
 */
class BreadCrumbs {

    private
        $levels = array(),
        $_db = null;
    public
        $separator = '',
        $lastActive = 0,
		$alter = '';

    function __construct()
    {
        $this->_db = MSCore::db();
        $this->addLevel('/', 'Главная', 1);
    }

    function __toString()
    {
        return $this->render();
    }

    /**
     * Возвращаем объект уровня
     *
     * @param int $levelKey
     *
     * @return BreadCrumbsLevel
     */
    public function level($levelKey = 0)
    {
        return $this->levels[$levelKey];
    }

    /** Добавляем новый уровень */
    public function addLevel($url = '', $title = '', $active = 1, $insertTo = null)
    {

        $nowLevel = $this->getCountLevel();

        /** Обрабатываем запрос на добавление до или после указанного уровня,
         * в результате получаем измененный $nowLevel с ключом для добавления **/
        if (is_array($insertTo) && count($insertTo) > 1)
        {

            list($where, $key) = $insertTo;

            if ($key < 0 || empty($this->levels[$key]))
            {
                exit(__CLASS__ . ': invalid key');
            }

            switch ($where)
            {

                case 'before':
                    $nowLevel = $key;
                    break;

                case 'after':
                    $nowLevel = ++$key;
                    break;

                default:
                    exit(__CLASS__ . ': only before or after');
            }

            $this->levels = array_merge(
                array_slice($this->levels, 0, $nowLevel),
                array(0), /** сюда запишется новый уровень */
                array_slice($this->levels, $nowLevel)
            );
        }

        $this->levels[$nowLevel] = new BreadCrumbsLevel();
        $this->level($nowLevel)->setUrl($url);
        $this->level($nowLevel)->setTitle($title);
        $this->level($nowLevel)->setActive($active);
    }

    public function fastGet($id, $again = false)
    {
        global $fastGetArray;

        if ($again == false && isset($fastGetArray))
        {

            unset($fastGetArray);
        }

        $item = $this->_db->getRow('SELECT * FROM `'.PRFX.'www` WHERE `path_id` = ' . $id);

        if (count($item))
        {

            $fastGetArray[$item['path_id']] = $item;
        }

        if ($item['parent'] > 1)
        {

            return $this->fastGet($item['parent'], true);
        }

        if (is_array($fastGetArray))
        {

            $fastGetArray = array_reverse($fastGetArray, true);

            foreach ($fastGetArray as $path_id => $item)
            {

                if ($path_id == 1)
                {

                    continue;
                }

                $this->addLevel(
                    '/' . trim(path($path_id), '/') . '/',
                    $item['title_menu'],
                    empty($item['notfound']) ? 1 : 0
                );
            }
        }
    }

    /** Строим HTML-код для вывода клиенту */
    public function render()
    {

        $crumbs = array();

        /**
         * @var BreadCrumbsLevel $obj
         */
        foreach ($this->levels as $key => $obj)
        {
	        if(2 == $key && preg_match('~[0-9]+ кВт~ui',$obj->getTitle())) {
		        $obj->setTitle('Дизельные генераторы '.$obj->getTitle());
	        }

			if(!empty($this->alter) && 2 == $key) {
				$obj->setTitle($this->alter);
			}


            if (!$this->lastActive && $this->getLastKey() == $key)
            {
                $obj->setActive(0);
            }

            if ($obj->getActive()) {
                $crumbs[$key] = '<div itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
                    <a itemprop="item" title="' . $obj->getTitle() . '" href="' . $obj->getUrl() . '">
                                <span itemprop="name">' . $obj->getTitle() . '</span>
                                <meta itemprop="position" content="' . ($key + 1) . '">
                            </a>
                    </div>';
            } else {
                $crumbs[$key] = '<div itemscope="" itemprop="itemListElement" itemtype="http://schema.org/ListItem">
					<a class="inactive" itemprop="item" title="' . $obj->getTitle() . '" href="#" onclick="return false">
                        <span itemprop="name">' . $obj->getTitle() . '</span>
                        <meta itemprop="position" content="' . ($key + 1) . '">
                    </a>
                    </div>';
            }
        }

        return '<ul>' . implode($crumbs, $this->separator) . '</ul>';
    }

    /** Удаляем уровень с указанным ключом */
    public function remove($levelKey)
    {

        unset($this->levels[$levelKey]);

        $this->levels = array_values($this->levels);
    }

    /** Получаем количество уровней */
    private function getCountLevel()
    {

        return count($this->levels);
    }

    /** Получаем ключь последнего уровня */
    private function getLastKey()
    {

        return $this->getCountLevel() - 1;
    }
}

/**
 * Class BreadCrumbsLevel
 *
 * @package Core.system
 */
class BreadCrumbsLevel
{

    private $url, $title, $active;

    public function getUrl()
    {

        return $this->url;
    }

    public function setUrl($value)
    {

        $this->url = $value;
    }

    public function getTitle()
    {

        return $this->title;
    }

    public function setTitle($value)
    {

        $this->title = $value;
    }

    public function getActive()
    {

        return $this->active;
    }

    public function setActive($value = 1)
    {

        $this->active = $value;
    }
}