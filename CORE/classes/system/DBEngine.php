<?php

/**
 * Class DBEngine
 * @package Core.system
 */
class DBEngine {

    public $configParams = array();
    public $afterSqls = array();

    /**
     * Заполняется всеми выполненными запросами в течение генерации страницы.
     * @var array
     */
    public $sqls = array();

    /**
     * ID элемента, полученный в результате выполнения последней команды INSERT
     * в таблицу с autoincrement'ным полем.
     * @var integer
     */
    public $id = 0;

	public $indexField = null;

    /**
     * Кол-во строк вернувшееся запросом
     * @var integer
     */
    public $rowsNumber = 0;

    /**
     * Время выполения последнего запроса.
     * @var float
     */
    public $lastQueryTime = 0;

    /**
     * Суммарное время выполнения всех запросов, со времени начала генерации страницы.
     * @var float
     */
    public $allQueriesTime = 0;

    /**
     * Текущее соединение с базой
     * @var
     */
    private $_connection;


    /**
     *  Конструктор класса.
     *
     * @param string $type Тип BD (в настоящее время только mysql)
     * @param string $server Host сервера (напр., localhost)
     * @param string $user Имя пользователя (напр., root)
     * @param string $pass Пароль пользователя (может быть пустым)
     * @param string $dbname Имя базы данных (напр., invictum )
     *
     * @exception Если неизвестный тип BD, то выдает ошибку
     */
    public function __construct($type, $server, $user, $pass, $dbname) {
        $this->type = $type; //mysql
        switch ($this->type) {
            case 'mysql' :
                $this->configParams = array('host' => $server, 'user' => $user, 'pass' => $pass, 'dbname' => $dbname);
                $this->connectMySQL($server, $user, $pass, $dbname);
                break;
            default :
                die('Unknow DB: Change type of DB!');
                break;
        }

        /** Файл, с которого вызван текущий запрос */
        $this->file = null;

        /** Строка, с которой вызван текущий запрос */
        $this->line = null;

        /** Текущий запрос */
        $this->sql = null;

        /** Текущая операция */
        $this->operation = null;

        /** Выводить ли ошибку, если текущий запрос ошибочен */
        $this->showErrors = true;
    }

    /**
     * Подключение к MySQL-серверу.
     *
     * @param string $server Host сервера (напр., localhost)
     * @param string $user Имя пользователя (напр., root)
     * @param string $pass Пароль пользователя (может быть пустым)
     * @param string $dbname Имя базы данных (напр., invictum )
     * @exception При невозможности присоединения выдает ошибку
     * @return void
     */
    private function connectMySQL($server, $user, $pass, $dbname) {
        $this->_connection = mysqli_connect($server, $user, $pass);
        if ($this->_connection === false) {
            die('Can\'t connect to DB. Server. ' . $server . ', user: ' . $user);
        }
        if (!mysqli_select_db($this->_connection, $dbname)) {
            die('Can\'t select DB ' . $dbname . ' ');
        }
        mysqli_query($this->_connection, 'SET NAMES utf8');
    }

    /**
     *  Распределяет выполнение запроса на необходимый тип БД
     *  Заполняет $this->{@link sqls},  $this->{@link time_query},
     *  $this->{@link AllTimeQueries},  $this->{@link id},
     *   и, при необходимости, логирует запросы.
     *  Если затребовано, то при некорректном запросе генерирует критическую ошибку.
     *
     * @param bool $log
     * @internal param \TYPE $variable Распределяет выполнение запроса на необходимый тип БД
     * @return mixed
     */
    private function operation($log = true) {
        if ($this->sql === null) {
            return;
        }

        $this->sql = (string)@ trim($this->sql);
        $this->lastQueryTime = $this->_mctime();

        switch ($this->type) {
            case 'mysql' :
                $result = $this->MySql($log);
                break;
            default :
                $result = false;
                break;
        }

        if ($result !== false) {
            $this->lastQueryTime = round($this->_mctime() - $this->lastQueryTime, 4);
            $this->allQueriesTime += $this->lastQueryTime;

            if (MS && MS_DEBUG) {
                $cur = &$this->sqls[];
                $cur['operation'] = $this->operation;
                $cur['file'] = $this->file;
                $cur['line'] = $this->line;
                $cur['query'] = $this->sql;
                $cur['time'] = $this->lastQueryTime;
                $cur['result'] = sizeof($result);
            }

            unset ($cur);

            $this->id = mysqli_insert_id($this->_connection);
            $this->_clear();

            return $result;
        }

        if ($this->showErrors) {
            $error = mysqli_error($this->_connection);
            $filter = array();
            if (preg_match('#^[^\']*? \'(.*?)\' in#', $error, $filter)) {
                $this->sql = str_replace($filter[1], '<b>' . $filter[1] . '</b>', $this->sql);
            }
            $str = '<B>Ошибка DB:</B> <BR><BR>' . $error . ' <BR><BR> ' . $this->sql . '<br><br>' . __FILE__ . '';

            if (function_exists('critical_error')) {
                critical_error($str, $this->file, $this->line);
            } else {
                die($str);
            }
        }

        return false;
    }

    /**
     * Выполнение запроса на MySQL сервере.
     * @param bool $log
     * @return mixed Результат запроса
     */
    private function MySql($log = true) {
        if ($this->sql === null) {
            return;
        }

        $query = mysqli_query($this->_connection, $this->sql);
        if ($query === false) {
            return false;
        }

        switch ($this->operation) {
            case 'getOne' :
                $this->rowsNumber = mysqli_num_rows($query);
                $result = mysqli_fetch_assoc($query);
                if ($result === false) {
                    $result = array();
                }
                $result = (sizeof($result) != 0) ? current($result) : '';
                break;

            case 'getRow' :
                $this->rowsNumber = mysqli_num_rows($query);
                $result = mysqli_fetch_assoc($query);
                if ($result === false) {
                    $result = array();
                }
                break;

            case 'getAll' :
                $this->rowsNumber = mysqli_num_rows($query);
                $result = array();
                while ($row = mysqli_fetch_assoc($query)) {
					if ($this->indexField && isset($row[$this->indexField])) {
						$result[$row[$this->indexField]] = $row;
					} else {
						$result[] = $row;
					}
                }
                break;

            case 'getCol' :
                $this->rowsNumber = mysqli_num_rows($query);
                $result = array();
                while ($row = mysqli_fetch_assoc($query)) {
	                if ($this->indexField && isset($row[$this->indexField])) {
		                $result[$row[$this->indexField]] = current($row);
	                } else {
		                $result[] = current($row);
	                }
                }
                break;

            case 'insert':
                $result = mysqli_affected_rows($this->_connection);
                break;

            case 'update':
                $result = mysqli_affected_rows($this->_connection);
                break;

            case 'delete':
                $result = mysqli_affected_rows($this->_connection);
                break;

            case 'replace':
                $result = mysqli_affected_rows($this->_connection);
                break;

            case 'execute' :
                return true;
        }

        return $result;
    }

    /**
     * Подготовить переменную к вносу в БД (квотирование и слеширование).
     * @param $string
     * @internal param string $string Var
     * @return string slashedVar
     */
    public function prepare($string) {
        $filteredString = trim($string);

        if (isset($string[0]) && $string[0] == ' ') {
            $filteredString = ' ' . $filteredString;
        }

        if (isset($string[count($string) - 1]) && $string[count($string) - 1] == ' ') {
            $filteredString = $filteredString . ' ';
        }
        return mysqli_real_escape_string($this->_connection, $filteredString);
    }

    /**
     * Alias функции {@link $this->prepare()}
     * @param $string
     * @return string slashedVar
     */
    public function pre($string) {
        return $this->prepare($string);
    }

    /**
     * Возвращает предыдущий выполненный запрос.
     * Или указаную инфрмацию по предыдущему запросу.
     *
     * @param string $field
     * @internal param \file|\line|\operation|\query|\result|\time $string Допустимые поля
     * @return string
     */
    public function lastQuery($field = 'query') {
        $last = end($this->sqls);
        return $last[$field];
    }

    /**
     * Проверяет существует ли таблица.
     * @param string $table Имя таблицы
     * @param boolean $ShowError Генерировать ошибку при неверном запросе
     * @return boolean
     */
    public function tableExists($table, $ShowError = true) {
        return (boolean)sizeof((array)$this->operation('getAll', "SHOW TABLE STATUS LIKE '" . $this->pre($table) . "'", $ShowError));
    }

    /**
     * Выбрать только одно значение.
     * @param string $sql SQL-запрос
     * @param boolean $ShowError Генерировать ошибку при неверном запросе
     * @return mixed
     */
    public function getOne($sql, $ShowError = true) {
        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'getOne';
        $this->showErrors = (bool)$ShowError;
        return $this->operation();
    }


    /**
     * Выбрать строку.
     * @param string $sql SQL-запрос
     * @param boolean $ShowError Генерировать ошибку при неверном запросе
     * @return mixed
     */
    public function getRow($sql, $ShowError = true) {
        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'getRow';
        $this->showErrors = (bool)$ShowError;

        return $this->operation();
    }

    /**
     * Выбрать столбец.
     * @param string $sql SQL-запрос
     * @param boolean $ShowError Генерировать ошибку при неверном запросе
     * @return mixed
     */
    public function getCol($sql, $ShowError = true) {
        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'getCol';
        $this->showErrors = (bool)$ShowError;

        return $this->operation();
    }

    /**
     * Выбрать ассоциированный массив.
     * @param string $sql SQL-запрос
     * @param boolean $ShowError Генерировать ошибку при неверном запросе
     * @return mixed
     */
    public function getAll($sql, $ShowError = true) {
        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'getAll';
        $this->showErrors = (bool)$ShowError;

        return $this->operation();
    }

    /**
     * Обернуть значение для вставки "Как есть"
     * @param $string
     * @return stdClass
     */
    static public function expr($string) {
        $obj = new stdClass();
        $obj->value = $string;
        return $obj;
    }

    /**
     * Вставка строки в базу
     * @param $table
     * @param array $items
     * @param bool|true $ShowError
     * @return bool|mixed
     */
    public function insert($table, array $items, $ShowError = true) {
        if (empty($items)) return false;

        // Generate sql
        $key = key($items);
        if (is_array($items[$key])) {
            $columns = array_keys($items[0]);
        } else {
            $columns = array_keys($items);
        }

        $columns = array_map(
            function ($column) {
                return '`' . $column . '`';
            },
            $columns
        );

        // Prepare values
        $entities = array();
        $self = clone $this;
        if (is_array($items[$key])) {
            foreach ($items as $_item) {
                $values = array_map(
                    function ($element) use ($self) {
                        if ($element instanceof stdClass) {
                            $element = $element->value;
                        } else {
                            $element = "'".$self->pre($element)."'";
                        }
                        return $element;
                    },
                    array_values($_item)
                );
                $entities[] = '(' . implode(',', $values) . ')';
                unset($values);
            }
        } else {
            $values = array_map(
                function ($element) use ($self) {
                    if ($element instanceof stdClass) {
                        $element = $element->value;
                    } else {
                        $element = "'".$self->pre($element)."'";
                    }
                    return $element;
                },
                array_values($items)
            );
            $entities[] = '(' . implode(',', $values) . ')';
            unset($values);
        }
        unset($self);

        $sql = 'INSERT INTO ' . $table . ' (' . implode(', ', $columns) . ') VALUES ' . implode(', ', $entities);
        unset($columns);
        unset($entities);

        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'insert';
        $this->showErrors = (bool)$ShowError;
        return $this->operation();
    }

    /**
     * Замена строки в базе
     * @param $table
     * @param array $items
     * @param bool|true $ShowError
     * @return bool|mixed
     */
    public function replace($table, array $items, $ShowError = true) {
        if (empty($items)) return false;

        // Generate sql
        $key = key($items);
        if (is_array($items[$key])) {
            $columns = array_keys($items[0]);
        } else {
            $columns = array_keys($items);
        }

        $columns = array_map(
            function ($column) {
                return '`' . $column . '`';
            },
            $columns
        );

        // Prepare values
        $entities = array();
        $self = clone $this;
        if (is_array($items[$key])) {
            foreach ($items as $_item) {
                $values = array_map(
                    function ($element) use ($self) {
                        if ($element instanceof stdClass) {
                            $element = $element->value;
                        } else {
                            $element = "'".$self->pre($element)."'";
                        }
                        return $element;
                    },
                    array_values($_item)
                );
                $entities[] = '(' . implode(',', $values) . ')';
                unset($values);
            }
        } else {
            $values = array_map(
                function ($element) use ($self) {
                    if ($element instanceof stdClass) {
                        $element = $element->value;
                    } else {
                        $element = "'".$self->pre($element)."'";
                    }
                    return $element;
                },
                array_values($items)
            );
            $entities[] = '(' . implode(',', $values) . ')';
            unset($values);
        }
        unset($self);

        $sql = 'REPLACE INTO ' . $table . ' (' . implode(', ', $columns) . ') VALUES ' . implode(', ', $entities);
        unset($columns);
        unset($entities);

        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'replace';
        $this->showErrors = (bool)$ShowError;
        return $this->operation();
    }

    /**
     * Обновление строки в базе
     * @param $table
     * @param array $result
     * @param string $where
     * @param bool|true $ShowError
     * @return bool|mixed
     */
    public function update($table, $result = array(), $where = '', $ShowError = true) {
        if (empty($result)) return false;
        if (is_array($where)) $where = implode(' AND ', $where);

        // Generate sql
        $condition = array();
        foreach ($result as $_column => $_value) {
            if ($_value instanceof stdClass) {
                $_value = $_value->value;
            } else {
                $_value = "'".$this->pre($_value)."'";
            }
            $condition[] = '`' . $_column . '` = ' . $_value;
        }

        $sql = 'UPDATE `' . trim($table, '`') . '` SET ' . implode(', ', $condition);
        if ($where) {
            $sql .= ' WHERE ' . $where;
        }

        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'update';
        $this->showErrors = (bool)$ShowError;
        return $this->operation();
    }

    /**
     * Удаление строки из базы
     * @param $table
     * @param string $where
     * @param bool|true $ShowError
     * @return mixed
     */
    public function delete($table, $where = '', $ShowError = true) {
        if (is_array($where)) $where = implode(' AND ', $where);
        $sql = 'DELETE FROM `' . trim($table, '`') . '`';
        if ($where) $sql .= ' WHERE ' . $where;

        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'update';
        $this->showErrors = (bool)$ShowError;
        return $this->operation();
    }

    /**
     * Выполнить запрос.
     * @param string $sql SQL-запрос
     * @param boolean $ShowError Генерировать ошибку при неверном запросе
     * @param bool $log
     * @return boolean Результат операции
     */
    public function execute($sql, $ShowError = true, $log = true) {
        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = $sql;
        $this->operation = 'execute';
        $this->showErrors = (bool)$ShowError;

        return $this->operation($log);
    }

    /**
     * Возвращает следующий номер автоинкрементного id в указанной таблице
     * @param string $tablename Имя таблицы (напр., ".PRFX."files)
     * @return integer
     */
    public function nextID($tablename) {
        list ($this->file, $this->line) = LastFileLine(1);
        $this->sql = "SHOW TABLE STATUS LIKE '" . $tablename . "'";
        $this->operation = 'getRow';
        $this->showErrors = true;
        $infotable = $this->operation();

        return (integer)@ $infotable['Auto_increment'];
    }

    /**
     * Возвращает значение временных переменных в исходное состояние
     * @return void
     */
    private function _clear() {
        $this->file = null;
        $this->line = null;
        $this->sql = null;
        $this->operation = null;
        $this->showErrors = true;
	    $this->indexField = null;
    }

    /**
     * Возвращает текущее число секунд и микросекунд.
     * @return float
     */
    private function _mctime() {
        list ($sec, $msec) = explode(' ', microtime());
        return $sec + $msec;
    }

    public function importModuleTable($table, $module_name) {
        global $CONFIG;
        $directory = MODULES_DIR . DS . $module_name . '/sqls';
        $file = $directory . '/' . $table . '.sql';

        if (is_file($file)) {
            $sqls = file($file);

            foreach ($sqls as $line) {
                $line = trim($line);
                if ($line == "") {
                    continue;
                }
                $this->execute($line);
            }
            unset($sqls);
            return true;
        } else {
            return false;
        }
    }

    public function createModuleTableMakeFields($name, $type, $default = '', $dbtype = '') {
        if (empty($dbtype)) {
            switch ($type) {
                case 'divider':
                case 'section':
                    return;

                case 'boolean':
                case 'checkbox':
                    return "`" . $name . "` TINYINT(1) NOT NULL DEFAULT '" . $default . "'";
                    break;

                case 'double':
                    return "`" . $name . "` DOUBLE(10,5) NOT NULL DEFAULT '" . ($default ?: 0) . "'";
                    break;

                case 'integer':
                    return "`" . $name . "` INT(11) NOT NULL DEFAULT '" . ($default ?: 0) . "'";
                    break;

                case 'price':
                case 'file':
                case 'pass':
                case 'password':
                case 'select':
                case 'static':
                case 'string':
                case 'color':
                    return "`" . $name . "` VARCHAR(255) NOT NULL DEFAULT '" . $default . "'";
                    break;

                case 'wysiwyg':
                case 'options':
                case 'list':
                case 'geocoder':
                case 'autocomplete':
                case 'phone':
                case '':
                case 'memo':
                case 'loader':
                case 'group_checkbox':
                case 'popup':
                case 'group_radio':
                    return "`" . $name . "` TEXT NOT NULL";
                    break;

                case 'calendar':
                    return "`" . $name . "` DATETIME " . ($default != '' ? "'" . $default . "'" : "");
                    break;

                case 'order':
                case 'int':
                    return "`" . $name . "` INT(11) NOT NULL DEFAULT '" . $default . "'";
                    break;

                case 'code':
                    $this->afterSqls[] = 'ALTER TABLE  `' . $this->tabName . '` ADD UNIQUE  `uniqCode` (  `path_id` , ' . (isset($this->moduleType) && $this->moduleType == 'catalog' ? '`parent`,' : '') . ' `' . $name . '` )';
                    return '`' . $name . '` VARCHAR(200) NULL';
                    break;

                default:
                    return '`' . $name . '` VARCHAR(255) NOT NULL';

            }
        } else {
            return "`" . $name . "` " . $dbtype . " NOT NULL " . ($default != '' ? "DEFAULT '" . $default . "'" : "");
        }
    }

    public function createModuleTable($config, $additionalFields = array()) {

        $this->tabName = PRFX . $config['db_name'];
        $sql_fields = array();
        $sql_fields[] = "`id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY";
        $sql_fields[] = "`path_id` INT NOT NULL";

        if (isset($config['config']['id'])) {
            unset($config['config']['id']);
        }

        if (isset($this->moduleType)) {

            if ($this->moduleType == 'catalog') {
                $sql_fields[] = "`parent` INT NOT NULL";
            }

            $sql_fields[] = "`order` INT NOT NULL";

        }

        foreach ($config['config'] as $name_field => $field) {
            if (is_numeric($name_field)) continue;

            $value = isset($field['value']) ? $field['value'] : '';
            $dbtype = isset($field['dbtype']) ? $field['dbtype'] : '';
            $result = $this->createModuleTableMakeFields($name_field, $field['type'], $value, $dbtype);
            if ($result) {
                $sql_fields[] = $result;
            }
        }

        $sql_fields = array_merge($sql_fields, $additionalFields);
        $sql = " CREATE TABLE IF NOT EXISTS `" . $this->tabName . "` (" . implode(', ', $sql_fields) . ") ENGINE = MYISAM ";
        $this->execute($sql);

        if (count($this->afterSqls)) {
            foreach ($this->afterSqls as $k => $q) {
                $this->execute($q);
                unset($this->afterSqls[$k]);

            }
        }
    }

    public function setIndexField($value) {
    	$this->indexField = $value;
    	return $this;
    }
}