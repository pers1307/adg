<?php


    /**
     * Класс построения форм конфигураций.
     * Файл подключается на стадии генерации
     * административной страницы, всегда.
     * @package Core.system
     */

    /**
     *  Класс используется для построения административного меню в файлах menu.inc.
     *  Класс доступен модулям только в административном разделе.
     *  для работы класса используются {@link $modules}.
     *
     * @access public
     */
    class FormConfigs
    {
        const FORM_ID_TEMPLATE = 5001;
        const FORM_ID_PARENT = 5002;
        const FORM_ID_SAVETPL = 5003;
        const FORM_ID_REDIRECT = 5004;
        const FORM_ID_PAGEPATH_FORM = 5005;
        const FORM_ID_TDK_FORM = 5006;
        /**
         *  Директори расположения конфигураторов.
         * @access public
         * @var string
         */
        var $directory;

        /**
         *  Массив информации о конфигураторах.
         * @access public
         * @var array
         */
        var $List;

        static $months1 = array(1 => 'Январь', 2 => 'Февраль', 3 => 'Март', 4 => 'Апрель', 5 => 'Май', 6 => 'Июнь', 7 => 'Июль', 8 => 'Август', 9 => 'Сентябрь', 10 => 'Октябрь', 11 => 'Ноябрь', 12 => 'Декабрь');
        static $months2 = array(1 => 'января', 2 => 'февраля', 3 => 'марта', 4 => 'апреля', 5 => 'мая', 6 => 'июня', 7 => 'июля', 8 => 'августа', 9 => 'сентября', 10 => 'октября', 11 => 'ноября', 12 => 'декабря');

        /**
         *  Конструктор.
         * @access private
         * @return void
         */
        function __construct()
        {

            /** Инкрементируемый ключ формы */
            $this->form = 1;

            /** Инкрементируемый ключ конфигураторов */
            $this->key = 1;

            /** Директория конфигураторов */
            $this->directory = CORE_DIR . '/forms/';

            /** Массив информации о конфигураторах */
            $this->configs = array();

            $this->_parse();
        }

        /**
         *  Возвращает информацию по конфигураторам
         * @access public
         * @return array
         */
        function ListConfigs()
        {

            $this->_parse();

            return $this->configs;
        }

        /**
         *  Строит HTML-код конфигураций
         *
         * @access public
         *
         * @param array $config Массив конфигураций
         *
         * @param string $template
         * @param $num
         *
         * @return HTML
         */
        function make($config, $template = '', $num = -1)
        {

            if ((int)$num != -1)
            {
                $this->form = (int)$num;
            }

            $list = $this->ListConfigs();

            $params['configurations'] = array();

            foreach ($config as $config_name => $data)
            {


                $type = $this->_checkType($config_name, $data);

                if (!$type || ($type !== 'caption' && !isset ($list[$type])))
                {
                    continue;
                }


                $cur = & $params['configurations'][];
                $cur['caption'] = isset ($data['caption']) && $data['caption'] ? ((isset($data['oblig']) && $data['oblig'] == 1) ? $data['caption'] = '*' . $data['caption'] : $data['caption']) : '';
                $cur['type'] = $type;

                if ($type !== 'caption')
                {
                    $cur['html_value'] = $this->_content($config_name, $data, $type);
                }

                $this->key++;
            }

            $this->form++;

            return ($template) ? template($template, $params) : template('configuration', $params);
        }

        /**
         *  Сохранение изменений в конфигурационном массиве.
         * @access public
         *
         * @param array $config Массив конфигураций
         *
         * @return array Измененный массив.
         */
        function save($config, $automat = false)
        {
            $list = $this->ListConfigs();
            $in = [];

            foreach ($config as $config_name => $data)
            {

                $type = $this->_checkType($config_name, $data);

                if (!$type || !isset ($list[$type]) || !is_file($this->directory . EndSlash($data['type'], DS) . 'save.php'))
                {
                    unset ($config[$config_name]);
                    continue;
                }

                if ($type !== 'caption')
                {
                    $config[$config_name] = $this->_changes($config_name, $data, $type);

                    if (is_numeric($config_name) && isset($data['in'])) {
                        $in[$data['in']][$data['name']] = $config[$config_name]['value'];

                        if (empty($config[$config_name]['value'])) {
                            unset($in[$data['in']][$data['name']]);
                        }
                    }

                    if ($config[$config_name]['type'] == 'password' && $config[$config_name]['value'] == "")
                    {
                        unset($config[$config_name]);
                        continue;
                    }

                    if($config[$config_name]['type'] == 'static')
                    {
                        unset($config[$config_name]['value']);
                    }
                }
            }

            if (!empty($in)) {
                foreach ($in as $key => $value) {
                    $config[$key]['value'] = empty($value) ? null : json_encode($value);
                }
                unset($in);
            }

            if ($automat === true)
            {
            }
            /**
             * Тут вставить сохранение автоматическое...
             * Обязательные параметры table - таблица для запроса, id - записи остальные поля считаются относящимися к обновлению
             */
            else
            {
                return $config;
            }
        }

        /**
         *  Производит изменения с получеными из формы данными для конфигуратора.
         * @access private
         *
         * @param string $config_name Системное имя конфигуратора
         * @param array $data Данные по конфигуратору
         * @param string $type Тип конфигуратора
         *
         * @return string
         */
        function _changes($config_name, $data, $type)
        {
            static $array_name = null;

            if ($array_name === null)
            {
                $array_name = request('conf', array());
                if (!sizeof($array_name) || !intval(key($array_name)))
                {
                    die('Конфигурационный массив небыл передан браузером!');
                }

                $array_name = $array_name[key($array_name)];
            }

            $result = isset($array_name[$config_name]) ? $array_name[$config_name] : '';

            extract($data);
            $value = isset ($value) ? $value : '';
            $values = isset ($values) ? $values : array();

            if (is_file($this->directory . EndSlash($type, DS) . 'save.php'))
            {
                include $this->directory . EndSlash($type, DS) . 'save.php';
            }

            unset($config_name);

            return $data;
        }

        /**
         *  Возвращает HTML-код конфигуратора.
         *
         * @access private
         *
         * @param string $config_name Системное имя конфигуратора
         * @param array $data Данные по конфигуратору
         * @param string $type Тип конфигуратора
         *
         * @return string
         */

        function _content($config_name, $data, $type)
        {
            $sysname = 'conf[' . $this->form . '][' . $config_name . ']';
            extract($data);
            $value = isset ($value) ? $value : '';
            $values = isset ($values) ? $values : array();
            $key = $this->key;

            return include $this->directory . EndSlash($type, DS) . 'echo.php';
        }

        /**
         *  Возвращает тип конфигуратора
         * @access private
         *
         * @param string $config_name Системное имя конфигуратора
         * @param array $data Данные по конфигуратору
         *
         * @return false|string Тип конфигуратора
         */
        function _checkType($config_name, $data)
        {

            if (isset ($data['type']))
            {
                return (string)$data['type'];
            }

            if (!isset ($data['value']))
            {
                return 'caption';
            }

            if (is_bool($data['value']))
            {
                return 'boolean';
            }

            if (is_int($data['value']))
            {
                return 'integer';
            }

            if (is_float($data['value']))
            {
                return 'float';
            }

            if (is_string($data['value']))
            {
                return 'string';
            }

            if (isset ($data['values']))
            {
                return 'select';
            }

            return false;
        }

        /**
         *  Разбирает существующие конфигураторы
         * @access private
         * @return void
         */
        function _parse()
        {
            static $already = false;

            if ($already)
            {
                return;
            }

            $already = true;

            $dirs = GetDirsFromDir($this->directory);
            foreach ($dirs as $dir)
            {

                $cur_dir = $this->directory . EndSlash($dir, DS);

                $about_file = $cur_dir . 'about';
                if (!is_file($about_file))
                {
                    continue;
                }

                $data = parse_ini_file($about_file);

                $cur = & $this->configs[$dir];


                $cur['Name'] = !isset ($data['Name']) || !$data['Name'] ? 'Безымянный конфигуратор' : $data['Name'];
                $cur['About'] = !isset ($data['About']) ? '' : $data['About'];
                $cur['Hidden'] = isset ($data['Hidden']) && $data['Hidden'];
                $cur['Memo'] = isset ($data['Memo']) && $data['Memo'];
            }
        }


        function writeModuleItemTdParams($config)
        {
            $file = $this->directory . $config['type'] . '/config.php';
            if (is_file($file))
            {
                include($file);

                $conf_params = array();
                $conf_params[] = (isset($conf_align) && $conf_align != "" ? ' align="' . $conf_align . '" ' : '');
                $conf_params[] = (isset($conf_width) && $conf_width != "" ? ' width="' . $conf_width . '" ' : '');

                return implode(' ', $conf_params);

                unset($conf_align);
                unset($conf_width);
            }
            else
            {
                return '';
            }
        }

        function writeModule_LinksPath()
        {
            ob_start();

            if (isset($_SESSION['link_path']))
            {
                $array = $_SESSION['link_path'];

                $outs = array();

                foreach ($array as $num => $val)
                {
                    $outs[] = '<span><b>' . MSCore::modules()->info[$val['back2module']]['module_caption'] . '</b></span>';
                }

                reset($array);
                $link_path = array_pop($array);

                if (is_array($link_path))
                {
                    $outs[] = '<span><b>' . MSCore::modules()->info[$link_path['inmodule']]['module_caption'] . '</b></span>';
                }

                if (sizeof($outs))
                {
                    echo '<button class="grey backButton" onclick="doLoad(\'\', \'/' . ROOT_PLACE . '/admin/back2module/' . $link_path['back2module'] . '/' . $link_path['inmodule'] . '/\', \'fast_table_' . $link_path['inmodule'] . '_linked\', \'post\', \'rewrite\', callback_returnBackModule)">Назад</button>';
                }
            }

            return ob_get_clean();
        }

        function writeModuleTableStart()
        {
            global $CONFIG;

            return '
        <div class="scrollableTable baron-scroll">
			<form id="filter_' . $CONFIG['module_name'] . '" action="#" method="post" enctype="multipart/form-data">
			<table>';
        }

        function writeModuleTableEnd($bottom_content)
        {
            global $CONFIG, $cur_config;

            $out = '
				</table>
				' . ((isset($bottom_content) && $bottom_content != "") ? '
					<div class="module_pager_cont">
						' . $bottom_content . '
					</div>				
				' : '') . '
			</form></div>
			';

            if (isset($cur_config) && isset($cur_config['import']) && $cur_config['import'] == true)
            {
                $out .= '<br><div><a style="float: left" onclick="displayMessage(\'/' . ROOT_PLACE . '/admin/import_csv/' . $CONFIG['module_name'] . '/' . $cur_config['db_name'] . '/' . $cur_config['path_id'] . '/\', 340,200)" href="#" class="button">Импортировать из CSV</a></div>';
            }

            return $out;
        }

        function moduleTableHeaderCell($config, $params1, $params2)
        {
            global $CONFIG, $path_id, $page, $output_id, $isorder, $articles_isorder, $items_isorder, $brands_isorder;


            $mod_name = $CONFIG['module_name'];

            if (isset($config['ignore_field']) && $config['ignore_field'] == 1)
            {
                return;
            }

            if (isset($config['in_list']) && $config['in_list'] == 1)
            {
                $conf_width = '';
                $conf_align = '';

                $file_config = CORE_DIR . '/forms/' . $config['type'] . '/config.php';
                if (is_file($file_config))
                {
                    include($file_config);
                }

                return '<th ' . ($conf_align != "" ? 'align="' . $conf_align . '"' : '') . ' ' . (isset($td['width']) ? 'width="' . $td['width'] . '"' : '') . '>' . $config['caption'] . '</th>';
            }
        }

        function writeModuleTableHeader($config_table, $use_control_bar = true)
        {
            global $CONFIG, $path_id, $page, $output_id, $cur_config, $parent;

            if (!isset($path_id))
            {
                $path_id = 0;
            }
            if (!isset($page))
            {
                $page = 0;
            }
            if ($page == 0)
            {
                $page = 1;
            }

            $cur_config = $config_table;
            $cur_config['path_id'] = $path_id;

            $out_filters = array();

            /* Рисуем заголовки таблицы */
            $num_f_c = 0;
            $num_h_c = 0;

            $title_th_cells = array();
            foreach ($config_table['config'] as $field => $td)
            {
                if ($field == 'order' && (!AUTH_IS_ROOT && !auth_Access($CONFIG['module_name'], 'move')))
                {
                    continue;
                }

                $th = $this->moduleTableHeaderCell($td, '', '');

                if ($th != "")
                {
                    $title_th_cells[] = $th;
                    $num_h_c++;

                    /* Генерация заголовков таблицы с фильтрами */
                    $filter_content = '';
                    if (isset($td['filter']) && $td['filter'] == 1)
                    {
                        $filter_type = $td['type'];
                        $table_name = $config_table['db_name'];
                        $ftitle = $table_name . '_0_' . $field . '_0_' . $filter_type;
                        $output_id = trim($output_id, "'");
                        $path_to_reload = (int)$path_id . '/' . (int)$page . '/' . (empty($parent) ? 0 : (int)$parent);

                        if (is_file(CORE_DIR . '/forms/' . $filter_type . '/filter.php'))
                        {
                            $out = '';
                            $filters = array();
                            if (isset($_SESSION['filters'][$table_name]) && $_SESSION['filters'][$table_name] != "")
                            {
                                $filters = unserialize($_SESSION['filters'][$table_name]);
                            }

                            include(CORE_DIR . '/forms/' . $filter_type . '/filter.php');

                            $num_f_c++;

                            $filter_content = $out;

                            unset($out);
                        }
                    }
                    else
                    {
                        $filter_content = '';
                    }

                    $out_filters[] = '<th' . (isset($td['width']) ? 'width: ' . $td['width'] . '' : '') . '>' . $filter_content . '</th>';
                    unset($filter_content);
                }
            }


            $del_click = "if (confirm('Сбросить фильтр?')) doLoad('','/" . ROOT_PLACE . "/" . $CONFIG['module_name'] . "/clear_filter/" . (int)$path_id . "/" . (int)$page . "/" . (empty($parent) ? 0 : (int)$parent) . "/','" . $output_id . "')";

            empty($num_f_c) or $out_filters[] = '<th><a href="#" class="remove-filters-link" onclick="' . $del_click . '; return false;"><i class="icon-remove"></i></a></th>';

            /* Дефолтные команды */
            $title_th_cells[] = '<th></th>';

            /* Ссылки для связки */
            if (isset($config_table['links']) && sizeof($config_table['links']))
            {
                $links = 0;
                foreach ($config_table['links'] as $num => $link2mod)
                {
                    if (isset(MSCore::modules()->info[$link2mod['target_module']]))
                    {
                        $links++;
                    }
                }

                if ($links > 0 && (AUTH_IS_ROOT || auth_Access($CONFIG['module_name'], 'link')))
                {
                    $title_th_cells[] = '<th>Ссылка</th>';
                    if (count($out_filters))
                    {
                        $out_filters[] = '<th></th>';
                    }
                }
            }

            $header_cells = '<tr>' . implode("\n", $title_th_cells) . '</tr>';

            return '<thead>' . $header_cells . (sizeof($out_filters) && $num_f_c > 0 ? '<tr id="filterHeaderCells" class="filters-wrap">' . implode("\n", $out_filters) . '</tr>' : '') . '</thead>';
        }

        function moduleTableCell($config_table, $data, $control_bar = '', $ed_act)
        {
            global $CONFIG, $path_id, $page, $output;

            $out = '<tr' . ($ed_act ? ' onDblClick="' . $ed_act . '"' : '') . '>';

            foreach ($config_table['config'] as $field => $td)
            {
                if (isset($td['ignore_field']) && $td['ignore_field'] == 1)
                {
                    continue;
                }

                if (isset($td['in_list']) && $td['in_list'] == 1)
                {
                    if ($field == 'order' && (!AUTH_IS_ROOT && !auth_Access($CONFIG['module_name'], 'move')))
                    {
                        continue;
                    }

                    $td['field_table'] = $field;

                    $out .= '<td ' . $this->execConfField($td['type']) . '>';
                    $out .= $this->writeModuleItemField($td, $data);
                    $out .= '</td>';
                }
            }

            $out .= '<td class="module-controls">';

            empty($data['controls']) or $out .= '<span>' . $data['controls'] . '</span>';

            $out .= '</td>';

            if (isset($config_table['links']) && sizeof($config_table['links']))
            {
                $links = array();
                foreach ($config_table['links'] as $num => $link2mod)
                {
                    if (isset(MSCore::modules()->info[$link2mod['target_module']]))
                    {
                        $imod = MSCore::modules()->info[$link2mod['target_module']];

                        $path_id = isset($data['path_id']) ? (int)$data['path_id'] : 0;

                        $key_field = isset($CONFIG['tables']['items']['key_field']) ? $CONFIG['tables']['items']['key_field'] : 'id';

                        $output = $data['sub_config']['output_id'] != "center" ? 'fast_table_' . $link2mod['target_module'] /*$CONFIG['module_name']*/ : 'center';
                        $links[] = '<a href="#" onclick="doLoad(\'\', \'/' . ROOT_PLACE . '/admin/link2module/' . $link2mod['target_module'] . '/' . $CONFIG['module_name'] . '/' . (int)$data[$key_field] . '/' . $link2mod['target_key'] . '/' . $link2mod['main_key'] . '/' . $path_id . '/' . $output . '/\', \'' . $output . '_linked\', \'\', \'\', callback_link2module); return false;">' . MSCore::modules()->info[$link2mod['target_module']]['module_caption'] . '</a>';
                    }
                }

                if (sizeof($links) && (AUTH_IS_ROOT || auth_Access($CONFIG['module_name'], 'link')))
                {
                    $out .= '<td align="center">';
                    $out .= implode('<br>', $links);
                    $out .= '</td>';
                }
            }

            $out .= '</tr>';

            return $out;
        }

        /**
         *  Выводим значение из БД согласно конфигу
         */
        function writeModuleItemField($config, $item)
        {
            global $CONFIG;

            $file = $this->directory . $config['type'] . '/field.php';

            ob_start();
            if (is_file($file))
            {
                require($file);
            }


            return ob_get_clean();
        }

        function writeQuickEdit($file, $sysname, $value, $i)
        {
            $quick_edit = true;

            return include($file);
        }

        /**
         *  Подгружается конфиг конфигуратора формы для поля при выводе в виде ячейки таблицы
         **/
        function execConfField($type)
        {
            $conf_width = $conf_align = '';

            $file_config = CORE_DIR . '/forms/' . $type . '/config.php';
            if (is_file($file_config))
            {
                require($file_config);
            }

            $s = '' . ($conf_align != "" ? 'align="' . $conf_align . '"' : '') . ' ' . ($conf_width != "" ? 'width="' . $conf_width . '"' : '') . '';

            return $s;
        }
    }