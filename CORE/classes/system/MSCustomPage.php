<?php

class MSCustomPage extends MSPage
{
	/**
	 * Name item of catalog
	 *
	 * @var string
	 */
	public $itemName = '';


	/**
	 * Name category of catalog
	 *
	 * @var string
	 */
	public $categoryName = '';

	public $titleCustomPage = null;
	public $metaCustomDescription = null;
	public $metaCustomKeywords = null;
	public $price = '';

	/**
	 * Титлотимизер
	 */
	public function checkTitle()
	{
		$titleRules = MSCore::db()->getAll($q = 'SELECT `type_insert`, `text`, `url` FROM `' . PRFX . 'titles` WHERE "' . MSCore::urls()->requestUri . '" LIKE  `url` AND `active` = 1 ORDER by `sort` ASC');

		$keyForDelete = [];

		foreach ($titleRules as $key => $titleRule) {
			$resultUrl = str_replace('%', '', $titleRule['url']);

			if ($resultUrl == MSCore::urls()->requestUri) {
				$keyForDelete[] = $key;
			}
		}

		foreach ($keyForDelete as $key) {
			unset($titleRules[$key]);
		}

		if (count($titleRules))
		{
			$matchTitle = preg_match('|<title>(.*)</title>|Uis', $this->_html, $match);

			if ($matchTitle) {
				$r_title = $match[1];

				if ($this->titleCustomPage !== $r_title) {
					$r_title = '';
				}

				foreach ($titleRules as $tr) {
					$r_title = $this->makeChangePageAttributes($r_title, $tr['text'], $r_title, $tr['type_insert']);
				}

				$r_title = $this->replaceSpecialCommand($r_title);

				$this->_html = preg_replace('#<title>(.*)</title>#iUs', '<title>' . trim($r_title) . '</title>', $this->_html);
			}
		}
	}

	public function checkKeyword()
	{
		$keywordsRules = MSCore::db()->GetAll($q = 'SELECT `type_insert`, `text`, `url`  FROM `' . PRFX . 'keywords` WHERE "' . MSCore::urls()->requestUri . '" LIKE  `url` AND `active` = 1 ORDER by `sort` ASC');

		$keyForDelete = [];

		foreach ($keywordsRules as $key => $titleRule) {
			$resultUrl = str_replace('%', '', $titleRule['url']);

			if ($resultUrl == MSCore::urls()->requestUri) {
				$keyForDelete[] = $key;
			}
		}

		foreach ($keyForDelete as $key) {
			unset($keywordsRules[$key]);
		}

		if (count($keywordsRules))
		{
			$matchTitle = preg_match('|<meta name="keywords" content="(.*)">|Uis', $this->_html, $match);

			if ($matchTitle) {
				$r_title = $match[1];

				if ($this->metaCustomKeywords !== $r_title) {
					$r_title = '';
				}

				foreach ($keywordsRules as $tr) {
					$r_title = $this->makeChangePageAttributes($r_title, $tr['text'], $r_title, $tr['type_insert']);
				}

				$r_title = $this->replaceSpecialCommand($r_title);

				$this->_html = preg_replace('#<meta name="keywords" content="(.*)">#iUs', '<meta name="keywords" content="' . trim($r_title) . '">', $this->_html);
			} else {
				$r_title = '';

				foreach ($keywordsRules as $tr) {
					$r_title = $this->makeChangePageAttributes($r_title, $tr['text'], $r_title, $tr['type_insert']);
				}

				$r_title = $this->replaceSpecialCommand($r_title);

				$this->_html = preg_replace('#</head>#iUs', '<meta name="keywords" content="' . trim($r_title) . '"></head>', $this->_html);
			}
		}
	}

	public function checkDescription()
	{
		$descriptionRules = MSCore::db()->GetAll($q = 'SELECT `type_insert`, `text`, `url`  FROM `' . PRFX . 'descriptions` WHERE "' . MSCore::urls()->requestUri . '" LIKE  `url` AND `active` = 1 ORDER by `sort` ASC');

		$keyForDelete = [];

		foreach ($descriptionRules as $key => $titleRule) {
			$resultUrl = str_replace('%', '', $titleRule['url']);

			if ($resultUrl == MSCore::urls()->requestUri) {
				$keyForDelete[] = $key;
			}
		}

		foreach ($keyForDelete as $key) {
			unset($descriptionRules[$key]);
		}

		if (count($descriptionRules))
		{
			$matchTitle = preg_match('|<meta name="description" content="(.*)">|Uis', $this->_html, $match);

			if ($matchTitle) {
				$r_title = $match[1];

				if ($this->metaCustomDescription !== $r_title) {
					$r_title = '';
				}

				foreach ($descriptionRules as $tr) {
					$r_title = $this->makeChangePageAttributes($r_title, $tr['text'], $r_title, $tr['type_insert']);
				}

				$r_title = $this->replaceSpecialCommand($r_title);

				$this->_html = preg_replace('#<meta name="description" content="(.*)">#iUs', '<meta name="description" content="' . trim($r_title) . '">', $this->_html);
			} else {
				$r_title = '';

				foreach ($descriptionRules as $tr) {
					$r_title = $this->makeChangePageAttributes($r_title, $tr['text'], $r_title, $tr['type_insert']);
				}

				$r_title = $this->replaceSpecialCommand($r_title);

				$this->_html = preg_replace('#</head>#iUs', '<meta name="description" content="' . trim($r_title) . '"></head>', $this->_html);
			}
		}
	}

	/**
	 * @param $source
	 * @param $new
	 * @param $result
	 * @param $type
	 *
	 * @return string
	 */
	private function makeChangePageAttributes($source, $new, $result, $type)
	{
		if ($type == 0) {

			return $new;
		} else if ($type == 1) {

			return $new . $source;
		} else if ($type == 2) {

			return $source . $new;
		} else if ($type == 3) {

			return $new . $result;
		} else if ($type == 4) {

			return $result . $new;
		} else if ($type == 5) {

			if (!empty($source)) {
				return $source;
			} else {
				return $new;
			}
		} else {

			return '';
		}
	}

	/**
	 * @param $string
	 *
	 * @return string
	 */
	private function replaceSpecialCommand($string)
	{
		$string = str_replace('%itemname%', $this->itemName, $string);
		$string = str_replace('%itemname|lowerCase%', mb_strtolower($this->itemName), $string);

		$string = str_replace('%categoryname%', $this->categoryName, $string);
		$string = str_replace('%categoryname|lowerCase%', mb_strtolower($this->categoryName), $string);

		$string = str_replace('%header%', $this->header, $string);
		$string = str_replace('%header|lowerCase%', mb_strtolower($this->header), $string);

		$string = str_replace('%price%', $this->price, $string);

		return $string;
	}

	/**
	 * Внедрение в HTML код дополнительных элементов и вывод на экран.
	 *
	 * @return mixed|string
	 */
	public function Append2HTML()
	{

		if($this->getIsRootPlace() === false)
		{
			AddBeforeTag(MSCore::settings()->metrika, '</body>');
		}

		if ($this->meta_description)
		{
			AddBeforeTag('<meta name="description" content="' . htmlentities($this->meta_description, ENT_QUOTES, SITE_ENCODING) . '">', '<title>');
		}

		if ($this->meta_keywords != "")
		{
			AddBeforeTag('<meta name="keywords" content="' . htmlentities($this->meta_keywords, ENT_QUOTES, SITE_ENCODING) . '">', '<title>');
		}

		/** для я.карт **/
		$this->_html = preg_replace_callback('/\[map=([\d]+)\]/i', 'mapInsert', $this->_html);

        /**
         * Galleries
         */
		$this->_html = preg_replace_callback('/\[gallery=([\d]+)\]/i', 'galleryInsert', $this->_html);

		foreach (MSCore::urls()->storeAddBefTag as $tag => $info)
		{
			if ($tag === '')
			{
				continue;
			}

			foreach ($info as $text)
			{

				/** Если есть тег,то вставляем перед ним */
				if (preg_match('#' . _pregQuote($tag) . '#sim', $this->_html))
				{
					$this->_html = preg_replace('#' . _pregQuote($tag) . '#sim', $text . $tag, $this->_html);
				}

			}
		}

		require_once BLOCKS_DIR . DS . 'system' . DS . 'ie-window.php';

		if ($this->path === '404')
		{
			header('HTTP/1.x 404 Not Found');

			//ob_start();
			//debug($this->lastFileLine, 0, 0);
			//$this->_html .= ob_get_clean();
		}

		$this->checkTitle();
		$this->checkKeyword();
		$this->checkDescription();

		header('Content-Type: '. $this->mimeType . '; charset=' . SITE_ENCODING);

		return $this->_html;
	}
}