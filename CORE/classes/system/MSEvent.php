<?php
/**
 * Event handler
 */

class MSEvent {
    /**
     * @var array
     */
    protected static $_events = array();

    /**
     * @param $name
     * @param $callback
     * @param bool $append
     * @return bool
     */
    public static function register($name, $callback, $append = true){
        if(!empty($name) && !empty($callback) && is_callable($callback)) {
            isset(self::$_events[$name]) or self::$_events[$name] = array();
            if($append) {
                self::$_events[$name][] = $callback;
            } else {
                array_unshift(self::$_events[$name], $callback);
            }
            return true;
        }
        return false;
    }

    /**
     * @param $name
     * @param null $callback
     * @return bool
     */
    public static function unregister($name, $callback = null){
        if(isset(self::$_events[$name])) {
            if(is_null($callback)) {
                unset(self::$_events[$name]);
            } else {
                foreach (self::$_events[$name] as $_i =>  $_callback) {
                    if($callback === $_callback) {
                        unset(self::$_events[$name][$_i]);
                    }
                }
            }
            return true;
        }
        return false;
    }

    /**
     * @param $name
     * @param array $data
     * @param bool $inverted
     * @return array
     */
    public static function trigger($name, $data = array(), $inverted = false){
        $result = array();
        if(isset(self::$_events[$name]) && !empty(self::$_events[$name])) {
            $events = $inverted ? array_reverse(self::$_events[$name]) : self::$_events[$name];
            foreach($events as $_event) {
                if (is_callable($_event)) {
                    $output = call_user_func($_event, $data);
                    if($output) {
                        $result[] = $output;
                    }

                }
            }
            unset($events);
        }
        return $result;
    }
}