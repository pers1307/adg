<?php
/**
 * @package Core.system
 */

class MSFiles
{
    static
        $dataKeyName = 'loader',
        $Imagick = null,
        $ImageToolbox = null;
    private
        $db,
        $data;

    /**
     *
     */
    public function __construct()
    {
        $this->db = MSCore::db();
        $this->data = isset($_SESSION[self::$dataKeyName]) ? $_SESSION[self::$dataKeyName] : null;
    }

    /**
     * @param $moduleName
     * @param $config_name
     *
     * @return string
     */
    public static function getItemsForSave($moduleName, $config_name)
    {
        $result = array();

        if (isset($_SESSION[self::$dataKeyName][$moduleName][$config_name]['items']))
        {
            // Fixes
            if(!empty($_SESSION[self::$dataKeyName][$moduleName][$config_name]['items'])) {
                $_SESSION[self::$dataKeyName][$moduleName][$config_name]['items'] = array_filter($_SESSION[self::$dataKeyName][$moduleName][$config_name]['items']);
            }

            $data = $_SESSION[self::$dataKeyName][$moduleName][$config_name];
            foreach ($data['items'] as $key => $item)
            {
                if (isset($item['status']) && $item['status'] == 'delete')
                {
                    continue;
                }
                unset($item['status']);
                $_SESSION[self::$dataKeyName][$moduleName][$config_name]['items'][$key]['status'] = 'saved';
                $result[] = $item;
            }
        }
        return serialize($result);
    }

    /**
     * @param $value
     *
     * @return array|mixed
     */
    public static function getItemsByValue($value)
    {
        $items = unserialize($value);

        if (empty($items))
        {
            return array();
        }

        return $items;
    }

    /**
     * @param $moduleName
     * @param $config_name
     * @param $result
     */
    public static function orderAndCheckFiles($moduleName, $config_name, $result)
    {
        $ids = !empty($result) ? array_keys($result) : array();

        if (isset($_SESSION[self::$dataKeyName][$moduleName][$config_name]['items']))
        {
            foreach ($_SESSION[self::$dataKeyName][$moduleName][$config_name]['items'] as &$item)
            {
                if (!in_array($item['id'], $ids))
                {
                    $item['status'] = 'delete';
                }
                else
                {
                    empty($result[$item['id']]) or $item = array_merge($item, $result[$item['id']]);
                }
            }

            unset($item);

            $items = assoc('id', $_SESSION[self::$dataKeyName][$moduleName][$config_name]['items']);
            $_SESSION[self::$dataKeyName][$moduleName][$config_name]['items'] = array_merge(
                array_fill_keys($ids, ''),
                $items
            );
        }
    }

    /**
     * @param $moduleName
     * @param $config_name
     * @param string $statusKey
     */
    public
    static function deleteNotSavedFiles(
        $moduleName,
        $config_name,
        $statusKey = 'new'
    ) {
        if (isset($_SESSION[self::$dataKeyName][$moduleName][$config_name]['items']))
        {
            foreach ($_SESSION[self::$dataKeyName][$moduleName][$config_name]['items'] as $key => $item)
            {
                if (!empty($item['status']) && $item['status'] == $statusKey)
                {
                    self::deleteFiles($item['path']);
                    unset($_SESSION[self::$dataKeyName][$moduleName][$config_name]['items'][$key]);
                }
            }
        }
    }

    /**
     * @param $source массив с файлами или путь до одного файла
     */
    public static function deleteFiles($source)
    {
        if (is_array($source))
        {
            foreach ($source as $file)
            {
                self::deleteFile($file);
            }
        }
        else
        {
            self::deleteFile($source);
        }
    }

    /**
     * @param $file
     */
    public static function deleteFile($file)
    {
        if (is_file($file = DOC_ROOT . DS . self::getFileFSPath($file)))
        {
            unlink($file);
        }
    }

    /**
     * Получить путь до файла, в формате файловой системы
     *
     * @param $path
     *
     * @return mixed
     */
    public static function getFileFSPath($path)
    {
        return (!IS_WIN ? DS : '') . trim(str_replace(array('/'), array(DS), trim($path, '\/')), '\/');
    }

    /**
     * @param $data
     */
    public static function renderItems($data)
    {
        foreach ($data['items'] as $item)
        {
            echo self::renderItem($item, $data);
        }
    }

    /**
     * @param $items
     * @param string $thumbId
     * @param int $number
     *
     * @return null
     */
    public static function getImageUrl($items, $thumbId = 'sys_thumb', $number = 0)
    {
        $return = null;

        is_array($items) or $items = unserialize($items);

        if (count($items))
        {
            $return = $items[$number]['path'][$thumbId];
        }

        return $return;
    }

    /**
     * @param $serializedFile
     * @param int $key
     * @return null
     */
    public static function getFilePath($serializedFile, $key = 0) {
        if(!emptyFiles($serializedFile)) {
            $arrayFile = unserialize($serializedFile);
            return $arrayFile[$key]['path'];
        }
        return null;
    }

    /**
     * @param $items
     * @param $thumbId
     * @param string $alt
     * @param int $number
     * @param string $class
     *
     * @return bool|string
     */
    public static function getImage($items, $thumbId, $alt = '', $number = 0, $class = '')
    {
        $return = false;

        is_array($items) or $items = unserialize($items);

        if ($url = self::getImageUrl($items, $thumbId, $number))
        {
            $alt = !empty($alt) ? htmlspecialchars($alt) : self::getImageDescription($items, $number);
            if(file_exists(DOC_ROOT . $url))
                list($w, $h, $p) = getimagesize(DOC_ROOT . $url);
            else
                return false;
            $return = '<img title="'.$alt.'" src="'.$url.'" width="'. $w .'" height="'. $h .'" alt="'.$alt.'"' . (!empty($class) ? 'class="'. $class .'"' : null) .'>';
        }

        return $return;
    }

    /**
     * @param $items
     * @param $thumbId
     *
     * @return array|bool
     */
    public static function getImages($items, $thumbId)
    {
        $images = false;

        is_array($items) or $items = unserialize($items);

        if (count($items))
        {

            foreach($items as $key => $value)
            {
                $images[] = self::getImage($items, $thumbId, '', $key);
            }

            return $images;
        }
        else
        {
            return false;
        }
    }

    /**
     * @param $items
     * @param $number
     * @param array $fields
     *
     * @return array|string
     */
    public static function getImageDescription($items, $number, $fields = array('text'))
    {
        $return = false;

        is_array($items) or $items = unserialize($items);

        if (count($items))
        {
            if(count($fields) > 1)
            {
                foreach($fields as $field)
                {
                    $return[$field] = isset($items[$number][$field]) ?  htmlspecialchars($items[$number][$field]) : NULL;
                }
            }
            else
            {
                $return = $items[$number][$fields[0]];
            }
        }

        return !is_array($return) ? htmlspecialchars($return) : $return;
    }

    /**
     *
     */
    public function uploadItem()
    {
        $moduleName = isset($_REQUEST['moduleName']) ? $_REQUEST['moduleName'] : null;
        $configName = isset($_REQUEST['configName']) ? $_REQUEST['configName'] : null;

        $data = isset($this->data[$moduleName][$configName]) ? $this->data[$moduleName][$configName] : null;

        /** Если превышен лимит, то ничего не делаем */
        if ($data['limit'] && isset($this->data['items']) && count($this->data['items']) >= $data['limit'])
        {
            die();
        }

        $uploadDir = self::getUploadFolder();

        /** @var результат загрузки файла $result */
        $result = $this->uploadFile($uploadDir, $data);

        if (!empty($result['success']))
        {

            $sourceFile = $uploadDir . DS . $result['uploadName'];
            $id = md5($result['uploadName']);

            /** делаем миниатюры, если это картинки */
            if (!is_null($data['thumbs']))
            {

//                /** Проверяем размеры картинки, те что меньше ожидаемых - посылаем нахер */
//                $maxW = $maxH = 0;
//                $err = array();
//                list($wI0, $hI0) = getimagesize($sourceFile);
//
//                //ищем самые большие значения размеров картинки
//                foreach ($data['thumbs'] as $thumbName => $thumbConfig)
//                {
//                    if(!empty($thumbConfig[0]) && $thumbConfig[0]>$maxW){
//                        $maxW = $thumbConfig[0];
//                    }
//
//                    if(!empty($thumbConfig[1]) && $thumbConfig[1]>$maxH){
//                        $maxH = $thumbConfig[1];
//                    }
//                }
//
//                //проверяем чтобы картинка загружаемая была больше\равна максимально требуемых
//                if($maxW>0 && $maxW>$wI0){
//                    $err[] = 'Минимальная ширина должна быть:'.$maxW.'px';
//                }
//
//                if($maxH>0 && $maxH>$hI0){
//                    $err[] = 'Минимальная высота должна быть:'.$maxH.'px';
//                }
//
//                //если что - выплевываем ошибку которую Js должен схавать
//                if(sizeof($err)){
//                    unset($result['success']);
//                    $result['error'] = '<span class="fileLoader-message error" style="margin: 0;">'.implode("; ",$err).'</span>';
//                    header("Content-Type: text/plain");
//                    echo json_encode($result);
//
//                    die();
//                }
//                /** end Проверяем размеры картинки, те что меньше ожидаемых - посылаем нахер */

                $path = array(
                    'original' => self::getFileRootPath($sourceFile),
                    'sys_thumb' => self::makeImageThumb($sourceFile, array(80, 80, true))
                );

                $thumbs = self::makeImageThumbs($sourceFile, $data['thumbs']);
                $path = array_merge($path, $thumbs);

                $data['items'][] = $newItem = array(
                    'id' => $id,
                    'path' => $path,
                    'status' => 'new'
                );
            }
            else
            {
                $data['items'][] = $newItem = array(
                    'id' => $id,
                    'fileName' => $result['uploadName'],
                    'fileSize' => self::fileSizeFormat($result['fileSize']),
                    'path' => self::getFileRootPath($sourceFile),
                    'status' => 'new'
                );
            }

            $result['itemContent'] = self::renderItem($newItem, $data);

            /** добавляем данные о новом файле в сессию */
            $_SESSION[self::$dataKeyName][$moduleName][$configName] = array_merge(
                $_SESSION[self::$dataKeyName][$moduleName][$configName],
                $data
            );
        }

        header("Content-Type: text/plain");
        echo json_encode($result);

        die();
    }

    /**
     * @return array|string
     */
    public static function getUploadFolder()
    {
        $folder = explode(':', date('Y:m:d', NOW_TIME));
        $folder = FILES_DIR . DS . implode(DS, $folder);

        if (!DirExists($folder))
        {
            debug('Не могу создать директорию ' . $folder, true, true);
        }

        return $folder;
    }

    /**
     * @param $uploadDir
     * @param $data
     *
     * @return array
     */
    public function uploadFile($uploadDir, $data)
    {
        $uploader = new qqFileUploader();
        $uploader->allowedExtensions = $data['allowedExtensions'];
        $uploader->sizeLimit = $data['sizeLimit'];
        $uploader->inputName = (!empty($data['inputName'])) ? $data['inputName'] : 'qqfile';
        $_REQUEST['qqfilename'] = self::validateFileName(
            $result['originalName'] = $uploader->getName()
        );
        $result = $uploader->handleUpload($uploadDir);

        $result['uploadName'] = $uploader->getUploadName();
        $result['fileSize'] = filesize($uploadDir . DS . $result['uploadName']);

        return $result;
    }

    /**
     * Транслитит имя файла, если в этом есть необходимость
     *
     * @param $filename имя файла
     *
     * @return string транслитизированное имя файла
     */
    public static function validateFileName($filename)
    {
        $filename = pathinfo($filename);

        return translitUrl($filename['filename']) . '.' . $filename['extension'];
    }

    /**
     * Получить путь до файла относительно корня (для вставки в базу)
     *
     * @param $path string полный путь до файла
     *
     * @return mixed путь до файла относительно корня сайта
     */
    public static function getFileRootPath($path)
    {
        return str_replace(array(DOC_ROOT, DS), array('', '/'), $path);
    }

    /**
     * @param $source
     * @param array $thumbConfig
     * @param null $target
     *
     * @return mixed
     */
    public static function makeImageThumb($source, $thumbConfig = array(0, 0, false), $target = null)
    {
        list($w0, $h0) = getimagesize($source);

        $pathInfo = pathinfo($source);

        //----создаем превьюшки аплоадного файла
        $w1 = (int)$thumbConfig[0];
        $h1 = (int)$thumbConfig[1];

        if (is_null($target))
        {
            $newFileName = self::validateFileName($pathInfo['basename']);
            $newFileName = self::addFileNamePrefix($newFileName, '_' . $w1 . '_' . $h1);
            $newFileName = self::uniqueFile($pathInfo['dirname'] . DS . $newFileName);
        }
        else
        {
            $newFileName = $target;
        }


        $crop = !empty($thumbConfig[2]);
        $top = !empty($thumbConfig[3]);

        //--------условия, при которых изменять размер изображения не надо
        if (
            ($w1 == 0 && $h1 == 0) || //----указаны нули - сохранение оригинала
            ($w1 != 0 && $h1 != 0 && $h0 <= $h1 && $w0 <= $w1) || //----указан прямоугольник для масшатбирования, но исходное изображение меньше прямоугольника
            ($h1 == 0 && $w0 <= $w1) || //----указано масштабирование по ширине, но она меньше указанной
            ($w1 == 0 && $h0 <= $h1) //----то же, только по высоте
        )
        {
            //-----сохраняем оригинал----------
            copy($source, $newFileName);
        }
        else
        {
            //------изменим размер---------------------------
            if ($w1 != 0 && $h1 != 0 && !$crop)
            {
                //-----надо вписывать в прямоугольник-------

                //-----пропорции изображения-----------------
                $p0 = $w0 / $h0;
                $p1 = $w1 / $h1;

                if ($p0 > $p1)
                {
                    $h1 = 0;
                }
                else
                {
                    $w1 = 0;
                }
            }

            if (class_exists('Imagick'))
            {
                $thumb = is_null(self::$Imagick) ? self::$Imagick = new Imagick() : self::$Imagick;
                $thumb->readImage($source);

                if ($crop)
                {
                    if ($top)
                    {
                        $geo = $thumb->getImageGeometry();

                        $width = $w1;
                        $height = $h1;

                        if (($geo['width'] / $width) < ($geo['height'] / $height))
                        {
                            $thumb->cropImage($geo['width'], floor($height * $geo['width'] / $width), 0, 0);
                        }
                        else
                        {
                            $thumb->cropImage(ceil($width * $geo['height'] / $height), $geo['height'], 0, 0);
                        }

                        $thumb->ThumbnailImage($width, $height, true);
                    }
                    else
                    {
                        $thumb->cropThumbnailImage($w1, $h1);
                    }
                }
                else
                {
                    $thumb->thumbnailImage($w1, $h1);
                }

                $thumb->writeImage($newFileName);
                $thumb->destroy();

                unset($thumb);
            }
            else
            {
                $thumb = is_null(
                    self::$ImageToolbox
                ) ? self::$ImageToolbox = new ImageToolbox() : self::$ImageToolbox;

                $thumb->newImage($source);
                $thumb->newOutputSize($w1, $h1, $crop, false);

                $thumb->save($newFileName, 'jpg');
            }
        }

        chmod($newFileName, 0644);

        return self::getFileRootPath($newFileName);
    }

    /**
     * @param $fileName
     * @param $prefix
     *
     * @return string
     */
    public static function addFileNamePrefix($fileName, $prefix)
    {
        $pathInfo = pathinfo($fileName);
        $rootDirs = array('.', '..');

        return (!in_array(
            $pathInfo['dirname'],
            $rootDirs
        ) ? $pathInfo['dirname'] . DS : '') . $pathInfo['filename'] . $prefix . '.' . $pathInfo['extension'];
    }

    /**
     * @param $path
     *
     * @return mixed
     */
    public static function uniqueFile($path)
    {
        if (is_file($path))
        {
            return self::uniqueFile(self::addFileNamePrefix($path, mt_rand(0, 99999)));
        }
        else
        {
            return $path;
        }
    }

    /**
     * @param $source
     * @param $thumbsConfig
     *
     * @return array
     */
    public static function makeImageThumbs($source, $thumbsConfig)
    {
        $result = array();

        foreach ($thumbsConfig as $thumbName => $thumbConfig)
        {
            $result[$thumbName] = self::makeImageThumb($source, $thumbConfig);
        }

        return $result;
    }

    /**
     * @param $size
     *
     * @return string
     */
    public static function fileSizeFormat($size)
    {
        $sizes = array(" байт", " Кб", " Мб", " Гб", " Тб", " Пб", " Еб", " Зб", " Иб");

        return $size ? round($size / pow(1024, ($i = (int)floor(log($size, 1024)))), 0) . $sizes[$i] : '0 байт';
    }

    /**
     * @param $item
     * @param $data
     *
     * @return string
     */
    public static function renderItem($item, $data)
    {
        $templateName = empty($data['thumbs']) ? 'fileLoaderItem_file' : 'fileLoaderItem_image';

        return template(
            $templateName,
            array(
                'sysName' => $data['sysName'] . '[' . $item['id'] . ']',
                'itemData' => $item,
                'showLabel' => isset($data['showLabel']) ? $data['showLabel'] : true
            )
        );
    }
}