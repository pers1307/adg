<?php

class MSJs {

    public static function addScript($code = null) {
        $script = MSRegistry::get('footer-scripts') ?: '';
        $script .= "\n" . $code;
        MSRegistry::set('footer-scripts', $script);
    }

    public static function addRegistryObject($key, $obj) {
        self::addScript('Site.registry.set(\''. $key .'\', \''. addslashes(json_encode($obj)) .'\');');
    }

    public static function getScripts() {
        if ($scripts = MSRegistry::get('footer-scripts')) {
            return '<script type="text/javascript">' . $scripts . '</script>';
        }
        return null;
    }
}