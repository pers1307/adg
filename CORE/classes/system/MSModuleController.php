<?php
    /**
     * Class MSModuleController
     *
     * @package Core.system
     */

    class MSModuleController extends MSBaseComponent
    {
        public function checkModuleIntegrity()
        {
            ob_start();

            global $CONFIG, $output_id;

            $tables = array();
            $allt = MSCore::db()->getAll('SHOW TABLES');
            foreach ($allt as $name)
            {
                $tables[] = $name['Tables_in_' . MSCore::db()->configParams['dbname']];
            }

            foreach ($CONFIG['tables'] as $table => $table_data)
            {
                $table_db_name = PRFX . $table_data['db_name'];

                if (!in_array($table_db_name, $tables))
                {
                    echo '<div style="border: 1px dashed black; background: #FFFDEF; padding: 10px; margin-bottom: 10px;">';
                    echo '<div>В Базе данных не обнаружена таблица <b>' . $table_db_name . '</b>, которая используется в модуле, в конфигурационном файле.</div>';
                    echo '<br><div><b>Раздел конфига:</b> [tables]=>[' . $table . ']=>[db_name]=>[' . $table_db_name . ']</div>';
                    echo '</div>';
                }
                else
                {
                    $all_fs = MSCore::db()->GetAll('DESC ' . $table_db_name . '');

                    $fs = array();
                    foreach ($all_fs as $data)
                    {
                        $fs[] = $data['Field'];
                    }

                    foreach ($table_data['config'] as $field => $field_data)
                    {
                        if(is_numeric($field)) continue;
                        if (!in_array($field, $fs))
                        {
                            echo '<div style="border: 1px dashed black; background: #FFFDEF; padding: 10px; margin-bottom: 10px;">';
                            echo '<div>В таблице базы данных не обнаружено поле <b>' . $field . '</b>, которое используется в модуле, в конфигурационном файле.</div>';
                            echo '<br><div><b>Раздел конфига:</b> [tables]=>[' . $table . ']=>[config]=>[' . $field . ']</div>';
                            echo '</div>';
                        }
                    }
                }
            }

            $result_out = ob_get_clean();

            if ($result_out != "")
            {
                if ($output_id != 'center')
                {

                    $addon = '';
                }
                else
                {
                    $addon = '<h3 Style="margin-left: 0;">Ошибки</h3>';
                }

                die($addon . $result_out);
            }
        }

        /**
         *  Подгружается конфиг модуля
         **/
        public function loadConfig()
        {
            global $CONFIG;

            $tmp_config = (MSCore::modules()->by_dir(MSCore::urls()->vars[0]));

            $CONFIG = MSCore::modules()->getModuleConfig($tmp_config['module_name']);

            unset($tmp_config);
        }

        /**
         * Создание условия фильра из памяти в SQL
         */
        public function generateFilterWhereSQL($table_db_name = '')
        {

            global $table_name;

            if ($table_db_name != "")
            {
                $table_name = $table_db_name;
            }


            if (isset($_REQUEST['filters']))
            {
                $data = $_REQUEST['filters'];
            }
            elseif (isset($_SESSION['filters'][$table_name]))
            {
                $data = unserialize($_SESSION['filters'][$table_name]);
            }
            else
            {
                $data = '';
            }

            if (!empty($data))
            {

                foreach ($data as $k => $v)
                {

                    if ($v == '' || $v == -1)
                    {

                        unset($data[$k]);

                    }

                }

            }

            $where = array();
            if (sizeof($data) && is_array($data))
            {
                foreach ($data as $key => $value)
                {
                    $tmp = explode('_0_', $key);
                    $table = $tmp[0];
                    $field = $tmp[1];
                    $ftype = $tmp[2];
                    if (isset($tmp[3]))
                    {
                        continue;
                    }

                    if ($table != "" && $field != "")
                    {

                        if ($value == "")
                        {
                            continue;
                        }

                        if ($ftype == 'select')
                        {
                            $where[] = '`' . $field . '`="' . $value . '"';
                        }
                        else
                        {
                            if ($ftype == 'calendar')
                            {
                                $value2 = $data[$key . '_0_2'];
                                $where[] = '(`' . $field . '` BETWEEN "' . $value . '" AND "' . $value2 . '")';

                            }
                            else
                            {
                                $where[] = '`' . $field . '` LIKE "%' . addslashes($value) . '%"';
                            }
                        }

                    }
                    else
                    {
                        continue;
                    }

                }

                return implode(' AND ', $where);
            }
            else
            {
                return '';
            }
        }

        /**
         * Очистка фильтров
         */
        public function clearFilters($table_name = '')
        {
            global $CONFIG;

            if ($table_name != "")
            {
                $name = $table_name;
            }
            else
            {
                $name = $CONFIG['module_name'];
            }

            if (isset($_SESSION['filters']) && isset($_SESSION['filters'][$name]))
            {
                unset($_SESSION['filters'][$name]);
            }
        }

        /**
         * Имеет ли модуль блок вывода
         **/
        public static function blockOnPage($module_name)
        {
            $output = 0;
            if(!empty(MSCore::modules()->info[$module_name]['output'])) {
                foreach (MSCore::modules()->info[$module_name]['output'] as $zone => $block)
                {
                    if ($block != "" && is_file(DES_DIR . DS . $block))
                    {
                        $output++;
                    }
                }
            }
            return $output > 0 ? true : false;
        }

        /**
         * Проверяется и создается поле для сортировки, если в таблице создано поле `ORDER`
         **/
        public function moduleOrderField($table_config)
        {
            $table_name = PRFX . $table_config['db_name'];

            $order_field = false;

            $tmp = MSCore::db()->getRow('SHOW COLUMNS FROM `' . $table_name . '` LIKE "order"');

            if (isset($tmp['Field']) && $tmp['Field'] == "order")
            {
                /*		$table_config['config']['order'] = array(
                                'caption' => '',
                                'value' => '',
                                'type' => 'order',
                                'in_list' => 1,
                            );*/

                $order_field = true;
            }

            return array($order_field, $table_config);
        }

        /**
         * Рисовалка постраничности для модулей
         */
        public function mod_pager($page, $pages, $link)
        {
            if (!is_numeric($page))
            {
                $page = 0;
            }
            $out = array();

            if ($pages <= 1)
            {
                return "";
            }

            for ($i = 1; $i <= $pages; $i++)
            {
                if (($i > $page + 5) || ($i < $page - 5))
                {
                    continue;
                }

                if ($i == $page)
                {
                    $out[] = '<span class="active">' . $i . '</span>';
                }
                else
                {
                    $out[] = '<a href="#" onclick="' . str_replace("%%", $i, $link) . '; return false;">' . $i . '</a>';
                }
            }

            if ($page > 1)
            {
                $out = array_merge(
                    array(
                        'prev' => "<a href=\"#\" onclick=\"" . str_replace(
                                "%%",
                                "1",
                                $link
                            ) . "\" class=\"first_page\" title=\"Назад\"><i class=\"icon-angle-left\"></i></a>"
                    ),
                    $out
                );
            }

            if ($page < $pages)
            {
                $out[] = "<a href=\"#\" onclick=\"" . str_replace(
                        "%%",
                        ($pages),
                        $link
                    ) . "\" class=\"last_page\" title=\"Вперед\"><i class=\"icon-angle-right\"></i></a>";
            }

            return '<div class="postr">' . implode("", $out) . '</div>';
        }

        public function getPagerTemplate($action = 'fastview', $output = '', $page, $num_pages, $uri_params = '')
        {
            global $CONFIG, $output_id, $path_id;

            if ($output != "")
            {
                $output_id = $output;
            }

            if (!isset($path_id))
            {
                $path_id = 0;
            }

            $output_id = static::prepareOutput($output_id);

            $ajax = false;
            if ($action != "")
            {
                $act = $action;
                $ajax = true;
            }
            else
            {
                $act = 'fastview';
            }

            $s = '';

            if ($page > 0 && $num_pages > 1 && $CONFIG['module_name'] != "")
            {
                if ($uri_params == "")
                {
                    $uri_params = '%%';
                }

                $s = '' .
                    $this->mod_pager(
                        $page,
                        $num_pages,
                        "doLoad('','/" . ROOT_PLACE . "/" . $CONFIG['module_name'] . "/" . $act . "/" . (int)$path_id . "/" . $uri_params . "/" . ($ajax ? '1/' : '') . "', " . $output_id . ")"
                    ) .
                    '';
            }

            return $s;
        }

        public static function prepareOutput($output_id)
        {
            $output_id = trim($output_id, '"');
            $output_id = trim($output_id, "'");
            if (!preg_match("#\(|\)#", $output_id))
            {
                $output_id = "'" . $output_id . "'";
                $output_id = str_Replace("''", "'", $output_id);
                $output_id = str_Replace('""', '"', $output_id);
            }

            return $output_id;
        }
    }