<?php

    /**
     * Класс для работы со страницей
     *
     * @property BreadCrumbs $breadCrumbs класс хлебных крошек
     * @property string $html Сгенерированный HTML
     * @property array $additionalFieldsKeys
     * @property array $zones массив с конфигурацией зон вывода страницы
     * @property array $allZones массив с конфигурацией всех зон вывода
     *
     * @package Core.system
     */

    class MSPage extends MSBaseComponent
    {
        /**
         * @var string тип контента
         */
        public $mimeType = 'text/html';
        /**
         * @var int родитель страницы
         */
        public $parent;
        /**
         * @var string путь страницы
         */
        public $path;
        /**
         * @var string путь корневой страницы
         */
        public $root_path;
        /**
         * @var int идентификатор страницы
         */
        public $path_id;
        /**
         * @var int тип страницы
         */
        public $www_type;
        /**
         * @var int порядковый номер
         */
        public $order;
        /**
         * @var boolean страница только для администраторов и супер администратора
         */
        public $root_only;
        /**
         * @var array массив с конфигурацией зон вывода страницы
         */
        public $config;
        /**
         * @var string главный шаблон вывода страницы
         */
        public $main_template;
        /**
         * @var boolean видимость страницы
         */
        public $visible;

        /**
         * @var string заголовок страницы <title></title>
         */
        public $title_page;
        /**
         * @var string Имя генератора
         */
        public $name_generator;
        /**
         * @var string Вариант исполения
         */
        public $variant_generator;
        /**
         * @var string заголовок меню
         */
        public $title_menu;
        /**
         * @var string
         */
        public $header;
        /**
         * @var string заголовок страницы <h1></h1>
         */
        public $title_h1;
        /**
         * @var string <img alt='$var'>
         */
        public $alt_for_image;
        /**
         * @var string описание страницы <meta name="description">
         */
        public $meta_description;
        /**
         * @var string ключевые страницы <meta name="keywords">
         */
        public $meta_keywords;

	    /**
	     * @var string seo линки
	     */
        public $canonical;

	    /**
	     * @var string image изображение страницы
	     */
        public $image;
        /**
         * @var string css сборка
         */
        public $css = 'site-default-styles';
        /**
         * @var string js сборка
         */
        public $js = 'site-default-scripts';
        /**
         * @var boolean можно ли удалять страницу в панели администрирования
         */
        public $nodel;
        /**
         * @var boolean можно ли добавлять дочерние страницы в панели администрирования
         */
        public $noadd;
        /**
         * @var boolean можно ли менять видимость страницы страницы в панели администрирования
         */
        public $noview;
        /**
         * @var boolean если страница нужна только для структуры, то нужно установить значение 1
         */
        public $notfound;

        /**
         * @var string разрешенные GET параметры
         */
        public $getvars;

        /**
         * @var int идентификатор страницы, на которую нужно сделать переадресацию
         */
        public $rdr_path_id;
        /**
         * @var string ссылка, на которую нужно сделать переадресацию с данной страницы
         */
        public $rdr_url;
        public $get_path;
        public $lastFileLine;

        protected $_html = '';
        private $_zones = null;
        private $_allZones = null;
        private $_zonesContent = array();
        private $_zonesOutput = array();
        private $_breadCrumbs = null;

        /**
         * @param array $config
         */
        public function __construct($config)
        {
            parent::__construct($config);

            $this->checkRedirects();

            $this->title_h1 = $this->header;
            !empty($this->title_page) or $this->title_page = $this->title_menu;
            !empty($this->title_h1) or $this->title_h1 = $this->title_menu;

	        if (!empty($this->image)) {
		        $this->image = unserialize($this->image);
	        }

            $rootPath = explode('/', $this->path);
            $this->root_path = $rootPath[0];
            unset($rootPath);
        }

        public function __get($name)
        {
            if (!isset($this->$name) && in_array($name, $this->additionalFieldsKeys))
            {
                return !isset($this->$name) ? $this->$name = null : $this->$name;
            }

            return parent::__get($name);
        }

        public function __set($name, $value)
        {
            if (!isset($this->$name) && in_array($name, $this->additionalFieldsKeys))
            {
                return $this->$name = $value;
            }

            return parent::__set($name, $value);
        }

        public function checkRedirects()
        {
            if ($this->notfound)
            {
                page404();
            }

            if ($this->root_only && (!auth_inGroups(array(1, 2)) || !auth_getUserId() || auth_getUSerId() == 1))
            {
                Redirects::go('/' . ROOT_PLACE . '/auth/');
            }

            if ($this->rdr_path_id > 1)
            {
                Redirects::go("/" . path($this->rdr_path_id));
            }

            if ($this->rdr_url != "")
            {
                Redirects::go($this->rdr_url);
            }
        }

        /**
         * Обработчик выходного буфера
         *
         * @param string $buffer Буфер вывода
         *
         * @return string $buffer
         */
        public static function obHandler($buffer)
        {
            $buffer = Minify_HTML::minify($buffer, array(/** TODO: сделать сжатие JS и CSS */
                //'cssMinifier' => array('Minify_CSS', 'minify'),

                //'jsMinifier' => array('JSMin', 'minify')

            ));

            return $buffer;
        }

        /**
         * @param string $html
         */
        public function setHtml($html)
        {
            $this->_html = $html;
        }

        public function getHtml()
        {
            return $this->_html;
        }

        /**
         * @return mixed|string
         */
        public function __toString()
        {
            return $this->render();
        }

        /**
         * @return mixed|string
         */
        public function render()
        {
            try
            {
                /** Подключаем основной шаблон */
                $mainTemplate = DES_DIR . str_replace('|', DS, $this->main_template);

                if (is_file($mainTemplate))
                {
                    extract($this->_zonesOutput);
                    /** @noinspection PhpIncludeInspection */
                    require_once $mainTemplate;
                }
                else
                {
                    debug('Не найден основной шаблон текущей страницы', true);
                }

                $this->html = ob_get_contents();
                ob_clean();

                return $this->Append2HTML();
            }
            catch (Exception $error)
            {
                ob_start();
                MSError::exceptionHandler($error);
                return ob_get_clean();
            }
        }

        /**
         * Титлотимизер
         */
        public function checkTitle()
        {
            //--------обработка титлов--------
            //type - 0 замена, 1 - до, 2 - после
            function makeChangeTitle($source,$new,$result,$type){
                switch ($type){
                    case 0:
                        return $new;

                    case 1:
                        return $new.$source;

                    case 2:
                        return $source.$new;

                    case 3:
                        return $new.$result;

                    case 4:
                        return $result.$new;
                }

                return '';
            }

            $titleRules = MSCore::db()->GetAll($q = 'SELECT `type_insert`, `text`  FROM `' . PRFX . 'titles` WHERE "' . MSCore::urls()->requestUri . '" LIKE  `url` AND `active` = 1 ORDER by `sort` ASC');

            if (count($titleRules))
            {
                $matchTitle = preg_match('|<title>(.*)</title>|Uis', $this->_html, $match);

                if ($matchTitle)
                {
                    $r_title = $match[1];

                    foreach ($titleRules as $tr)
                    {
                        $r_title = makeChangeTitle($r_title, $tr['text'], $r_title, $tr['type_insert']);
                    }

                    $this->_html = preg_replace('#<title>(.*)</title>#iUs', '<title>' . trim($r_title) . '</title>', $this->_html);
                }
            }
        }

        /**
         * Внедрение в HTML код дополнительных элементов и вывод на экран.
         *
         * @return mixed|string
         */
        public function Append2HTML()
        {
            if($this->getIsRootPlace() === false)
            {
                AddBeforeTag(MSCore::settings()->metrika, '</body>');
            }

            if ($this->meta_description)
            {
                AddBeforeTag('<meta name="description" content="' . htmlentities($this->meta_description, ENT_QUOTES, SITE_ENCODING) . '">', '<title>');
            }

            if ($this->meta_keywords != "")
            {
                AddBeforeTag('<meta name="keywords" content="' . htmlentities($this->meta_keywords, ENT_QUOTES, SITE_ENCODING) . '">', '<title>');
            }

            if ($this->canonical != "") {
	            AddBeforeTag('<link rel="canonical" href="' . htmlentities($this->canonical, ENT_QUOTES, SITE_ENCODING) . '" />', '<title>');
            }

            /** для я.карт **/
            $this->_html = preg_replace_callback('/\[map=([\d]+)\]/i', 'mapInsert', $this->_html);

            foreach (MSCore::urls()->storeAddBefTag as $tag => $info)
            {
                if ($tag === '')
                {
                    continue;
                }

                foreach ($info as $text)
                {

                    /** Если есть тег,то вставляем перед ним */
                    if (preg_match('#' . _pregQuote($tag) . '#sim', $this->_html))
                    {
                        $this->_html = preg_replace('#' . _pregQuote($tag) . '#sim', $text . $tag, $this->_html);
                    }

                }
            }

            require_once BLOCKS_DIR . DS . 'system' . DS . 'ie-window.php';

            if ($this->path === '404')
            {
                header('HTTP/1.x 404 Not Found');

                //ob_start();
                //debug($this->lastFileLine, 0, 0);
                //$this->_html .= ob_get_clean();
            }

            $this->checkTitle();

            header('Content-Type: '. $this->mimeType . '; charset=' . SITE_ENCODING);

            return $this->_html;
        }

        public function getZoneContent($zoneId = '_CONTENT_', $path_id = null)
        {
            !is_null($path_id) or $path_id = $this->path_id;
            $contentId = $path_id . $zoneId;

            if(!isset($this->_zonesContent[$contentId]))
            {
                $cur_zone_content = MSCore::db()->getOne('SELECT `content` FROM `' . PRFX . 'zones_content` WHERE `zone_id`= "' . $zoneId . '" AND `path_id` = ' . $path_id . ' LIMIT 1');
                $this->_zonesContent[$contentId] = html_entity_decode($cur_zone_content, ENT_QUOTES);
            }

            return $this->_zonesContent[$contentId];
        }

        public function setZoneContent($value, $zoneId = '_CONTENT_', $path_id = null)
        {
            !is_null($path_id) or $path_id = $this->path_id;
            $contentId = $path_id . $zoneId;

            $this->_zonesContent[$contentId] = $value;
        }

        public function setZoneOutput($name, &$value)
        {
            $this->_zonesOutput[$name] =& $value;
        }

        public function getZoneOutput($name)
        {
            return isset($this->_zonesOutput[$name]) ? $this->_zonesOutput[$name] : null;
        }

        public function getZones()
        {
            if($this->_zones === null)
            {
                $pageZones = array_keys($this->config);
                $this->_zones = MSCore::db()->getAll('SELECT * FROM `' . PRFX . 'zones` WHERE `value` IN ("'.implode('","', $pageZones).'") ORDER BY id');
            }

            return $this->_zones;
        }

        public function getAllZones()
        {
            if($this->_allZones === null)
            {
                $this->_allZones = MSCore::db()->getAll('SELECT * FROM `' . PRFX . 'zones` ORDER BY id');
            }

            return $this->_allZones;
        }

        public function getAdditionalFieldsKeys()
        {
            return array_keys($GLOBALS['PAGE_CONFIG']['pages']['config']);
        }

        public function getBreadCrumbs()
        {
            if($this->_breadCrumbs === null)
            {
                $this->_breadCrumbs = new BreadCrumbs();
				$this->_breadCrumbs->separator = '';
                $this->_breadCrumbs->fastGet($this->path_id);
            }

            return $this->_breadCrumbs;
        }

        public function getIsRootPlace()
        {
            return (bool)preg_match('#^' . ROOT_PLACE . '\/?#', trim(MSCore::urls()->requestUri, '/'));
        }
    }