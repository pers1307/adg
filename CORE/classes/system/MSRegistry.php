<?php

class MSRegistry
{
    /**
     * @var array
     */
    protected static $_vars = array();

    protected function __construct() {}
    protected function __clone() {}
    protected function __wakeup() {}

    /**
     * @param string $key
     * @param mixed $item
     * @return void
     */
    public static function set($key, $item) {
        self::$_vars[$key] = $item;
    }

    /**
     * @param string $key
     * @return null|mixed
     */
    public static function get($key) {
        if (isset(self::$_vars[$key])) return self::$_vars[$key];
        return null;
    }

    /**
     * @param string $key
     * @return void
     */
    public static function remove($key) {
        if (isset(self::$_vars[$key])) unset(self::$_vars[$key]);
    }
}
