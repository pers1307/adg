<?php
    /**
     * Class MSSettings
     *
     * @property array $formConfig
     * @property array $settings
     * @property array $dataBaseValues
     * @property int $moduleId
     *
     * @package Core.system
     */

    class MSSettings extends MSBaseComponent
    {
        public $moduleName = 'settings';
        public $pathId = 0;

        private $_settings = null;
        private $_formConfig = null;
        private $_moduleId = null;
        private $_dataBaseValues = null;

        public function __get($name)
        {
            if(isset($this->settings[$name]))
            {
                return $this->getValue($name);
            }

            return parent::__get($name);
        }

        public function __isset($name)
        {
            if(isset($this->settings[$name]))
            {
                return true;
            }

            return parent::__isset($name);
        }

        public function getFormConfig()
        {
            if($this->_formConfig === null)
            {
                $moduleConfig = MSCore::modules()->getModuleConfig($this->moduleName);

                if(isset($moduleConfig['settings']))
                {
                    $this->_formConfig = $moduleConfig['settings'];
                }
                else
                {
                    throw new Exception('Для модуля ' . $this->moduleName . ' нет конфигурации');
                }

                foreach($this->_formConfig as $key => &$config)
                {
                    if(isset($this->dataBaseValues[$key]))
                    {
                        $config['value'] = $this->dataBaseValues[$key];
                    }
                }
                unset($config);
            }

            return $this->_formConfig;
        }

        public function getValue($key)
        {
            if(isset($this->settings[$key]))
            {
                return $this->settings[$key];
            }
            else
            {
                throw new Exception('Нет свойства с ключом ' . $key);
            }
        }

        public function getSettings()
        {
            if($this->_settings === null)
            {
                $this->_settings = array();

                foreach($this->getFormConfig() as $key => $item)
                {
                    $this->_settings[$key] =& $item['value'];
                }
            }

            return $this->_settings;
        }

        public function getDataBaseValues()
        {
            if($this->_dataBaseValues === null)
            {
                $this->_dataBaseValues = array();

                $ValuesTable = new MSTable('{settings}');
                $ValuesTable->fields = array('key', 'value');
                $ValuesTable->filter = array(
                    'moduleId' => '`module_id` = ' . $this->moduleId,
                    'pathId' => '`path_id` = ' . $this->pathId
                );

                foreach($ValuesTable->items as $item)
                {
                    $this->_dataBaseValues[$item['key']] = $item['value'];
                }
            }

            return $this->_dataBaseValues;
        }

        public function getModuleId()
        {
            if($this->_moduleId === null)
            {
               $module = MSCore::modules()->by_name($this->moduleName);
               $this->_moduleId = $module['module_id'];
            }

            return $this->_moduleId;
        }

        public function save()
        {
            $configForSave = MSCore::forms()->save($this->formConfig);

            foreach($configForSave as $key => $config)
            {
                if($this->getValue($key) !== $config['value'])
                {
                    $key = MSCore::db()->pre($key);
                    $value = MSCore::db()->pre($config['value']);

                    MSCore::db()->execute("REPLACE INTO `".PRFX."settings` SET `path_id` = {$this->pathId}, `module_id` = {$this->moduleId}, `key` = '{$key}', `value` = '{$value}'");
                }
            }
        }
    }