<?php
    /**
     * Class MSTapeControl
     *
     * @package Core.system
     */

    class MSTapeControl extends MSModuleController
    {
        /**
         * Смена позиции сортировки
         */
        public function setSwapItemsOrder($table_name, $item_id, $action)
        {
            $item = MSCore::db()->getRow('SELECT * FROM `' . PRFX . $table_name . '` WHERE `id`="' . (int)$item_id . '"');

            if (!isset($item['parent']))
            {
                $item['parent'] = false;
            }

            switch ($action)
            {
                // Двигаем вверх
                case 1:
                {
                    $to_swap = $this->getItemsToSwap($table_name, $item['order'], true, $item['parent']);

                    if ((int)$to_swap > 0)
                    {
                        $target = MSCore::db()->getOne(
                            'SELECT `order` FROM `' . PRFX . $table_name . '` WHERE `id`="' . (int)$to_swap . '"'
                        );

                        $sql1 = "UPDATE `" . PRFX . $table_name . "` SET `order` = '" . ((int)$target) . "' WHERE `id` = '" . $item_id . "'";
                        $sql2 = "UPDATE `" . PRFX . $table_name . "` SET `order` = '" . ((int)$item['order']) . "' WHERE `id` = '" . (int)$to_swap . "'";

                        MSCore::db()->execute($sql1, true, false);
                        MSCore::db()->execute($sql2, true, true);
                    }
                }
                    break;

                // Двигаем вниз
                case 0:
                {
                    $to_swap = $this->getItemsToSwap($table_name, $item['order'], false, $item['parent']);

                    if ((int)$to_swap > 0)
                    {
                        $sql1 = "UPDATE `" . PRFX . $table_name . "` SET `order` = '" . ((int)$item['order'] + 1) . "' WHERE `id` = '" . $item_id . "'";
                        $sql2 = "UPDATE `" . PRFX . $table_name . "` SET `order` = '" . ((int)$item['order']) . "' WHERE `id` = '" . (int)$to_swap . "'";

                        MSCore::db()->execute($sql1, true, false);
                        MSCore::db()->execute($sql2, true, true);
                    }
                }
                    break;
            }
        }

        /**
         * Сброс сортировки
         */
        public function resetSwapItemsOrder($table_name)
        {
            global $path_id;

            $order = 0;

            $all = MSCore::db()->getAll(
                'SELECT `id` FROM `' . PRFX . $table_name . '` ' . ($path_id > 0 ? 'WHERE `path_id`="' . (int)$path_id . '"' : '') . ''
            );
            if (sizeof($all))
            {
                $order = 0;
                foreach ($all as $item)
                {
                    $order++;
                    $sql = 'UPDATE `' . PRFX . $table_name . '` SET `order`="' . (int)$order . '" WHERE `id`="' . $item['id'] . '"';
                    MSCore::db()->execute($sql, true, ($order == 1) ? true : false);
                }
            }

            return (boolean)$order;
        }

        public function generateVars()
        {
            global $table_name, $path_id, $page, $CONFIG, $output_id, $key_field;

            $path_id = !isset($path_id) ? 0 : (int)$path_id;
            $page = !isset($page) ? 1 : (int)$page;

            $d_width = (int)$CONFIG['tables']['items']['dialog']['width'];
            $d_height = (int)$CONFIG['tables']['items']['dialog']['height'];

            $moduleName = $CONFIG['module_name'];

            $Controls = new ModuleControls($moduleName);

            /*
             * Если в таблице БД есть `order`, то добавляем поле сортировки
             */
            list($isorder, $CONFIG['tables']['items']) = $this->moduleOrderField($CONFIG['tables']['items']);

            if ($table_name == "")
            {
                debug('Имя таблицы не известно. Путь: /' . implode('/' . MSCore::urls()->vars) . '/', true);
            }

            $not_in = (isset($CONFIG['tables']['items']['not_show']) && is_array(
                    $CONFIG['tables']['items']['not_show']
                ) && !AUTH_IS_ROOT) ? $CONFIG['tables']['items']['not_show'] : array();

            /* CREATE VIEW */
            $filter = $this->generateFilterWhereSQL();

            // Проверяется вложенность модуля в другой модуль
            $and = '';
            if ($filter != "")
            {
                $and = ' AND ';
            }

            if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path']))
            {
                $link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path']) - 1];
                if (isset($link_path['inmodule']) && $link_path['inmodule'] == $CONFIG['module_name'])
                {
                    $filter .= $and . '`' . $link_path['to_key'] . '`="' . $link_path['from_id'] . '"';
                }
            }

            if (!empty($CONFIG['tables']['items']['filters']))
            {

                $sql = array();

                foreach ($CONFIG['tables']['items']['filters'] as $sqlFilter)
                {

                    if (!AUTH_IS_ROOT or (AUTH_IS_ROOT && $sqlFilter['disabledForRoot'] == false))
                    {
                        $sql[] = $sqlFilter['filter'];
                    }

                }

                if (!empty($sql))
                {
                    $filter == '' or $filter .= ' AND ';
                    $filter .= '(' . implode(' AND ', $sql) . ')';
                }

            }

            do
            {
                list($items, $num_pages) = $this->getDataFromSQL(
                    '*',
                    $table_name,
                    $filter,
                    $path_id,
                    (isset($CONFIG['tables']['items']['order_field']) ? $CONFIG['tables']['items']['order_field'] : ($isorder ? '`order` ASC' : '`' . $key_field . '` DESC')),
                    '',
                    $page,
                    $CONFIG['tables']['items']['onpage'],
                    $not_in
                );
                if ($page > $num_pages)
                {
                    $page = $num_pages;
                }
                else
                {
                    break;
                }
            } while ($page <= $num_pages);

            foreach ($items as &$item)
            {

                $editAction = 'displayMessage(\'/' . ROOT_PLACE . '/' . $moduleName . '/add/' . (int)$path_id . '/' . (int)$page . '/' . (int)$item[$key_field] . '/\', ' . (int)$d_width . ', ' . (int)$d_height . ');';
                $moveUpAction = 'doLoad(\'\', \'/' . ROOT_PLACE . '/' . $moduleName . '/swap/' . (int)$path_id . '/' . (int)$page . '/' . (int)$item[$key_field] . '/up/\', \'' . $output_id . '\');';
                $moveDownAction = 'doLoad(\'\', \'/' . ROOT_PLACE . '/' . $moduleName . '/swap/' . (int)$path_id . '/' . (int)$page . '/' . (int)$item[$key_field] . '/down/\', \'' . $output_id . '\');';
                $deleteAction = 'if (confirm(\'Удалить запись?\')) doLoad(\'\',\'/' . ROOT_PLACE . '/' . $moduleName . '/delete/' . (int)$path_id . '/' . (int)$page . '/' . (int)$item[$key_field] . '/\',\'' . $output_id . '\')';

                $Controls->addData('item-id', $item[$key_field]);

                $Controls->addControl(
                    'edit',
                    array(
                        'events' => array(
                            'onClick="' . $editAction . '"'
                        )
                    )
                );
                if ($isorder)
                {
                    $Controls->addControl(
                        'move-up',
                        array(
                            'events' => array(
                                'onClick="' . $moveUpAction . '"'
                            )
                        )
                    );
                    $Controls->addControl(
                        'move-down',
                        array(
                            'events' => array(
                                'onClick="' . $moveDownAction . '"'
                            )
                        )
                    );
                }
                $Controls->addControl(
                    'del',
                    array(
                        'events' => array(
                            'onClick="' . $deleteAction . '"'
                        )
                    )
                );

                $item['editAction'] = $Controls->actionAccess('edit') ? $editAction : '';
                $item['controls'] = $Controls->render();
                $item['sub_config']['output_id'] = $output_id;
            }
            unset($item);

            $vars = array();
            $vars['CONFIG'] = $CONFIG;
            $vars['output_id'] = $output_id;
            $vars['isorder'] = $isorder;
            $vars['path_id'] = (int)$path_id;
            $vars['actionAdd'] = $Controls->actionAccess('add');
            $vars['actionEdit'] = $Controls->actionAccess('edit');

            $vars['items'] = $items;
            $vars['page'] = $page;
            $vars['num_pages'] = $num_pages;

            $vars['pager'] = $this->getPagerTemplate('fastview', '', $page, $num_pages);

            /* CREATE VIEW */

            return $vars;
        }

        public function prepareLinkPath($CONFIG)
        {
            if (isset($_SESSION['link_path']) && sizeof($_SESSION['link_path']) && $_SESSION['link_path'][sizeof(
                    $_SESSION['link_path']
                ) - 1]['inmodule'] == $CONFIG['module_name']
            )
            {
                $last = $_SESSION['link_path'][sizeof($_SESSION['link_path']) - 1];
                $output_id = $last['to_output'];

                unset($last);
            }
            else
            {
                $output = (int)MSModuleController::blockOnPage($CONFIG['module_name']);
                $output_id = ($output > 0 ? 'fast_table_' . $CONFIG['module_name'] : 'center');

                unset($output);
            }

            return array($output_id);
        }

        public function generateConfigValues($new_item_id)
        {
            global $CONFIG, $table_name, $key_field, $isorder;

            list($isorder, $CONFIG['tables']['items']) = $this->moduleOrderField($CONFIG['tables']['items']);

            if ($new_item_id > 0)
            {
                $list = MSCore::db()->getRow("SELECT * FROM `" . PRFX . $table_name . "` WHERE `" . $key_field . "`=" . $new_item_id);

                foreach ($CONFIG['tables']['items']['config'] as $k => $v)
                {
                    if (isset($list[$k]))
                    {
                        $CONFIG['tables']['items']['config'][$k]['value'] = $list[$k];
                    }
                }
            }

            if (isset($_SESSION['link_path']))
            {
                if (isset($_SESSION['link_path'][sizeof($_SESSION['link_path']) - 1]))
                {
                    $link_path = $_SESSION['link_path'][sizeof($_SESSION['link_path']) - 1];
                    if (isset($link_path['inmodule']) && $link_path['inmodule'] == $CONFIG['module_name'])
                    {
                        $CONFIG['tables']['items']['config'][$link_path['to_key']] = array(
                            'type' => 'hidden',
                            'value' => $link_path['from_id']
                        );
                    }
                }
            }

            return $CONFIG;
        }

        public function saveItem($showdberr = true)
        {
            global $CONFIG, $path_id, $table_name, $isorder, $key_field;

            $CONFIG['tables']['items']['config'] = MSCore::forms()->save($CONFIG['tables']['items']['config']);

            $sql_ = array("`path_id` = '" . $path_id . "'");
            foreach ($CONFIG['tables']['items']['config'] as $field => $info)
            {

                if (isset($info['value']))
                {

                    if ($info['type'] == 'code' && empty($info['value']))
                    {

                        $info['value'] = "NULL";

                    }
                    else
                    {

                        $info['value'] = "'" . MSCore::db()->pre($info['value']) . "'";

                    }

                    $sql_[] = "`" . $field . "` = " . $info['value'];

                }

            }


            if ((int)$_REQUEST[$key_field] > 0)
            {
                $sql = "UPDATE `" . PRFX . $table_name . "` SET " . implode(
                        ', ',
                        $sql_
                    ) . " WHERE `" . $key_field . "` = '" . (int)$_REQUEST[$key_field] . "'";
            }
            else
            {
                if ($isorder)
                {

                    /*$last_order = MSCore::db()->getOne('SELECT `order` FROM `'.PRFX.$table_name.'` ORDER BY `order` DESC LIMIT 1');

                    $last_order = $last_order=="" ? 1 : $last_order;

                    $sql_[] = "`order` = '".($last_order+1)."'";*/

                    //-----------уккажем сортировку в зависимости от того что указано в конфиге
                    if (!empty($CONFIG['tables']['items']['add_new_on_top']))
                    {
                        $order = MSCore::db()->GetOne(
                            "SELECT MIN(`order`) FROM `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` WHERE `path_id`='" . $path_id . "'"
                        );
                        $sql_[] = "`order`=" . ((int)$order - 1);
                    }
                    else
                    {
                        $order = MSCore::db()->GetOne(
                            "SELECT MAX(`order`) FROM `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` WHERE `path_id`='" . $path_id . "'"
                        );
                        $sql_[] = "`order`=" . ((int)$order + 1);
                    }

                }

                $sql = "INSERT INTO `" . PRFX . $table_name . "` SET " . implode(', ', $sql_);
            }

            if (MSCore::db()->execute($sql, $showdberr))
            {
                return $inserted_id = ($_REQUEST[$key_field]) ? intval($_REQUEST[$key_field]) : MSCore::db()->id;
            }
            else
            {
                return false;
            }

        }

        /**
         * Выбор итема для смени позиции
         */
       public function getItemsToSwap($table_name, $order = 0, $up = true, $parent = 0)
        {
            global $path_id;

            if ($order == 0)
            {
                return false;
            }

            if ($up)
            {
                $sql = "SELECT `id` FROM `" . PRFX . $table_name . "` WHERE `order`<" . $order . " " . ((int)$path_id > 0 ? ' AND `path_id`="' . (int)$path_id . '"' : '') . ($parent !== false ? " AND `parent`=" . $parent : "") . " ORDER BY `order` DESC LIMIT 1";
            }
            else
            {
                $sql = "SELECT `id` FROM `" . PRFX . $table_name . "` WHERE `order`>" . $order . " " . ((int)$path_id > 0 ? ' AND `path_id`="' . (int)$path_id . '"' : '') . ($parent !== false ? " AND `parent`=" . $parent : "") . " ORDER BY `order` ASC LIMIT 1";
            }


            $id = MSCore::db()->getOne($sql);

            return $id;
        }

        /**
         *  Функция, для выборки данных из таблицы
         **/
        public function getDataFromSQL(
            $fields,
            $table,
            $where = 1,
            $path_id = 0,
            $order = 'order',
            $order_pos = 'DESC',
            $_page = 0,
            $onpage = 0,
            $not_in = array()
        ) {
            if ($where == '')
            {
                $where = 1;
            }

            if ($fields != "*")
            {
                $fields = str_Replace('`', '', $fields);
                $fields = explode(",", $fields);
                $fields = "`" . implode("`,`", $fields) . "`";
            }

            if ($where == 1 && $path_id > 0)
            {
                $where = '`path_id`=' . (int)$path_id;
            }
            elseif ($where != 1 && $where != "" && $path_id > 0)
            {
                $where .= ' AND `path_id`=' . (int)$path_id;
            }

            if (count($not_in))
            {
                foreach ($not_in as $not_field => $not_value)
                {
                    $where .= ' AND `' . $not_field . '` not in (\'' . implode("','", $not_value) . '\')';
                }
            }

            $num_pages = 0;

            if ($onpage > 0)
            {
                if ($_page == 0 || !isset($_page))
                {
                    $_page = 1;
                }
                $from = ($_page - 1) * $onpage;
            }

            $sql = "SELECT {$fields} FROM `" . PRFX . $table . "` WHERE {$where} ORDER BY {$order} {$order_pos} " . ($onpage > 0 ? "LIMIT $from,$onpage" : "") . " ";
//debug($sql,false,true);
            $result = MSCore::db()->getAll($sql);

            if ($onpage > 0)
            {
                $count = MSCore::db()->getOne("SELECT count(*) FROM `" . PRFX . $table . "` WHERE {$where}");
                $num_pages = ceil($count / $onpage);
            }

            return array($result, $num_pages);
        }
    }