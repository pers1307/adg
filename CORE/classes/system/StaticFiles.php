<?php
/**
 * Класс для объединения и минимизации статичных CSS и JS файлов
 * @package Core.system
 */

class StaticFiles {

    /**
     * Директория для статики
     */
    const
        STATIC_DIR = 'STATIC';

    /**
     * Получить файл по имени в конфиге
     *
     * @param string $groupName Ммя в конфиге $GLOBALS['staticFiles'], расположенном в корневом config.php
     * @param boolean $minify Объединять и сжимать в один файл, либо выводит списком
     * @param string $type Тип файлов js или css
     *
     * @return bool|string
     */
    private static function getStatic($groupName, $minify, $type) {

        empty($GLOBALS['staticFiles'][$groupName]) or $filesConfig = $GLOBALS['staticFiles'][$groupName];

        if (!empty($filesConfig)) {

            if(isset($filesConfig['ref']) && is_array($filesConfig['ref'])) {
                $ref = $filesConfig['ref'];
                unset($filesConfig['ref']);
                foreach($ref as $refGroupName) {
                    if(isset($GLOBALS['staticFiles'][$refGroupName])) {
                        $filesConfig = array_merge($GLOBALS['staticFiles'][$refGroupName], $filesConfig);
                    }
                }
                unset($ref);
            }

            if ($minify) {

                $filePath = self::STATIC_DIR . '/' . $type . '/' . $groupName . '.' . $type;

                return self::returnByType(self::checkStaticFile($filesConfig, $filePath), $type);

            } else {

                $filesList = array();

                foreach ($filesConfig as $filePath) {

                    $filesList[] = self::returnByType(strtr($filePath, array('//' => '/')), $type);

                }

                return implode('', $filesList) . "\r\n";

            }

        }

        debug('Группы файлов «'.$groupName.'» не существует');
        return false;

    }

    /**
     * Проверяет изменения файлов по списку
     *
     * @param array $filesConfig
     * @param string $filePath Путь до объединяющего файла
     *
     * @return bool
     */
    private function checkStaticForRewrite($filesConfig, $filePath) {

        $rewrite = false;

        if (!file_exists($filePath)) {
            $rewrite = true;
        } else {
            $lastModified = filemtime($filePath);

            foreach ($filesConfig as $file) {

                $file = DOC_ROOT . DS . trim($file, '//');

                if (file_exists($file) && filemtime($file) > $lastModified) {

                    $rewrite = true;
                    break;
                }

            }

        }

        return $rewrite;

    }

    /**
     * Перезаписывает объединяющий файл, если любой файл из списка был изменен
     *
     * @param array $filesConfig $GLOBALS['staticFiles']
     * @param string $filePath Путь до объединяющего файла
     *
     * @return string
     */
    private function checkStaticFile($filesConfig, $filePath) {

        $pathInfo = pathinfo($filePath);

        if (self::checkStaticForRewrite($filesConfig, $filePath)) {

            $fileContent = file_get_contents('http://' . DOMAIN . '/min/' . $pathInfo['basename']);

            $dir = DOC_ROOT . DS . $pathInfo['dirname'];

            if (!is_dir($dir)) {
                mkdir($dir, 0755);
            }

            $file = DOC_ROOT . DS . $filePath;

            file_put_contents($file, $fileContent);
            chmod($file, 0644);

            $file .= '.gz';

            $gz = gzopen($file, 'w9');
            gzputs($gz, $fileContent);
            gzclose($gz);

            chmod($file, 0644);

            unset($fileContent, $pathInfo);

        }

        return '/' . $filePath;

    }

    /**
     * Возвращает HTML подключения файла, в зависимости от его типа
     *
     * @param string $filePath Путь до файла
     * @param string $type Тип файла
     *
     * @return bool|string
     */
    private static function returnByType($filePath, $type) {

        switch ($type) {

            case 'css':

                return self::returnCSS($filePath);

                break;

            case 'js':

                return self::returnJS($filePath);

                break;
        }

        return false;

    }

    /**
     * Возвращает HTML для CSS
     *
     * @param $filePath
     *
     * @return string
     */
    private static function returnCSS($filePath) {

        return '<link rel="stylesheet" href="' . $filePath . '" type="text/css">' . "\r\n";

    }

    /**
     * Возвращает HTML для JS
     *
     * @param $filePath
     *
     * @return string
     */
    private static function returnJS($filePath) {

        return '<script src="' . $filePath . '"></script>' . "\r\n";

    }

    /**
     * Возвращает сгенерированный HTML подключения CSS
     *
     * @param string $groupName Имя группы файлов из корневого config.php
     * @param bool $minify Сжимать или выводить списком
     *
     * @return bool|string
     */
    public static function getCSS($groupName, $minify = null) {

        !is_null($minify) or $minify = COMPRESS_CSS;

        return self::getStatic($groupName, $minify, 'css');

    }


    /**
     * Возвращает сгенерированный HTML подключения JS
     *
     * @param string $groupName Имя группы файлов из конфига
     * @param bool $minify Сжимать или выводить списком
     *
     * @return bool|string
     */
    public static function getJS($groupName, $minify = null) {

        !is_null($minify) or $minify = COMPRESS_JS;

        return self::getStatic($groupName, $minify, 'js');

    }

}