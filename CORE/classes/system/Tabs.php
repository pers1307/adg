<?php
/**
 * @package Core.system
 */

class Tabs {

    private
        $template = 'tabs',
        $tabs = array(),
        $activeTab = NULL,
        $tabParams = array();

    public function __construct($templateName = '') {

        empty($templateName) or $this->template = $templateName;

        $this->tabParams = array(
            'class' => array(),
            'attributes' => array(),
            'data' => array(),
            'content' => array()
        );

    }

    public function addTab($title, $tabId, $params = array()) {

        $params = array_merge_recursive($this->tabParams, $params);

        $tab = array(
            'id' => $tabId,
            'title' => $title,
            'tabId' => $tabId,
            'class' => $params['class'],
            'attributes' => $params['attributes'],
            'data' => $params['data'],
            'content' => implode('', $params['content']),
            'active' => false
        );

        $this->tabs[$tabId] =& $tab;

        $tab['data']['target'] = $tabId;

        if (is_null($this->activeTab) || $this->activeTab == $tabId) {
            $this->setActiveTab($tabId);
        }

        if (count($tab['class'])) {
            $tab['attributes']['class'] = implode(' ', $tab['class']);
        }

        if (count($tab['data'])) {

            foreach ($tab['data'] as $dataName => $dataValue) {
                $tab['attributes']['data-' . $dataName] =  $dataValue;
            }

        }

        if (count($tab['attributes'])) {

            $attributes = array();

            foreach ($tab['attributes'] as $attributeName => $attributeValue) {

                $attributes[] = $attributeName . ' = "' . htmlentities($attributeValue, ENT_QUOTES, SITE_ENCODING) . '"';

            }

            $tab['attributes'] = ' ' . implode(' ', $attributes);
        }

    }

    public function __toString() {

        return $this->render();

    }

    public function setActiveTab($tabId = '') {

        if (empty($tabId) && is_null($this->activeTab)) {

            $this->setActiveTab(key($this->tabs));

        } else {
            $this->activeTab = $tabId;
            if (!empty($this->tabs[$this->activeTab])) {
                $this->tabs[$this->activeTab]['class'][] = 'active';
                $this->tabs[$this->activeTab]['active'] = true;
            }
        }

    }

    private function render() {

        if (empty($this->tabs)) {
            return '';
        }

        return template($this->template, array('items' => $this->tabs));

    }

}