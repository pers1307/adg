<?

$help = (isset($help)) ? $help : '';
$value = (isset($value)) ? $value : '';
$limit = (isset($limit)) ? $limit : -1;

$table = $from['table_name'];
$keyField = $from['key_field'];
$nameField = $from['name_field'];
$where = (isset($from['where']) ? $from['where'] : '');
$order = (isset($from['order']) ? $from['order'] : '');

$template = '<input type="text" class="wide autocomplete" name="' . $sysname . '[]' . '" value="'.$value.'" data-table="'.$table.'" data-key-field="'.$keyField.'" data-name-field="'.$nameField.'" data-order="'.$order.'" data-where="'.$where.'" data-limit="'.$limit.'">';
$template .= '<span class="help">'.$help.'</span>';
return $template;

?>