<?php

$id_sysname = $sysname;
$id_sysname = str_Replace("[","_",$id_sysname);
$id_sysname = str_Replace("]","_",$id_sysname);
$id_sysname = str_Replace("__","_",$id_sysname);

$help = (isset($help)) ? $help : '';
$value = strtotime($value);
$showTime = empty($data['show_time']);
$date_format = $showTime ? 'Y-m-d' : 'Y-m-d H:i:s';
$date = $value ?  $value : date($date_format, time());
$date = empty($data['empty']) ? date($date_format, ($value ? $value : time())) : ($value ? date($date_format, $value) : '');

$out = '<input id="'.$id_sysname.'" class="date" type="text" name="'.$sysname.'" size="'.(empty($data['size']) ? '' : (int)$data['size']).'" value="'.$date.'" readonly>';
$out .= empty($data['empty']) ? '' : '<a href="javascript:void(0)" onclick="javascript:$(\'#'.$id_sysname.'\').val(\'\')" style="margin-left:10px" title="Очистить"><img src="/DESIGN/ADMIN/images/delete_page.gif" alt="Удалить" width="14" height="14" style="border:0" /></a><span class="help">'.$help.'</span>';

ob_start();
?>

<script type="text/javascript">
    var options = {
        timeFormat: "HH:mm:ss",
        dateFormat: "yy-mm-dd",
        showButtonPanel: true
    };
    $('#<?php echo $id_sysname; ?>')<?php echo $showTime ? '.datepicker(options)' : '.datetimepicker(options)';?>;
</script>

<?

$out .= ob_get_clean();
return $out;

?>