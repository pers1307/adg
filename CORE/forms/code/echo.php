<?

$help = (isset($help)) ? $help : '';
ob_start();

?>

<input id="code" onchange="codeChange(this, '<?=htmlspecialchars($value)?>')" class="wide <?=empty($value) ? '' : 'conf'?>" name="<?=$sysname?>" type="text" value="<?=htmlspecialchars($value)?>">
<span class="help"><?= $help ?></span>
<?

return ob_get_clean();

?>