<?php
/**
 * echo.php
 * Предствление для отображения Color picker
 * внутри модуля
 */

if (isset($size)) {
	$size = 'style="width:' . ($size * 10) . ' margin-right: 20px;"';
} else {
	$size = 'style="width: 123px; margin-right: 20px;"';
}

if (isset($help)) {
	$help = $help;
} else {
	$help = '';
}

$classWithColorPicker = 'js-' . $config_name;
$classWithColorPickerJs = '.' . $classWithColorPicker;

$classWithColorCode = 'js-color-' . $config_name;
$classWithColorCodeJs = '.' . $classWithColorCode;

return '
	<input
		class="wide ' . $classWithColorCode . '"
		' . $size . '
		name="' . $sysname . '"
		id="' . $sysname . '"
		type="text"
		value="' . htmlspecialchars($value) . '"
		readonly="readonly"
	/>

	<input
		class="' . $classWithColorPicker . '"
		type="text"
		data-type="' . $type . '"
		data-colorCodeClass="' . $classWithColorCodeJs . '"
		data-color="' . htmlspecialchars($value) . '"
	/>

	<span class="help">' . $help . '</span>
';
?>
