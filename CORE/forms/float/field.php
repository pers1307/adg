<?php

$value = $item[$config['field_table']];
if(isset($config['precision'])) {
    $precision = pow(10, intval($config['precision']));
    $value = str_replace(',', '.',floor($value * $precision) / $precision);
}
echo $value;