<?

/**
* Шаблон поля double
*/
$data['value'] = preg_replace('/[^0-9\.]/', '', str_replace(',', '.',$result));
if($result < 0) {
    $data['value'] = '-' . $data['value'];
}

if(isset($data['precision'])) {
    $precision = pow(10, intval($data['precision']));
    $data['value'] = str_replace(',', '.',floor($data['value'] * $precision) / $precision);
}