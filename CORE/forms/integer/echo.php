<?
$over = (isset($over)) ? $over : '';
$out = (isset($out)) ? $out : '';
$help = (isset($help)) ? $help : '';

$onover = 'onmouseover="'.$over.'"';
$onout = 'onmouseout="'.$out.'"';
$validate = (!empty($validate)) ? $validate : array();
$to_code = isset($to_code) ? ' onkeyup="toCode(this)" onchange="toCode(this)" ': '';

return '<input class="wide numberInt'.(!empty($validate['required'])?' required':'').'" name="'.$sysname.'" type="text" value="'.htmlspecialchars($value).'" '.$onover.' '.$onout.' '.$to_code.' '.(isset($readonly)?'readonly':'').'><span class="help">'.$help.'</span>';
?>