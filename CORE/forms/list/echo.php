<?
    ob_start();
    isset($value) or $value = serialize(array());
    $items = unserialize($value);
    $help = (isset($help)) ? $help : '';
?>

<script type="text/javascript">
    $(document).ready(function(){
        $(document)
            .off("click", ".list-<?=$config_name?>-add")
            .on("click", ".list-<?=$config_name?>-add", function() {
                var $table = $(this).closest('table.list-<?=$config_name?>-table');
                if($table.find('tr.list-<?=$config_name?>-empty').size() == 1){
                    $table.find('tr.list-<?=$config_name?>-empty').remove();
                }

                // Get templates
                var $parameters_row_template = '<tr class="list-<?=$config_name?>-row">\
                    <td>\
                        <input type="text" value="" name="<?=$sysname?>[]">\
                    </td>\
                    <td class="action">\
                        <a href="#" class="list-<?=$config_name?>-remove"><i class="icon-minus-sign"></i></a>\
                    </td>\
                </tr>';

                $table.children('tbody').append($parameters_row_template);
                return false;
            });

        $(document)
            .off("click", ".list-<?=$config_name?>-remove")
            .on("click", ".list-<?=$config_name?>-remove", function() {
                var $table = $(this).closest('table.list-<?=$config_name?>-table');
                var $parameters_empty = '<tr class="list-empty list-<?=$config_name?>-empty"><td colspan="2">Список пуст</td></tr>';

                $(this).closest('tr.list-<?=$config_name?>-row').remove();
                if($table.find('tr.list-<?=$config_name?>-row').size() == 0){
                    $table.children('tbody').append($parameters_empty);
                }
                return false;
            });
    });
</script>

<table class="table table-list list-<?=$config_name?>-table">
    <thead>
        <th></th>
        <th width="20" class="action"><a href="#" class="list-<?=$config_name?>-add"><i class="icon-plus-sign"></i></a></th>
    </thead>

    <tbody>

    <? if($items) { ?>
        <? foreach($items as $_item) { ?>
            <tr class="list-<?=$config_name?>-row">
                <td><input type="text" value="<?=$_item?>" name="<?=$sysname?>[]"></td>
                <td class="action"><a href="#" class="list-<?=$config_name?>-remove"><i class="icon-minus-sign"></i></a></td>
            </tr>
        <? } ?>
    <? } else { ?>
        <tr class="list-empty list-<?=$config_name?>-empty">
            <td colspan="2">Список пуст</td>
        </tr>
    <? } ?>
    </tbody>
</table>
<span class="help"><?= $help ?></span>

<?
    return ob_get_clean();
?>