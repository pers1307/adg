<?php
$value = $item[$config['field_table']];

if (empty($config['thumbs']))
{

    $file = MSFiles::getItemsByValue($value);
    if(isset($file[0]['fileName']) && isset($file[0]['path'])) {
        echo '<a href="'.$file[0]['path'].'" target="_blank">'.$file[0]['fileName'].'</a>';
    }

}
else
{
    $imageUrl = MSFiles::getImageUrl($value);

    if ($imageUrl)
    {
        ?><img src="<?php echo $imageUrl ?>" alt=""><?php
    }
}