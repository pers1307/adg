<?
    $output = (isset($output)) ? $output : '';
    $caption = (isset($caption)) ? $caption : '';

    if($output == 'open') {

        $showBlock = '';
        if (isset($toggle)) {
            $caption = '<a class="section-toggle" href="#" onclick="$(this).closest(\'fieldset\').find(\'.fieldset-block\').slideToggle();return false;">' . $caption . '</a>';
            $showBlock = $toggle == 'hide' ? 'display: none;' : '';
        }

        return '<fieldset><legend>'.$caption.'</legend><div class="fieldset-block" style="' . $showBlock .'">';
    } elseif($output == 'close') {
        return '</div></fieldset>';
    }
?>