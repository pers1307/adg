<?
/**
* Шаблон поля boolean.
*/


$over = (isset($over)) ? $over : '';
$out = (isset($out)) ? $out : '';
$help = (isset($help)) ? $help : '';

$onover = 'onmouseover="'.$over.'"';
$onout = 'onmouseout="'.$out.'"';

if(AUTH_IS_ROOT)return $str='<input class="wide" name="'.$sysname.'" type="text" value="'.htmlspecialchars($value).'" '.$onover.' '.$onout.'><span class="help">'.$help.'</span>';
else return '<span>'.htmlspecialchars($value).'</span><input id="'.$sysname.'" name="'.$sysname.'" type="hidden" value="'.htmlspecialchars($value).'">';