<?php

    $wysiwygConfig = '';
    $help = (isset($help)) ? $help : '';

    if (!defined('TINYMCE'))
    {

        ob_start();
        require_once(CORE_DIR . '/forms/wysiwyg/config.php');
        $wysiwygConfig = ob_get_clean();

        define('TINYMCE', 1);

    }

    return $wysiwygConfig . '<textarea name="' . $sysname . '" id="' . $sysname . '" style="width:' . (isset($data['width']) ? (int)$data['width'] : '100%') . ';height:' . (isset($data['height']) ? (int)$data['height'] : 200) . 'px;" class="wysiwyg">' . $value . '</textarea><span class="help">'.$help.'</span>';