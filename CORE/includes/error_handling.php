<?php

// Register error handlers
set_exception_handler(array('MSError', 'exceptionHandler'));
set_error_handler(array('MSError', 'errorHandler'));
register_shutdown_function(array('MSError', 'shutdownHandler'));