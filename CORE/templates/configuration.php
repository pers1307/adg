<? foreach ($configurations as $data) { ?>
    <? if ($data['type'] == 'caption') { ?>
        <div class="table">
            <div><?=$data['caption']?></div>
        </div>
    <? } elseif ($data['type'] == 'section') { ?>
        <?=$data['html_value']?>
    <? } elseif ($data['type'] == 'divider') { ?>
        <div class="table">
            <div><?=$data['html_value']?></div>
        </div>
    <? } elseif ($data['type'] == 'hidden') { ?>
        <div class="table hidden"><?=$data['html_value']?></div>
    <? } else { ?>
        <div class="table">
            <div><?=$data['caption']?></div>
            <div><?=$data['html_value']?></div>
        </div>
    <?
    }
    ?>
<? } ?>