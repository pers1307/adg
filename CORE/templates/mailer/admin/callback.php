<p>Заявка на обратный звонок от <?= sql2date(date('Y-m-d H:i:s'), 7) ?></p>

<p>
    <? if(!empty($name)): ?>
        Имя: <?= $name ?><br>
    <? endif; ?>
    <? if(!empty($phone)): ?>
        Номер телефона: <?= $phone ?><br>
    <? endif; ?>
    <? if(!empty($text)): ?>
        Комментарий:<br> <?= nl2br($text) ?>
    <? endif; ?>
</p>