<div class="generalModulesWrap">
    <div id="mod_config<?= $mod ?>" class="form-wrap">
        <div class="border">
            <div align="left"><h3>Укажите какие блоки выводить, когда модуль будет назначен на страницу:</h3></div>
            <div class="hr"></div>
            <form action="/<?= current_url() ?>" method="post">
                <?= $_FORM_ ?>
            </form>
        </div>
    </div>
</div>