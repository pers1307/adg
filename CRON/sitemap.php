<?
set_time_limit(0);

require_once dirname(__FILE__).'/../console.php';

$xml = array();
$SITE = 'http://www.' . DOMAIN . '/';
$file = '';

getLinksPage($SITE);

if (!empty($xml)) {
    foreach ($xml as $row) {
        $file .= '
		<url>
			<loc>' . $row . '</loc>
			<changefreq>weekly</changefreq>
		</url>
		';
    }


    $file = '<?xml version="1.0" encoding="'.SITE_ENCODING.'"?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
	' . $file . '
</urlset>';

    file_put_contents(DOC_ROOT . DS . 'sitemap.xml', $file);
}


function getLinksPage($url = '') {
    global $xml, $SITE;

    if (empty($url)) {
        return;
    }

    $h = get_headers($url);
    if (isset($h[0]) && mb_strpos($h[0], '200 OK')) {
        $cont = file_get_contents($url);
        if (!empty($cont)) {
            if (!in_array($url, $xml)) {
                $xml[] = $url;

                preg_match_all("/href=\"\/(.*?)\/\"/i", $cont, $res);
                if (!empty($res[1])) {

                    foreach ($res[1] as $r) {

                        if (!in_array($SITE . $r . '/', $xml)) {
                            getLinksPage($SITE . $r . '/');
                        }

                    }
                }
            }
        }
    }
}