<?php

    /** Подключаем класс административного меню */
    if (isset(MSCore::urls()->vars[0]))
    {
        $moduleName = MSCore::urls()->vars[0];
        $admin_file = MODULES_DIR . DS . $moduleName . DS . 'admin.php';

        if (is_file($admin_file))
        {
            MSCore::modules()->UseModule($moduleName, __FILE__, __LINE__);

            require_once($admin_file);
        }

        unset($vars);
    }