<?php
    /**
     * User: Andruha
     * Date: 27.03.13
     * Time: 15:40
     */

    $PagesTree = new PagesTree();

    $TABS = new Tabs('admin/tabs');

    $TABS->addTab('Страницы', 'pagesTree', array(
        'content' => $PagesTree->makeTree()->render(),
        'class' => 'tab-pages',
        'attributes' => array(
            'title' => 'Структура сайта'
        )
    ));

    $TABS->addTab('Управление', 'generalModules', array(
        'content' => getModulesList(),
        'class' => 'tab-modules',
        'attributes' => array(
            'title' => 'Общие модули управления'
        )
    ));

    if (AUTH_IS_ROOT)
    {
        $TABS->addTab('Root', 'rootControl', array(
            'class' => 'root',
            'content' => generateMenuDiv(),
        ));
    }

    $TABS->addTab('<i class="icon-question-sign"></i>', 'supportInformation', array(
        'content' => template('helpTab'),
        'class' => 'support',
        'attributes' => array(
            'title' => 'Информация и техническая поддержка'
        )
    ));

    echo $TABS;