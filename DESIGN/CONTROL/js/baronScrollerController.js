/**
 * User: Andruha
 * Date: 15.05.13
 * Time: 15:00
 */

var Site = Site || {};

(function(global, window, $, undefined)
{
    'use strict';

    function ScrollerController(element, settings)
    {

        var defaults = {
            selector: '.baron'
        };

        this.settings = $.extend(defaults, settings);
        this.container = element;

        this.init.call(this);

    }

    ScrollerController.prototype.init = function()
    {

        var wrapper = $('<div class="baron-wrapper"></div>'),
            scroller = $('<div class="baron-scroller"></div>'),
            scrollerBar = $('<div class="baron-scrollerTrack"><div class="baron-scroller__bar"><i class="icon-reorder"></i></div></div>');

        wrapper = this.container.addClass('container').wrap(scroller).parent().append(scrollerBar).wrap(wrapper);

        wrapper.baron({
            scroller: '.baron-scroller',
            bar: '.baron-scroller__bar',
            barOnCls: 'baron-scroller__bar_state_on'
        });

        this.scroller = $(wrapper);

        this.container.removeClass(this.settings.selector.replace(/\./, ''));

        this.scroller.on('scrollBottom.ScrollerController', this.scrollBottom.bind(this));

        $(window).on('updateBaronScrollers.ScrollerController allImagesLoaded.ScrollerController', function()
        {
            this.scroller.trigger('sizeChange');
        }.bind(this));

        this.scroller.data('api', this);
    };

    ScrollerController.prototype.scrollBottom = function(){

        this.scroller.animate({
            scrollTop: this.container.height()
        }, 500);

    };

    ScrollerController.prototype.scrollMove = function(step){

        step = '+=' + step;
        this.scroller.stop( true, true ).animate({
            scrollTop: step
        }, 0);
    };

    $(document).on('ready.ScrollerController ajaxSuccess.ScrollerController', function(event, xhr, settings)
    {
        var selector = '.baron-scroll';

        $(selector).initBaronScroller({
            selector: selector
        });
    });

    $(function()
    {
        $.fn.initBaronScroller = function(settings)
        {
            $.each(this, function()
            {
                new ScrollerController($(this), settings);
            });
        };
    });

})(Site, window, jQuery);