/**
 * User: Andruha
 * Date: 24.04.13
 * Time: 15:56
 */

var Site = Site || {};

(function (global, window, $, undefined) {
    'use strict';

    function CustomCheckbox(element) {

        if (!element.length) return;

        this.element = element;

        element.addClass('init').wrap('<span class="checkbox"></span>');
        this.wrap = element.parent();

        this.wrap.on('click', this.change.bind(this));
        this.element.on('change', this.checkStatus.bind(this)).trigger('change');

    }

    CustomCheckbox.prototype.change = function(event){

        if(this.wrap.parent('label').length) return;
        this.element.prop('checked', !this.element.is(':checked')).trigger('change');

    };

    CustomCheckbox.prototype.checkStatus = function () {

        if (this.element.is(':checked')) {
            this.wrap.addClass('checked');
        } else {
            this.wrap.removeClass('checked');
        }

    };

    function CustomRadioButton(element) {

        if (!element.length) return;

        this.element = element;

        element.addClass('init').wrap('<span class="radio"></span>');
        this.wrap = element.parent();
        this.checkStatus.call(this);

        this.wrap.on('click', this.onClick.bind(this));
        this.element.on('change', this.onClick.bind(this));

    }

    CustomRadioButton.prototype.onClick = function () {

        var name = this.element.prop('name');

        $(":radio[name='"+name+"']").prop('checked', false).parent().removeClass('checked');
        this.element.prop('checked', true);
        this.checkStatus.call(this);

    };

    CustomRadioButton.prototype.checkStatus = function () {

        if (this.element.is(':checked')) {
            this.wrap.addClass('checked');
        } else {
            this.wrap.removeClass('checked');
        }

    };

    $(function () {

        $.fn.initCustomCheckbox = function () {

            $.each(this, function () {
                new CustomCheckbox($(this));
            });


        };

        $.fn.initCustomRadioButton = function () {

            $.each(this, function () {
                new CustomRadioButton($(this));
            });
        };

    });

    var initCustomElements = function(){

        var $select = $('select').not('.select2-offscreen, .no-custom');

        $select.not('.select2-offscreen, .no-custom').select2({
            'width': '100%'
        });

        // workaround solution for correct sorting multiple select
        (function () {
            var movedCounter = 0;
            $select.on('selected', function (e) {
                var $select, selectedValue, $selectedOption, $movedOptions;
                $select = $(e.currentTarget);
                selectedValue = e.val;
                $selectedOption = $select.find('option[value='+ selectedValue +']');
                $selectedOption.attr('data-moved', ++movedCounter).detach().prependTo($select);
                $movedOptions = $select.find('option[data-moved]');
                $movedOptions.detach();
                $movedOptions.sort(function (a, b) {
                    var contentA =parseInt( $(a).attr('data-moved'));
                    var contentB =parseInt( $(b).attr('data-moved'));
                    return (contentA < contentB) ? -1 : (contentA > contentB) ? 1 : 0;
                });
                $select.prepend($movedOptions);
            });
        })();




        $('.autocomplete').not('.loaded').each(function(){
            var paramTable = $(this).data('table'),
                paramKeyField = $(this).data('key-field'),
                paramNameField = $(this).data('name-field'),
                paramOrder = $(this).data('order'),
                paramWhere = $(this).data('where'),
                paramLimit = $(this).data('limit') || -1;

            $(this).addClass('loaded').select2({
                width: '100%',
                placeholder: 'Не выбрано',
                maximumSelectionSize: paramLimit,
                formatSelectionTooBig: function (limit) {
                    return 'Выбрано максимальное число элементов';
                },
                multiple: true,
                ajax: {
                    url: '/' + global.ROOT_PLACE + '/admin/autocomplete/',
                    dataType: 'json',
                    type: 'POST',
                    quietMillis: 250,
                    data: function (term, page) {
                        return {
                            'type': 'list',
                            'q': term,
                            'page': page,
                            'table': paramTable,
                            'key-field': paramKeyField,
                            'name-field': paramNameField,
                            'order': paramOrder,
                            'where': paramWhere
                        };
                    },
                    results: function (data, page) {
                        var more = (page * 10) < data.total_count;
                        return { results: data.items, more: more };
                    }
                },
                initSelection: function(element, callback) {
                    var value = $(element).val().split(',');
                    if(value.length){
                        $.ajax('/' + global.ROOT_PLACE + '/admin/autocomplete/', {
                            dataType: 'json',
                            type: 'POST',
                            data: {
                                'type': 'init',
                                'items': value,
                                'table': paramTable,
                                'key-field': paramKeyField,
                                'name-field': paramNameField,
                                'order': paramOrder,
                                'where': paramWhere
                            }
                        }).done(function(data) {
                            callback(data.items);
                        });
                    }
                },
                minimumInputLength: 1
            });
        });

        $('.geocoder').not('.loaded').each(function(){

            $(this).addClass('loaded').select2({
                width: '100%',
                placeholder: 'Адрес не указан',
                multiple: false,
                ajax: {
                    url: '/' + global.ROOT_PLACE + '/admin/geocoder/',
                    dataType: 'json',
                    type: 'POST',
                    quietMillis: 400,
                    data: function (term) {
                        return {
                            'type': 'list',
                            'q': term
                        };
                    },
                    results: function (data) {
                        return { results: data.items, more: false };
                    }
                },
                initSelection: function(element, callback) {
                    var value = $(element).val(),
                        text = JSON.parse(value);

                    if(value.length){
                        callback({id:value, text:text.text});
                    }
                },
                minimumInputLength: 3
            });
        });

        $('input:checkbox').not('.init,.native').initCustomCheckbox();
        $('input:radio').not('.init,.native').initCustomRadioButton();

        // Metrika
        var $metrikaCodeField = $('#metrikaCodeField'),
            $metrikaDelegateField = $('#metrikaDelegateField'),
            $metrikaFetchLink = $('#metrikaFetchLink'),
            $metrikaDelegateLink = $('#metrikaDelegateLink');

        $metrikaFetchLink.not('.loaded').on('click', function(e){
            $(this).addClass('loaded');
            e.preventDefault();
            $metrikaCodeField.attr('disabled', true).addClass('loading');
            $.ajax({
                url: '/' + global.ROOT_PLACE + '/admin/metrika_fetch/',
                type: 'POST',
                data: {},
                dataType: 'json',
                success: function(response) {
                    response.code = response.code || '';
                    if(response.code) {
                        $metrikaCodeField.val(response.code);
                    }
                    $metrikaCodeField.attr('disabled', false).removeClass('loading');
                }
            });
        });

        $metrikaDelegateLink.not('.loaded').on('click', function(e){
            e.preventDefault();
            $(this).addClass('loaded');

            var $el = $(this);
            if($el.hasClass('disabled')) return false;
            $el.addClass('disabled');

            var $metrikaDelegateTable = $('#metrikaDelegateTable'),
                $metrikaDelegateError = $('#metrikaDelegateError'),
                login = $metrikaDelegateField.val();

            $metrikaDelegateError.empty();
            if(!login) {
                $metrikaDelegateError.text('Логин не может быть пустым');
                $el.removeClass('disabled');
            } else {
                $.ajax({
                    url: '/' + global.ROOT_PLACE + '/admin/metrika_delegate/',
                    type: 'POST',
                    data: {login: login},
                    dataType: 'json',
                    success: function(response) {
                        response.login = response.login || '';
                        response.error = response.error || '';
                        if(response.error) {
                            $metrikaDelegateError.html(response.error);
                        } else {
                            var template = '<tr><td>'+response.login +'</td></tr>',
                                empty = $metrikaDelegateTable.find('.list-empty');

                            if(empty.size()){
                                empty.remove();
                            }
                            $metrikaDelegateTable.find('tbody').append(template);
                            $metrikaDelegateField.val('');
                        }
                        $el.removeClass('disabled');
                    },
                    error: function() {
                        $el.removeClass('disabled');
                    }
                });
            }
        });

        /**
         * Вызов для Color picker (spectrum)
         */
        $('input[data-type="color"]').not('.loaded').each(function() {
            var classWithColorPickerJs = '.' + $(this).attr('class');
            var classWithColorCodeJs = $(this).attr('data-colorCodeClass');
            var color = $(this).attr('data-color');

            /**
             * Локализация плагина Spectrum
             * @type {{cancelText: string, chooseText: string, clearText: string, noColorSelectedText: string, togglePaletteMoreText: string, togglePaletteLessText: string}}
             */
            var localization = $.spectrum.localization["ru"] = {
                cancelText: "Отмена",
                chooseText: "Выбрать",
                clearText: "Сбросить",
                noColorSelectedText: "Цвет не выбран",
                togglePaletteMoreText: "Ещё",
                togglePaletteLessText: "Скрыть"
            };

            $.extend($.fn.spectrum.defaults, localization);

            /**
             * Вызов плагина
             */
            $(classWithColorPickerJs).spectrum({
                color: color,
                showInput: true,
                className: "full-spectrum",
                showInitial: true,
                showPalette: true,
                showSelectionPalette: true,
                maxSelectionSize: 10,
                preferredFormat: "hex",
                localStorageKey: "spectrum.demo",
                move: function (color) {

                },
                show: function () {

                },
                beforeShow: function () {

                },
                hide: function () {

                },
                change: function(color) {
                    $(classWithColorCodeJs).attr("value", color.toHexString());
                },
                palette: [
                    ["rgb(0, 0, 0)", "rgb(67, 67, 67)", "rgb(102, 102, 102)",
                        "rgb(204, 204, 204)", "rgb(217, 217, 217)","rgb(255, 255, 255)"],
                    ["rgb(152, 0, 0)", "rgb(255, 0, 0)", "rgb(255, 153, 0)", "rgb(255, 255, 0)", "rgb(0, 255, 0)",
                        "rgb(0, 255, 255)", "rgb(74, 134, 232)", "rgb(0, 0, 255)", "rgb(153, 0, 255)", "rgb(255, 0, 255)"],
                    ["rgb(230, 184, 175)", "rgb(244, 204, 204)", "rgb(252, 229, 205)", "rgb(255, 242, 204)", "rgb(217, 234, 211)",
                        "rgb(208, 224, 227)", "rgb(201, 218, 248)", "rgb(207, 226, 243)", "rgb(217, 210, 233)", "rgb(234, 209, 220)",
                        "rgb(221, 126, 107)", "rgb(234, 153, 153)", "rgb(249, 203, 156)", "rgb(255, 229, 153)", "rgb(182, 215, 168)",
                        "rgb(162, 196, 201)", "rgb(164, 194, 244)", "rgb(159, 197, 232)", "rgb(180, 167, 214)", "rgb(213, 166, 189)",
                        "rgb(204, 65, 37)", "rgb(224, 102, 102)", "rgb(246, 178, 107)", "rgb(255, 217, 102)", "rgb(147, 196, 125)",
                        "rgb(118, 165, 175)", "rgb(109, 158, 235)", "rgb(111, 168, 220)", "rgb(142, 124, 195)", "rgb(194, 123, 160)",
                        "rgb(166, 28, 0)", "rgb(204, 0, 0)", "rgb(230, 145, 56)", "rgb(241, 194, 50)", "rgb(106, 168, 79)",
                        "rgb(69, 129, 142)", "rgb(60, 120, 216)", "rgb(61, 133, 198)", "rgb(103, 78, 167)", "rgb(166, 77, 121)",
                        "rgb(91, 15, 0)", "rgb(102, 0, 0)", "rgb(120, 63, 4)", "rgb(127, 96, 0)", "rgb(39, 78, 19)",
                        "rgb(12, 52, 61)", "rgb(28, 69, 135)", "rgb(7, 55, 99)", "rgb(32, 18, 77)", "rgb(76, 17, 48)"]
                ]
            });

            $(this).addClass('loaded');
        });

    };

    $(document).on('ready ajaxSuccess', function(event, xhr, settings){
        initCustomElements();
    });
})(Site, window, jQuery);