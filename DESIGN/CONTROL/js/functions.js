var enableCache = false;
var dialogs = new Array();
var catalog_write_bg_last_art = null;
var tree_tpls = null;
var last_dialog_name = null;
var calendarObjForForm = null;
var last_opened_page = null;
var active_zone2reload = null;

var lastActType = ''; // openpage|doload
var action_doload_url = '';
var action_doload_target = '';

/**
 * Каталог: Выделять все бренды галочками в каталоге
 */

function setFilter(mod_name, path_to_reload, output_id){
    doLoad(getObj('filter_' + mod_name), '/' + ROOT_PLACE + '/' + mod_name + '/filter/' + path_to_reload + '/', output_id);
}

function checkAllBrands(myobj){
    var objs = getObj('brand_list_checks').getElementsByTagName('INPUT');
    for (var i = 0; i < objs.length; i++)
    {
        if (myobj.checked)
        {
            objs[i].checked = true;
        }
        else
        {
            objs[i].checked = false;
        }
    }
}

/**
 * Левые табы
 */
function showLeftTab(num){
    var tab = getObj('left_tab' + active_left_tab);
    if (tab)
    {
        tab.style.display = 'none';

        v1 = getObj('tab_item' + active_left_tab);
        v2 = getObj('tab_item' + active_left_tab + '_img');

        if (v1 && v2)
        {
            v1.className = 'tab_item_inactive';
            v2.className = 'tab_item2_inactive';
        }
    }

    active_left_tab = num;

    var tab = getObj('left_tab' + num);
    if (tab)
    {
        tab.style.display = '';

        v1 = getObj('tab_item' + num);
        v2 = getObj('tab_item' + num + '_img');

        if (v1 && v2)
        {
            v1.className = 'tab_item_active';
            v2.className = 'tab_item2_active';
        }
    }
}

/**
 * Центральные табы
 */
function showCenterTab(num){
    if (typeof active_center_tab == 'undefined' || !active_center_tab)
    {
        active_center_tab = 1;
    }

    if (active_center_tab == num)
    {
        active_center_tab = 1;
    }

    var tab_prev = document.getElementById('center_tab' + active_center_tab);
    if (tab_prev)
    {
        tab_prev.style.display = 'none';

        v1 = document.getElementById('center_tab_item' + active_center_tab);
        v2 = document.getElementById('center_tab_item' + active_center_tab + '_img');

        if (v1 && v2)
        {
            v1.className = 'tab_item_inactive';
            v2.className = 'tab_item2_inactive';
        }
    }

    var cur_tab = document.getElementById('center_tab' + num);
    v1 = document.getElementById('center_tab_item' + num);
    v2 = document.getElementById('center_tab_item' + num + '_img');

    if (v1 && v2 && cur_tab)
    {
        v1.className = 'tab_item_active';
        v2.className = 'tab_item2_active';

        cur_tab.style.display = '';

        active_center_tab = num;
    }
    else
    {
        showCenterTab(num);
    }

//	document.getElementById('content_div_2').style.height=screen.availHeight-520;

//	document.getElementById('content_div_1').style.width=document.getElementById('content_div_2').clientWidth;
//	document.getElementById('content_div_1').style.height=document.getElementById('content_div_2').clientHeight;

}

/**
 * Табуляторы Зон
 */
function showzoneTab(num){
    var ztab_prev = document.getElementById('zone_tab' + active_zone_tab);
    if (ztab_prev)
    {
        ztab_prev.style.display = 'none';

        v1 = document.getElementById('zone_tab_item' + active_zone_tab);
        v2 = document.getElementById('zone_tab_item' + active_zone_tab + '_img');

        if (v1 && v2)
        {
            v1.className = 'tab_item_inactive';
            v2.className = 'tab_item2_inactive';
        }
    }

    var zcur_tab = document.getElementById('zone_tab' + num);
    v1 = document.getElementById('zone_tab_item' + num);
    v2 = document.getElementById('zone_tab_item' + num + '_img');

    if (v1 && v2 && zcur_tab)
    {
        v1.className = 'tab_item_active';
        v2.className = 'tab_item2_active';

        zcur_tab.style.display = '';

        active_zone_tab = num;
    }
}

/**
 *    Раскараска TR в каталоге разделов
 */
function catalog_write_bg(obj){
    if (catalog_write_bg_last_art != null)
    {
        catalog_write_bg_last_art.style.backgroundColor = '#fff'
    }

    obj.style.backgroundColor = '#f4edbd';
    catalog_write_bg_last_art = obj;
}

/**
 *    Закрытие аякс окна
 */
function closeAjaxWorking(){
    /*var obj = getObj('ajax_working');
     if (obj) {
     obj.style.display = 'none';
     }*/
}

/**
 *    Возвращение пути, хз, Хплоит писал
 */
function return_path(path, path_id, zone, isconf){
    /**
     Параметр isconf характеризует что меняется конфиг модуля...
     поэтому path_id несете в себе информацию о модуле
     zone зона в которой поисходит изменение
     path путь до скрипта
     */

    if (isconf)
    {
        url = '/' + ROOT_PLACE + '/ajax/change_config/' + path_id + '/' + zone + '/?path=' + path;
        url = url.replace('/dialog/', '');
        doLoad('', url, 'mod_config' + path_id.replace('/dialog/', ''), 'POST');
    }
    else
    {
        doLoad('', '/' + ROOT_PLACE + '/ajax/add_block/' + path_id + '/' + zone + '/?path=' + path, path_id + '' + zone, 'POST');
    }

    closeDialog();
}

/**
 *    Функция которая создает новый таб на какой либо колонке“
 */
function openPage(position, id, contentUrl, title, tabTitle){
    $('.tapeModuleTopControls > .backButton').trigger('click');
    /* Смена индикации того, какая страница открыта */
    if (last_opened_page && typeof last_opened_page[1] != 'undefined' && last_opened_page[1] != "")
    {
        var path_id = last_opened_page[1];

        path_id = path_id.replace('edit_path', '');

        var tr = getObj('tree_node' + path_id);
        if (tr && path_id > 0)
        {
            tr.style.fontWeight = 'normal';
        }
    }

    var path_id = id.replace('edit_path', '');
    var tr = getObj('tree_node' + path_id);
    if (tr && path_id > 0)
    {
        tr.style.fontWeight = 'bold';
    }

    /* Очистка от мусорных визивигов */
    var iframes = document.getElementsByTagName('iframe');
    if (iframes && iframes.length > 0)
    {
        for (var i = 0; i < iframes.length; i++)
        {
            if (iframes[i].getAttribute('src') == 'javascript:void(0)')
            {
                discardElement(iframes[i]);
            }
        }
    }

    /* Меняем заголовок */
    var obj = getObj('title_page_cont');
    if (obj) obj.innerHTML = title;

    /* Динамически загружаем контент и обрабатываем в нем JS */
    /*var dynContent = new DHTMLSuite.dynamicContent();
     dynContent.loadContent('center', contentUrl, true);
     dynContent = false;*/

    $('#center').addClass('ajaxLoading').load(contentUrl, function(){
        $(this).removeClass('ajaxLoading');
    });

    /* Запоминаем какую страницу открыли */
    last_opened_page = new Array(position, id, contentUrl, title, tabTitle);

    /* Делаем индикацию активности правой зоны */
    //setActiveZone('page');
}

/**
 * Делаем индикацию активности правой зоны
 */
function setActiveZone(zone_id){
    var o1 = getObj('tree_zone');
    var o2 = getObj('content_zone');

    if (zone_id == 'tree')
    {
        active_zone2reload = zone_id;
        o1.style.display = '';
        o2.style.display = 'none';
    }

    if (zone_id == 'page')
    {
        active_zone2reload = zone_id;
        o2.style.display = '';
        o1.style.display = 'none';
    }
}

/**
 *    Быстрое получение объекта по ID
 */
function getObj(id){
    var obj = document.getElementById(id);
    if (obj)
    {
        return obj;
    }
}

/**
 *    Возвращение текста из большого визивика в маленький
 */
function wisywigDoBackText(sysname){
    document.getElementById(sysname).value = tinyMCE.get('fsw_' + sysname).getContent();
}

/**
 *    Создание диалогового окна с АЯКС содержимым контентом
 */

function displayMessage(url, width, height, backContentId, fullscreen){
    Site.modal.open('MediaPublisher 6.0', url);
}

/**
 *    Отображение обычного диалогового окна с HTML контентом
 */
function displayAlert(content){
    if (content == "")
    {
        return;
    }

    Site.modal.open('Ошибка', {
        'content': '<div style="padding: 15px">' + content + '</div>'
    });
}

/**
 *    Добавление кнопок в диалоговое окно
 */
function dialogAddButtons(arr_buts){
    var cur_dialog = dialogs[dialogs.length - 1];

    if (cur_dialog && arr_buts.length && arr_buts.length > 0)
    {
        cur_dialog.setButtons(arr_buts);
    }
}

/**
 *    Возвращает ID контент области текущего окна
 */
function currentDialogContent(){
    var cur_dialog = dialogs[dialogs.length - 1];

    if (cur_dialog)
    {
        return cur_dialog.contentId;
    }
    else
        return '';
}

/**
 *    Возвращает ID контент области предыдущего окна
 */
function previousDialogContent(){
    var previous_dialog = dialogs[dialogs.length - 2];

    if (previous_dialog)
    {
        return previous_dialog.contentId;
    }
    else
        return '';
}

/**
 *    Возвращает объект контента текущего модального окна
 */
function currentDialogContentObj(){
    var cur_dialog = dialogs[dialogs.length - 1];

    if (cur_dialog)
    {
        return getObj(cur_dialog.contentId);
    }
    else
        return null;
}

/**
 *    Закрывает текущее окно
 */
function closeDialog(){

    var cur_dialog = dialogs[dialogs.length - 1];
    if (cur_dialog && cur_dialog.modalDialogName)
    {
        closeCMSDialog(cur_dialog.modalDialogName);
        closeAjaxWorking();
    }

//removeEditor();
}

/**
 *    Закрывает и удаляет текущее модальное окно
 */

function removeElement(element){
    element.parentNode.removeChild(element);
    return true;
}
function closeCMSDialog(name){
    // Закрываем календарик
    if (typeof calendarObjForForm == 'object' && calendarObjForForm != null && calendarObjForForm.isVisible())
    {
        calendarObjForForm.hide();
    }

    // это если колор пикер открыт
    if (typeof color_picker_div == 'object' && color_picker_div != null)
    {
        if (closeColorPicker) closeColorPicker();
        discardElement(color_picker_div);
        color_picker_div = null;
    }

    var obj1 = document.getElementById(name + 'level1');
    var obj2 = document.getElementById(name + 'level2');
    var obj3 = document.getElementById(name + 'level3');
    var obj4 = document.getElementById(name + 'level4');

    if (obj1 && obj2 && obj3)
    {
        obj1.style.display = 'none';
        obj2.style.display = 'none';
        obj3.style.display = 'none';

        removeElement(obj1);
        removeElement(obj2);
        removeElement(obj3);

        if (obj4)
        {
            obj4.style.display = 'none';
            removeElement(obj4);
        }

        dialogs.pop();
        last_dialog_name = null;
    }

    var x = document.getElementsByTagName('input');
    if (isset(x[0]))x[0].focus();
    $("iframe[src='javascript:void(0)']").remove();
    $('.mceListBoxMenu, .mceDropDown').remove();

    if (typeof window.tinyMCE != 'undefined') window.tinyMCE.editors = [];

}
function isset(mixed_var){    // Determine whether a variable is set
    // 
    // +   original by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)

    var i = 0, argc = arguments.length, argv = arguments, set = true;

    for (i = 0; i < argc; i++)
    {
        if (argv[i] === undefined)
        {
            set = false;
            break;
        }
    }

    return set;
}
/**
 *    Создается полноэкранный визуальный редактор в модальном окне
 */
function fullscreen_w(w_name){
    var w = document.body.clientWidth - 90;
    var h = document.body.clientHeight - 120;

    displayMessage('/' + ROOT_PLACE + '/admin/fullscreen_w/' + w_name + '/' + (h - 72) + '/', w, h);
}

/**
 *    Отправим запрос на обновление дерева
 */
function update_tree(){
    doLoad('', '/' + ROOT_PLACE + '/ajax/update_tree/', 'tree_div');
}

/**
 *    Удаление объекта из DOM
 */
function discardElement(element){
    var garbageBin = document.getElementById('IELeakGarbageBin');
    if (!garbageBin)
    {
        garbageBin = document.createElement('DIV');
        garbageBin.id = 'IELeakGarbageBin';
        garbageBin.style.display = 'none';
        document.body.appendChild(garbageBin);
    }

    // move the element to the garbage bin
    garbageBin.appendChild(element);
    garbageBin.innerHTML = '';
    document.body.removeChild(garbageBin);
}

/**
 *    Обработка полученых данных из АЯКС обработчика
 */
function appendData(data_str, parent_node, mode){
    //вставляем получившийся текст с метками

    var target = typeof parent_node == 'object' ? parent_node : $('#' + parent_node);

    switch (mode)
    {

        case 'rewrite':
            target.html(data_str);
            break;

        case 'before':
            target.prepend(data_str);
            break;

        case 'after':
            target.append(data_str);
            break;

    }

    target.removeClass('ajaxLoading').trigger('update');
    $(window).trigger('updateBaronScrollers');
}
/**
 *    Запросы АЯКСА
 */
function doLoad(value, path, target, method, write_type, callback){

    var i;

    if (value != undefined)
    {

        if (typeof value == 'object')
        {

            value = $(value).serialize();

        }
        else
        {

            value = {'data': value};

        }

    }

    $.ajax({
        url: path,
        data: value,
        timeout: 5000,
        type: method || 'POST',
        dataType: 'json',

        beforeSend: function(){

            if (target.indexOf(";") == -1)
            {

                $('#' + target).addClass('ajaxLoading');

            }
            else
            {

                var targets = target.split(";");
                $.each(targets, function(i, value){
                    $('#' + value).addClass('ajaxLoading');
                });

            }

        }
    })
        .done(function(data){

            if (callback && typeof callback != 'undefined')
            {
                callback(data);
            }

            if (write_type == 'undefined' || !write_type || write_type == '') write_type = 'rewrite';

            if (target.indexOf(";") == -1)
            {

                appendData(data.content, target, write_type);

            }
            else
            {
                target = target.split(";");
                $.each(target, function(i, value){

                    appendData(data.content[i], value, write_type);

                });
            }

        })
        .always(function(data){

        })

        .fail(function(jqXHR, textStatus, errorThrown){
            //displayAlert(jqXHR.responseText);
            $('.ajaxLoading').removeClass('ajaxLoading');
        });
}

/**
 *    Сворот/разворот пунктов меню
 */

function in_array(val, array){
    for (var i = 0; i < array.length; i++)
    {
        if (array[i] == val)
        {
            return true;
            break;
        }
    }

    return false;
}

function addElement(val, array){
    array.push(val);

    return array;
}

function deleteElement(val, array){
    var newarray = new Array();

    for (var i = 0; i < array.length; i++)
    {
        if (val != array[i])
        {
            newarray.push(array[i]);
        }
    }

    return newarray;
}

function show_hide2(id, module){
    var achilds = new Array();

    /* Получение кукиса */
    var schilds = DHTMLSuite.commonObj.getCookie(module + "_sel_childs");
    if (schilds != "" && schilds != null)
    {
        if (schilds.indexOf(',') > -1)
        {
            achilds = schilds.split(',');
        }
        else achilds.push(schilds);
    }

    /* Обработа запроса */
    var obj = getObj(module + '_childs' + id);
    if (typeof obj != 'undefined')
    {
        if (obj.style.display == 'none')
        {
            if (!in_array(id, achilds))
            {
                achilds = addElement(id, achilds);
            }

            obj.style.display = '';
            img2 = 'minus.gif';
            img1 = 'plus.gif';
        }
        else
        {
            if (in_array(id, achilds))
            {
                achilds = deleteElement(id, achilds);
            }

            obj.style.display = 'none';
            img1 = 'minus.gif';
            img2 = 'plus.gif';
        }
    }

    var obj = getObj(module + '_colapsik' + id);
    if (typeof obj != 'undefined')
    {
        obj.src = obj.src.replace(img1, img2);
    }

    /* Сохранение кукиса */
    DHTMLSuite.commonObj.setCookie(module + "_sel_childs", achilds.join(','));
}

function show_hide_ajax(id, module, plus_minus){
    var achilds = new Array();

    if (plus_minus == 1) doLoad(id, '/control/ajax/unload_subtree/' + id + '/', 'tree_3', 'POST');
    else doLoad(id, '/control/ajax/load_subtree/' + id + '/', 'tree_3', 'POST');

    /* Получение кукиса */
    var schilds = DHTMLSuite.commonObj.getCookie(module + "_sel_childs");
    if (schilds != "" && schilds != null)
    {
        if (schilds.indexOf(',') > -1)
        {
            achilds = schilds.split(',');
        }
        else achilds.push(schilds);
    }

    /* Обработа запроса */
    var obj = getObj(module + '_childs' + id);
    if (typeof obj != 'undefined')
    {
        if (obj.style.display == 'none')
        {
            if (!in_array(id, achilds))
            {
                achilds = addElement(id, achilds);
            }

            obj.style.display = '';
            img2 = 'minus.gif';
            img1 = 'plus.gif';
        }
        else
        {
            if (in_array(id, achilds))
            {
                achilds = deleteElement(id, achilds);
            }

            obj.style.display = 'none';
            img1 = 'minus.gif';
            img2 = 'plus.gif';
        }
    }

    var obj = getObj(module + '_colapsik' + id);
    if (typeof obj != 'undefined')
    {
        obj.src = obj.src.replace(img1, img2);
    }

    /* Сохранение кукиса */
    DHTMLSuite.commonObj.setCookie(module + "_sel_childs", achilds.join(','));
}

/**
 * Проверка выбранности галочек в дереве в разделе управления шаблонами
 */
function setCheckNode(id, set_type){
    if (!set_type) set_type = -1;

    if (!tree_tpls) return

    for (var i = 0; i < tree_tpls.length; i++)
    {
        var child = tree_tpls[i];
        var childClassName = child.className;

        if (set_type == -1)
        {
            if ((childClassName == 'tpl_tree_child' + id))
            {
                var cb = child.getElementsByTagName('INPUT');
                var input = cb[0];

                if (!input.checked) input.checked = true;
                else input.checked = false;

                setCheckNode(child.getAttribute('path_id'), set_type);
            }
        }
        else
        {
            if (child.getAttribute('www_type') == set_type)
            {
                var cb = child.getElementsByTagName('INPUT');
                var input = cb[0];

                if (!input.checked) input.checked = true;
                else input.checked = false;

                //setCheckNode(child.getAttribute('path_id'), set_type);
            }
        }
    }
}

function findPosY(obj){
    var curtop = 0;

    while (obj)
    {
        curtop += obj.offsetTop;
        obj = obj.offsetParent;
    }

    return curtop;
}

function findPosX(obj){
    var curleft = 0;

    while (obj)
    {
        curleft += obj.offsetLeft;
        obj = obj.offsetParent;
    }

    return curleft;
}

/* Обработчик клавиш */
function showDown(evt){
    var ee = typeof evt != 'undefined' ? evt : (typeof event != 'undefined' ? event : null);

    /* Установка обработчика событий на клавиатуру, на ESC для закрытия окон */
    if (ee.keyCode == 27)
    {
        var cur_dialog = dialogs[dialogs.length - 1];

        if (cur_dialog)
        {
            if (cur_dialog.modalDialogName)
                closeCMSDialog(cur_dialog.modalDialogName);

            closeAjaxWorking();
        }
    }

    /* Перехват CTRL+S */
    if (ee.ctrlKey == true && ee.keyCode == 83)
    {
        displayAlert('Операция сохранения пока еще не реализована');
        cancelKey(ee);
        return true;
    }

    /* Переключение между вкладками у дерева ALT+1, ALT+2, ALT+3 */
    if (ee.altKey == true && (ee.keyCode == 49 || ee.keyCode == 50 || ee.keyCode == 51) && active_zone2reload == 'tree')
    {
        showLeftTab(ee.keyCode - 48);
        cancelKey(ee);
        return true;
    }

    /* Обработка F5 и перехват */
    if (ee.ctrlKey == true) return true;
    /*if (ee.keyCode == 116) {
     if (active_zone2reload == 'tree') {
     update_tree();

     last_opened_page = new Array();
     lastActType = '';
     action_doload_url = '';
     action_doload_target = '';
     }

     if (active_zone2reload == 'page' && last_opened_page.length > 0 && lastActType == 'openpage') {
     openPage(last_opened_page[0], last_opened_page[1], last_opened_page[2], last_opened_page[3], last_opened_page[4]);
     }

     if (active_zone2reload == 'page' && action_doload_url != '' && action_doload_target != '' && lastActType == 'doload') {
     doLoad('', action_doload_url, action_doload_target);
     }

     cancelKey(ee);
     }*/
}

/* Отмена пресса клавиши */
function cancelKey(evt){
    if (evt.preventDefault)
    {
        evt.preventDefault();
        return false;
    }
    else
    {
        evt.keyCode = 0;
        evt.returnValue = false;
    }
}

/* Инициализатор всего */
window.onload = function(){
    document.onkeydown = showDown;
}

// ################################################################################################# 
// #################################################################################################
// #################################################################################################
// #################################################################################################

/* CALLBACKS */

function callback_updateTree(params){

}

function callback_link2module(params){
    var contentFromOutput = 'fast_table_' + params.fromModule+'_linked',
        contentToOutput = 'fast_table_' + params.toModule+'_linked',
        $contentFromObject = $('#'+contentFromOutput),
        $fromRootObject = $('#center'),
        modulePageTemplate = '<div id="' + contentToOutput + '" class="fast_table_stack main" style="background: #fff;"></div>';

    if($contentFromObject.length) {
        $contentFromObject.before(modulePageTemplate);
        $contentFromObject.hide();
    } else {
        $fromRootObject.before(modulePageTemplate);
        $fromRootObject.hide();
    }

    doLoad('', params.fastcall, contentToOutput);
}

function callback_returnBackModule(params){
    var contentFromOutput = 'fast_table_' + params.fromModule+'_linked',
        contentToOutput = 'fast_table_' + params.toModule+'_linked',
        $contentFromObject = $('#'+contentFromOutput),
        $contentToObject = $('#'+contentToOutput),
        $fromRootObject = $('#center');

    if($contentFromObject.length) {
        $contentFromObject.remove();
        if($contentToObject.length) {
            $contentToObject.show();
        } else {
            $fromRootObject.show();
        }
    } else {
        $fromRootObject.show();
    }

    $(window).trigger('updateBaronScrollers');
}

function clearLinkPaths_and_loadQuickModule(module, output, title){

    $('.tapeModuleTopControls > .backButton').trigger('click');
    /* Меняем заголовок */
    var obj = getObj('title_page_cont');
    if (obj) obj.innerHTML = title;
    doLoad('', '/' + ROOT_PLACE + '/admin/clear_linkpath/' + module + '/' + output + '/', '', 'post', 'rewrite', callback_doLoad_quickModule);
}

function callback_doLoad_quickModule(params){

    doLoad('', params.fastcall, params.output);
}

// #################################################################################################
// #################################################################################################
// #################################################################################################
// #################################################################################################

/* FCK KeyEvents */

function DENOnKeyDownFunction(editorInstance){
    var clicker_save = getObj('save_' + editorInstance.Name);

    if (clicker_save)
    {
        //alert('save');
        clicker_save.onclick();
    }
}

function DenIE_OnKeyDown(editorInstance){
    var e = editorInstance.EditorWindow.event;
    var alreadyRun = false;

    if ((e.keyCode == 83 && e.ctrlKey == true) || (e.keyCode == 115 && e.ctrlKey == true) || (e.keyCode == 13 && e.ctrlKey == true))
    {
        e.cancelBubble = true;
        e.returnValue = false;

        if (alreadyRun == false)
        {
            DENOnKeyDownFunction(editorInstance);
            alreadyRun = true
        }

        return false;
    }
}

var DenGecko_OnKeyDown = function(e, editorInstance){
    var alreadyRun = false;

    /* CTRL+s | CTRL+S */

    if ((e.which == 83 && e.ctrlKey == true) || (e.which == 115 && e.ctrlKey == true) || (e.which == 13 && e.ctrlKey == true))
    {
        e.preventDefault();
        e.stopPropagation();

        if (alreadyRun == false)
        {
            DENOnKeyDownFunction(editorInstance);
            alreadyRun = true
        }

        return false;
    }

    return true
};

function FCKeditor_OnComplete(editorInstance){
    if (editorInstance.Name.indexOf('content') > -1)
    {
        if (document.all)
        {
            editorInstance.EditorDocument.attachEvent("onkeydown", function(){
                DenIE_OnKeyDown(editorInstance);
            });
        }
        else
        {
            editorInstance.EditorDocument.addEventListener("keypress", function(e){
                DenGecko_OnKeyDown(e, editorInstance);
            }, true);
        }
    }
}

function replaceDiv(div){
    if (editor)
        editor.destroy();

    editor = CKEDITOR.replace(div);
}

function createEditor(){
    if (editor)
        return;

    var html = document.getElementById('editorcontents').innerHTML;

    // Create a new editor inside the <div id="editor">
    editor = CKEDITOR.appendTo('editor');
    editor.setData(html);

    // This sample may break here if the ckeditor_basic.js is used. In such case, the following code should be used instead:
    /*
     if ( editor.setData )
     editor.setData( html );
     else
     CKEDITOR.on( 'loaded', function()
     {
     editor.setData( html );
     });
     */
}

function removeEditor(){
    if (!editor)
        return;

    // Retrieve the editor contents. In an Ajax application, this data would be
    // sent to the server or used in any other way.
    document.getElementById('editorcontents').innerHTML = editor.getData();

    // Destroy the editor.
    editor.destroy();
    editor = null;
}

function toCode(obj){

    var stringFormat = Site.translitUrl(obj.value),
        toObj = $('#code');

    if (!toObj.hasClass('conf') || toObj.val() == '')
    {
        toObj.val(stringFormat);
    }

}

function codeChange(obj, origValue){

    if(!$(obj).hasClass('conf')) return;

    if (origValue !== obj.value)
    {
        if (confirm('Вы уверены, что хотите изменить символьный код? Это сделает старый URL недоступным'))
        {
            obj.value = Site.translitUrl(obj.value);
        }
        else
        {
            obj.value = Site.translitUrl(origValue);
        }
    }
    else
    {
        obj.value = Site.translitUrl(obj.value);
    }

}
	