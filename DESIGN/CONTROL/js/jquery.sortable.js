/*
 * HTML5 Sortable jQuery Plugin
 * http://farhadi.ir/projects/html5sortable
 *
 * Copyright 2012, Ali Farhadi
 * Released under the MIT license.
 */
(function($) {
    var dragging, placeholders = $();
    $.fn.sortable = function(options) {
        var method = String(options),
            draggingOuterHeight;
        options = $.extend({
            connectWith: false
        }, options);
        return this.each(function() {

            var parent = $(this);

            if (/^enable|disable|destroy$/.test(method)) {
                var items = $(this).children($(this).data('items')).attr('draggable', method == 'enable');
                if (method == 'destroy') {
                    items.add(this).removeData('connectWith items')
                        .off('dragstart.h5s dragend.h5s selectstart.h5s dragover.h5s dragenter.h5s drop.h5s');
                }
                return;
            }
            var isHandle, index, items = $(this).children(options.items);
            var placeholder = $('<' + (/^ul|ol$/i.test(this.tagName) ? 'li' : 'div') + ' class="sortable-placeholder">');
            items.find(options.handle).mousedown(function() {
                isHandle = true;
            }).mouseup(function() {
                    isHandle = false;
                });
            $(this).data('items', options.items)
            placeholders = placeholders.add(placeholder);
            if (options.connectWith) {
                $(options.connectWith).add(this).data('connectWith', options.connectWith);
            }
            items.attr('draggable', 'true').on('dragstart.h5s', function(e) {

                e.stopPropagation();

                if (options.handle && !isHandle) {
                    return false;
                }
                isHandle = false;
                var dt = e.originalEvent.dataTransfer;
                dt.effectAllowed = 'move';
                dt.setData('Text', 'dummy');
                index = (dragging = $(this)).addClass('sortable-dragging').index();

                draggingOuterHeight = dragging.outerHeight();

                $(parent).addClass('dragging');

            }).on('dragend.h5s', function() {
                    if (!dragging) {
                        return;
                    }
                    dragging.removeClass('sortable-dragging').attr('style', '');
                    placeholders.detach();
                    if (index != dragging.index()) {
                        dragging.parent().trigger('sortupdate', [dragging]);
                    }
                    dragging = null;

                    $(parent).removeClass('dragging');

                }).not('a[href], img').on('selectstart.h5s', function() {
                    this.dragDrop && this.dragDrop();
                    return false;
                }).end().add([this, placeholder]).on('dragover.h5s dragenter.h5s drop.h5s', function(e) {
                    if (!items.is(dragging) && options.connectWith !== $(dragging).parent().data('connectWith')) {
                        return true;
                    }
                    if (e.type == 'drop') {
                        e.stopPropagation();
                        placeholders.filter(':visible').after(dragging);
                        dragging.trigger('dragend.h5s');
                        return false;
                    }

                    dragging.trigger('dragProcess.h5s',e);

                    e.preventDefault();
                    e.originalEvent.dataTransfer.dropEffect = 'move';

                    var center = Math.round($(this).offset().top + $(this).height() / 2);

                    if (items.is(this) && (e.originalEvent.pageY > center - 10 && e.originalEvent.pageY < center + 10)) {

                        if (options.forcePlaceholderSize) {
                            placeholder.height(draggingOuterHeight);
                        }

                        dragging.css({
                            visibility: 'hidden',
                            position: 'absolute'
                        });

                        $(this)[placeholder.index() < $(this).index() ? 'after' : 'before'](placeholder);
                        placeholders.not(placeholder).detach();
                    } else if (!placeholders.is(this) && !$(this).children(options.items).length) {
                        placeholders.detach();
                        $(this).append(placeholder);
                    }
                    return false;
                });
        });
    };
})(jQuery);