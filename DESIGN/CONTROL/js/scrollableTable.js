/**
 * User: Andruha
 * Date: 17.05.13
 * Time: 17:42
 */
var Site = Site || {};

(function (global, window, $, undefined) {
    'use strict';

    function ScrollableTable(object, settings){

        var defaultSettings = {

        };

        this.settings = $.extend(defaultSettings, settings);

        this.object = object;
        this.init.call(this);

    }

    ScrollableTable.prototype.init = function(){

        this.thead = $('thead', this.object);

        this.initEmptyThead.call(this);

        $(window).on('resize.scrollableTable baronInited.scrollableTable updateBaronScrollers.scrollableTable allImagesLoaded.scrollableTable', this.onResize.bind(this));
        this.onResize.call(this);

        this.object.addClass('init');

    };

    ScrollableTable.prototype.onResize = function(){

        var tableWidth = this.object.width(),
            theadHeight = this.thead.height();

        this.thead.width(tableWidth);
        //this.object.css('padding-top', theadHeight);

        this.setTheadSize.call(this);
    };

    ScrollableTable.prototype.initEmptyThead = function(){

        var emptyHead = this.thead.clone().addClass('emptyThead'),
            th = emptyHead.find('th');

        this.thead.before(emptyHead).css('position', 'absolute');

        this.emptyHeadTH = th;

    };

    ScrollableTable.prototype.setTheadSize = function(){

        var theads = $('th', this.thead);

        this.emptyHeadTH.each(function(key, value){

            theads.eq(key).innerWidth($(value).innerWidth());

        }.bind(this));

    };

    $(function(){

        $.fn.initScrollableTable = function (settings) {

            $.each(this, function () {
                new ScrollableTable($(this), settings);
            });
        };

        $(document).on('ready.scrollableTable ajaxSuccess.scrollableTable', function(event, xhr, settings) {

            $('.scrollableTable').not('.init').initScrollableTable();

        });



    });

})(Site, window, jQuery);