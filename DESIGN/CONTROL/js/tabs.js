/**
 * User: Andruha
 * Date: 28.03.13
 * Time: 13:00
 */
var Site = Site || {};

(function (global, window, $, undefined) {
    'use strict';

    function Tabs(container, tabsSelector) {

        tabsSelector = tabsSelector || '.tabContainer';

        this.container = $(container || window.document);

        this.container.on('click', tabsSelector + ' .tabs li', this.onClick.bind(this));
        this.container.on('deactivate', tabsSelector + ' .tabs li', this.onDeactivate.bind(this));

    }

    Tabs.prototype.onClick = function (event) {
        var currentTarget = $(event.currentTarget),
            activeItem = $('.active', currentTarget.parent('ul')),
            tabContainer = $('#' + currentTarget.data('target'));

        activeItem.trigger('deactivate');

        tabContainer.addClass('active');
        currentTarget.addClass('active');


    };

    Tabs.prototype.onDeactivate = function (event) {
        var currentTarget = $(event.currentTarget),
            tabContainer = $('#' + currentTarget.data('target'));

        tabContainer.removeClass('active');
        currentTarget.removeClass('active');

        setTimeout(function(){
            $(window).trigger('updateBaronScrollers');
            $(window).trigger('updateBaronScrollers');
        }, 1);

    };

    $(function () {
        global.tabs = new Tabs;
    });

})(Site, window, jQuery);