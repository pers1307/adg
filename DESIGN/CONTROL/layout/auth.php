<?php
    /**
     * User: Andruha
     * Date: 25.03.13
     * Time: 10:43
     *
     * @var $_CONTENT_ контент зоны вывода «_CONTENT_»
     */
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="<?php echo SITE_ENCODING ?>">
    <title><?php echo MSCore::page()->title_page ?></title>
    <?php echo StaticFiles::getCSS('admin-general-styles', COMPRESS_CSS) ?>
    <link rel="icon" href="/DESIGN/CONTROL/images/favicon.ico" type="image/x-icon">
    <link rel="shortcut icon" href="/DESIGN/CONTROL/images/favicon.ico" type="image/x-icon">

    <script>
        var Site = Site || {},
            ROOT_PLACE = Site.ROOT_PLACE = '<?php echo ROOT_PLACE ?>';
    </script>

</head>
<body>

<?php echo $_CONTENT_ ?>

<?php echo StaticFiles::getJS('admin-general-scripts', COMPRESS_JS) ?>
</body>
</html>