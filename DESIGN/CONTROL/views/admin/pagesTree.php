<?php
/**
 * User: Andruha
 * Date: 28.03.13
 * Time: 11:27
 */

isset($items) or $items = array();

?>
<div class="leftMenu treeMenu baron-scroll">
    <?php if (count($items)) { ?>
        <ul>
            <?php
            foreach($items as $item){
                echo template('admin/pagesTreeItem', array(
                    'item' => $item,
                    'firstLevel' => true,
                    'countItems' => count($items)
                ));
            }
            ?>
        </ul>
    <?php } ?>
</div>