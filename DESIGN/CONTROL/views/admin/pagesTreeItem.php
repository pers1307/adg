<?php
    /**
     * User: Andruha
     * Date: 29.03.13
     * Time: 10:58
     */

    if (empty($item))
    {
        return '';
    }
    isset($firstLevel) or $firstLevel = null;
    isset($countItems) or $countItems = 0;
?>
<li id="<?= $item['type'] . $item['id'] ?>">
    <span class="<?php echo !$item['visible'] ? ' invisible' : '' ?><?php echo isset($_SESSION['PagesTreeActive']) && $_SESSION['PagesTreeActive'] == $item['id'] ? ' wasActive' : '' ?>">
        <?php if (!empty($item['moveAccess']) && empty($firstLevel) && $item['type'] == 'page' && $countItems > 1)
        { ?><span class="icon-move move-handler" title="Переместить"></span><?php } ?>
        <?php if (empty($firstLevel) && $item['type'] == 'page' && !empty($item['children']))
        { ?>
            <i class="icon-sort-<?= $item['openChild'] ? 'up' : 'down' ?>" data-item-id="<?= $item['id'] ?>"><span></span></i><?php
        }
        ?><?php if ($item['type'] == 'page') {
        ?><a href="#" data-id="<?= $item['id'] ?>" title="<?php echo $item['infoLink'] ?>"><?php } ?>

            <?php if (!empty($item['icon']))
            { ?>
                <i class="<?= $item['icon'] ?>"></i>
            <?php } ?>

            <?php echo $item['title'] ?>

            <?php if ($item['type'] == 'page') { ?></a><?php } ?>

        <?= $item['controls'] ?>
    </span>
    <?php if (!empty($item['children']))
    { ?>
        <ul<?php echo !empty($item['openChild']) ? ' class="visible"' : '' ?>>
            <?php
                foreach ($item['children'] as $child)
                {
                    echo template('admin/pagesTreeItem', array(
                        'item' => $child,
                        'countItems' => count($item['children'])
                    ));
                }
            ?>
        </ul>
    <?php } ?>
</li>