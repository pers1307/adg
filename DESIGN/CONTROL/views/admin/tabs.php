<?php
/**
 * User: Andruha
 * Date: 27.03.13
 * Time: 17:36
 */

isset($items) or $items = array();

if (count($items)) {

    ?>
    <div class="tabContainer">
        <ul class="tabs">
            <?php
            foreach ($items as $item) {
                ?><li<?=$item['attributes']?>><?=$item['title']?></li><?php
            }
            ?>
        </ul>
        <?php
        foreach ($items as $item) {

            ?><div id="<?=$item['id']?>" class="tabBlock<?=$item['active'] ? ' active' : ''?>"><?=$item['content']?></div><?php
        }
        ?>
    </div>
<?php

}