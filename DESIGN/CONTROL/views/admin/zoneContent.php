<?php
    /**
     * User: Andruha
     * Date: 08.04.13
     * Time: 11:38
     */


    $edit_action = "displayMessage('/" . ROOT_PLACE . "/admin/zone_wysiwyg_message/" . $zone . "/" . $info['path_id'] . "/'); return false;";
?>
<div class="zoneContent-wrapper<?=AUTH_IS_ROOT ? ' controls' : ''?>">
    <div class="zoneContent-content-wrapper" ondblclick="<?= $edit_action ?>">
        <div id="td_content<?= $zone ?>" class="zoneContent-content baron-scroll">
            <div id="content_div_<?= $zone ?>_2">
                <?= strip_tags(html_entity_decode(MSCore::db()->getOne('SELECT `content` FROM `' . PRFX . 'zones_content` WHERE `zone_id`="' . $zone . '" AND `path_id`=' . (int)$info['path_id']), ENT_QUOTES), '<img><p><h1><h2><h3><h4><h5><h6><span><div><br><b><i><small><strong><table><tr><td><tbody><thead><ol><ul><li>'); ?>
            </div>
        </div>
        <i title="Редактировать" class="icon-pencil"></i>
    </div>

    <?php
        if (AUTH_IS_ROOT)
        {
            ?>

            <div class="zoneContent-controls">
                <table width="100%">
                    <tr>
                        <? if (AUTH_IS_ROOT)
                        {
                            ?>
                            <td width="84" align="left">
                                <button class="grey" onclick="displayMessage('/<?= current_path('/admin/structure/add_module/' . $info['path_id'] . '/' . $zone) ?>', 500, 400); return false;">Модуль</button>
                            </td>
                            <td width="60" align="left">
                                <button class="grey" onclick="displayMessage('/<?= current_path('/admin/structure/add_block/' . $info['path_id'] . '/' . $zone . '/' . $pathArray[0]) ?>',600,400); return false;">Блок</button>
                            </td>
                            <td width="250" id="zone_save_report">&nbsp;</td>
                            <td>&nbsp;</td>

                            <td>
                                <div id="<?= $info['path_id'] . $zone ?>">

                                    <?
                                        if (isset($info['config'][$zone]) && sizeof($info['config'][$zone]))
                                        {

                                            $info['config'][$zone] = array_values($info['config'][$zone]);
                                            ?>
                                            <table>
                                                <?
                                                    foreach ($info['config'][$zone] as $key => $data)
                                                    {
                                                        ?>
                                                        <tr style="clear:both;">
                                                            <td width="5">
                                                                <a href="#" title="Удалить блок" onclick="doLoad('<?= $key ?>','/<?= ROOT_PLACE; ?>/ajax/delete_script/<?= $info['path_id'] ?>/<?= $key ?>/<?= $zone ?>/','<?= $info['path_id'] . $zone ?>'); return false;" class="delete"><i class="icon-trash"></i></a>
                                                            </td>
                                                            <td width="5">
                                                                <a href="#" title="Переместить вверх" onclick="doLoad('<?= $key ?>','/<?= ROOT_PLACE; ?>/ajax/swap_scripts/<?= $info['path_id'] ?>/<?= $key ?>/up/<?= $zone ?>/','<?= $info['path_id'] . $zone ?>'); return false;" class="up"><i class="icon-chevron-up"></i></a>
                                                            </td>
                                                            <td width="5">
                                                                <a href="#" title="Переместить вниз" onclick="doLoad('<?= $key ?>','/<?= ROOT_PLACE; ?>/ajax/swap_scripts/<?= $info['path_id'] ?>/<?= $key ?>/down/<?= $zone ?>/','<?= $info['path_id'] . $zone ?>'); return false;" class="down"><i class="icon-chevron-down"></i></a>
                                                            </td>
                                                            <td title="<?= $data['script']; ?>" style="cursor: pointer">
                                                                <?= getScriptName($data['script']) ?>
                                                            </td>
                                                        </tr>
                                                    <? } ?>
                                            </table>
                                        <? } ?>
                                </div>
                            </td>
                        <? } ?>
                        <td></td>
                    </tr>
                </table>
            </div>
        <?php
        }
    ?>
</div>