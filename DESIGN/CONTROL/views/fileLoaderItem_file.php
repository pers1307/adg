<?php
/**
 * User: Andruha
 * Date: 20.06.13
 * Time: 13:48
 */

    $itemData = array_merge(array(
		'text' => ''
    ), $itemData);

	isset($showLabel) or $showLabel = true;
?>
<div class="fileLoader-item fileLoader-itemFile clearfix">
    <span class="icon-paper-clip"></span>
    <span class="fileLoader-item-fileSize"><?php echo $itemData['fileSize'] ?></span>
    <span class="fileLoader-item-fileName"><?php echo $itemData['fileName'] ?></span>
	<?php
	if ($showLabel)
	{
		?>
		<label class="fileLoader-item-text">
			<input type="hidden" name="<?php echo $sysName ?>[text]" value="<?php echo safe($itemData['text']) ?>">
		</label>
	<?php
	}
	?>
    <span class="fileLoader-deleteItem">
        <i class="icon-trash" title="Удалить"></i>
    </span>
    <input type="hidden" name="<?php echo $sysName ?>[id]" value="<?php echo $itemData['id'] ?>">
</div>