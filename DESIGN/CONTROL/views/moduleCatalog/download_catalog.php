<form id="add" enctype="multipart/form-data">
<div id="div_inserted_id">
<input id="inserted_id" type="hidden" name="id" value="<?=$id?>">
</div>
    <div class="form-wrap">
        <div class="table">

            <STYLE type="text/css">
                .download-button {
                    display: block;
                    margin-left: auto;
                    margin-right: auto;
                    border: 0;
                    color: #fff;
                    padding: 7px 15px 8px;
                    background: linear-gradient(#f1503f, #cc1712);
                    font-family: inherit;
                    font-size: 100%;
                    outline: none;
                    text-decoration: none;
                    width: 200px;
                    height: 20px;
                }

                .download-button span {
                    position: absolute;
                    left: 39%;
                }
            </STYLE>

            <a class="download-button" download href="<?= $pathToFile ?>"><span>Сохранить файл с каталогом</span></a>
        </div>
    </div>
</form>