<div class="treeMenu catalogModuleWrap-articles-label">
    <span>Набор характеристик <?= $controls ?></span>
</div>
<div class="catalogModuleWrap-articles-wrap baron-scroll treeMenu">
    <ul>
        <li>
            <?php echo empty($articles_html) ? '<div>Нет характеристик</div>' : $articles_html; ?>
        </li>
    </ul>
</div>
<script>$('.catalogModuleWrap-articles-wrap .wasActive a').trigger('setActive');</script>