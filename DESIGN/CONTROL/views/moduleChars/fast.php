<?php

$d_articles_width = $CONFIG['tables']['articles']['dialog']['width'];
$d_articles_height = $CONFIG['tables']['articles']['dialog']['height'];

echo MSCore::forms()->writeModule_LinksPath();
?>

<div class="catalogModuleWrap">
    <div id="articles" class="catalogModuleWrap-articles">

        <?=$articles;?>

    </div>
    <script>
        $(function(){
            new Site.treeMenu($('#articles'), 'catalog<?=$path_id?>_sel_childs');
            $('.catalogModuleWrap-articles-wrap a:first').trigger('click');
        });
    </script>
    <!--div id="items" class="catalogModuleWrap-items"></div-->
</div>