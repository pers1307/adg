<?php
/**
 * User: Andruha
 * Date: 09.04.13
 * Time: 11:30
 */

isset($data) or $data = '';

if (!empty($items)) {
    ?>
    <i class="icon-cog module-controls-wrap">
        <span class="module-controls-menu" <?=$data?>><?php foreach ($items as $action => $control) { ?>
            <span data-action="<?= $action ?>"<?= !empty($control['class']) ? ' class="' . $control['class'] . '"' : '' ?><?=!empty($control['events']) ? $control['events'] : ''?>>
                <i class="<?= $control['icon'] ?>" title="<?= $control['title'] ?>"></i>
            </span>
            <?php } ?>
        </span>
    </i>
<?php
}
?>