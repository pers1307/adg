<h3>Импорт каталога компрессоров из excel. Формат файла .xls, .xlsx.<br> Пример файла можно поулчить, сделав экспорт каталога в разделе "Компрессоры"</h3>
<form id="upload" enctype="multipart/form-data">
	<input type="file" name="file" accept=".xls, .xlsx">
</form>
<br>
<button href="#" onclick="uploadFile()">Загрузить файл</button>
<div id="upload_result"></div>
<div id="upload_errors"></div>
<script>
	function uploadFile(){
		var input = $('#upload input[type=file]');
		var fd = new FormData;
		var content = $('#upload_result');
		var errors = $('#upload_errors');

		fd.append('compressors', input.prop('files')[0]);

		$.ajax({
			url: '/<?= ROOT_PLACE ?>/<?= $config['module_name'] ?>/upload/',
			data: fd,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (data) {
				if (data.error) {
					errors.html(data.error);
					content.html('');
				} else {
					content.html(data.content);
				}
			},
			beforeSend: function () {
				errors.html('');
				if (input.prop('files')[0] !== undefined) {
					content.html('Загружаем данные...');
				} else {
					content.html('');
				}
			}
		});
	}
</script>