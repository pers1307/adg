<?php
    /**
     * @var string $_FORM_
     * @var array $CONFIG
     */
?>
<form id="add" enctype="multipart/form-data">
    <div class="form-wrap"><?=$_FORM_?></div>
</form>

<script>
    function postForm() {
        doLoad(getObj('add'), '/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add/<?=(int)$path_id;?>/<?= $moduleName ?>/', 'div_inserted_id');
    }

</script>
<div class="form-bottom-controls">
    <ul>
        <li>
            <button onclick="doLoad(getObj('add'),'/<?= ROOT_PLACE; ?>/<?= $CONFIG['module_name']; ?>/add/<?= (int)$path_id; ?>/<?= $moduleName ?>/', 'div_inserted_id', null, 'rewrite'); Site.modal.close()">Сохранить</button>
        </li>
        <li>
            <button onclick="doLoad(getObj('add'),'/<?= ROOT_PLACE; ?>/<?= $CONFIG['module_name']; ?>/add/<?= (int)$path_id; ?>/<?= $moduleName ?>/', 'div_inserted_id');" class="grey">Применить</button>
        </li>
        <li>
            <button class="grey" onclick="Site.modal.close()">Отменить</button>
        </li>
    </ul>
</div>
