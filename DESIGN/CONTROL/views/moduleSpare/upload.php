<form id="upload" enctype="multipart/form-data">
	<div class="form-wrap">
		<div class="table">
			<div>
				<input type="file" name="file" accept=".xls, .xlsx">
			</div>
		</div>
		<div id="upload_result"></div>
	</div>
</form>
<script>
	function uploadFile(){
		var input = $('#upload input[type=file]');
		var fd = new FormData;
		var resultBlock = $('#upload_result');

		fd.append('spare', input.prop('files')[0]);

		$.ajax({
			url: '/<?= ROOT_PLACE ?>/<?= $CONFIG['module_name'] ?>/upload/<?= $path_id ?>/<?= $parent ?>/<?= $page ?>/',
			data: fd,
			processData: false,
			contentType: false,
			type: 'POST',
			success: function (data) {
				if (data.error) {
					resultBlock.addClass('error').html(data.error);
				} else {
					resultBlock.removeClass('error').html(data.resultText);
					$('#items').html(data.content);
					setTimeout(function() {
						Site.modal.close();
					}, 1500);
				}
			},
			beforeSend: function () {
				if (input.prop('files')[0] !== undefined) {
					resultBlock.removeClass('error').html('Загружаем данные...');
				} else {
					resultBlock.addClass('error').html('Необходимо выбрать файл');
					return false;
				}
			}
		});
	}
</script>
<div class="form-bottom-controls">
	<ul>
		<li>
			<button onclick="uploadFile()">Импортировать</button>
		</li>
		<li>
			<button class="grey" onclick="Site.modal.close()">Отменить</button>
		</li>
	</ul>
</div>