<?php

// Если блок прикреплен самостоятельно и он новый, то нужно делать проверку на 404
if (!empty(MSCore::urls()->vars[0])) {
    page404();
}

$model = new MSBaseTape([
    'itemsTableName' => '{manufacture}',
    'pathId' => MSCore::page()->path_id,
    'path' => MSCore::page()->path,
]);

echo template('about/manufacture', $model->getItems());