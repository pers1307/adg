<?php

$model = new MSBaseTape([
    'itemsTableName' => '{advantages}',
    'pathId' => null,
    'pagination' => false,
    'order' => '`order`',
]);

$data = $model->getItems();
$data['title'] = 'Преимущества наших генераторов';
echo template('advantages/widget', $data);