<?php

if (isset($_GET['generators']) && $_GET['generators'] != '1') {
    page404();
}

$modelSettings = MSCore::settings('engines', MSCore::page()->path_id);

$addressModel = new MSBaseTape([
    'itemsTableName' => '{address}'
]);

$phoneStock = $addressModel->getItems()['items']['1']['phone'];

$model = new EngineModel([
        'pathId' => MSCore::page()->path_id,
        'vars' => MSCore::urls()->vars,
        'onlyHaveItems' => false,
        'articlesTreeFields' => ['text', 'image'],
        'itemsOrder' => '`mainPowerKw` ASC',
        'pagination' => [
            'onPage' => $modelSettings->onPage,
        ]
    ]
);

$content = $_CONTENT_;
$_CONTENT_ = '';

if ($model->isCatalogIndexPage) {
    $model->pagination['linkMask'] = $model->path . '/%link%/';

    $data = $model->getItems(null, false);
    $data['content'] = $content;
    $data['content2'] = MSCore::page()->getZoneContent('_CONTENT2_');

    echo template('catalog/common', [
        'leftAside' => template('catalog/leftAside', [
            'content' => template('catalog/engines/leftMenu', [
                'model' => $model,
            ])
        ]),
        'content' => template('catalog/engines/index', $data)
    ]);
} else {
    $article = $model->getArticle(['*'], [
        'fields' => ['*'],
        'recursive' => true
    ]);

    if ($model->isItem === false) {

        if ($article !== false) {

        	//Убираю подкачку мета-данных для основной страницы каталога
            //$article['article'] = HelperSeoDieselEngines::transform($article['article']);
	        
	        if(isset($article['article']) && $article['article']['code'] == getParam(0))           {
	        	$brandName = $article['article']['name'];
		        $brandTitle = 'Дизельные двигатели '.$brandName.' для электростанций в Екатеринбурге';
		        $brandDescr = 'Продажа дизельных двигателей '.$brandName.' для дизельных генераторов с доставкой по России и странам СНГ. Уточняйте подробности по телефону 8 800 775 65 10 - Звонок по России бесплатный';
		        $brandKey = 'Дизельные двигатели '.$brandName.', продажа, характеристики, цена, купить';

		        if($article['article']['title_page'] == '') {
			        $article['article']['title_page'] = $brandTitle;
		        }

		        if($article['article']['meta_description'] == '') {
			        $article['article']['meta_description'] = $brandDescr;
		        }

		        if($article['article']['meta_keywords'] == '') {
			        $article['article']['meta_keywords'] = $brandKey;
		        }
	        }

            HelperSeo::addAllSeo($article['article']);

            echo template('catalog/common', [
                'leftAside' => template('catalog/leftAside', [
                    'content' => template('catalog/engines/leftMenu', [
                        'model' => $model,
                        'menuTitle' => $modelSettings->menuTitle
                    ])
                ]),
                'content' => template('catalog/engines/list', $article)
            ]);
        } else {
            page404();
        }
    } else {
        if (($item = $model->getItem()) !== false) {
            $item = array_merge($item, $model::getProperties($item['item']));

            $item['showImage'] = true;

            $item['tabs'] = $model->getTabs($item['item']);
            foreach ($item['tabs'] as $key => &$data) {
                if ($data['active'] != true) {
                    continue;
                }

                if ($key == 0) {
                	//убираю подкачку мета-данных для чего-то
                    //$item['item'] = HelperSeoDieselEngineCard::transform($item['item']);
                    $data['content'] = template(
                        'catalog/engines/tabs/characteristics',
                        ['characteristics' => $item['characteristics']]
                    );
                }

                if (
                    $key == 1
                    && $model->isParts == true
                ) {
                    $modelSpareParts = new ModelSpareParts();

                    $data['content'] = template(
                        'catalog/engines/tabs/parts',
                        [
                            'parts' => $modelSpareParts->getFormatParts(),
                            'model' => $modelSpareParts
                        ]
                    );

                    $item['item'] = $model->replaceMetaForParts(
                        $item['item'],
                        $modelSettings
                    );

                    $item['item'] = PowerStationForPartEngine::transform($item['item'], $article['article'], $phoneStock);
                }

                if ($key == 2) {
                    $item['showImage'] = false;

                    $powerStationsModel = new PowerStationModel([
                            'pathId' => 188,
                            'vars' => MSCore::urls()->vars,
                            'onlyHaveItems' => false,
                            'articlesTreeFields' => ['*'],
                            'itemsOrder' => '`order` ASC'
                        ]
                    );
                    $engineModel = new EngineModel();

                    $statuses = MSConfig::get('common/config:ps_statuses');
                    $variants = VariantModel::getVariants();

                    $articles = $powerStationsModel->getArticlesFormat($engineModel, [], $item['item']['id']);
                    $item['item'] = PowerStationForDieselEngine::transform($item['item'], $article['article'] , $articles, $phoneStock);

                    $data['content'] = template('catalog/power_stations/enginesContent', [
                        'model'       => $powerStationsModel,
                        'engineModel' => $model,
                        'content'     => '',
                        'content2'    => '',
                        'statuses'    => $statuses,
                        'variants'    => $variants,
                        'articles'    => $articles,
                    ]);
                }
            }

            //For left menu
            $model->itemsTable->unsetFilter('keyFilter');
            $model->pagination = false;

            if(!isset($_GET['generators'])) {
            	if('' == $item['item']['title_page']) {
		            $item['item']['title_page'] = 'Дизельный двигатель '.$item['item']['title'].': Технические характеристики';
	            }

	            if('' == $item['item']['meta_keywords']) {
		            $item['item']['meta_keywords'] = ' Дизельный двигатель '.$item['item']['title'].', характеристики, продажа, цена, купить';
	            }

	            if('' == $item['item']['meta_description']) {
		            $item['item']['meta_description'] = 'Продажа дизельного двигателя '.$item['item']['title'].' для дизельных  генераторов с доставкой по России и странам СНГ. Уточняйте подробности  по телефону 8 800 775 65 10 - Звонок по России бесплатный';
	            }
            }

	        if (isset($_GET['generators'])) {
		        $tempArr = [];
				foreach($articles as $k=>$v) {
					$tempArr[] = $articles[$k]['name'];
				}

				if(count($tempArr) == 1) {
					$tempArr = $tempArr[0];
				} elseif(count($tempArr) > 1) {
					$tempArr = implode(', ', $tempArr);
				} else {
					$tempArr = '';
				}

		        $item['item']['title_h1'] = 'Дизельные электростанции с двигателем '.$item['item']['title'];
	            $item['item']['title_page'] = 'Дизельные генераторы с двигателем '.$item['item']['title'];
		        $item['item']['meta_keywords'] = 'Дизельные генераторы на базе двигателя '.$item['item']['title'].', цена, купить, Екатеринбург';
		        $item['item']['meta_description'] = 'На базе дизельного двигателя '.$item['item']['title'].' собираются дизельные электростанции : '.$tempArr.'. Подробности по телефону 8 800 775 65 10';

		        unset($tempArr);
	        }

            HelperSeo::addAllSeo($item['item']);
            echo template('catalog/common', [
                'leftAside' => template('catalog/leftAside', [
                    'content' => template('catalog/engines/leftMenuItem', [
                        'model' => $model,
                        'menuTitle' => $modelSettings->menuTitle
                    ])
                ]),
                'content' => template('catalog/engines/item', $item)
            ]);
        } else {
            page404();
        }
    }
}