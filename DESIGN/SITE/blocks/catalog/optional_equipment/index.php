<?php
$modelSettings = MSCore::settings('optional_equipment', MSCore::page()->path_id);

$model = new OptionalEquipmentModel([
        'pathId' => MSCore::page()->path_id,
        'vars' => MSCore::urls()->vars,
        'onlyHaveItems' => false,
        'articlesTreeFields' => ['text', 'image'],
        'itemsOrder' => '`order` ASC',
        'pagination' => [
            'onPage' => $modelSettings->onPage,
        ]
    ]
);

$content = $_CONTENT_;
$_CONTENT_ = '';

if ($model->isCatalogIndexPage) {
    $model->pagination['linkMask'] = $model->path . '/%link%/';
    $data = $model->getItems(null, false);
    $data['content'] = $content;
    $data['content2'] = MSCore::page()->getZoneContent('_CONTENT2_');

    echo template('catalog/common', [
        'leftAside' => template('catalog/leftAside', [
            'content' => template('catalog/optional_equipment/leftMenu', [
                'model' => $model,
            ])
        ]),
        'content' => template('catalog/optional_equipment/index', $data)
    ]);
} else {
    $article = $model->getArticle(['*'], [
        'fields' => ['*'],
        'recursive' => true,
        'withLimit' => true

    ]);

    if ($model->isItem === false) {

        if ($article !== false) {
            HelperSeo::addAllSeo($article['article']);

            echo template('catalog/common', [
                'leftAside' => template('catalog/leftAside', [
                    'content' => template('catalog/optional_equipment/leftMenu', [
                        'model' => $model,
                    ])
                ]),
                'content' => template('catalog/optional_equipment/list', $article)
            ]);
        } else {
            Page404();
        }
    } else {

        if (($item = $model->getItem()) !== false) {
            HelperSeo::addAllSeo($item['item']);

            echo template('catalog/common', [
                'leftAside' => template('catalog/leftAside', [
                    'content' => template('catalog/optional_equipment/leftMenuItems', [
                        'menuItems' => $article['items'],
                        'currentItem' => $item['item'],
                        'menuTitle' => $article['article']['name'],
                    ]),
                ]),
                'content' => template('catalog/optional_equipment/item', $item)
            ]);
        } else {
            Page404();
        }
    }
}