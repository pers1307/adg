<?php
$modelSettings = MSCore::settings('power_stations', MSCore::page()->path_id);

$addressModel = new MSBaseTape([
    'itemsTableName' => '{address}'
]);

$phoneStock = $addressModel->getItems()['items']['1']['phone'];

$model = new PowerStationModel([
        'pathId' => MSCore::page()->path_id,
        'vars' => MSCore::urls()->vars,
        'onlyHaveItems' => false,
        'articlesTreeFields' => ['*'],
        'itemsOrder' => '`order` ASC',
        'pagination' => [
            'onPage' => $modelSettings->onPage,
        ]
    ]
);

$industryModel = new IndustryModel([
    'itemsTableName' => '{industry}',
    'pathId' => MSCore::page()->path_id,
    'path' => '/' . MSCore::page()->path,
    'order' => '`order` ASC',
    'pagination' => false
]);

$modelList = $industryModel->items['items'];

$engineModel = new EngineModel();

$content = $_CONTENT_;
$_CONTENT_ = '';

//statuses articles
$statuses = MSConfig::get('common/config:ps_statuses');
$variants = VariantModel::getVariants();

$processLoadNextModule = false;
$model->allowPagesForIndexPage();

$allEngines = MSCore::db()->getAll("
	SELECT `id`,`name`,`code`, `image` FROM `mp_engines_articles`
	ORDER BY `order`
");

$getEngine = [];
if(isset($_GET['engine'])) {
	foreach($_GET['engine'] as $k=>$id) {
		$getEngine[] = $id;
	}
}


if ($model->isCatalogIndexPage) {

	if(!isset($_GET['engine'])) {
		$articles = $model->getArticlesFormat($engineModel);
	} else {
		foreach($_GET['engine'] as $k => $v) {
			$v = (int)$v;
		}
		$articles = $model->getArticlesFormat($engineModel, $_GET['engine']);
	}


    $item = $industryModel->getItem(getParam(0));
	HelperSeo::addAllSeo($item['item']);

    $pagination = $model->getPaginationForArticle(count($articles));
    $articles   = $model->filterArticleForPagination($articles, $pagination);

    echo template('catalog/common', [
        'leftAside' => template('catalog/leftAside', [
            'content' => template('catalog/power_stations/leftAside', [
                'model' => $model,
                'industryModel' => $industryModel,
	            'modelList' => $modelList
            ])
        ]),
        'content' => template('catalog/power_stations/index', [
            'model'       => $model,
            'content'     => $content,
            'content2'    => MSCore::page()->getZoneContent('_CONTENT2_'),
            'statuses'    => $statuses,
            'variants'    => $variants,
            'articles'    => $articles,
            'allEngines'  => $allEngines,
            'getEngine'   => $getEngine,
            'pagination'  => $pagination,
        ])
    ]);
} else {
    $article = $model->getArticle(['*'], [
        'fields' => ['*'],
        'recursive' => true
    ]);

    if ($model->isItem === false) {
        if ($article !== false) {

            $article['statuses'] = $statuses;
            $article['variants'] = $variants;
            HelperSeo::addAllSeo($article['article']);

            switch ($article['article']['level']) {
                case 0:
                default:
                    $article['article'] = PowerStationMainPageHelper::transformMainPage($article['article'], $model, $phoneStock);
                    HelperSeo::addAllSeo($article['article']);

                    echo template('catalog/common', [
                        'leftAside' => template('catalog/leftAside', [
                            'content' => template('catalog/power_stations/leftAside', [
                                'model'         => $model,
                                'industryModel' => $industryModel,
	                            'modelList' => $modelList
	                            //'modelForIndustry' => $modelForIndustry
                            ])
                        ]),
                        'content' => template('catalog/power_stations/list',
                            $article + [
                                'engineModel' => $engineModel,
                            ]
                        )
                    ]);
                    break;

                case 1:
                    /** Сюда попадвем, если просто конечная страница */
                    MSCore::page()->main_template = '|SITE|layout|catalogItem.php';

                    $article['spareModel'] = new MSBaseTape([
                        'itemsTableName' => '{spares}',
                        'pathId' => SPARES_PATH_ID,
                        'order' => '`order` ASC',
                    ]);

                    $article['article'] = PowerStationTitleHelper::transform($article['article'], $model, $phoneStock);
                    HelperSeo::addAllSeo($article['article']);

                    if (!empty($article['items'][0])) {
                        $movableType =
                            array_values(
                                array_filter($article['items'], function ($value) {
                                    return $value['title'] == 2;
                                })
                            );

                        if (!empty($movableType[0])) {
                            $model->_currentItem = $movableType[0];
                        } else {
                            $model->_currentItem = $article['items'][0];
                        }
                    }

                    $article['OEPrices'] = $model->getOEPrices();
                    $newArticleItems = [];
                    $parentArticle = $model->getArticleById($article['article']['parent']);
                    foreach ($article['items'] as $otherVariants) {
                        $dummyItem['variant']['id'] = $otherVariants['title'];
                        $dummyItem['price']   = 0;
                        $otherVariants['price'] = PowerGeneratorHelper::getPrice($dummyItem, $article['items'], $article['OEPrices']);
                        $item['item']['variant']['titleGenerator'] = $variants[$otherVariants['title']]['titleGenerator'];
                        $otherVariants['title_h1'] =  HelperSeoPowerStationItem::generateH1(
                            $parentArticle,
                            $article['article'],
                            $item['item']
                        );
                        $newArticleItems[] = $otherVariants;
                    }
                    $article['items'] = $newArticleItems;

                    $article['article'] = $model->initCharacteristics();
                    $article['article'] = PowerStationTitleHelper::transform($article['article'], $model, $phoneStock);
                    echo template('catalog/power_stations/article', $article);

                    break;
            }
        }
        else {
            $processLoadNextModule = true;
            //Проверка будет в модуле отраслевых решений
            //page404();
        }
    } else {
        /**
         * todo: сюда попадаем в случае разных вариантов корпуса
         */
        $article['article'] = $model->initCharacteristics();

        if (($item = $model->getItem()) !== false) {
            $vars = MSCore::urls()->vars;
            if ($item['item']['code'] != $vars[2]) {
                page404();
            }

            MSCore::page()->main_template = '|SITE|layout|catalogItem.php';

            $parentArticle = $model->getArticleById($article['article']['parent']);
            $item['item']['title_h1'] = HelperSeoPowerStationItem::generateH1(
                $parentArticle,
                $article['article'],
                $item['item']
            );




            $item = PowerStationOpenPageHelper::transformOpenPage($item, $article['article'], $model, $phoneStock);
            $item['item']['name_article'] = $article['article'];
            HelperSeo::addAllSeo($item['item']);

            $article['spareModel'] = new MSBaseTape([
                'itemsTableName' => '{spares}',
                'pathId' => SPARES_PATH_ID,
                'order' => '`order` ASC',
            ]);

            $article['OEPrices']   = $model->getOEPrices();

            if ($item['item']['variant']['id'] == 2) {
                $article['OEPrices']   = PowerGeneratorHelper::deleteChassisCodeFromOptions($article['OEPrices']);
            }

            $item['item']['price'] = PowerGeneratorHelper::getPrice($item['item'], $article['items'], $article['OEPrices']);

            $priceForApi = ceil($item['item']['price']);

            $article['item'] = $item['item'];
            $article['statuses'] = $statuses;
            $article['variants'] = $variants;

            $newArticleItems = [];

            $movableType =
                array_values(
                    array_filter($article['items'], function ($value) {
                        return $value['title'] == 2;
                    })
                );
            if (!empty($movableType[0])) {
                $movableType = $movableType[0];
            }
            $OEPrices = $model->getOEPrices($movableType);

            foreach ($article['items'] as $otherVariants) {
                $dummyItem['variant']['id'] = $otherVariants['title'];
                $dummyItem['price']   = 0;

                $otherVariants['price'] = PowerGeneratorHelper::getPrice($dummyItem, $article['items'], $OEPrices);
                $item['item']['variant']['titleGenerator'] = $variants[$otherVariants['title']]['titleGenerator'];
                $otherVariants['title_h1'] =  HelperSeoPowerStationItem::generateH1(
                    $parentArticle,
                    $article['article'],
                    $item['item']
                );
                $newArticleItems[] = $otherVariants;
            }
            $article['items'] = $newArticleItems;

            echo template('catalog/power_stations/item', $article + [
                'priceForApi' => $priceForApi
                ]);
        } else {
            page404();
        }
    }
}