<?php

$PSModel = new PowerStationModel([
        'pathId' => POWER_STATION_PATH_ID,
        'onlyHaveItems' => false,
    ]
);

echo template('common/content-bottom-carousel', [
    'PSModel' => $PSModel,
    'title' => getTextMod(6),
]);