<?php

$model = new MSBaseTape([
    'itemsTableName' => '{common_slider}',
]);
$data = $model->getItems();
$data['fileDownload'] = FileDownload::questionnaire();

if ($data['items']) {
    echo template('common/content-bottom', $data);
}