<?php
/**
 * @var bool $filesVar
 */
$query = new MSTable('{download_files_attach} dfa');
$query->setFields([
    'df.title',
    'df.file'
]);
$query->setFilter(['`dfa`.`path_id` = ' . (int)MSCore::page()->path_id], 'pathId');

$query->setJoin('{download_files} df', 'INNER', 'df.id = dfa.attach_file');

if ($files = $query->getItems()) {
    foreach ($files as $key => $data) {
        $files[$key]['file'] = FileDownload::fileData($data['file']);
    }

    echo template('common/download-files', [
        'files' => $files
    ]);
}

