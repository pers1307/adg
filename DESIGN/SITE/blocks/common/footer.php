<?php

$model = new MSBaseTape([
    'itemsTableName' => '{common_slider2}',
    'pathId' => null,
    'pagination' => false
]);

$data = $model->getItems();

echo template('common/footer', [
    'menu' => ModelMenu::getMainMenu(1, null, 2, 'order'),
    'slides' => $data['items'],
]);