<?php

echo template('common/nav', [
    'menu'           => ModelMenu::getMainMenu(1, null, 2, 'order'),
    'menuItems'      => ModelMenu::getMainMenu(1, null, 7, 'order'),
    'showItemsCount' => 4,
]);