<?php
/**
 * @var string $zone
 */

$content = MSCore::page()->getZoneContent($zone);
$text = explode('<p>[#READMORE#]</p>', $content);

if (count($text)>1) {
  $content = '<section class="text-bottom">
<br>
<article class="wrapper">
   <div class="long-text">'. $text[0] .
      '<div class="read-more1" style="display: none;">' .$text[1] . '</div>
			<a href="#" class="show-full-text read-more-btn read-more">Показать еще</a>
		</div>
</article>
</section>';
}

echo template('common/text', array(
    'text' => $content
));