<?php

$files = [
    'common/text.php' => ['Текстовое содержание'],
    'common/download-files.php' => ['Файлы для скачивания'],
    'contacts/address.php' => ['Адреса'],
    'contacts/staff.php' => ['Сотрудники'],
    'contacts/index.php' => ['Контакты'],
    'delivery/index.php' => ['Доставка в регионы'],
    'about/manufacture.php' => ['Производство'],
    'services/index.php' => ['Услуги'],
    'spares/index.php' => ['Запчасти'],
    'advantages/index.php' => ['Приемущества'],
    'variants/index.php' => ['Варианты исполнения 2.0'],
    'industry/index.php' => ['Отраслевые решения'],

    'catalog/optional_equipment/index.php' => ['Доп. оборудование'],
    'catalog/engines/index.php' => ['Дизельные двигатели'],
    'catalog/power_stations/index.php' => ['Дизельные электростанции'],
    'catalog/power_stations_equipment/index.php' => ['Стандартная комплектация'],
    'common/without-in-page.php' => ['Без вложенных страниц'],
];