<?php

$addressModel = new MSBaseTape([
    'itemsTableName' => '{address}',
    'pathId' => MSCore::page()->path_id,
    'path' => MSCore::page()->path,
]);

$address = template('contacts/address', $addressModel->getItems());