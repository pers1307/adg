<?php
/**
 * @var MSBaseTape $addressModel
 * @var MSBaseTape $staffModel
 * @var string $address Html of address view
 * @var string $staff Html of staff view
 */

$map = strip_tags(MSCore::page()->getZoneContent('_CONTENT_'));
$downloadFiles = $_CONTENT_;
$_CONTENT_ = '';

echo template('contacts/index', [
    'addressModel' => $addressModel,
    'staffModel' => $staffModel,
    'address' => $address,
    'staff' => $staff,
    'map' => $map,
    'downloadFiles' => $downloadFiles
]);
