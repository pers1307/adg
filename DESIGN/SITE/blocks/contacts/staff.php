<?php

$staffModel = new MSBaseTape([
    'itemsTableName' => '{staff}',
    'pathId' => MSCore::page()->path_id,
    'path' => MSCore::page()->path,
]);

$staff = template('contacts/staff', $staffModel->getItems());
