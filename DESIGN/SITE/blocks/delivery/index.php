<?php

$model = new MSBaseTape([
    'itemsTableName' => '{delivery_prices}',
    'pathId' => MSCore::page()->path_id,
    'path' => MSCore::page()->path,
]);
$model->itemsTable->setOrder('title');
$data = $model->getItems();
$items = [];
foreach ($data['items'] as $item) {
    $item['title'] = mb_strtolower($item['title'], 'UTF-8');
    $first = mb_substr($item['title'], 0, 1);
    $items[$first] = isset($items[$first]) ? $items[$first] : [];

    $items[$first][] = $item;
}
ksort($items);

if (!empty($items)) {
    echo template('delivery/index', [
        'items' => $items
    ]);
}

