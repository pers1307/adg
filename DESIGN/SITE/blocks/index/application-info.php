<?php

$industryModel = new IndustryModel([
    'itemsTableName' => '{industry}',
    'pathId' => POWER_STATION_PATH_ID,
    'path' => '/' . trim(path(POWER_STATION_PATH_ID), '/'),
    'order' => '`order` ASC',
    'pagination' => false
]);

echo template('index/application-info', [
    'content' => getTextModFull(9)
    ] + $industryModel->getItems());