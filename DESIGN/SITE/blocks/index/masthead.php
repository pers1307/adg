<?php

$model = new PowerStationModel([
        'pathId' => POWER_STATION_PATH_ID,
        'onlyHaveItems' => false,
        'articlesTreeFields' => ['*'],
        'itemsOrder' => '`order` ASC',
        'pagination' => false
    ]
);

$articles = [];
$defaultImageUrl = null;
foreach ($model->articles as $article) {
    $model->_currentArticle = null;
    if ($article['level'] == 1 && $article['inSlider']) {
        if (!isset($articles[$article['parent']])) {
            $art = $model->getArticleById($article['parent']);
            if (!$defaultImageUrl) {
                $defaultImageUrl = MSFiles::getImageUrl($art['image'], 'view');
            }

            $articles[$article['parent']] = $art;
        } else {
            continue;
        }
    }
}


if ($articles) {
    uasort($articles, function($a, $b){
        if ($a['order'] == $b['order']) {
            return 0;
        }
        return ($a['order'] < $b['order']) ? -1 : 1;
    });

    foreach ($articles as &$article) {
        foreach ($article['children'] as $childKey => $child) {
            if ($child['inSlider'] == '0') {
                unset($article['children'][$childKey]);
            }
        }
    }

    foreach ($articles as &$article) {
        $resultByColumn = [];
        $count = 0;

        foreach ($article['children'] as $childKey => $child) {
            if ($childKey % 2 == 0) {
                ++$count;
            }

            $resultByColumn[$count][] = $child;
        }

        $article['children'] = $resultByColumn;
    }

    echo template('index/masthead', [
        'model' => $model,
        'articles' => $articles,
        'defaultImageUrl' => $defaultImageUrl
    ]);
}
