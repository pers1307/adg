<?php

$model = new VariantModel([
        'pathId' => VARIANTS_PATH_ID,
        'onlyHaveItems' => false,
        'articlesTreeFields' => ['announce', 'image'],
    ]
);

echo template('index/variants', [
    'model' => $model,
]);