<?php

//if enabled variable from power_station module
if ($processLoadNextModule) {
    $industryModel = new IndustryModel([
        'itemsTableName' => '{industry}',
        'pathId' => MSCore::page()->path_id,
        'path' => '/' . MSCore::page()->path,
        'order' => '`order` ASC',
        'pagination' => false
    ]);

    $industryModel->currentPage = getParam(0);

    if (count(MSCore::urls()->vars) > 1) {
        page404();
    }

    if (false !== $item = $industryModel->getItem(getParam(0))) {

        if (empty($item)) {
            Page404();
        }

        HelperSeo::addAllSeo($item['item']);

        $modelSettings = MSCore::settings('power_stations', MSCore::page()->path_id);

        $model = new PowerStationModel([
                'pathId' => MSCore::page()->path_id,
                'vars' => MSCore::urls()->vars,
                'onlyHaveItems' => false,
                'articlesTreeFields' => ['*'],
                'itemsOrder' => '`order` ASC',
                'pagination' => [
                    'onPage' => $modelSettings->onPage,
                ]
            ]
        );
        $engineModel = new EngineModel();

        $statuses = MSConfig::get('common/config:ps_statuses');
        $variants = VariantModel::getVariants();

        $model->allowPagesForIndexPage();
        $articles = $model->getArticlesFormatToIndustry($engineModel, $item['item']['list']);

        $pagination = $model->getPaginationForArticle(count($articles));
        $articles   = $model->filterArticleForPagination($articles, $pagination);

        //show all items
        $industryModel->itemsTable->unsetFilter('itemKey');

	    MSCore::page()->breadCrumbs->alter = $item['item']['title_h1'];

        echo template('catalog/common', [
            'leftAside' => template('catalog/leftAside', [
                'content' => template('catalog/power_stations/leftAside', [
                    'model' => $model,
                    'industryModel' => $industryModel,
	                'modelList' =>$modelList
                ])
            ]),
            'content' => template('industry/list', array_merge($item, [
                'powerStationModel' => $model,
                'engineModel'       => $engineModel,
                'statuses'          => $statuses,
                'variants'          => $variants,

                'articles'          => $articles,
                'pagination'        => $pagination,
            ]))
        ]);

    // todo: тут нужно проверить на корректность
    } elseif(!$industryModel->getCurrentPage() && !empty(MSCore::urls()->vars[0])) {
        page404();
    }
}
