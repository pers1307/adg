<?php
$modelSettings = MSCore::settings('news', MSCore::page()->path_id);

$model = new ModelNews([
	'pathId' => MSCore::page()->path_id,
	'path' => '/' . MSCore::page()->path,
	'order' => '`date` DESC',
	'pagination' => [
		'onPage' => $modelSettings->onPage,
		'linkPref' => 'page'
	]
]);

$model->currentPage = getParam(0);

if (false !== $item = $model->getItem(getParam(0))) {
	if (empty($item)) {
		Page404();
	}
	HelperSeo::addAllSeo($item['item']);
	echo template('information/item', $item);

} elseif(!$model->getCurrentPage() && !empty(MSCore::urls()->vars[0])) {
	page404();
} else {
	$items = $model->getItems(['id', 'code', 'title', 'date', 'image', 'shortText', 'text']);
	echo template('information/itemList', $items);
}