<?php
$modelSettings = MSCore::settings('services', MSCore::page()->path_id);

$model = new MSBaseTape([
    'itemsTableName' => '{services}',
    'pathId' => MSCore::page()->path_id,
    'path' => '/' . MSCore::page()->path,
    'order' => '`id` ASC',
    'pagination' => [
        'onPage' => $modelSettings->onPage,
        'linkPref' => 'page'
    ]
]);

$model->currentPage = getParam(0);

if (false !== $item = $model->getItem(getParam(0))) {
    if (empty($item)) {
        Page404();
    }
    HelperSeo::addAllSeo($item['item']);
    echo template('services/item', $item);

} elseif(!$model->getCurrentPage() && !empty(MSCore::urls()->vars[0])) {
    page404();
} else {
    $items = $model->getItems(['id', 'code', 'title', 'image', 'announce']);
    echo template('services/itemList', $items);
}