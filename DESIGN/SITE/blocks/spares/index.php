<?php
$modelSettings = MSCore::settings('spares', MSCore::page()->path_id);

$model = new MSBaseTape([
    'itemsTableName' => '{spares}',
    'pathId' => MSCore::page()->path_id,
    'path' => '/' . MSCore::page()->path,
    'order' => '`order` ASC',
    'pagination' => [
        'onPage' => $modelSettings->onPage,
        'linkPref' => 'page'
    ]
]);

$model->currentPage = getParam(0);

if (false !== $item = $model->getItem(getParam(0))) {
    if (empty($item)) {
        Page404();
    }
    HelperSeo::addAllSeo($item['item']);
    echo template('spares/item', $item);

} elseif(!$model->getCurrentPage() && !empty(MSCore::urls()->vars[0])) {
    page404();
} else {
    $items = $model->getItems(['id', 'code', 'title', 'image', 'announce']);
    echo template('spares/itemList', $items);
}