<?php
if(getParam(1)) {
	page404();
}

$model = new VariantModel([
        'pathId' => MSCore::page()->path_id,
        'vars' => MSCore::urls()->vars,
        'onlyHaveItems' => false,
        'articlesTreeFields' => ['announce', 'image'],
        'itemsOrder' => '`i`.``order` ASC',
        'pagination' => false,
    ]
);

//Ключом для каждого массива выступает $article['code']
$tabs = [
	'otkritiy' => [
		'diesel_engine' => 'Дизельные генераторы',
		'description_of_the_open_version' => 'Описание открытого варианта исполнения',
	],

	'kozhuh' => [
		'diesel_engine' => 'Дизельные генераторы',
		'description_of_the_open_version' => 'Описание генераторов в кожухе',
	],

	'konteyner' => [
		'diesel_engine' => 'Дизельные генераторы',
		'description_of_the_open_version' => 'Описание генераторов в контейнере',
	],

	'peredvizhnye' => [
		'diesel_engine' => 'Дизельные генераторы',
		'description_of_the_open_version' => 'Описание передвижных вариантов исполнения',
	],
];

$content = $_CONTENT_;
$_CONTENT_ = '';

$PSModel = new PowerStationModel([
        'pathId' => POWER_STATION_PATH_ID,
        'onlyHaveItems' => false,
    ]
);

if ($model->isCatalogIndexPage) {

    echo template('variants/index', [
        'model' => $model,
        'PSModel' => $PSModel,
        'content' => $content,
    ]);
} else {

    $article = $model->getArticle(['*'], [
        'fields' => ['*'],
        'recursive' => true
    ]);

    if ($article == false) {
        page404();
    }

    HelperSeo::addAllSeo($article['article']);
    echo template('variants/article', $article + [
        'PSModel' => $PSModel,
		'tabs'    => $tabs,
    ]);
}