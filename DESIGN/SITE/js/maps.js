var Site = Site || {};

(function (global, window, $, undefined) {
    'use strict';

    function YaMap(id, options) {

        this.mapWrap = id;
        if (!this.mapWrap.size()) return;

        this.options = $.extend({
            mapCoord: null,
            isGeocoded: false,
            mapZoom: null,
            mapIcon: null,
            baloonTitle: null,
            baloonContent: null,
            iconPreset: 'twirl#blueStretchyIcon'

        }, options);

        $(document).ready(function () {

            this.init.call(this);

        }.bind(this));


    }

    YaMap.prototype.init = function () {

        this.currentObj = this.options.mapCoord;
        ymaps.ready(this.initMap.bind(this));


    };


    YaMap.prototype.initMap = function () {

        if (this.options.isGeocoded) {

            this.DrawMap();

        } else {

            this.GeocodeMap();

        }
    };

    YaMap.prototype.DrawMap = function () {


        this.map = new ymaps.Map(this.mapWrap[0],
            {
                center: this.currentObj,
                behaviors: ['default'],
                zoom: this.options.mapZoom,
                controls: []
            });

        //this.map.controls
        // Кнопка изменения масштаба
        //.add('zoomControl')
        // Список типов карты
        //.add('typeSelector')
        // Кнопка изменения масштаба - компактный вариант
        // Расположим её справа
        //.add('smallZoomControl', { right: 5, top: 75 })
        // Стандартный набор кнопок
        //.add('mapTools');

        //если есть иконка
        if (this.options.mapIcon) {

            this.myPlacemark = new ymaps.Placemark(this.currentObj, {
                // Свойства балуна
                balloonContentHeader: this.options.baloonTitle,
                balloonContentBody: this.options.baloonContent,
                balloonContentFooter: ''
            }, {
                // Опции иконки
                iconImageHref: this.options.mapIcon,
                iconImageSize: [this.options.mapIconWidth, this.options.mapIconHeight],
                iconImageOffset: [-(this.options.mapIconWidth/2), -this.options.mapIconHeight]

            });

        } else {

            this.myPlacemark = new ymaps.Placemark(this.currentObj, {
                // Свойства балуна
                iconContent: this.options.baloonTitle,
                balloonContentHeader: this.options.baloonTitle,
                balloonContentBody: this.options.baloonContent,
                balloonContentFooter: ''
            }, {
                // Опции иконки (стандартная)
                preset: this.options.iconPreset

            });
        }

        this.map.geoObjects.add(this.myPlacemark);
        this.map.controls.add('zoomControl');

    };


    YaMap.prototype.GeocodeMap = function () {

        ymaps.geocode(this.currentObj, { results: 1 }).then(ymaps.util.bind(function (res) {

            // Выбираем первый результат геокодирования
            this.firstGeoObject = res.geoObjects.get(0);

            this.map = new ymaps.Map(this.mapWrap[0], {
                center: this.firstGeoObject.geometry.getCoordinates(),
                behaviors: ['default'],
                zoom: this.options.mapZoom,
                controls: []
            });

            //this.map.controls
            // Кнопка изменения масштаба
            //.add('zoomControl')
            // Список типов карты
            // .add('typeSelector')
            // Кнопка изменения масштаба - компактный вариант
            // Расположим её справа
            //.add('smallZoomControl', { right: 5, top: 75 })
            // Стандартный набор кнопок
            //.add('mapTools');

            //если есть иконка
            if (this.options.mapIcon) {
                // Свойства балуна
                this.firstGeoObject.properties.set({
                    balloonContentHeader: this.options.baloonTitle,
                    balloonContentBody: this.options.baloonContent,
                    balloonContentFooter: ''
                });
                // Опции иконки
                this.firstGeoObject.options.set("iconImageHref", this.options.mapIcon);
                this.firstGeoObject.options.set("iconImageSize", [this.options.mapIconWidth, this.options.mapIconHeight]);
                this.firstGeoObject.options.set("iconImageOffset", [-(this.options.mapIconWidth/2), -this.options.mapIconHeight]);

            } else {
                // Свойства балуна
                this.firstGeoObject.properties.set({
                    iconContent: this.options.baloonTitle,
                    balloonContentHeader: this.options.baloonTitle,
                    balloonContentBody: this.options.baloonContent,
                    balloonContentFooter: ''
                });
                // Опции иконки (стандартная)
                this.firstGeoObject.options.set("preset", this.options.iconPreset);
            }


            this.map.geoObjects.add(this.firstGeoObject);
            this.map.controls.add('zoomControl');

        }, this));

    };

    $(function () {
        $.fn.initYaMap = function ()
        {
            $.each(this, function ()
            {
                if ($(this).attr('data-coords') !== undefined)
                {
                    var coordinates = $(this).data('coords').split(','),
                        lat = coordinates[0],
                        lng = coordinates[1];
                }
                else
                {
                    var address = $(this).attr('data-address');
                }

                var map = new YaMap($(this), {
                    mapCoord: [ address ? address : lat, lng  ],
                    isGeocoded: ( $(this).data('geocoded') != 0 ),
                    mapZoom: parseInt($(this).data('zoom')),
                    mapIcon: $(this).data('image'),
                    mapIconWidth: $(this).data('image-width'),
                    mapIconHeight: $(this).data('image-height'),
                    baloonTitle: $(this).data('title'),
                    baloonContent: $(this).siblings('.baloon-content').html()
                });
            });
        };

        $('.yamap').initYaMap();
    });

})(Site, window, jQuery);