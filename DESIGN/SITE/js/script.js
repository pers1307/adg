function initMask() {
  $('.tel').mask("9 (999) 999 99 99");
}


function toggleText() {
  $('.read-more').on('click', function () {
    if ($('.read-more1').css("display") == 'none') {
      $('.read-more1').css("display", "block");
      $(this).removeClass('active');
    } else {
      $('.read-more1').css("display", "none");
      $(this).addClass('active');
    }
    $(this).html($(this).hasClass('active') ? '<span> Показать еще  </span>' : '<span>Скрыть</span>');

    return false
  });

  $('.show-more-img').on('click', function () {
    if (!$(this).hasClass('active')) {
      $(this).data('last-text', $(this).text());
    }
    $(this).closest('.image-list').find('.drop').slideToggle();
    $(this).toggleClass('active');
    $(this).html($(this).hasClass('active') ? '<span>Скрыть</span>' : '<span>' + $(this).data('last-text') + '</span>');

    return false
  });
}

function initValidete() {
  function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  function validateName(name) {
    var re = /^[A-Za-zА-Яа-я\s]+$/;
    return re.test(name);
  }

  function validateNumber(name) {
    var re = /^\d{1,13}$/;
    return re.test(name);
  }

  function validateTel(tel) {
    return tel != "";
  }

  function eventInput(th) {
    var isValide = true;
    var _thisValue = th.val();
    var _thisName = th.data('name');
    if (_thisName == 'name') {
      isValide = isValide && validateName(_thisValue);
      if (validateName(_thisValue)) {
        th.closest('.form-item').removeClass('error')
          .addClass('done')
      }
      else {
        th.closest('.form-item').removeClass('done')
          .addClass('error')
      }
      return validateName(_thisValue)

    }
    if (_thisName == 'number') {
      isValide = isValide && validateNumber(_thisValue);
      if (validateNumber(_thisValue)) {
        th.closest('.form-item').removeClass('error')
          .addClass('done')
      }
      else {
        th.closest('.form-item').removeClass('done')
          .addClass('error')
      }
      return validateNumber(_thisValue)

    }
    if (_thisName == 'email') {
      isValide = isValide && validateEmail(_thisValue);
      if (validateEmail(_thisValue)) {
        th.closest('.form-item').removeClass('error')
          .addClass('done')
      }
      else {
        th.closest('.form-item').removeClass('done')
          .addClass('error')
      }
      return validateEmail(_thisValue)

    }
    if (_thisName == 'tel') {
      isValide = isValide && validateTel(_thisValue);
      if (validateTel(_thisValue)) {
        th.closest('.form-item').removeClass('error')
          .addClass('done')
      }
      else {
        th.closest('.form-item').removeClass('done')
          .addClass('error')
      }
      return validateTel(_thisValue)
    }
    if (_thisName == 'text') {
      if (_thisValue.length > 1) {
        th.closest('.form-item').removeClass('error')
          .addClass('done')
      }
      else {
        th.closest('.form-item').removeClass('done')
          .addClass('error')
      }
      return _thisValue.length > 1
    }
    if (_thisName == 'pass') {
      if (_thisValue.length > 8) {
        th.closest('.form-item').removeClass('error')
          .addClass('done')
      }
      else {
        th.closest('.form-item').removeClass('done')
          .addClass('error')
      }
      return _thisValue.length > 1
    }
    if (_thisName == 'polit') {
      if (th.prop('checked')) {
        th.closest('.polit').removeClass('error')
          .addClass('done')
      }
      else {
        th.closest('.polit').removeClass('done')
          .addClass('error')
      }
      isValide = isValide && th.prop('checked');
    }
    if (_thisName == 'textarea') {
      if (th.val().length > 1) {
        th.closest('.form-item').removeClass('error')
          .addClass('done')
      }
      else {
        th.closest('.form-item').removeClass('done')
          .addClass('error')
      }
      isValide = isValide && th.val().length > 1;
    }


  }


  $('input').on('change', function () {
    eventInput($(this));
    return false
  });

  $('textarea').on('change', function () {
    eventInput($(this));
    return false
  });

  $('form').on('submit', function () {
    var form = $(this);
    form.find('input').each(function () {
      eventInput($(this))
    });
    form.find('textarea').each(function () {
      eventInput($(this))
    });
    if (!form.find('.error').length) {
      return true
    }
    return false
  });
}

function initTab() {
  $('.tab-menu li').on('click', function () {
    $(this).closest('.link-list').removeClass('active').children('.show-mobile').text($(this).text())
    if (!$(this).closest('.slick-slide').length) {
      $(this).addClass('active').siblings().removeClass('active');
      $(this).closest('.tab').find('.tab-item').hide().removeClass('active').eq($(this).index()).show().addClass('active')
      if ($(this).closest('.tab').find('.tab-item').eq($(this).index()).find('.slick-slider').length) {
        $(this).closest('.tab').find('.tab-item').eq($(this).index()).find('.slick-slider').slick('setPosition');
      }
      if (!!$(this).data('big-default')) {
        var newUrlImg = $(this).data('big-default');
        $('.js-preview').attr('src', newUrlImg);
        $('.js-preview').attr('data-url-default', newUrlImg);
      }
    }

    if ($(this).closest('.slick-slide').length) {
      $(this).closest('.tab').find('.tab-item').hide().removeClass('active').eq($(this).closest('.slick-slide').index()).show().addClass('active')
      if ($(this).closest('.tab').find('.tab-item').eq($(this).index()).find('.slick-slider').length) {
        $(this).closest('.tab').find('.tab-item').eq($(this).index()).find('.slick-slider').slick('setPosition');
      }
      if (!!$(this).data('big-default')) {
        var newUrlImg = $(this).data('big-default');
        $('.js-preview').attr('src', newUrlImg);
        $('.js-preview').attr('data-url-default', newUrlImg);
      }
    }
    if ($(this).closest('.tab').find('.tab-item').eq($(this).index()).find('.slick-slider').length) {
      $(this).closest('.tab').find('.tab-item').eq($(this).index()).find('.slick-slider').slick('setPosition');
    }
  })
}

function initSlick() {
  $('.masthead_carousel').slick();

  $('.application-info_carousel').slick({
    dots: true,
    arrows: false,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1360,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
        }
      },
      {
        breakpoint: 999,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
        }
      }
      // You can unslick at a given breakpoint now by adding:
      // settings: "unslick"
      // instead of a settings object
    ]
  });


  $('.footer_carousel').slick({
    adaptiveHeight: true
  });

  $('.slider-for').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.slider-nav'

  });
  $('.slider-nav').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for',
    dots: false,
    focusOnSelect: true,
    prevArrow: '<button type="button" class="slick-next">Next</button>',
    nextArrow: '<button type="button" class="slick-prev">Previous</button>',
    responsive: [
      {
        breakpoint: 1360,
        settings: {
          slidesToShow: 3
        }
      },
      {
        breakpoint: 600,
        settings: {}
      },
      {
        breakpoint: 480,
        settings: {}
      }
    ]
  });


  $('.slider-for2').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    dots: false,
    fade: true,
    asNavFor: '.slider-nav2'
  });
  $('.slider-nav2').slick({
    slidesToShow: 4,
    slidesToScroll: 1,
    asNavFor: '.slider-for2',
    dots: false,
    focusOnSelect: true,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3
        }
      }
    ]
  });

  $('.img-carousel').slick({
    dots: true

  });



  $('.content-bottom-carousel_in, .masthead_left .tab-menu_in').addClass('is-slider');
  $('.masthead_left .tab-menu_in').sly( {
    horizontal: 1,
    itemNav: 'basic',
    smart: 1,
    scrollBy: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,

  });

  var $wrap   = $('.content-bottom-carousel_in').parent();

  $('.content-bottom-carousel_in').sly( {
    horizontal: 1,
    itemNav: 'basic',
    smart: 1,
    activateOn: 'click',
    mouseDragging: 1,
    touchDragging: 1,
    releaseSwing: 1,
    startAt: 3,
    scrollBy: 1,
    activatePageOn: 'click',
    speed: 300,
    elasticBounds: 1,
    dragHandle: 1,
    dynamicHandle: 1,
    clickBar: 1,
    prevPage: $wrap.find('.prev'),
    nextPage: $wrap.find('.next'),

  });


}

function initChangeHoverImg() {
  $('.js-hover-change').on('mouseover', function () {
    $(this).closest('.js-hover-wrap').find('.js-preview')
      .attr('src', $(this).data('url-big'))
  });

  $('.js-hover-wrap').on('mouseleave', function () {
    $(this).find('.js-preview')
      .attr('src', $(this).find('.js-preview').data('url-default'))
  })
}

function initAddFile() {
  $('.add-file input').on('change', function () {
    if ($(this).val() != "") {
      $(this).next().addClass('choice').text($(this)[0].files[0].name)
    }
    else {
      $(this).next().removeClass('choice').text('Прикрепить файл')
    }
  })
}

function initFancy() {
  $('.js-callback').fancybox({
    margin: $(window).width() < 768 ? 40 : 70,
    minWidth: 240,
    scrolling: 'no',
    fitToView: false,
    afterShow: function () {
      initHint()
    }
  });

  $('.fancybox-pic').fancybox({
    afterShow: function () {
      console.log(111)
      var moveIn;
      var hammertime = new Hammer(document.querySelector('.fancybox-inner'));

      hammertime.add(new Hammer.Pan({direction: Hammer.DIRECTION_ALL, threshold: 0}));

      hammertime.on('pan', function (ev) {
        if (ev.additionalEvent == "panup") {
          moveIn = 'panup';
          $.fancybox.close()
          return
        }
        if (ev.additionalEvent == "panleft") {
          moveIn = 'panleft'
          $.fancybox.next()
        }
        if (ev.additionalEvent == "panright") {
          moveIn = 'panright'
          $.fancybox.prev()
        }

      });

      hammertime.on('panend', function (ev) {
        if (moveIn == 'panleft') {
        }
        if (moveIn == 'panright') {
        }
        if (moveIn == 'panup') {
        }
      });

    }
  });

  $('.fancybox').fancybox({
    helpers: {
      title: {
        type: 'outside'
      },
      thumbs: {
        width: 50,
        height: 50
      }
    },
    afterShow: function () {
      var moveIn;
      var hammertime = new Hammer(document.querySelector('.fancybox-inner'));

      hammertime.add(new Hammer.Pan({direction: Hammer.DIRECTION_ALL, threshold: 0}));

      hammertime.on('pan', function (ev) {
        if (ev.additionalEvent == "panup") {
          moveIn = 'panup';
          $.fancybox.close()
          return
        }
        if (ev.additionalEvent == "panleft") {
          moveIn = 'panleft'
          $.fancybox.next()
        }
        if (ev.additionalEvent == "panright") {
          moveIn = 'panright'
          $.fancybox.prev()
        }

      });

      hammertime.on('panend', function (ev) {
        if (moveIn == 'panleft') {
        }
        if (moveIn == 'panright') {
        }
        if (moveIn == 'panup') {
        }
      });

    }
  });

  $('.js-open-fancy').on('click', function () {
    $(this).closest('.js-hover-wrap').find('.js-hover-change').first().trigger('click');
    return false
  })

  if ($(window).width() < 768) {
    $('.js-click-change').fancybox({
      helpers: {
        title: {
          type: 'outside'
        },
        thumbs: {
          width: 50,
          height: 50
        }
      },
      afterShow: function () {
        var moveIn;
        var hammertime = new Hammer(document.querySelector('.fancybox-inner'));

        hammertime.add(new Hammer.Pan({direction: Hammer.DIRECTION_ALL, threshold: 0}));

        hammertime.on('pan', function (ev) {
          if (ev.additionalEvent == "panup") {
            moveIn = 'panup';
            $.fancybox.close()
            return
          }
          if (ev.additionalEvent == "panleft") {
            moveIn = 'panleft'
            $.fancybox.next()
          }
          if (ev.additionalEvent == "panright") {
            moveIn = 'panright'
            $.fancybox.prev()
          }

        });

        hammertime.on('panend', function (ev) {
          if (moveIn == 'panleft') {
          }
          if (moveIn == 'panright') {
          }
          if (moveIn == 'panup') {
          }
        });

      }
    });
  }

}

function showNav() {
  $('.catalog-nav_in .show-more').on('click', function () {
    $(this).toggleClass('active').text($(this).hasClass('active') ? "Скрыть" : $(this).data('text-default'));

    $(this).closest('.catalog-nav_in').find('.hide').toggle();
    return false
  })
}

function initTooltip() {
  $('.tooltip').tooltipster({
    contentAsHTML: true,
    maxWidth: 270,
    trigger: 'click',
    functionPosition: function (instance, helper, position) {
      position.coord.left += $(window).width() > 1360 ? 100 : -10;
      return position;
    }
  });

  $('.masthead-carousel_item').on('click', function (e) {
    if ($(e.target).hasClass('tooltip')) {
      return false
    }
  })
}

function initAccordion() {
  $('.accordion-title h5, .accordion-title .title-h5').on('click', function () {
    $(this).parent().siblings().slideToggle()
      .parent().toggleClass('active')
    //.siblings().removeClass('active')
    //.find('.drop').slideUp();
  })
}

function changePreview() {
  $('.js-click-change').on('click', function () {
    if ($(window).width() > 767) {
      $('.js-click-change').removeClass('active');
      $(this).addClass('active');
      $('.js-change-preview').attr('src', $(this).attr('href'));
      return false
    }

  })
}

function toFixPreview() {
  if ($('.container-info_preview .preview').length) {
    if ($(window).scrollTop() >
      $('.container-info_preview .container-info-preview_wrap').offset().top - 33) {
      $('.container-info_preview .preview').addClass('is-fix');
      if ($(window).scrollTop() >
        $('.container-info').offset().top + $('.container-info').innerHeight() - $('.js-change-preview').innerHeight() - 53) {
        $('.container-info_preview .preview').addClass('stop-move');
      }
      else {
        $('.container-info_preview .preview').removeClass('stop-move');
      }
    }
    else {
      $('.container-info_preview .preview').removeClass('is-fix');
    }
  }
}

function togglerTelHeader() {
  $('.header_tel').on('click', function () {
    $(this).toggleClass('open')
  })

  //$('.header-tel_item i.icon-arr-mobile').on('click', function () {
  //  $(this).closest('.header_tel').toggleClass('open')
  //})

  $('body').on('click', function (e) {
    if (!$(e.target).parents().hasClass('header_tel')) {
      if (!$(e.target).hasClass('header_tel')) {
        $('.header_tel')
          .removeClass('open')
      }

    }
  })
}

function toggleTabletNav() {
  $('.has-drop > a').on('click', function () {
    $(this).next().slideToggle();
    $(this).parent().addClass('open')
      .siblings().removeClass('open')
      .find('.drop').slideUp();
    return false
  });

  $('body').on('click', function (e) {
    if (!$(e.target).closest('.nav-menu_in').length) {
      $('.has-drop')
        .removeClass('open')
        .find('.drop').hide();
    }
  })

}

function tabletAccordion() {
  $('.tab-title').on('click', function () {
    $(this).parent().toggleClass('active').siblings().removeClass('active');
    $('.container-info_preview .preview').removeClass('stop-move is-fix')

    $('body, html').scrollTop($(this).parent().offset().top)
  })
}

function mobileChoicePower() {
  $('.link-list p').on('click', function () {
    $(this).parent().toggleClass('active')
  });

  $('body').on('click', function (e) {
    if (!$(e.target).parents().hasClass('link-list')) {
      if (!$(e.target).hasClass('link-list')) {
        $('.link-list').removeClass('active')
      }
    }
  })
}

function initMobileSlick() {
  if ($(window).width() < 768) {
    $('.mobile-carousel, .certificate-list, .country-table_top').not('.slick-initialized').slick({
      variableWidth: true,
      arrows: false,
      focusOnSelect: true
    });
    $('.mobile-carousel, .certificate-list, .country-table_top').on('beforeChange', function (event, slick, currentSlide, nextSlide) {
      if ($(this).parent().hasClass('tab-menu')) {
        $(this).closest('.tab').find('.tab-item').hide().removeClass('active').eq(nextSlide).show().addClass('active')
      }
    });
  }
  else {
    $('.mobile-carousel.slick-initialized, .certificate-list.slick-initialized, .country-table_top.slick-initialized').slick('unslick');
  }
}

function destroySlick() {
  if ($(window).width() < 768) {
    $('.content-bottom-carousel_in.is-slider, .masthead_left .tab-menu_in.is-slider').sly(false);
    $('.content-bottom-carousel_in.is-slider, .masthead_left .tab-menu_in.is-slider').removeClass('is-slider');
  }
  else{
    $('.masthead_left .tab-menu_in').not('.is-slider').sly( {
      horizontal: 1,
      itemNav: 'basic',
      smart: 1,
      scrollBy: 1,
      activateOn: 'click',
      mouseDragging: 1,
      touchDragging: 1,

    });

    var $wrap   = $('.content-bottom-carousel_in').parent();

    $('.content-bottom-carousel_in').not('.is-slider').sly( {
      horizontal: 1,
      itemNav: 'basic',
      smart: 1,
      activateOn: 'click',
      mouseDragging: 1,
      touchDragging: 1,
      releaseSwing: 1,
      startAt: 3,
      scrollBy: 1,
      activatePageOn: 'click',
      speed: 300,
      elasticBounds: 1,
      dragHandle: 1,
      dynamicHandle: 1,
      clickBar: 1,
      prevPage: $wrap.find('.prev'),
      nextPage: $wrap.find('.next'),

    });
    $('.content-bottom-carousel_in, .masthead_left .tab-menu_in').addClass('is-slider');


  }
}

function toggleNavMobile() {
  $('.nav-icon').on('click', function () {
    $('.nav-menu_in, .overlay').toggleClass('open')
  })

  $('.overlay').on('click', function () {
    $('.nav-menu_in, .overlay').removeClass('open')
  })
}

function mobileToggleSelect() {
  $('.catalog-nav h5, .catalog-nav .title-h5').on('click', function () {
    $(this).parent().toggleClass('open');
  })

  $('body').on('click', function (e) {
    if (!$(e.target).parents().hasClass('catalog-nav')) {
      $('.catalog-nav h5, .catalog-nav .title-h5').parent().removeClass('open');
    }
  })
}

function moveContent() {
  $('.product-in_wrap .btn').on('click', function () {
    $('body, html').animate({
      scrollTop: $($(window).width() < 768) ? $('#content_bottom .content-callback-form').offset().top : $('#content_bottom').offset().top
    }, 1500)
    return false
  })

  $('.product-in_bottom').on('click', function () {
    $('body, html').animate({
      scrollTop: $('.product-more-info').offset().top
    }, 1500);
    $('.container-info_preview .stop-move').removeClass('stop-move')
    $('.product-more-info .tab-menu li:first-child').trigger('click')
    return false
  })
}


function initHint() {
  $('form.ajax-form input, form.ajax-form textarea').on('change keypress', function () {
    if ($(this).parent().hasClass(errorClass)) {
      $(this).parent().removeClass(errorClass).find('.hint').remove();
    }
  });
}

let errorClass = 'error';


function scrollTop() {
  $('.btn-up').on('click', function () {
    $('body, html').animate({
      scrollTop: 0
    }, 1500);
    return false
  })
}

function showBtnUp() {
  if ($(window).scrollTop() > $(window).height() / 2) {
    $('.btn-up').fadeIn()
  }
  else {
    $('.btn-up').fadeOut()
  }
}

$(document).ready(function () {
  scrollTop();
  moveContent();
  mobileToggleSelect();
  toggleNavMobile();
  mobileChoicePower();
  tabletAccordion();
  toggleTabletNav();
  togglerTelHeader();
  toFixPreview();
  changePreview();
  toggleText();
  initAccordion();
  initTooltip();
  showNav();
  initFancy();
  initAddFile();
  initChangeHoverImg();
  initSlick();
  initTab();
  initValidete();
  initMask();
  destroySlick();
  initMobileSlick();
});

$(window).on('scroll', function () {
  showBtnUp();
  toFixPreview();
})

$(window).on('resize', function () {
  destroySlick();
  initMobileSlick();

  if ($(window).width() < 768) {
    $('.js-click-change').fancybox({
      helpers: {
        title: {
          type: 'outside'
        },
        thumbs: {
          width: 50,
          height: 50
        }
      },
      afterShow: function () {
        var moveIn;
        var hammertime = new Hammer(document.querySelector('.fancybox-inner'));

        hammertime.add(new Hammer.Pan({direction: Hammer.DIRECTION_ALL, threshold: 0}));

        hammertime.on('pan', function (ev) {
          if (ev.additionalEvent == "panup") {
            moveIn = 'panup';
            $.fancybox.close()
            return
          }
          if (ev.additionalEvent == "panleft") {
            moveIn = 'panleft'
            $.fancybox.next()
          }
          if (ev.additionalEvent == "panright") {
            moveIn = 'panright'
            $.fancybox.prev()
          }

        });

        hammertime.on('panend', function (ev) {
          if (moveIn == 'panleft') {
          }
          if (moveIn == 'panright') {
          }
          if (moveIn == 'panup') {
          }
        });

      }
    });
  }
})

$('#priceItemForm').on({
  click: function () {
    let form = $(this).closest('form#priceItemForm');

    $.ajax({
      type: form.attr('method'),
      url: form.attr('action'),
      data: form.serialize(),
      success: function (response) {
        switch (response.status.code) {
          case 200:
            $('#cash-box').html(response.data.content);

            let options = '';
            if (response.data.options) {
              $.each(response.data.options, function (key, value) {
                options += '<input type="hidden" data-name="options-' + value + '" name="options[' + value + ']">\r\n';
              });
            }
            $('#order-popup #itemOptions').html(options);
            break;
          case 1001:
            break;
        }

      }
    });
  }
}, 'input[type=checkbox]');

{

  $(document).on({
    click: function () {
      let form = $(this).closest('form.ajax-form');

      $.ajax({
        type: form.attr('method'),
        url: form.attr('action'),
        data: new FormData(form[0]),
        contentType: false,
        cache: false,
        processData: false,
        success: function (response) {
          form.find('input, textarea').parent().removeClass(errorClass);

          switch (response.status.code) {
            case 200:
              form.parent().html('<div class="callback-success">' + response.data.content + '</div>');
              break;
            case 1001:
              $.each(response.data.errors, function (key, value) {
                form.find('input[name=' + key + '], textarea[name=' + key + ']').after('<span class="hint">' + value + '</span>').parent().addClass(errorClass);
              });
              break;
          }
          $.fancybox.update()
        }
      });
    }
  }, 'button.sending');


  initHint()
}