<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js " lang="en">
<!--<![endif]-->
<?= template('common/head') ?>
<body>
<?php include_once BLOCKS_DIR . DS . 'common/header.php'; ?>
<?php include_once BLOCKS_DIR . DS . 'common/nav.php'; ?>
<div class="content content_error">
    <article class="wrapper">
        <div class="content-error_in">
            <h1>Страница <br>
                не найдена</h1>
            <p>К сожалению страница не найдена или не существует, <br>
                пожалуйста перейдите на главную страницу.</p>
            <a href="/" class="btn btn-green">На главную</a>
        </div>
    </article>
</div>
<?php include_once BLOCKS_DIR . DS . 'common/content-bottom-carousel.php'; ?>

<?php include_once BLOCKS_DIR . DS . 'common/footer.php'; ?>
<?php include_once BLOCKS_DIR . DS . 'common/popups.php'; ?>
</body>
</html>