<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js " lang="en">
<!--<![endif]-->
	<?= template('common/head') ?>
<body>
	<?php include_once BLOCKS_DIR . DS . 'common/header.php'; ?>
	<?php include_once BLOCKS_DIR . DS . 'common/nav.php'; ?>
    <div class="content">
        <?php include_once BLOCKS_DIR . DS . 'index/masthead.php'; ?>
        <?php include_once BLOCKS_DIR . DS . 'index/variants.php'; ?>
        <?php include_once BLOCKS_DIR . DS . 'advantages/index.php'; ?>
        <?php include_once BLOCKS_DIR . DS . 'index/application-info.php'; ?>


        <?= $_CONTENT_ ?>

        <?php include_once BLOCKS_DIR . DS . 'common/content-bottom.php'; ?>
        <?php include_once BLOCKS_DIR . DS . 'common/content-bottom-carousel.php'; ?>
    </div>
	<?php include_once BLOCKS_DIR . DS . 'common/footer.php'; ?>
    <?php include_once BLOCKS_DIR . DS . 'common/popups.php'; ?>
</body>
</html>