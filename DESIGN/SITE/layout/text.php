<!doctype html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="en"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9 lt-ie8" lang="en"> <![endif]-->
<!--[if IE 9]>
<html class="no-js lt-ie9" lang="en"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js " lang="en">
<!--<![endif]-->
<?= template('common/head') ?>
<body>
<?php include_once BLOCKS_DIR . DS . 'common/header.php'; ?>
<?php include_once BLOCKS_DIR . DS . 'common/nav.php'; ?>
<div class="content">
    <section class="product-info">
        <article class="wrapper">
            <?= template('common/breadcrumbs') ?>
            <?= template('common/breadcrumbsMobile') ?>
        </article>
    </section>

    <section class="product-more-info product-more-info_bd">
        <article class="wrapper">
            <div class="product-text">
                <?= $_CONTENT_ ?>
            </div>
        </article>
    </section>

    <?php include_once BLOCKS_DIR . DS . 'common/content-bottom.php'; ?>
    <?php include_once BLOCKS_DIR . DS . 'common/content-bottom-carousel.php'; ?>
</div>

<?php include_once BLOCKS_DIR . DS . 'common/footer.php'; ?>
<?php include_once BLOCKS_DIR . DS . 'common/popups.php'; ?>

</body>
</html>