<?php
/**
 * @var MSBaseTape $model
 * @var array $items
 */

if (!empty($items)) {
    ?><div class="img-carousel"><?php
    foreach ($items as $item) {
        if ($image = MSFiles::getImage($item['image'], 'origin')) {
            ?><div class="carousel-item"><?= $image; ?></div><?php
        }
    }
    ?></div><?php
}