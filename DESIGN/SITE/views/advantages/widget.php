<?php
/**
 * @var array $items
 * @var string $title
 */

if (!empty($items)) { ?>
    <section class="pluses-list">
        <article class="wrapper">
            <h2><?= $title ?></h2>
            <div class="tab mobile-accordion">
                <div class="pluses_in tab-menu hide-mobile">
                    <ul>
                        <?php
                        foreach ($items as $key => $item) {
                            ?><li class="<?= $key ? '' : 'active'?>"><?= $item['title'] ?></li><?php
                        }
                        ?>
                    </ul>
                </div>
                <div class="tab-content center pluses_info">
                    <?php
                    foreach ($items as $key => $item) {
                        ?>
                        <? $alt = $title = PowerStationForAdvantageHelper::transformAltForImg($item,  MSCore::page()->name_generator, MSCore::page()->variant_generator); ?>
                        <div class="tab-item<?= $key ? '' : ' active'?>">
                            <div class="tab-title"><?= $item['title'] ?><i></i></div>
                            <div class="hide-mobile">
                                <?php
                                if ($images = unserialize($item['gallery'])) {
                                    ?>
                                    <div class="preview js-hover-wrap">
                                        <a href="#" class="big-preview">
                                            <?= MSFiles::getImage($images, 'preview','Дизельный генератор'.$alt,0, 'js-preview js-open-fancy') ?>
                                        </a>
                                        <div class="small-preview-list">
                                            <?php
                                            foreach ($images as $index => $image) {
                                                ?>
                                                <a rel="fancybox<?= ($key+1) ?>" data-url-big="<?= MSFiles::getImageUrl($images, 'preview', $index)?>"
                                                   class="js-hover-change fancybox" href="<?= MSFiles::getImageUrl($images, 'view', $index)?>">
                                                    <?= MSFiles::getImage($images, 'min', '', $index)?>
                                                </a>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="pluses_text">
                                    <?= $item['text'] ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </article>
    </section>
    <?php
}