<?php
/**
 * @var string $leftAside
 * @var string $content
 */
?>
<?= isset($leftAside) ? $leftAside : '' ?>
<div class="catalog_in">
    <?= template('common/breadcrumbs') ?>
    <?= $content ?>
</div>