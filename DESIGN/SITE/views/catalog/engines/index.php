<?php
/**
 * @var OptionalEquipmentModel $model
 * @var array $items
 * @var string $content
 * @var string $content2
 * @var MSBasePagination $pagination
 */
?>
<div class="text-top">
    <?= $content ?>
</div>
<div class="catalog-list2">
    <?php foreach ($items as $item) { ?>
        <a href="<?= $item['itemLink']?>" class="catalog-item_engine">
            <span class="catalog-item-engine_in">
                <span class="preview">
                    <?php
                    if ($image = MSFiles::getImage($item['image'], 'preview')) {
                        echo $image;
                    }
                    ?>
                </span>
                <span class="name"><?= $item['title'] ?></span>
                <span class="param">Мощность: <?= $item['mainPowerKw'] ?> кВт</span>
            </span>
        </a>
        <?php
    } ?>
</div>

<?= $pagination ?>

<div class="text-bottom">
    <?= $content2 ?>
</div>