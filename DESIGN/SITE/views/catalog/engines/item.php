<?php
/**
 * @var array $item
 * @var array $characteristics
 */
?>

<? if (
    ($images = unserialize($item['image']))
    && $showImage == true
): ?>
    <div class="preview-column">
        <? foreach($images as $key => $image): ?>
            <a rel="gallery1" href="<?= MSFiles::getImageUrl($images, 'big', $key)?>" class="fancybox-pic">
                <?= MSFiles::getImage($images, 'view', '', $key)?>
            </a>
        <? endforeach; ?>
    </div>
<? endif; ?>

<div class="catalog-item-engine-info_in">

<div class="catalog-item-engine-info_in tab">

  <div class="catalog-item-engine-tab-menu">
    <ul class="mobile-carousel">
        <? foreach($tabs as $data): ?>
            <? if ($_SERVER['REQUEST_URI'] == $data['url']): ?>
                <li class="active">
                    <a href="<?= $data['url'] ?>">
                        <?= $data['title'] ?>
                    </a>
                </li>
            <? else: ?>
                <li>
                    <a href="<?= $data['url'] ?>">
                        <?= $data['title'] ?>
                    </a>
                </li>
            <? endif; ?>
        <? endforeach; ?>
    </ul>
  </div>

  <div>
      <? foreach($tabs as $data): ?>
          <? if (isset($data['content'])): ?>
              <?= $data['content'] ?>
          <? endif; ?>
      <? endforeach; ?>
  </div>

  <div class="text-bottom">
    <?= $item['text'] ?>
  </div>
</div>