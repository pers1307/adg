<?php
/**
 * @var MSBaseCatalog $model
 * @var array $menuItems
 * @var string $menuTitle
 */

if (isset($model) && $model instanceof MSBaseCatalog) {
    $menuItems = $model->fullTree;
}
?>
<div class="catalog-nav_rubric catalog-nav-rubric_no-select">
    <?php
    if (isset($menuTitle)) {
        ?><h5><?= $menuTitle ?></h5><?php
    }

    ?><ul><?php
        foreach ($menuItems as $item) {
            if (isset($model)) {
                $path = $model->getArticleLink($item['id']);
            } else {
                $path = $item['path'];
            }
            ?><li<?= $item['selected'] ? ' class="active"' : '' ?>><a href="<?= $path ?>"><?= $item['name'] ?></a></li><?php
        }
    ?>
    </ul>
</div>