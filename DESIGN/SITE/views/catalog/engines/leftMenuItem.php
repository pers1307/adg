<?php
/**
 * @var EngineModel $model
 * @var string $menuTitle
 */
?>
<div class="catalog-nav_rubric">
    <?php
    if (isset($menuTitle)) {
        ?><h5><?= $menuTitle ?></h5><?php
    }
    ?>
    <ul>
        <?php
        foreach ($model->fullTree as $article) {
            ?>
            <li<?= $article['selected'] ? ' class="active"' : '' ?>>
                <a href="<?= $model->getArticleLink($article['id']) ?>"><?= $article['name'] ?></a>
                <?php
                if ($article['selected']) {
                    $data = $model->getItems();
                    $currentItem = $model->getCurrentItem();
                    ?><ul class="drop-menu"><?php
                    foreach ($data['items'] as $item) {
                        $active = ($currentItem !== null && $currentItem['id'] == $item['id']);
                        ?><li<?= $active ? ' class="active"' : '' ?>><a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a></li><?php
                    }
                    ?></ul><?php
                }
                ?>
            </li>
            <?php
        }
        ?>
    </ul>
</div>