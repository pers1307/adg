<?php
/**
 * @var EngineModel $model
 * @var array $article
 * @var string $content
 * @var MSBasePagination $pagination
 */
?>
<div class="text-top">
    <div class="logo-info">
        <? $imageLogoUrl = MSFiles::getImageUrl($article['image'], 'min'); ?>

        <? if(!empty($imageLogoUrl)): ?>
            <span class="preview">
              <img
                      src="<?= $imageLogoUrl ?>"
                      width="200"
                      height="200"
                      alt="<?= $article['name'] ?>"
              >
            </span>
        <? endif; ?>

        <div class="center">
            <?= $article['text'] ?>
        </div>
    </div>
</div>
<div class="catalog-list2">
    <?php foreach ($items as $item) { ?>
        <a href="<?= $item['itemLink']?>" class="catalog-item_engine">
            <span class="catalog-item-engine_in">
                <span class="preview">
                    <?php
                    if ($image = MSFiles::getImage($item['image'], 'preview')) {
                        echo $image;
                    }
                    ?>
                </span>
                <span class="name"><?= $item['title'] ?></span>
                <span class="param">Мощность: <?= $item['mainPowerKw'] ?> кВт</span>
            </span>
        </a>
        <?php
    } ?>
</div>

<?= $pagination ?>

<div class="text-bottom">
    <?= $article['textBottom'] ?>
</div>