<? if(!empty($parts)): ?>

<div class="accordion catalog-item-engine--accordion">
  <? foreach($parts as $name => $items): ?>
    <div class="accordion-item">
      <div class="accordion-title">
        <h5><?= $name ?></h5>
      </div>
      <div class="drop accordion-drop">
        <table>
          <? foreach($items as $item): ?>
            <tr>
              <td><?= $item['title'] ?></td>
              <td><?= $model->formatAvailable($item['available']) ?>
            </tr>
          <? endforeach; ?>
        </table>
      </div>

    </div>
  <? endforeach; ?>
</div>

<? endif; ?>