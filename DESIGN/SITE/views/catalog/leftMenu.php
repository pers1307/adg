<?php
/**
 * @var MSBaseCatalog $model
 * @var array $menuItems
 * @var string $menuTitle
 */

if (isset($model) && $model instanceof MSBaseCatalog) {
    $menuItems = $model->fullTree;
}
?>
<div class="catalog-nav_rubric">
    <?php
    if (isset($menuTitle)) {
        ?><div class="title-h5"><?= $menuTitle ?></div><?php
    }

    ?><ul><?php
        foreach ($menuItems as $item) {
            if (isset($model)) {
                $path = $model->getArticleLink($item['id']);
            } else {
                $path = $item['path'];
            }
            ?><li<?= $item['selected'] ? ' class="active"' : '' ?>><a href="<?= $path ?>" title="<?= $item['title_h1'] ?>"><?= $item['name'] ?></a></li><?php
        }
    ?>
    </ul>
</div>