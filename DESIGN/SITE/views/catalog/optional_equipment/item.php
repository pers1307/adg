<?php

?>
<div class="preview-column">
    <?php
    if ($images = unserialize($item['image'])) {
        foreach ($images as $key => $image) {
            ?><a rel="gallery1" href="<?= MSFiles::getImageUrl($images, 'big', $key)?>" class="fancybox-pic"><?= MSFiles::getImage($images, 'view', '', $key)?></a><?php
        }
    }
    ?>
</div>

<div class="catalog-item-engine-info_in">

    <? if (!empty($item['price']) && $item['price'] != '0.00'): ?>
        <div class="cash"><span><?= number_format($item['price'], 0, '', ' ')?></span> руб</div>
    <? else: ?>
        <div class="cash">Цена предоставляется по запросу</div>
    <? endif; ?>

  <div class="text-bottom">
    <?= $item['text'] ?>
  </div>
</div>