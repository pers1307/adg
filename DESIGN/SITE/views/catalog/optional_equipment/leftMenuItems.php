<?php
/**
 * @var array $menuItems
 * @var array $currentItem
 * @var string $menuTitle
 */

?>
<div class="catalog-nav_rubric">
    <?php
    if (isset($menuTitle)) {
        ?><h5><?= $menuTitle ?></h5><?php
    }

    ?><ul><?php
        foreach ($menuItems as $item) {
            ?><li<?= $item['id'] == $currentItem['id'] ? ' class="active"' : '' ?>><a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a></li><?php
        }
    ?>
    </ul>
</div>