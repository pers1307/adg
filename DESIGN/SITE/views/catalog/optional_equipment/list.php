<?php
/**
 * @var OptionalEquipmentModel $model
 * @var array $article
 * @var string $content
 * @var MSBasePagination $pagination
 */
?>
<div class="text-top">
    <?= $article['text'] ?>
</div>
<div class="catalog-list-equipment">
    <?php foreach ($items as $item) { ?>
        <div class="catalog-item-equipment">
            <div class="catalog-item-equipment_top">
                <?php
                if (!empty($item['price'])) {
                    ?><div class="cash"><span><?= number_format($item['price'], 0, '', ' ')?></span> руб</div><?php
                }
                ?>

                <a href="<?= $item['itemLink']?>" class="name"><span><?= $item['title']?></span></a>
            </div>
            <div class="catalog-item-equipment_in">
                <?php
                if ($image = MSFiles::getImage($item['image'], 'view')) {
                    ?>
                    <a href="<?= $item['itemLink'] ?>" class="preview">
                        <?= $image ?>
                    </a>
                    <?php
                }
                ?>

                <div class="text">
                    <?= $item['announce'] ?>
                </div>
            </div>
        </div>
        <?php
    } ?>
</div>

<?= $pagination ?>

<div class="text-bottom">
    <?= $article['textBottom'] ?>
</div>