<?php
/**
 * @var PowerStationModel $model
 * @var MSBaseTape $spareModel
 * @var array $article
 * @var array $items
 * @var array $statuses
 * @var array $variants
 * @var array $OEPrices
 */
?>
<section class="product-info js-hover-wrap">
    <article class="wrapper">
        <?= template('common/breadcrumbs') ?>
        <?= template('common/breadcrumbsMobile') ?>
        <div class="product-info_in">
            <?php
            if ($imageUrl = MSFiles::getImageUrl($article['image'], 'view')) {
                ?><div class="preview">
                    <img data-url-default="<?= $imageUrl ?>" class="js-preview" src="<?= $imageUrl ?>" alt="<?= $article['title_h1'] ?>" title="<?= $article['title_h1'] ?>">
                </div><?php
            }
            ?>

            <div class="product-info_right">
                <div class="product_in">
                    <div class="product-in_wrap">
                        <?php
                        if ($presentation = MSFiles::getFilePath($article['presentation'])) {
                            ?><a href="<?= $presentation ?>" download class="download-link download-link_green"><i class="icon-download-green"></i>Скачать спецификацию</a><?php
                        }

                        if ($model::getMinPriceFromItems($items) > 0) {
                          ?><div class="cash">от  <span><?= number_format($model::getMinPriceFromItems($items), 0, '.', ' ') ?></span> руб <i title="Указана рекомендуемая розничная цена в базовом исполнении" class="icon-question tooltip">?</i></div><?php
                        }
                        ?>

                      <ul>
                            <li>Основная мощность: <span><?= !empty($article['characteristics']['mainPowerKw']['value']) ? $article['characteristics']['mainPowerKw']['value'] . ' кВт' : '' ?> / <?= !empty($article['characteristics']['mainPowerKwa']['value']) ? $article['characteristics']['mainPowerKwa']['value'] . ' кВа' : '' ?></span></li>
                            <li>Резервная мощность: <span><?= !empty($article['characteristics']['reservedPowerKw']['value']) ? $article['characteristics']['reservedPowerKw']['value'] . ' кВт' : '' ?> / <?= !empty($article['characteristics']['reservedPowerKwa']['value']) ? $article['characteristics']['reservedPowerKwa']['value'] . ' кВа' : '' ?></span></li>
                            <li>Напряжение: <span><?= !empty($article['characteristics']['voltage']['value']) ? $article['characteristics']['voltage']['value'] . ' В' : '' ?></span></li>
                            <li>Модель двигателя: <span><?= !empty($article['engine']) ? $article['engine']['title'] : '' ?></span></li>
                        </ul>
                        <button class="btn btn-green">Запросить коммерческое предложение</button>
                    </div>
                </div>
            </div>

        </div>
    </article>

    <section class="version-list">
        <article class="wrapper">
            <h2>Варианты исполнения</h2>
            <div class="version-list_in version-list-in_style">
            <?php
            foreach ($items as $vItem) {
                ?>
                <div class="version-list_item">
                    <a href="<?= $vItem['itemLink'] ?>">
                        <span class="label label-<?= $vItem['stock'] == 1 ? 'orange' : 'green' ?>"><?= $statuses[$vItem['stock']] ?></span>
                        <span data-url-big="<?= MSFiles::getImageUrl($vItem['image'], 'preview')?>" class="preview js-hover-change">
                            <?= MSFiles::getImage($vItem['image'], 'preview', $vItem['title_h1'])?>
                        </span>
                        <span class="name"><?= $variants[$vItem['title']]['titleGenerator1'] ?></span>
                        <?php
                        if ($vItem['price'] > 0) {
                          ?><div class="cash">от  <span><?= number_format($vItem['price'], 0 , '.', ' ')?></span> руб</div><?php
                        }
                        ?>
                    </a>
                </div>
                <?php
            }
            ?>
            </div>
        </article>
    </section>
</section>

<?php
$tabs = [
    'characteristics' => 'Технические характеристики',
    'description' => 'Описание генератора',
    'equipment' => 'Стандартная комплектация',
    'spares' => 'Запчасти и расходные материалы',
];

$vars = [
    'model' => $model,
    'article' => $article,
    'spareModel' => $spareModel,
];

?>
<section class="product-more-info tab tablet-accordion">
    <article class="wrapper">
        <div class="tab-menu hide-tablet">
            <ul>
            <?php
                $i = 0;
                foreach ($tabs as $tabTitle) {
                    ?><li<?= ($i++ === 0 ? ' class="active"': '')?>><?= $tabTitle ?></li><?php
                }
            ?>
            </ul>
        </div>
        <div class="tab-content">
            <?php
                $i = 0;
                foreach ($tabs as $tabName => $tabTitle) {
                    ?>
                    <div class="tab-item<?= !$i++ ? ' active' : '' ?>">
                        <div class="tab-title"><span><?= $tabTitle ?></span></div>
                        <div class="hide-tablet ">
                            <?php echo template('catalog/power_stations/tabs/' . $tabName, $vars); ?>
                        </div>
                    </div>
                    <?php
                }
                unset($i);
            ?>
        </div>
    </article>
</section>