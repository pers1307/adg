<?php
/**
 * @var PowerStationModel $model
 * @var string $content
 * @var string $content2
 * @var array $statuses
 * @var array $variants
 * @var EngineModel $engineModel
 */
?>
<div class="text-top">
    <?= $content ?>
</div>

<? if(!empty($allEngines)): ?>
    <div class="engines-filter">
        <h2>Подбор дизельных электростанций по марке двигателя</h2>
        <form action="<?= setFormUrl() ?>" method="get">
            <div class="engine-wrapper">
                <?php
                foreach($allEngines as $k=>$v) { ?>
                    <span class="engine-wrapper-item">
                  <input
                          id="engine-wrapper-<?= $v['id'] ?>"
                          type="checkbox"
                          name="engine[]"
                          value="<?= $v['id'] ?>"
                      <?php
                      if(in_array($v['id'], $getEngine)) echo ' checked ';
                      ?>
                  >

                    <label for="engine-wrapper-<?= $v['id'] ?>">
							<?php
                            if(!empty($v['image'])	&& $imageUrl = MSFiles::getImageUrl($v['image'], 'list')) {
                                echo '<img src="https://adg-energy.ru'.$imageUrl.'" alt="'.$v['name'].'"><br>';
                            }
                            ?>
                            <?= $v['name'] ?>
					</label>
				</span>
                    <?php
                }
                ?>
            </div>
            <div class="engine-wrapper_btn">
                <div>
                    <input class="btn btn-orange" type="submit" value="Показать">
                </div>
                <div>
                    <a href="<?= getClearUrl() ?>" class="reset-button btn btn-white ">Сбросить</a>
                </div>
            </div>

        </form>
    </div>
<? endif; ?>

<div class="catalog-list">
    <?php
        foreach ($articles as $key => $item) {

            $items  = $item['items'];
            $parent = $item['parent'];
            ?>
            <div class="catalog-item">
                <div class="catalog-item_top">
                  <?php
                    if ($filePath = MSFiles::getFilePath($item['presentation'])) {
                        ?>
                      <a href="<?= $filePath?>" download class="download-link download-link_green"><i class="icon-download-green"></i>Скачать спецификацию</a>
                      <?php
                    }

                    if ($model::getOpenTypePrice($items) > 0) {
                        ?><div class="cash">от  <span><?= number_format($model::getOpenTypePrice($items), 0, '.', ' ')?></span> руб</div><?php
                    }
                    ?>

                    <a href="<?= $model->getArticleLink($item['id']) ?>" class="name">
                        <span><?= $item['name'] ?> <?= $parent['name'] ?></span>

                        <? if (
                            !empty($item['engine']['brand']['image'])
                            && $imageUrl = MSFiles::getImageUrl($item['engine']['brand']['image'], 'list')
                        ): ?>
                            <span class="preview">
                                <img src="<?= $imageUrl ?>"
                                     width="200"
                                     height="200"
                                     alt="<?= $item['engine']['brand']['name'] ?>"
                                >
                            </span>
                        <? endif; ?>
                    </a>
                </div>
                <div class="catalog-item_preview-group js-hover-wrap">
                    <div class="catalog-item-preview_big">
                        <?php
                        if ($imageUrl = MSFiles::getImageUrl($item['image'], 'view')) {
                            ?>
                            <? $alt = PowerStationMainPageHelper::transformAltForImg($item) ?>
                            <a href="<?= $model->getArticleLink($item['id']) ?>">
                                <img data-url-default="<?= $imageUrl ?>" class="js-preview" src="<?= $imageUrl ?>" alt="<?= $alt ?>" title="<?= $alt ?>">
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="catalog-item-preview_small">
                        <div class="title">Варианты исполнения:</div>
                        <?php
                        if (!empty($items)) {

                            ?><div class="catalog-item-preview-small_list"><?php

                            foreach ($items as $_item) {
                                ?>

                                <? $alt = PowerStationMainPageHelper::transformAltForImgInVariants($_item,$variants,$item['name']); ?>

                                <a href="<?= $_item['itemLink'] ?>" class="catalog-item-preview-small_item">
                                    <span class="label label-<?= $_item['stock'] == 1 ? 'orange' : 'green' ?>"><?= $statuses[$_item['stock']] ?></span>
                                    <span data-url-big="<?= MSFiles::getImageUrl($_item['image'], 'preview')?>" class="preview js-hover-change">
                                    <?= MSFiles::getImage($_item['image'], 'view', $alt)?>
                                </span>
                                    <?= $variants[$_item['title']]['name'] ?>
                                </a>
                                <?php
                            }
                            ?></div><?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
    ?>
</div>

<? if(!empty($pagination)): ?>
    <?= $pagination ?>
<? endif; ?>

<div class="text-bottom">
    <?= $content2 ?>
</div>