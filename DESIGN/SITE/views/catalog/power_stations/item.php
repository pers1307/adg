<?php
/**
 * @var PowerStationModel $model
 * @var MSBaseTape $spareModel
 * @var array $article
 * @var array $items
 * @var array $item
 * @var array|null $item    if $model->isItem()
 * @var array $statuses
 * @var array $variants
 * @var array $OEPrices
 * @var float $priceForApi
 */
?>
<section class="product-info js-hover-wrap">
    <article class="wrapper">
        <?= template('common/breadcrumbs') ?>
        <?= template('common/breadcrumbsMobile') ?>
        <div class="product-info_in">
            <?php

            if ($gallery = unserialize($item['image'])) {
                $bigSlides = $smallSlides = [];
                foreach ($gallery as $key => $value) {
                    $title_h1  = $item['title_h1'] . " " . ($key + 1);
                    $bigSlides[] = '<div class="carousel-item fancybox" data-fancybox-href="' . MSFiles::getImageUrl($gallery, 'big', $key). '" data-fancybox-group="group">' . MSFiles::getImage($gallery, 'view', $title_h1 , $key) . '</div>';
                    $smallSlides[] = '<div class="carousel-item">' . MSFiles::getImage($gallery, 'small', $item['title_h1'] , $key). '</div>';
                }
                ?>
                <div class="product-gallery">
                    <div class="big-slider-product slider-for">
                        <?= implode('', $bigSlides) ?>
                    </div>
                    <div class="small-slider-product slider-nav">
                        <?= implode('', $smallSlides) ?>
                    </div>
                </div>
                <?php
            }
            ?>
            <div class="product-info_right">
                <div class="product_in">
                    <div class="product-in_wrap">
                        <?php
                        if ($presentation = MSFiles::getFilePath($article['presentation'])) {
                            ?><a href="<?= $presentation ?>" download class="download-link download-link_green"><i class="icon-download-green"></i>Скачать спецификацию</a><?php
                        }

                        if ($item['price'] > 0) {
                          ?><div class="cash">от  <span><?= number_format($item['price'], 0, '.', ' ') ?></span> руб <i title="Указана рекомендуемая розничная цена завода в базовом исполнении." class="icon-question tooltip">?</i></div><?php
                        }
                        ?>
                        <ul>
                            <li>Основная мощность: <span><?= !empty($article['characteristics']['mainPowerKw']['value']) ? $article['characteristics']['mainPowerKw']['value'] . ' кВт' : '' ?> / <?= !empty($article['characteristics']['mainPowerKwa']['value']) ? $article['characteristics']['mainPowerKwa']['value'] . ' кВа' : '' ?></span></li>
                            <li>Резервная мощность: <span><?= !empty($article['characteristics']['reservedPowerKw']['value']) ? $article['characteristics']['reservedPowerKw']['value'] . ' кВт' : '' ?> / <?= !empty($article['characteristics']['reservedPowerKwa']['value']) ? $article['characteristics']['reservedPowerKwa']['value'] . ' кВа' : '' ?></span></li>
                            <li>Напряжение: <span><?= !empty($article['characteristics']['voltage']['value']) ? $article['characteristics']['voltage']['value'] . ' В' : '' ?></span></li>
                            <li>Модель двигателя: <span><?= !empty($article['engine']) ? $article['engine']['title'] : '' ?></span></li>
                        </ul>
                        <button class="btn btn-green">Запросить коммерческое предложение</button>
                    </div>

                    <div class="product-in_bottom">
                        Выберите необходимую комплектацию
                        <i></i>
                    </div>
                </div>
            </div>

        </div>
    </article>

    <section class="version-list">
        <article class="wrapper">
            <h2>Варианты исполнения</h2>
            <div class="version-list_in version-list-in_style">
            <?php
            foreach ($items as $vItem) {
                ?>
                <div class="<?= $item['id'] == $vItem['id'] ? 'active ' : '' ?>version-list_item">
                    <?= ($item['id'] != $vItem['id'] ? '<a href="' . $vItem['itemLink'] .'">' : '<span>') ?>
                        <span class="label label-<?= $vItem['stock'] == 1 ? 'orange' : 'green' ?>"><?= $statuses[$vItem['stock']] ?></span>
                        <span data-url-big="<?= MSFiles::getImageUrl($vItem['image'], 'preview')?>" class="preview js-hover-change">
                            <?= MSFiles::getImage($vItem['image'], 'preview', $vItem['title_h1'] )?>
                        </span>
                        <span class="name"><?= $variants[$vItem['title']]['titleGenerator1'] ?></span>
                        <?php
                        if ($vItem['price'] > 0) {
                            ?><div class="cash">от  <span><?= number_format($vItem['price'], 0 , '.', ' ')?></span> руб</div><?php
                        }
                        ?>
                    <?= ($item['id'] != $vItem['id'] ? '</a>' : '</span>') ?>
                </div>
                <?php
            }
            ?>
            </div>
        </article>
    </section>
</section>

<?php
$tabs = [
    'variant' => 'Описание ' . mb_strtolower($item['variant']['name'], 'utf-8'),
    'characteristics' => 'Технические характеристики',
    'description' => 'Описание генератора',
    'equipment' => 'Стандартная комплектация',
    'spares' => 'Запчасти и расходные материалы',
];

$vars = [
    'model' => $model,
    'article' => $article,
    'spareModel' => $spareModel,
    'item' => $item,
    'OEPrices' => $OEPrices,
    'items' => $items,
    'variants' => $variants,
    'priceForApi' => $priceForApi
];

?>
<section class="product-more-info tab tablet-accordion">
    <article class="wrapper">
        <div class="tab-menu hide-tablet">
            <ul>
            <?php
                $i = 0;
                foreach ($tabs as $tabTitle) {
                    ?><li<?= ($i++ === 0 ? ' class="active"': '')?>><?= $tabTitle ?></li><?php
                }
            ?>
            </ul>
        </div>
        <div class="tab-content">
            <?php
                $i = 0;
                foreach ($tabs as $tabName => $tabTitle) {
                    ?>
                    <div class="tab-item<?= !$i++ ? ' active' : '' ?>">
                        <div class="tab-title"><span><?= $tabTitle ?></span></div>
                        <div class="hide-tablet ">
                            <?php echo template('catalog/power_stations/tabs/' . $tabName, $vars); ?>
                        </div>
                    </div>
                    <?php
                }
                unset($i);
            ?>
        </div>
    </article>
</section>

<div id="order-popup" class="callback-popup">
    <?= template( 'common/forms/order', [
        'formCaption' => 'Рассчитать стоимость доставки',
        'formText' => 'После получения заявки мы рассчитаем стоимость доставки и отправим Вам предложение с общей итоговой ценой',
        'action' => '/api/powerStation.order/',
        'uniqueId' => md5('form-order'),
        'item' => $item,
        'priceForCalc' => $priceForApi
    ]); ?>
</div>