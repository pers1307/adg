<?php
/**
 * @var PowerStationModel $model
 * @var MSBaseTape $industryModel
 */
echo template('catalog/leftMenu', [
    'model' => $model,
    'menuTitle' => 'Мощность'
]);

echo template('industry/leftMenu', [
    'model' => $industryModel,
    'title' => 'Отраслевые решения',
	'modelList'=> $modelList
]);