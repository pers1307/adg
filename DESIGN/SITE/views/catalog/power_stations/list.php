<?php
/**
 * @var PowerStationModel $model
 * @var array $article
 * @var string $content
 * @var MSBasePagination $pagination
 * @var array $statuses
 * @var array $variants
 */
?>
<div class="text-top">
    <?= $article['text'] ?>
</div>

<div class="catalog-list">
    <?php
    foreach ($model->getChildrenTree($article['id']) as $key => $item) {

        $parent = $model->getArticleById($item['parent']);

        $model->_currentArticle = $item;
        $itemsData = $model->getItems(null, false);
        $items = $itemsData['items'];
        if (!empty($item['engine'])) {
            $item['engine'] = $engineModel->getItemById($item['engine']);
        }

        ?>
        <div class="catalog-item">
            <div class="catalog-item_top">
                <?php
                if ($filePath = MSFiles::getFilePath($item['presentation'])) {
                    ?><a href="<?= $filePath ?>" download class="download-link download-link_green"><i class="icon-download-green"></i>Скачать спецификацию</a><?php
                }

                if ($model::getOpenTypePrice($items) > 0) {
                    ?><div class="cash">от  <span><?= number_format($model::getOpenTypePrice($items), 0, '.', ' ')?></span> руб</div><?php
                }
                ?>

                <a href="<?= $model->getArticleLink($item['id']) ?>" class="name">
                    <span><?= $item['name'] ?> <?= $parent['name'] ?></span>

                    <? if (
                        !empty($item['engine']['brand']['image'])
                        && $imageUrl = MSFiles::getImageUrl($item['engine']['brand']['image'], 'list')
                    ): ?>
                        <span class="preview">
                                <img src="<?= $imageUrl ?>"
                                     width="200"
                                     height="200"
                                     alt="<?= $item['engine']['brand']['name'] ?>"
                                >
                            </span>
                    <? endif; ?>
                </a>
            </div>
            <div class="catalog-item_preview-group js-hover-wrap">
                <div class="catalog-item-preview_big">
                    <?php
                    if ($imageUrl = MSFiles::getImageUrl($item['image'], 'view')) {
                        ?>
                        <a href="<?= $model->getArticleLink($item['id']) ?>">
                            <img data-url-default="<?= $imageUrl ?>" class="js-preview" src="<?= $imageUrl ?>" title="<?= 'Дизельная электростанция (генератор) ADG-ENERGY '. $item['name'] . ' ' . $article['name'] ?>" alt="<?= 'Дизельная электростанция (генератор) ADG-ENERGY '. $item['name'] . ' ' . $article['name'] ?>">
                        </a>
                        <?php
                    }
                    ?>
                </div>
                <div class="catalog-item-preview_small">
                    <div class="title">Варианты исполнения:</div>
                    <?php
                    if (!empty($items)) {

                        ?><div class="catalog-item-preview-small_list"><?php

                        foreach ($items as $_item) {
                            ?>
                            <? $_item = PowerStationTitleHelper::transformAltForImage($_item, $article, $variants, $item); ?>
                            <a href="<?= $_item['itemLink'] ?>" class="catalog-item-preview-small_item">
                                <span class="label label-<?= $_item['stock'] == 1 ? 'orange' : 'green' ?>"><?= $statuses[$_item['stock']] ?></span>
                                <span data-url-big="<?= MSFiles::getImageUrl($_item['image'], 'preview')?>" class="preview js-hover-change">
                                    <?= MSFiles::getImage($_item['image'], 'preview', $_item['alt'])?>
                                </span>
                                <?= $variants[$_item['title']]['name'] ?>
                            </a>
                            <?php
                        }
                        ?></div><?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>

<div class="text-bottom">
    <?= $article['textBottom'] ?>
</div>