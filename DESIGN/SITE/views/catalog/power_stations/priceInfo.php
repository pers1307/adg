<?php
/**
 * @var float $price
 * @var bool $adjustment
 */
?>
<span><?= number_format($price, 0, '', ' ') . ($adjustment ? '<span>*</span>' : '') ?></span> руб
<?php
if ($adjustment) {
    ?>
  <script>
    $('.product-result_in').addClass('full')
  </script>
    <span><?= getTextMod(10, false, true) ?></span>
    <?php
} else{
  ?>
  <script>
    $('.product-result_in').removeClass('full')
  </script>
  <?
}
?>