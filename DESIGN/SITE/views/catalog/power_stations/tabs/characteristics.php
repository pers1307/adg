<?php
/**
 * @var PowerStationModel $model
 * @var array $article
 */

$tables = [
    'characteristics' => 'Основные характеристики',
    'control' => 'Система управления',
    'electrical' => 'Система электрооборудования',
    'dimensions' => 'Размер и вес',
    'generator' => 'Генератор',
    'engine' => 'Двигатель',
    'information' => 'Дополнительная информация'
];
$i = 0;

?>
<div class="column">
    <?php
        foreach ($tables as $key => $title) {
            if ($key == 'engine') {
                $props = EngineModel::getProperties($article[$key])['characteristics'];
                $props = array_merge([[
                    'caption' => 'Модель',
                    'value' => $article[$key]['title'],
                ]], $props);
            } else {
                $props = $article[$key];
            } ?>
            <div class="product-more-info_item">
                <div class="product-more-info-item_top">
                    <i><img src="/DESIGN/SITE/images/characteristic_icons/icon<?= (++$i) ?>.png" alt=""></i>
                    <?= $title ?>
                </div>
                <table>
                    <?php foreach ($props as $value) {

                        if (empty($value['value'])) {
                            continue;
                        }

                        if (isset($value['output'])) {
                            switch ($value['output']) {
                                case 'gold-star':
                                    $value['value'] = Html::stars($value['value']);
                                    break;
                                default:
                                    break;
                            }
                        }
                        ?>
                        <tr>
                            <td>
                                <?= $value['caption'] ?>
                                <? if(isset($value['name']) && !empty(HelperPropsDescription::getDescriptionByProp($value['name']))): ?>
                                    <? $text = HelperPropsDescription::getDescriptionByProp($value['name']) ?>
                                    <i title="<?= $text ?>" class="icon-question tooltip">?</i>
                                <? endif; ?>
                            </td>
                            <td
                                <?= isset($value['output']) ? ' class="' . $value['output'] . '"' : '' ?>><?= $value['value']; ?>
                            </td>
                        </tr>
                    <?php } ?>
                </table>
            </div>
            <?php
            if ($i == 4) {
                ?></div><div class="column"><?php
            }
        } ?>
</div>