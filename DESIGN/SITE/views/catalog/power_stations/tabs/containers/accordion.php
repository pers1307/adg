<?php
/**
 * @var array $options
 * @var bool $withPrice
 * @var bool $more
 */
?>
<div class="accordion">
    <?php
    foreach ($options as $key => $option) {
        $additionalItem = new AdditionalItem($option);
        ?>
        <div class="accordion-item">
            <div class="accordion-title">
                <?php
                if ($withPrice) {
                    ?>
                    <div class="checkbox">
                        <label for="checkbox-accordion<?= $option['id'] ?>">
                            <input type="checkbox" id="checkbox-accordion<?= $option['id'] ?>" name="options[<?= $option['id'] ?>]">
                            <span class="icon"></span>
                            <span>
                                <?php
                                if ($additionalItem->individual) {
                                    ?>Рассчитывается<br>индивидуально<?php
                                } else {
                                    ?><span class='cash'><span class='plus'>+</span><?= number_format(CalculatePrice::processPrice($additionalItem), 0, '',' ') ?></span><i>&nbsp; руб</i><?php
                                }
                                ?>
                            </span>
                        </label>
                    </div>
                    <?php
                }
                ?>
                <i><?= str_pad(++$key,2,"0",STR_PAD_LEFT) ?>.</i>
                <h5><span><?= $option['title'] ?></span></h5>
            </div>
            <div class="accordion-drop drop">
                <?php
                if ($imageUrl = MSFiles::getImageUrl($option['image'], 'view')) {
                    ?><div class="img fl_l"><img src="<?= $imageUrl ?>"></div><?php
                }
                echo $option['text'];

                if ($option['itemLink']) {
                    ?><a href="<?= $option['itemLink'] ?>" target="_blank">Подробнее</a><?php
                }
                ?>
            </div>
        </div>
        <?php
    }
    ?>
</div>