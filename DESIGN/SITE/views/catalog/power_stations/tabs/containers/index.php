<?php
/**
 * @var string $title
 * @var string $text
 * @var array $options
 * @var bool $withPrice
 */
?>

<div class="container-info_item">
    <div class="title"><?= $title ?></div>

    <?php
    if (!empty($text)) {
        echo template('catalog/power_stations/tabs/containers/text', ['text' => $text]);
    } else {
        echo template('catalog/power_stations/tabs/containers/accordion', [
            'options' => $options,
            'withPrice' => isset($withPrice) && $withPrice == true,
        ]);
    }
    ?>
</div>