<?php
/**
 * @var array $items
 * @var array $item
 * @var array $variants
 */
?>
<h5>Чтобы рассчитать стоимость выберите вариант исполнения:</h5>
<div class="link-group">
    <?php
    foreach ($items as $key => $value) {
        //$value['title'] == 1 говнокостыль, убрать из списка открытый вариант
        if ($value['id'] == $item['id'] || $value['title'] == 1) {
            continue;
        }
        echo '<a href="' . $value['itemLink'] . '">' . $variants[$value['title']]['name'] .'</a>';
    }
    ?>
</div>