<?php
/**
 * @var PowerStationModel $model
 * @var array $article
 */
?>
<div class="product-text">
    <h2>Описание дизельного генератора <?= $article['fullName'] ?></h2>
    <?= $article['text'] ?>

    <?php
    if (!empty($article['industries'])) {
        ?>
        <h3>Применение генератора по отраслям</h3>
        <ul class="list-link">
        <?php
        foreach ($article['industries'] as $industry) {
            ?><li><a href="<?= $item['itemLink'] = $model->path . '/' . ($industry['code'] ? $industry['code'] : $industry['id']) . '/'?>"><?= $industry['title'] ?></a></li><?php
        }
        ?></ul><?php
    }
    ?>
</div>