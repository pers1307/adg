<?php
/**
 * @var PowerStationModel $Model
 * @var array $article
 */
?>
<div class="product-equipment">
    <h2>Комплектация дизельного генератора <?= $article['fullName'] ?></h2>
    <!--h3>Стандартная (H3)</h3-->
    <div class="accordion">
        <?php
        foreach ($article['equipment'] as $key => $equipment) {
            ?><div class="accordion-item <? if ($key === 1) { ?>active<? }?>">
                <div class="accordion-title">
                    <i><?= str_pad(++$key,2,"0",STR_PAD_LEFT) ?>.</i>
                    <h5><span><?= $equipment['title'] ?></span></h5>
                </div>
                <div <? if ($key === 1) { ?>style="display: block;"<? }?> class="drop accordion-drop">
                    <?php
                    if ($image = MSFiles::getImage($equipment['image'], 'min')) {
                        ?><div class="img fl_l"><?= $image?></div><?php
                    }
                    ?>
                  <div class="product-text">
                    <?= $equipment['text'] ?>
                  </div>
                </div>
            </div><?php
        }
        ?>
    </div>
</div>