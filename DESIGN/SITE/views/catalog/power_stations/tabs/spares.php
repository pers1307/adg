<?php
/**
 * @var PowerStationModel $model
 * @var array $article
 */
?>
<div class="product-text">
    <h2>Запасные части и расходные материалы для электростанции <?= $article['fullName'] ?></h2>

    <div class="accordion">
        <?php
        if ($spareItems = $spareModel->getItems()['items']) {
            foreach ($spareItems as $sKey => $sValue) {
                ?>
                <div class="accordion-item">
                    <div class="accordion-title">
                        <i><?= str_pad(($sKey+1),2,"0",STR_PAD_LEFT) ?>.</i>
                        <h5><span><?= $sValue['title'] ?></span></h5>
                    </div>
                    <div class="drop accordion-drop">
                        <?php
                        if ($spareImage = MSFiles::getImage($sValue['image'], 'preview')) {
                            ?>
                            <div class="img fl_l">
                                <?= $spareImage ?>
                            </div>
                            <?php
                            unset($spareImage);
                        }

                        echo $sValue['text'];
                        ?>
                    </div>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>