<?php
/**
 * @var PowerStationModel $model
 * @var array $article
 * @var array $item
 * @var array $OEPrices
 * @var float $priceForApi
 */
?>
<div class="container-info">
    <aside class="container-info_preview">
        <div class="container-info-preview_wrap">
            <div class="preview">
                <img class="js-change-preview" src="<?= MSFiles::getImageUrl($item['image'], 'view') ?>" alt="">
            </div>
        </div>
    </aside>

    <div class="container-info_left">
        <h4>Для электростанции <?= $item['title'] ?> дополнительно можно установить</h4>
        <div class="container-info_in">
            <form id="priceItemForm" method="post" action="/api/powerStation.calc/">
                <input type="hidden" value="<?= $item['id'] ?>" name="itemId">
                <input type="hidden" value="<?= $priceForApi ?>" name="priceForApi">
                <?php
                foreach ($item['variant']['items'] as $variantItem) {
                    if ($variantItem['show_in_station']) {
                        if (!empty($variantItem['text'])) {
                            $variantItem['text'] .= template('catalog/power_stations/tabs/containers/linkGroup', [
                                'item' => $item,
                                'items' => $items,
                                'variants' => $variants,
                            ]);
                        }
                        echo template('catalog/power_stations/tabs/containers/index', $variantItem);
                    }
                }

                if (!empty($OEPrices)) {
                    $OEPath = path(OPTIONAL_EQUIPMENT);
                    foreach ($OEPrices as $OEPrice) {
                        foreach ($OEPrice['options'] as $key => $price) {
                            $OEPrice['options'][$key]['itemLink'] = DS . $OEPath . ($price['parentCode'] ? $price['parentCode'] : $price['parent']) . DS . ($price['code'] ? $price['code'] : $price['id']) . DS;
                            unset($price);
                        }
                        $OEPrice['withPrice'] = true;
                        echo template('catalog/power_stations/tabs/containers/index', $OEPrice);
                    }
                }
                ?>
            </form>

            <? if ($item['variant']['id'] != 2): ?>
                <div class="product-result_in">
                    <p>Итого:</p>
                    <div class="cash" id="cash-box">
                        <?php
                        $price = new CalculatePrice($item);
                        echo template('catalog/power_stations/priceInfo', [
                            'price' => $price->calculate(),
                            'adjustment' => $price->getAdjustment()
                        ])?>
                    </div>
                    <button data-fancybox-href="#order-popup" class="btn btn-orange js-callback">Купить</button>
                </div>
            <? endif; ?>
        </div>
    </div>
</div>