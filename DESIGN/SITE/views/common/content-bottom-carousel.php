<?php
/**
 * @var PowerStationModel $PSModel
 * @var string $title
 */
?>
<section class="content_bottom-carousel">
    <article class="wrapper">
        <?= $title ?>
      <div class="carousel-wrap">
        <button class="btn next"></button>
        <button class="btn prev"></button>
        <div class="content-bottom-carousel_in">
          <ul>
            <?php
            foreach ($PSModel->fullTree as $PSArticle) {
              ?><li><a href="<?= $PSModel->getArticleLink($PSArticle['id'])?>" title="Дизельные электростанции <?= $PSArticle['name']?>"><?= $PSArticle['name']?></a></li><?php
            }
            ?>
          </ul>

        </div>

      </div>
    </article>
</section>