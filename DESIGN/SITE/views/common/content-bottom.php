<?
/**
 * @var MSBaseTape $model
 * @var array $items
 * @var array $fileDownload
 */
?>
<section id="content_bottom" class="content_bottom">
    <article class="wrapper">
        <div class="content-callback tab">
            <div class="tab-menu">
                <ul class="mobile-carousel">
                    <?php
                    foreach ($items as $key => $item) {
                        ?><li<?= !$key ? ' class="active"' : '' ?>><?= $item['title'] ?></li><?php
                    }
                    ?>
                </ul>
            </div>
            <div class="content-callback_contacts">
                <div class="content-callback-contacts_tel">
                    <?= getTextMod(3); ?>
                </div>
                <div class="content-callback-contacts_email">
                    <span>Электронная почта <br> для заявок:</span>
                    <?= getTextMod(1) ?>
                </div>
            </div>
            <div class="tab-content content-callback_text">
                <?php
                foreach ($items as $key => $item) {
                    ?><div class="tab-item"><?= $item['content'] ?></div><?php
                }
                ?>
            </div>
        </div>
        <div class="content-callback-form">
            <div class="content-callback-form_in">
                <?= template( 'common/forms/callback', [
                    'formCaption' => 'Обратная связь',
                    'fileDownload' => $fileDownload,
                    'action' => '/api/callback.call/',
                    'uniqueId' => md5('form-callback-2'),
                ]); ?>
            </div>
        </div>
    </article>
</section>