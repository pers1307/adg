<?php
/**
 * @var array $files
 */
?>
<h3>Список документов</h3>
<ul class="list-documentations">
    <?php
    foreach ($files as $data) {
        ?>
        <li>
            <a target="_blank" download href="<?= $data['file']['path'] ?>">
                <i>
                    <img src="<?= $data['file']['icon'] ?>" alt="">
                </i>
                <p><?= $data['title'] ?></p>
                <span><?= $data['file']['extension'] ?> - <?= $data['file']['fileSize'] ?></span>
            </a>
        </li>
        <?php
    }
    ?>
</ul>