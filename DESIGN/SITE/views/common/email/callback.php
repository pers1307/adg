<?php
/**
 * @var string $name
 * @var string $phone
 * @var string $email
 * @var string $comment
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Обратная связь</title>
</head>

<body>
    <table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse;">
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Страница сайта</strong></td>
            <td><?= $page ?></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Имя</strong></td>
            <td><?= $full ?></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Номер</strong></td>
            <td><?= $phone ?></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Почта</strong></td>
            <td><?= $email ?></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Комментарий</strong></td>
            <td><?= $comment ?></td>
        </tr>
    </table>
</body>
</html>