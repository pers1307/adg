<?php
/**
 * @var string $name
 * @var string $phone
 * @var string $email
 * @var string $address
 * @var CalculatePrice $price
 * @var array $item
 */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Данные заказа</title>
</head>

<body>
    <table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse;">
        <tr>
            <th colspan="2" style="text-align: left;">Информаця о клиенте</th>
        </tr>
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Имя</strong></td>
            <td><?= $full ?></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Номер</strong></td>
            <td><?= $phone ?></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Почта</strong></td>
            <td><?= $email ?></td>
        </tr>
        <tr>
            <td style="text-align: right; width: 200px; vertical-align: top;"><strong>Адрес доставки</strong></td>
            <td><?= $address ?></td>
        </tr>
    </table>

    <table cellpadding="5" border="1" style="width: 100%; border-collapse: collapse; margin-top: 20px;">
        <tr>
            <th colspan="2" style="text-align: left;">Информация о заказе</th>
        </tr>
        <tr>
            <td style="font-weight: bold;">Наименование</td>
            <td style="width: 120px;">Цена (базовая)</td>
        </tr>
        <tr>
            <td><a href="<?= $item['itemLink'] ?>" target="_blank"><?= $item['parent']['name'] ?> (<?= $item['variant']['name'] ?>)</a></td>
            <td style="text-align: right;"><?= isset($priceForCalc) ? number_format($priceForCalc, 0, '.', ' ') : number_format($price->basePrice(), 0, '.', ' ') ?> руб.</td>
        </tr>
        <?php
        if ($price->getAdditionalItems()) {
            ?>
            <tr>
                <td colspan="2" style="font-weight: bold;">Дополнительное оборудование</td>
            </tr>
            <?php
            /**
             * @var AdditionalItem $option
             */
            foreach ($price->getAdditionalItems() as $option) {
                ?>
                <tr>
                    <td><?= $option->title ?></td>
                    <td style="text-align: right;width: 120px;">
                        <?php
                        if ($option->individual) {
                            echo 'Рассчитывается<br>индивидуально';
                        } else {
                            echo number_format(CalculatePrice::processPrice($option), 0, '.', ' ') . ' руб.';
                        } ?>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr style="text-align: right;font-weight: bold;">
                <td>Итого</td>
                <td><?= number_format($price->calculate(), 0, '.', ' ') ?><?= $price->getAdjustment() ? '<span>*</span>' : '' ?> руб.</td>
            </tr>
            <?php
        }
        ?>
    </table>
</body>
</html>