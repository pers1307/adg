<?php
/**
 * @var array $items
 */

if (!empty($items)) { ?>
    <div class="footer_carousel">
        <?php
        foreach ($items as $item) {
            ?><div class="carousel-item"><?= $item['title'] ?></div><?php
        }
        ?>
    </div>
    <?php
}