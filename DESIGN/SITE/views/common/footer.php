<?php
/**
 * @var array $menu
 * @var array $slides
 */
?>

<footer class="footer">
    <nav class="footer_nav">
        <article class="wrapper">
            <div class="footer-nav_in">
                <ul>
                    <? foreach($menu as $item): ?>
                        <li>
                            <? if ($item['here'] == true): ?>
                                <span><?= $item['title'] ?></span>
                            <? else: ?>
                                <a href="<?= $item['path'] ?>"><?= $item['title'] ?></a>
                            <? endif; ?>
                        </li>
                    <? endforeach; ?>
                </ul>
            </div>
        </article>
    </nav>

    <section class="footer_content">
        <article class="wrapper">
            <div class="footer_left">
                <a href="/" class="footer-logo">
                    <img src="/DESIGN/SITE/images/footer-logo.png" alt="">
                </a>
                <div class="footer_social">
									<!--noindex-->
                    <a rel="nofollow" href="https://www.facebook.com/adgenergy/" target="_blank"><i class="icon-fb"></i></a>
                    <a rel="nofollow" href="https://vk.com/adgenergy" target="_blank"><i class="icon-vk"></i></a>
                    <a rel="nofollow" href="https://twitter.com/adgenergy" target="_blank"><i class="icon-tw"></i></a>
                  <a rel="nofollow" href="https://www.instagram.com/adgenergy/" target="_blank"><i class="icon-inst"></i></a>
									<!--/noindex-->
                </div>
            </div>
            <div class="footer-content_in">
                <?= template('common/footer-carousel', ['items' => $slides])?>
                <div class="footer_contacts">
                    <div class="footer-contacts_item footer-contacts-item_big">
                        <?= getTextMod(3); ?>
                    </div>

                    <div class="footer-contacts_item">
                        <?= getTextMod(4); ?>
                    </div>
                    <div class="footer-contacts_item">
                        <span>Телефон:</span>
                        <?= getTextMod(5); ?>
                    </div>
                    <div class="footer-contacts_item">
                        <span>E-mail: </span>
                        <?= getTextMod(1) ?>
                    </div>
                </div>
            </div>
        </article>
    </section>

    <section class="footer_bottom">
        <article class="wrapper">
            <a href="/<?= path(186); ?>">Политика обработки персональных данных</a>
        </article>
    </section>
</footer>

<div class="btn-up"></div>

<?= StaticFiles::getJS('site-async-scripts') ?>