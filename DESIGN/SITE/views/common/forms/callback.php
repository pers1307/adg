<?php
/**
 * @var string $formCaption
 * @var array $fileDownload [title => fileTitle, path => filePath, size => 30MB, extension => pdf]
 * @var string $uniqueId
 */
?>
<div class="title_h3"><?= $formCaption ?></div>
<?php if (isset($fileDownload) && $fileDownload) {
   ?><a  href="<?= $fileDownload['file']['path'] ?>" download class="download-link download-link_green">
        <i class="icon-download-green"></i>Скачать <?= $fileDownload['title'] ?>
        <span><?= $fileDownload['file']['extension'] ?> - <?= $fileDownload['file']['fileSize'] ?></span>
    </a><?php
}?>
<form action="<?= $action ?>" method="post" enctype="multipart/form-data" class="ajax-form">
    <input type="hidden" data-name="page" name="page" value="<?= MSCore::page()->path_id?>">
    <div class="form-item hdn">
        <input type="text" data-name="name" name="name">
    </div>
    <div class="form-item">
        <input type="text" data-name="full" name="full" placeholder="Ваше имя">
    </div>
    <div class="form-item">
        <input type="text" data-name="phone" name="phone" class="phone" placeholder="Номер телефона">
    </div>
    <div class="form-item">
        <input type="email" data-name="email" name="email" placeholder="Электронная почта">
    </div>
    <div class="form-item">
        <textarea name="comment" placeholder="Принимаются любые вопросы и предложения"></textarea>
    </div>
    <div class="polit">
        <div class="checkbox">
            <label for="checkbox-callback-<?=$uniqueId?>">
                <input type="checkbox" id="checkbox-callback-<?=$uniqueId?>" checked name="confirmation">
                <span class="icon"></span>
                <span>Я согласен с <a href="/<?= path(POLICY) ?>" target='_blank'>политикой обработки</a> персональных данных и даю <a href="/<?= path(CONSENT) ?>" target='_blank'>свое согласие</a> на их обработку</span>
            </label>
        </div>
    </div>
    <div class="btn-group">
        <div class="add-file">
          <a href="mailto:sales@adg-energy.ru">sales@adg-energy.ru</a>
        </div>
        <button class="btn btn-green sending">Отправить</button>
    </div>
</form>