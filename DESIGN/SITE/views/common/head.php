<head>
	<meta charset="<?= SITE_ENCODING ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">

	<title><?= MSCore::page()->title_page ?></title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">
    <?= StaticFiles::getCSS(MSCore::page()->css) ?>
	<?= StaticFiles::getJS(MSCore::page()->js) ?>

	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">

    <link rel="icon" type="image/x-icon" href="/favicon16.ico" sizes="16x16">
    <link rel="icon" type="image/x-icon" href="/favicon32.ico" sizes="32x32">
    <link rel="icon" type="image/x-icon" href="/favicon48.ico" sizes="48x48">
    <? setCanonicalUrl(); ?>
</head>