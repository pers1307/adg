<head>
	<meta charset="<?= SITE_ENCODING ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width">
    <? setCanonicalUrl(); ?>
	<title><?= MSCore::page()->title_page ?></title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext" rel="stylesheet">
    <?= StaticFiles::getCSS(MSCore::page()->css) ?>
<!--    --><?//= StaticFiles::getCSS('catalog-styles') ?>
	<?= StaticFiles::getJS(MSCore::page()->js) ?>

	<link rel="icon" href="/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
</head>