<?php
/**
 * @var string $logoImage
 */
?>
<header class="header">
    <section class="header_top">
        <article class="wrapper">
            <div class="header-top_nav">
                <?php include_once BLOCKS_DIR . DS . 'common/menu.php'; ?>
            </div>
            <div class="header-top_email">
                E-mail: <?=getTextMod(1, false, true)?>
            </div>
        </article>
    </section>

    <section class="header_content">
        <article class="wrapper">
            <a class="logo" href="/"><?= $logoImage ?><span><?=getTextMod(2, true)?></span></a>
            <div class="header_tel">
                <div class="header-tel_in">
                    <div class="header-tel_item">
                        <a href="tel:88007756510">8 800 775 65 10</a>
                        <i class="icon-arr-mobile"></i>
                        <span>Звонок по России бесплатно</span>
                    </div>
                    <div class="header-tel_item">
                        <a href="tel:+73432879394">+7 (343) 287-93-94</a>
                        <span>Телефон в Екатеринбурге</span>
                    </div>
                </div>
            </div>
            <div class="header_button-group">
                <a href="/<?= path(195) ?>">
                    <button class="btn btn-orange">Приглашаем дилеров</button>
                </a>
                <button data-fancybox-href="#callback-popup" class="btn btn-white js-callback">Написать нам</button>
            </div>
        </article>
    </section>
</header>