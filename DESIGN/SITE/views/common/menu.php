<?php
/**
 * @var array $menu
 */
?>
<ul>
	<? foreach($menu as $item): ?>
    <li>
		<? if ($item['here'] == true): ?>
			<span><?= $item['title'] ?></span>
		<? else: ?>
			<a href="<?= $item['path'] ?>"><?= $item['title'] ?></a>
		<? endif; ?>
    </li>
	<? endforeach; ?>
</ul>