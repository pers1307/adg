<?php
/**
 * @var array $menuItems
 * @var array $menu
 * @var int $showItemsCount
 */
?>
<nav class="nav-menu">
  <article class="wrapper">
    <div class="nav-menu_in hide-tablet">
      <div class="nav-icon">
        <div class="burger">
          <span></span>
          <span></span>
          <span></span>
        </div>
        Меню
      </div>
      <ul>
          <? foreach($menuItems as $menuItem): ?>
              <li<?= $menuItem['current'] ? ' class="active"' : ''; ?>>
                  <a href="<?= $menuItem['path']; ?>"><?= $menuItem['title']; ?></a>
              </li>
          <? endforeach; ?>

          <? foreach($menu as $menuItem): ?>
              <li class="only-mobile <?= $menuItem['current'] ? 'active' : ''; ?>">
                  <a href="<?= $menuItem['path']; ?>"><?= $menuItem['title']; ?></a>
              </li>
          <? endforeach; ?>

        <li class="only-mobile">
          <a class="btn btn-orange" href="/<?= path(195) ?>">
            Приглашаем дилеров
          </a>

          <a href="#callback-popup" class="btn btn-white js-callback">Написать нам</a>
        </li>
      </ul>
    </div>
    <div class="nav-menu_in show-tablet hide-mobile">
      <ul>
        <?php
        $items = [];
        $i = 0;
        foreach ($menuItems as $key => $menuItem) {
          $items[$i][] = '<li' . ($menuItem['current'] ? ' class="active"' : '') . '><a href="' . $menuItem['path'] . '">' . $menuItem['title'] . '</a></li>';
          if (($key + 1) == $showItemsCount) {
            $i++;
          }
        }

        if ($items[0]) {
          echo implode('', $items[0]);
        }

        if (isset($items[1])) {
          ?>
          <li class="has-drop">
          <a href="#">Еще</a>
          <ul class="drop">
            <?= implode('', $items[1]); ?>
          </ul>
          </li><?php
        }
        ?>
      </ul>
    </div>
  </article>
</nav>