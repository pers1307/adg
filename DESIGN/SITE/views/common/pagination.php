<?php
/**
 * @var array $pages
 * @var PaginationPage $page
 */
?>
<div class="pagination">
    <?php

    $left  = '';
    $right = '';

    foreach ($pages as $page) {

        //left arrow
        if ($page->class == 'left') {
            $left = '<a href="' . $page->url .'" class="btn btn-prev">Назад</a>';
        }

        if ($page->class == 'right') {
            $right = '<a href="' . $page->url .'" class="btn btn-next">Вперед</a>';
        }
    }
    ?>
    <ul>
        <?php
        echo $left;

        foreach ($pages as $page) {

            if ($page->class == 'link') {
                ?><li><a href="<?= $page->url ?>"><?= $page->value ?></a></li><?php
            }

            if ($page->type == 'current') {
                ?><li><a href="<?= $page->url ?>" class="active"><?= $page->value ?></a></li><?php
            }

            //...
            if ($page->type == 'sep') {
                ?><li><?= $page->value ?></li><?php
            }
        }

        echo $right;
        ?>
    </ul>
</div>