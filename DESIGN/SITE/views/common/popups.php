<?php
/**
 * @var array $fileDownload
 */
?>
<div id="callback-popup" class="callback-popup">
  <?= template( 'common/forms/callback', [
      'formCaption' => 'Есть вопросы <br>или предложения? <br>Напишите нам',
      'fileDownload' => $fileDownload,
      'action' => '/api/callback.call/',
      'uniqueId' => md5('form-callback-1'),
  ]); ?>
</div>

<div class="overlay"></div>