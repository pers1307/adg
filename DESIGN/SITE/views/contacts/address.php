<?php
/**
 * @var MSBaseTape $model
 * @var array $items
 */
?>

<? foreach($items as $item): ?>
    <div class="contacts-item" itemprop="address" itemscope="" itemtype="http://schema.org/PostalAddress">

        <div class="title"><?= $item['title']; ?></div>

        <ul>
            <? if (!empty($item['schedule'])): ?>
                <li>
                    График работы:
                    <span><?= $item['schedule']; ?></span>
                </li>
            <? endif; ?>

            <? if (!empty($item['address'])): ?>
                <li>
                    Адрес:
                    <span itemprop="streetAddress">
                       <?= strip_tags($item['address']); ?>
                    </span>
                </li>
            <? endif; ?>

            <? if (!empty($item['phone'])): ?>
                <li>
                    Телефон:
                    <span itemprop="telephone">
                        <a href="tel:<?= preg_replace(['/\d/', '/\+/'], '', $item['phone']); ?>">
                            <?= $item['phone']; ?>
                        </a>
                    </span>
                </li>
            <? endif; ?>

            <? if (!empty($item['email'])): ?>
                <li>
                    Email:
                    <span itemprop="email">
                        <a href="mailto:<?= $item['email'] ?>">
                            <?= $item['email'] ?>
                        </a>
                    </span>
                </li>
            <? endif; ?>
        </ul>
    </div>
<? endforeach; ?>