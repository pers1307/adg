<?php
/**
 * @var string $map
 * @var string $downloadFiles
 */
?>
<aside class="contacts-right">
    <div class="contacts-map">
        <?= $map ?>
    </div>
    <?= $downloadFiles ?>
</aside>
<div class="contacts-info_in" itemscope="" itemtype="http://schema.org/Organization">
		<p itemprop="name">Завод автономных дизельных генераторов ADG-ENERGY</p>
    <div class="contacts-top">
        <?= getTextMod(3) ?>
    </div>
    <?= $address; ?>
    <div class="contacts-map show-tablet">
        <?= $map ?>
    </div>
    <?= $staff; ?>
</div>
<div class="show-tablet">
    <?= $downloadFiles ?>
</div>