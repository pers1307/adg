<?php
/**
 * @var MSBaseTape $model
 * @var array $items
 */
foreach ($items as $item) {
    ?>
    <h3><?= $item['title']; ?></h3>
    <div class="team-item">
        <?php
        if ($image = MSFiles::getImage($item['image'], 'origin')) {
            ?><span class="preview"><?= $image; ?></span><?
        }
        ?>
        <div class="team-info">
            <div class="name"><?= $item['name']; ?></div>
            <div class="pos"><?= $item['post']; ?></div>
            <table>
                <?php
                if ($item['phone'] || $item['phone2']) {
                    ?><tr>
                        <td>
                            Телефон:
                        </td>
                        <td>
                            <?php
                            if ($item['phone'] && $item['phone2']) {
                                echo $item['phone'] . '<br>' . $item['phone2'];
                            } else {
                                echo !empty($item['phone']) ? $item['phone'] : $item['phone2'];
                            }
                            ?>
                        </td>
                    </tr><?
                }
                if ($item['email']) {
                    ?>
                    <tr>
                        <td>
                            Email:
                        </td>
                        <td><a href="mailto:<?= $item['email']; ?>"><?= $item['email']; ?></a></td>
                    </tr>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
    <?php
}