<?php
/**
 * @var array $items
 */
?>
<h3>Быстрая навигация по областям</h3>
<div class="country-table">
    <div class="country-table_top">
        <?php
        foreach (array_keys($items) as $key => $value) {
            ?><a href="#l<?= $key ?>"><?= $value; ?></a><?php
        }
        ?>
    </div>
    <table>
        <tr>
            <td>Куда</td>
            <td>Стоимость</td>
            <td>Срок</td>
        </tr>
        <?php
        $i = 0;
        foreach ($items as $key => $item) {
            ?>
            <tr id="l<?= $i; ?>">
                <th colspan="3"><?= $key; ?></th>
            </tr>
            <?php
            $i++;
            foreach ($item as $region) {
                ?>
                <tr>
                    <td><?= $region['title'] ?></td>
                    <td>от <?= number_format($region['price'], 0, '.', ' ')?> руб.</td>
                    <td><?= $region['time']; ?></td>
                </tr><?php
            }
        }
        ?>
    </table>
</div>