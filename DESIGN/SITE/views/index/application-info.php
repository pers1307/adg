<?php
/**
 * @var IndustryModel $model
 * @var array $items
 * @var array $content
 */
if (!empty($items)) {
    ?>
    <section class="application-info">
        <article class="wrapper">
            <div class="application-info_text">
                <h2><?= $content['title'] ?></h2>
                <?= $content['text'] ?>
            </div>
            <div class="application-info_carousel">

                <div class="carousel-item">
                    <?php
                    foreach ($items as $key => $item) {
                    ?>
                    <a href="<?= $item['itemLink'] ?>" class="application-info_item">
                        <span>
                            <?= MSFiles::getImage($item['icon'], 'origin') ?>
                        </span>
                        <?= $item['title'] ?>
                    </a>
                    <?php
                        if ($key%2) {
                            ?></div><div class="carousel-item"><?php
                        }
                    }
                    ?>
                </div>
            </div>
            <h2 class="show-mobile"><?= $content['title'] ?></h2>
        </article>
    </section>
    <?php
}