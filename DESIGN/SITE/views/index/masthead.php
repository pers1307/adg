<?php
/**
 * @var PowerStationModel $model
 * @var array $articles;
 * @var string $defaultImageUrl;
 */
?>
<section class="masthead">
    <article class="wrapper">
        <?php
        if ($presentation = FileDownload::presentation()) {
            ?><a href="<?= $presentation['file']['path'] ?>" download class="download-link"><i class="icon-download"></i>Скачать презентацию <span>pdf - <?= $presentation['file']['fileSize'] ?></span></a><?php
        }
        ?>
        <h1><?= MSCore::page()->title_h1 ?></h1>
        <div class="tab">
            <div class="masthead_in js-hover-wrap ">
                <div class="masthead_left">
                    <div class="link-list">
                        <p class="show-mobile">Выберите мощность</p>
                        <div class="tab-menu">
                          <div class="tab-menu_in">
                            <ul>
                              <?php
                              $i = 0;
                              foreach ($articles as $article) {
                                ?><li data-big-default="<?= MSFiles::getImageUrl($article['image'], 'view')?>" <?= !$i++ ? 'class="active "' : '' ?>><span><?= $article['name'] ?></span></li><?php
                              }
                              ?>
                            </ul>
                          </div>
                          <a class="all" href="<?= $model->path . DS?>">Все мощности</a>

                        </div>

                    </div>
                    <div class="masthead-left_preview">
                        <img data-url-default="<?= $defaultImageUrl ?>" class="js-preview" src="<?= $defaultImageUrl ?>" alt="Дизельные генераторы в Екатеринбурге" title="Дизельные генераторы в Екатеринбурге">
                    </div>
                </div>
                <div class="tab-content">
                    <?php
                    foreach ($articles as $article) {
                        ?>
                        <div class="tab-item">
                            <div class="masthead_right">
                                <div class="masthead-right_top">
                                    <a href="<?= $model->getArticleLink($article['id']) ?>">
                                        <span>Дизельные генераторы ADG Energy <?= $article['name'] ?></span>
                                    </a>
                                </div>

                                <div class="masthead_carousel">
                                    <? foreach($article['children'] as $key => $column): ?>
                                        <div class="carousel-item">
	                                        	<?php $i = 1; ?>
                                            <? foreach($column as $child): ?>
                                                <?
	                                              if(2 < $i) break;
                                                $model->_currentArticle = $child;
                                                $characteristics = json_decode($child['characteristics'], true);
                                                $price = $model::getOpenTypePrice($model->getItems(null, false)['items']);
                                                ?>
                                                <a href="<?= $model->getArticleLink($child['id']) ?>" class="masthead-carousel_item">
                                                  <div class="preview js-hover-change" data-url-big="<?= MSFiles::getImageUrl($child['image'], 'view')?>">
                                                    <img src="<?= MSFiles::getImageUrl($child['image'], 'min')?>" alt="Дизельные генераторы <?= $child['name'].' '.$article['name'] ?>" title="Дизельные генераторы <?= $child['name'].' '.$article['name'] ?>">
                                                  </div>
                                                    <div class="params center">
                                                        <div class="name"><?= $child['name'] ?> <?= $article['name'] ?></div>
                                                        <ul>
                                                            <li>Основная мощность: <span><?= isset($characteristics['mainPowerKw']) ? $characteristics['mainPowerKw'] . ' кВт' : '' ?><?= isset($characteristics['mainPowerKwa']) ? ' / ' . $characteristics['mainPowerKwa'] . ' кВа' : '' ?></span></li>
                                                            <li>Резервная мощность: <span><?= isset($characteristics['reservedPowerKw']) ? $characteristics['reservedPowerKw'] . ' кВт' : '' ?><?= isset($characteristics['reservedPowerKwa']) ? ' / ' . $characteristics['reservedPowerKwa'] . ' кВа' : '' ?></span></li>
                                                            <li>Напряжение: <span><?= isset($characteristics['voltage']) ? $characteristics['voltage'] . ' В' : '' ?></span></li>
                                                        </ul>

                                                        <? if ($price > 0): ?>
                                                            <div class="cash">от  <span><?= number_format($price, 0, '.', ' ') ?></span> руб <i title="Указана минимальная цена в базовом исполнении." class="tooltip icon-question">?</i></div>
                                                        <? endif; ?>
                                                    </div>
                                                </a>
	                                            <?php ++$i; ?>
                                            <? endforeach; ?>
                                        </div>
                                    <? endforeach; ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

    </article>
    <img class="bg" src="/DESIGN/SITE/images/masthead-bg.jpg" alt="">
</section>