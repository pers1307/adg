<?php
/**
 * @var VariantModel $model
 */
?>
<section class="version-list">
    <article class="wrapper">
        <h2>Варианты исполнения дизельных электростанций</h2>
        <div class="version-list_in">
        <?php
        foreach ($model->articles as $article) {
            ?>
            <div class=" version-list_item">
                <a href="<?= $model->getArticleLink($article['id']) ?>">
                    <span class="preview">
                        <?= MSFiles::getImage($article['image'], 'view') ?>
                    </span>
                    <span class="name"><?= $article['name'] ?></span>
                </a>
            </div>
            <?php
        }
        ?>
        </div>
    </article>
</section>