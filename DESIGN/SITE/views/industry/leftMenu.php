<?php
/**
 * @var IndustryModel $model
 * @var string $title
 */

if (isset($model) && $model instanceof IndustryModel) {
    $menuItems = $model->items['items'];
}

?>
<div class="catalog-nav_in">
    <?php
    if (isset($menuTitle)) {
        ?><h5><?= $menuTitle ?></h5><?php
    }
    ?>
    <ul>
        <?php
        foreach ($modelList as $key => $item) {
            ?><li class="<?= $model->currentItem && $model->currentItem['id'] == $item['id'] ? 'active' : (++$key > IndustryModel::LEFT_MENU_SHOT_LIMIT ? ' hide' : '') ?>"><a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a></li><?php
        }
        ?>
    </ul>
    <?php
    if (count($modelList) > IndustryModel::LEFT_MENU_SHOT_LIMIT) {
        ?><button class="btn show-more">Развернуть весь список</button><?php
    }
    ?>
</div>