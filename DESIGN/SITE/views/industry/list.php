<?php
/**
 * @var PowerStationModel $powerStationModel
 * @var array $item
 * @var array $statuses
 * @var array $variants
 */

?>
<div class="text-top">
    <?= $item['text'] ?>
</div>

<div class="catalog-list">
    <?php
    if (!empty($articles)) {

        foreach ($articles as $article) {
            ?>
            <div class="catalog-item">
                <div class="catalog-item_top">
                    <?php
                    if ($filePath = MSFiles::getFilePath($article['presentation'])) {
                        ?><a href="<?= $filePath ?>" download class="download-link download-link_green"><i class="icon-download-green"></i>Скачать спецификацию</a><?php
                    }

                    if ($powerStationModel::getOpenTypePrice($article['items']) > 0) {
                        ?><div class="cash">от  <span><?= number_format($powerStationModel::getOpenTypePrice($article['items']), 0, '.', ' ')?></span> руб</div><?php
                    }
                    ?>

                    <a href="<?= $powerStationModel->getArticleLink($article['id']) ?>" class="name">
                        <span><?= $article['name'] ?> <?= $article['parent']['name'] ?></span>

                        <? if (
                            !empty($article['engine']['brand']['image'])
                            && $imageUrl = MSFiles::getImageUrl($article['engine']['brand']['image'], 'list')
                        ): ?>
                            <span class="preview">
                                <img src="<?= $imageUrl ?>"
                                     width="200"
                                     height="200"
                                     alt="<?= $article['engine']['brand']['name'] ?>"
                                >
                            </span>
                        <? endif; ?>
                    </a>
                </div>
                <div class="catalog-item_preview-group js-hover-wrap">
                    <div class="catalog-item-preview_big">
                        <?php
                        if ($imageUrl = MSFiles::getImageUrl($article['image'], 'view')) {
                            ?>
                            <a href="<?= $powerStationModel->getArticleLink($article['id']) ?>">
                                <img data-url-default="<?= $imageUrl ?>" class="js-preview" src="<?= $imageUrl ?>" alt="<?= $article['name'] ?>">
                            </a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="catalog-item-preview_small">
                        <div class="title">Варианты исполнения:</div>
                        <?php
                        if (!empty($article['items'])) {

                            ?><div class="catalog-item-preview-small_list"><?php

                            foreach ($article['items'] as $_item) {
                                ?>
                                <a href="<?= $_item['itemLink'] ?>" class="catalog-item-preview-small_item">
                                    <span class="label label-<?= $_item['stock'] == 1 ? 'orange' : 'green' ?>"><?= $statuses[$_item['stock']] ?></span>
                                    <span data-url-big="<?= MSFiles::getImageUrl($_item['image'], 'preview')?>" class="preview js-hover-change">
                                        <?= MSFiles::getImage($_item['image'], 'view')?>
                                    </span>
                                    <?= $variants[$_item['title']]['name'] ?>
                                </a>
                                <?php
                            }
                            ?></div><?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
    }
    ?>
</div>

<?= $pagination ?>

<div class="text-bottom">
    <?= $item['text_bottom'] ?>
</div>