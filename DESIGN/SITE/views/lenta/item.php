<?php
/**
 * @var array $item
 */
?>
<div class="information-module one">
	<div class="display-table">
		<h1 class="cell"><?= $item['title'] ?></h1>
		<div class="cell date"><?= sql2Date($item['date'], 10) ?></div>
	</div>
	<?php if ($image = MSFiles::getImage($item['image'], 'view', '', 0, 'float-left image')) {
		echo $image;
	}?>
	<?= $item['text'] ?>

</div>