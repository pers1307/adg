<?php
foreach($items as $item) {
	if (!$image = MSFiles::getImage($item['image'], 'view')) {
		$image = '';
	} else {
		$image = '<a href="'. $item['itemLink']. '">' . $image . '</a>';
	}
	?>
	<div class="item">
	<?=  $image ?>
	<?php
	if (isset($showDate) && $showDate === true) {
		?><div class="date"><?= sql2Date($item['date'], 10) ?></div><?php
	}
	?>
	<div class="title"><a href="<?= $item['itemLink'] ?>"><?= $item['title'] ?></a></div>
	</div><?php
}
?>