<?php
foreach ($items as $item) {
    ?>
    <div class="material-big-item">
        <?php
        if ($image = MSFiles::getImage($item['image'], 'preview')) {
            ?><a href="<?= $item['itemLink'] ?>" class="preview"><?= $image?></a><?php
        }
        ?>
        <div class="center">
            <a href="<?= $item['itemLink'] ?>" class="name"><span><?= $item['title'] ?></span></a>
            <div class="text">
                <?= $item['announce'] ?>
            </div>
        </div>
    </div>
    <?php
}
?>