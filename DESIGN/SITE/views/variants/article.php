<?php
/**
 * @var VariantModel $model
 * @var array $article
 * @var array $items
 * @var PowerStationModel $PSModel
 */
?>

<div class="container-info">
    <aside class="container-info_preview">
        <div class="container-info-preview_wrap">
            <div class="preview">
              <?
              $imageUrl = MSFiles::getImageUrl($article['image'], 'view')
              ?>
              <?/*= MSFiles::getImage($item['image'], 'view')*/?>

              <img  class="js-change-preview" src="<?= $imageUrl ?>">
            </div>
        </div>
    </aside>
    <div class="container-info_left">
        <div class="title_h4"><?= !empty($article['titleName']) ? $article['titleName'] : $article['name'] ?></div>
        <div class="container-info_in">
            <div class="container-info_item">
                <section class="product-more-info tab tablet-accordion">
                    <article class="wrapper">
                        <div class="tab-menu hide-tablet">
                            <ul>
                                <?php
                                $i = 0;
                                foreach ($tabs[$article['code']] as $tabTitle) {
                                    ?><li<?= ($i++ === 0 ? ' class="active"': '')?>><?= $tabTitle ?></li><?php
                                }
                                ?>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class="container-info_text">

                                    <?php
                                    $i = 0;
                                    foreach ($tabs[$article['code']] as $tabName => $tabTitle) {
                                        ?>
                                        <?=$tabName === "diesel_engine" ?  '<ul class="list-link">' : ""; ?>
                                        <div class="tab-item<?= !$i++ ? ' active' : '' ?>">
                                            <div class="tab-title"><span><?= $tabTitle ?></span></div>
                                            <div class="hide-tablet ">
                                                <? $vars = array(
                                                    'PSModel' => $PSModel,
                                                    'article' => $article) ?>
                                                <?php echo template('catalog/power_stations/tabs/' . $tabName, $vars); ?>
                                            </div>
                                        </div>
                                        <?= $tabName == "diesel_engine" ?  '</ul>' : ""; ?>
                                        <?php
                                    }
                                    unset($i);
                                    ?>
                            </div>
                        </div>
                    </article>
                </section>
            </div>
            <?php
            foreach ($items as $item) {
                ?>
                <div class="container-info_item">
                    <div class="title"><?= $item['title'] ?></div>
                    <div class="container-info_text">
                        <div class="accordion">
                            <?php
                            if ($item['text']) {
                                ?>
                                <div class="container-info_text">
                                    <?= $item['text'] ?>
                                    <?php
                                    if ($gallery = unserialize($item['gallery'])) {
                                        ?><div class="img-list"><?php
                                        foreach ($gallery as $gKey => $gImage) {
                                            ?><a href="<?= MSFiles::getImageUrl($gallery, 'preview', $gKey) ?>" rel="gal<?= $item['id'] ?>" class="js-click-change"><?= MSFiles::getImage($gallery, 'min', '', $gKey) ?></a><?php
                                        }
                                        ?></div><?php
                                        unset($gallery);
                                    }
                                    ?>
                                </div><?php
                            } elseif (!empty($item['options'])) {//options
                                foreach ($item['options'] as $iKey => $iOption) {
                                    ?>
                                    <div class="accordion-item">
                                        <div class="accordion-title">
                                            <i><?= str_pad(($iKey+1),2,"0",STR_PAD_LEFT) ?>.</i>
                                            <h5><span><?= $iOption['title'] ?></span></h5>
                                        </div>
                                        <div class="drop accordion-drop">
                                            <?= $iOption['text'] ?>
                                            <?php
                                            if ($gallery = unserialize($iOption['gallery'])) {
                                                ?><div class="img-list"><?php
                                                foreach ($gallery as $gKey => $gImage) {
                                                    ?><a href="<?= MSFiles::getImageUrl($gallery, 'preview', $gKey) ?>" rel="gal<?= $iKey ?>" class="js-click-change"><?= MSFiles::getImage($gallery, 'min', '', $gKey) ?></a><?php
                                                }
                                                ?></div><?php
                                            }
                                            ?>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
    </div>
</div>