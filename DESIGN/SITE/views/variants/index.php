<?php
/**
 * @var VariantModel $model
 * @var PowerStationModel $PSModel
 */
?>
<div class="version-big-list_in">
  <?php foreach ($model->articles as $article) {
    ?>
    <div class="version-big-item">
      <?php if ($image = MSFiles::getImage($article['image'], 'preview')) {
        ?><a href="<?= $model->getArticleLink($article['id']) ?>" class="preview"><?= $image ?></a><?php
      } ?>
      <div class="center">
        <a href="<?= $model->getArticleLink($article['id']) ?>" class="name"><span><?= $article['name'] ?></span></a>
        <div class="version-big-item-text">
          <?= $article['announce'] ?>
          <ul class="list-link">
            <?php
            foreach ($PSModel->fullTree as $PSArticle) {
              ?>
              <li><a href="<?= $PSModel->getArticleLink($PSArticle['id']) ?>"><?= $PSArticle['name'] ?></a></li><?php
            }
            ?>
          </ul>
        </div>

      </div>
    </div>
    <?php
  }
  ?>
</div>
