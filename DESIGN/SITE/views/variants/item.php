<?php
/**
 * @var array $item
 * @var PowerStationModel $PSModel
 */

?>

<div class="container-info">
    <aside class="container-info_preview">
        <div class="container-info-preview_wrap">
            <div class="preview">
              <?
              $imageUrl = MSFiles::getImageUrl($item['image'], 'view')
              ?>
              <?/*= MSFiles::getImage($item['image'], 'view')*/?>

                <img  class="js-change-preview" src="<?= $imageUrl ?>">

            </div>
        </div>
    </aside>
    <div class="container-info_left">
        <div class="title_h4"><?= !empty($item['titleItem']) ? $item['titleItem'] : $item['title'] ?></div>
        <div class="container-info_in">
            <div class="container-info_item">
                <div class="title">Дизельные генераторы в кожухе</div>
                <div class="container-info_text">
                    <ul class="list-link">
                        <?php
                        foreach ($PSModel->fullTree as $PSArticle) {
                            ?><li><a href="<?= $PSModel->getArticleLink($PSArticle['id'])?>"><?= $PSArticle['name']?></a></li><?php
                        }
                        ?>
                    </ul>
                </div>
            </div>
            <div class="container-info_item">
                <div class="title">Стандартная комплектация в контейнере</div>
                <div class="container-info_text">
                    <div class="accordion">
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>01.</i>
                                <h5><span>Резервное освещение</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                <p>Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.</p>
                                <div class="img-list">
                                    <a href="images/pluses-small1.jpg" rel="gal1" class="js-click-change"><img src="images/pluses-small1.jpg" alt=""></a>
                                    <a href="images/pluses-small2.jpg" rel="gal1" class="js-click-change"><img src="images/pluses-small2.jpg" alt=""></a>
                                    <a href="images/pluses-small3.jpg" rel="gal1" class="js-click-change"><img src="images/pluses-small3.jpg" alt=""></a>
                                    <a href="images/pluses-small4.jpg" rel="gal1" class="js-click-change"><img src="images/pluses-small4.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>02.</i>
                                <h5><span>Панель управления дизель-генераторной установкой (ДГУ)</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>03.</i>
                                <h5><span>Стандартные монтажные транспортировочные узлы</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>04.</i>
                                <h5><span>Порошковая система пожаротушения</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>05.</i>
                                <h5><span>Система газовыхлопа с шумоглушителем</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>06.</i>
                                <h5><span>Основное освещение</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container-info_item">
                <div class="title">Передвижение</div>
                <div class="container-info_text">
                    <div class="accordion">
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>01.</i>
                                <h5><span>Стационарный</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                <p>Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков.</p>
                                <div class="img-list">
                                    <a href="images/pluses-small1.jpg" rel="gal2" class="js-click-change"><img src="images/pluses-small1.jpg" alt=""></a>
                                    <a href="images/pluses-small2.jpg" rel="gal2" class="js-click-change"><img src="images/pluses-small2.jpg" alt=""></a>
                                    <a href="images/pluses-small3.jpg" rel="gal2" class="js-click-change"><img src="images/pluses-small3.jpg" alt=""></a>
                                    <a href="images/pluses-small4.jpg" rel="gal2" class="js-click-change"><img src="images/pluses-small4.jpg" alt=""></a>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>02.</i>
                                <h5><span>Автомобильные шасси</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>03.</i>
                                <h5><span>Тракторные шасси</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>04.</i>
                                <h5><span>Полозья</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <i>05.</i>
                                <h5><span>Сани</span></h5>
                            </div>
                            <div class="drop accordion-drop">
                                Если бы был создан робот, способный стать общественным деятелем, он был бы самым лучшим из них. По законам Роботехники, он не мог бы причинять людям зла, был бы чужд тирании, подкупа, глупости или предрассудков. И прослужив некоторое время, он ушёл бы в отставку, хотя он и бессмертен, — ведь для него было бы невозможно огорчить людей, дав им понять, что ими управляет робот. Это было бы почти идеально.
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>