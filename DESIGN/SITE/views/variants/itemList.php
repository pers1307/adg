<?php
/**
 * @var array $items
 * @var PowerStationModel $PSModel
 */
?>
<div class="version-big-list_in">
<?php foreach ($items as $item) {
    ?>

        <div class="version-big-item">
            <?php if ($image = MSFiles::getImage($item['image'], 'preview')) {
                ?><a href="<?= $item['itemLink'] ?>" class="preview"><?= $image?></a><?php
            } ?>
            <div class="center">
                <a href="<?= $item['itemLink'] ?>" class="name"><span><?= $item['title'] ?></span></a>

              <div class="version-big-item-text">
                <?= $item['announce'] ?>
                <ul class="list-link">
                  <?php
                  foreach ($PSModel->fullTree as $PSArticle) {
                    ?><li><a href="<?= $PSModel->getArticleLink($PSArticle['id'])?>"><?= $PSArticle['name']?></a></li><?php
                  }
                  ?>
                </ul>
              </div>

            </div>
        </div>
    <?php
}
?>
</div>
