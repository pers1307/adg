<?php

class ConvertMainPowerKwEnginesItems extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("UPDATE mp_engines_items
                              SET mainPowerKw = REPLACE(mainPowerKw, ',', '.')");

        $this->execute("ALTER TABLE mp_engines_items MODIFY mainPowerKw FLOAT;");
    }

    public function down()
    {

    }
}
