<?php

class AddCodeInCommonSlider extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("ALTER TABLE mp_common_slider ADD code VARCHAR(255) DEFAULT '' NULL;");
    }//up()

    public function down()
    {
    }//down()
}
