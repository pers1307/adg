<?php

class DuplicatePowerStationItems extends Ruckusing_Migration_Base
{
    private $insertItems = [
        [
            'path_id'          => 188,
            'order'            => -4,
            'title'            => 1,
            'code'             => 'open',
            'price_rub'        => 0,
            'price_usd'        => 3149,
            'price_eur'        => 0,
            'price_jpy'        => 0,
            'active_currency'  => 978,
            'image'            => 'a:9:{i:0;a:3:{s:2:"id";s:32:"311272bb67d66409c7b673f770682fd1";s:4:"path";a:6:{s:8:"original";s:35:"/UPLOAD/2018/10/17/adg-energy-1.jpg";s:9:"sys_thumb";s:41:"/UPLOAD/2018/10/17/adg-energy-1_80_80.jpg";s:5:"small";s:43:"/UPLOAD/2018/10/17/adg-energy-1_100_100.jpg";s:7:"preview";s:43:"/UPLOAD/2018/10/17/adg-energy-1_300_300.jpg";s:4:"view";s:43:"/UPLOAD/2018/10/17/adg-energy-1_400_400.jpg";s:3:"big";s:45:"/UPLOAD/2018/10/17/adg-energy-1_1200_1200.jpg";}s:4:"text";s:110:"Открытое исполнение дизельной электростанции ADG-ENERGY АД-10-Т400";}i:1;a:3:{s:2:"id";s:32:"26589c81f38c7ed93f1aeae3eeec6198";s:4:"path";a:6:{s:8:"original";s:35:"/UPLOAD/2018/10/17/adg-energy-7.jpg";s:9:"sys_thumb";s:41:"/UPLOAD/2018/10/17/adg-energy-7_80_80.jpg";s:5:"small";s:43:"/UPLOAD/2018/10/17/adg-energy-7_100_100.jpg";s:7:"preview";s:43:"/UPLOAD/2018/10/17/adg-energy-7_300_300.jpg";s:4:"view";s:43:"/UPLOAD/2018/10/17/adg-energy-7_400_400.jpg";s:3:"big";s:45:"/UPLOAD/2018/10/17/adg-energy-7_1200_1200.jpg";}s:4:"text";s:110:"Открытое исполнение дизельной электростанции ADG-ENERGY АД-10-Т400";}i:2;a:3:{s:2:"id";s:32:"7c9fa3b8ea29bdf821cfd0e9dfb225b7";s:4:"path";a:6:{s:8:"original";s:35:"/UPLOAD/2018/10/17/adg-energy-9.jpg";s:9:"sys_thumb";s:41:"/UPLOAD/2018/10/17/adg-energy-9_80_80.jpg";s:5:"small";s:43:"/UPLOAD/2018/10/17/adg-energy-9_100_100.jpg";s:7:"preview";s:43:"/UPLOAD/2018/10/17/adg-energy-9_300_300.jpg";s:4:"view";s:43:"/UPLOAD/2018/10/17/adg-energy-9_400_400.jpg";s:3:"big";s:45:"/UPLOAD/2018/10/17/adg-energy-9_1200_1200.jpg";}s:4:"text";s:110:"Открытое исполнение дизельной электростанции ADG-ENERGY АД-10-Т400";}i:3;a:3:{s:2:"id";s:32:"adf0a7744d96cabaf9909de2fdd3f0da";s:4:"path";a:6:{s:8:"original";s:28:"/UPLOAD/2018/10/23/adg-1.jpg";s:9:"sys_thumb";s:34:"/UPLOAD/2018/10/23/adg-1_80_80.jpg";s:5:"small";s:36:"/UPLOAD/2018/10/23/adg-1_100_100.jpg";s:7:"preview";s:36:"/UPLOAD/2018/10/23/adg-1_300_300.jpg";s:4:"view";s:36:"/UPLOAD/2018/10/23/adg-1_400_400.jpg";s:3:"big";s:38:"/UPLOAD/2018/10/23/adg-1_1200_1200.jpg";}s:4:"text";s:0:"";}i:4;a:3:{s:2:"id";s:32:"205c9e326624df184d63f7e4eb4a8ba0";s:4:"path";a:6:{s:8:"original";s:28:"/UPLOAD/2018/10/23/adg-8.jpg";s:9:"sys_thumb";s:34:"/UPLOAD/2018/10/23/adg-8_80_80.jpg";s:5:"small";s:36:"/UPLOAD/2018/10/23/adg-8_100_100.jpg";s:7:"preview";s:36:"/UPLOAD/2018/10/23/adg-8_300_300.jpg";s:4:"view";s:36:"/UPLOAD/2018/10/23/adg-8_400_400.jpg";s:3:"big";s:38:"/UPLOAD/2018/10/23/adg-8_1200_1200.jpg";}s:4:"text";s:0:"";}i:5;a:3:{s:2:"id";s:32:"757fdae45185d1486c07a34bd7a8f563";s:4:"path";a:6:{s:8:"original";s:29:"/UPLOAD/2018/10/23/adg-10.jpg";s:9:"sys_thumb";s:35:"/UPLOAD/2018/10/23/adg-10_80_80.jpg";s:5:"small";s:37:"/UPLOAD/2018/10/23/adg-10_100_100.jpg";s:7:"preview";s:37:"/UPLOAD/2018/10/23/adg-10_300_300.jpg";s:4:"view";s:37:"/UPLOAD/2018/10/23/adg-10_400_400.jpg";s:3:"big";s:39:"/UPLOAD/2018/10/23/adg-10_1200_1200.jpg";}s:4:"text";s:0:"";}i:6;a:3:{s:2:"id";s:32:"d608cf2b3d63e2b61bb5653cdcd9a98a";s:4:"path";a:6:{s:8:"original";s:29:"/UPLOAD/2018/10/23/adg-11.jpg";s:9:"sys_thumb";s:35:"/UPLOAD/2018/10/23/adg-11_80_80.jpg";s:5:"small";s:37:"/UPLOAD/2018/10/23/adg-11_100_100.jpg";s:7:"preview";s:37:"/UPLOAD/2018/10/23/adg-11_300_300.jpg";s:4:"view";s:37:"/UPLOAD/2018/10/23/adg-11_400_400.jpg";s:3:"big";s:39:"/UPLOAD/2018/10/23/adg-11_1200_1200.jpg";}s:4:"text";s:0:"";}i:7;a:3:{s:2:"id";s:32:"2a1bfe37551d03f364e9e2483aca3535";s:4:"path";a:6:{s:8:"original";s:29:"/UPLOAD/2018/10/23/adg-13.jpg";s:9:"sys_thumb";s:35:"/UPLOAD/2018/10/23/adg-13_80_80.jpg";s:5:"small";s:37:"/UPLOAD/2018/10/23/adg-13_100_100.jpg";s:7:"preview";s:37:"/UPLOAD/2018/10/23/adg-13_300_300.jpg";s:4:"view";s:37:"/UPLOAD/2018/10/23/adg-13_400_400.jpg";s:3:"big";s:39:"/UPLOAD/2018/10/23/adg-13_1200_1200.jpg";}s:4:"text";s:0:"";}i:8;a:3:{s:2:"id";s:32:"42aacbc6cff3a7fabd2315e848dde452";s:4:"path";a:6:{s:8:"original";s:29:"/UPLOAD/2018/10/23/adg-14.jpg";s:9:"sys_thumb";s:35:"/UPLOAD/2018/10/23/adg-14_80_80.jpg";s:5:"small";s:37:"/UPLOAD/2018/10/23/adg-14_100_100.jpg";s:7:"preview";s:37:"/UPLOAD/2018/10/23/adg-14_300_300.jpg";s:4:"view";s:37:"/UPLOAD/2018/10/23/adg-14_400_400.jpg";s:3:"big";s:39:"/UPLOAD/2018/10/23/adg-14_1200_1200.jpg";}s:4:"text";s:0:"";}}',
            'text'             => '',
            'active'           => 1,
            'stock'            => 1,
            'title_h1'         => 'Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в открытом исполнении',
            'title_page'       => 'Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в открытом исполнении',
        ],
        [
            'path_id'          => 188,
            'order'            => -3,
            'title'            => 3,
            'code'             => 'kozhuh',
            'price_rub'        => 0,
            'price_usd'        => 3862,
            'price_eur'        => 0,
            'price_jpy'        => 0,
            'active_currency'  => 978,
            'image'            => 'a:9:{i:0;a:3:{s:2:"id";s:32:"90651859568b12df83f8ff69ad0ae574";s:4:"path";a:6:{s:8:"original";s:35:"/UPLOAD/2018/10/17/adg-energy-2.jpg";s:9:"sys_thumb";s:41:"/UPLOAD/2018/10/17/adg-energy-2_80_80.jpg";s:5:"small";s:43:"/UPLOAD/2018/10/17/adg-energy-2_100_100.jpg";s:7:"preview";s:43:"/UPLOAD/2018/10/17/adg-energy-2_300_300.jpg";s:4:"view";s:43:"/UPLOAD/2018/10/17/adg-energy-2_400_400.jpg";s:3:"big";s:45:"/UPLOAD/2018/10/17/adg-energy-2_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}i:1;a:3:{s:2:"id";s:32:"bdaac0df428f14056b4df2bbef68b0ba";s:4:"path";a:6:{s:8:"original";s:36:"/UPLOAD/2018/10/17/adg-energy-16.jpg";s:9:"sys_thumb";s:42:"/UPLOAD/2018/10/17/adg-energy-16_80_80.jpg";s:5:"small";s:44:"/UPLOAD/2018/10/17/adg-energy-16_100_100.jpg";s:7:"preview";s:44:"/UPLOAD/2018/10/17/adg-energy-16_300_300.jpg";s:4:"view";s:44:"/UPLOAD/2018/10/17/adg-energy-16_400_400.jpg";s:3:"big";s:46:"/UPLOAD/2018/10/17/adg-energy-16_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}i:2;a:3:{s:2:"id";s:32:"51321b08ee85adf3c97e1569ada080d1";s:4:"path";a:6:{s:8:"original";s:36:"/UPLOAD/2018/10/17/adg-energy-17.jpg";s:9:"sys_thumb";s:42:"/UPLOAD/2018/10/17/adg-energy-17_80_80.jpg";s:5:"small";s:44:"/UPLOAD/2018/10/17/adg-energy-17_100_100.jpg";s:7:"preview";s:44:"/UPLOAD/2018/10/17/adg-energy-17_300_300.jpg";s:4:"view";s:44:"/UPLOAD/2018/10/17/adg-energy-17_400_400.jpg";s:3:"big";s:46:"/UPLOAD/2018/10/17/adg-energy-17_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}i:3;a:3:{s:2:"id";s:32:"3f989cb190343a8677043dae8c96eb8d";s:4:"path";a:6:{s:8:"original";s:36:"/UPLOAD/2018/10/17/adg-energy-19.jpg";s:9:"sys_thumb";s:42:"/UPLOAD/2018/10/17/adg-energy-19_80_80.jpg";s:5:"small";s:44:"/UPLOAD/2018/10/17/adg-energy-19_100_100.jpg";s:7:"preview";s:44:"/UPLOAD/2018/10/17/adg-energy-19_300_300.jpg";s:4:"view";s:44:"/UPLOAD/2018/10/17/adg-energy-19_400_400.jpg";s:3:"big";s:46:"/UPLOAD/2018/10/17/adg-energy-19_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}i:4;a:3:{s:2:"id";s:32:"33e8213a41384f8b1b679d8a3211448b";s:4:"path";a:6:{s:8:"original";s:28:"/UPLOAD/2018/10/23/adg-3.jpg";s:9:"sys_thumb";s:34:"/UPLOAD/2018/10/23/adg-3_80_80.jpg";s:5:"small";s:36:"/UPLOAD/2018/10/23/adg-3_100_100.jpg";s:7:"preview";s:36:"/UPLOAD/2018/10/23/adg-3_300_300.jpg";s:4:"view";s:36:"/UPLOAD/2018/10/23/adg-3_400_400.jpg";s:3:"big";s:38:"/UPLOAD/2018/10/23/adg-3_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}i:5;a:3:{s:2:"id";s:32:"0b9b60249eb73f178450de4fa67ad414";s:4:"path";a:6:{s:8:"original";s:28:"/UPLOAD/2018/10/23/adg-4.jpg";s:9:"sys_thumb";s:34:"/UPLOAD/2018/10/23/adg-4_80_80.jpg";s:5:"small";s:36:"/UPLOAD/2018/10/23/adg-4_100_100.jpg";s:7:"preview";s:36:"/UPLOAD/2018/10/23/adg-4_300_300.jpg";s:4:"view";s:36:"/UPLOAD/2018/10/23/adg-4_400_400.jpg";s:3:"big";s:38:"/UPLOAD/2018/10/23/adg-4_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}i:6;a:3:{s:2:"id";s:32:"1cb22fc6c5c7dfed0cb95bc3fdc87b04";s:4:"path";a:6:{s:8:"original";s:28:"/UPLOAD/2018/10/23/adg-6.jpg";s:9:"sys_thumb";s:34:"/UPLOAD/2018/10/23/adg-6_80_80.jpg";s:5:"small";s:36:"/UPLOAD/2018/10/23/adg-6_100_100.jpg";s:7:"preview";s:36:"/UPLOAD/2018/10/23/adg-6_300_300.jpg";s:4:"view";s:36:"/UPLOAD/2018/10/23/adg-6_400_400.jpg";s:3:"big";s:38:"/UPLOAD/2018/10/23/adg-6_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}i:7;a:3:{s:2:"id";s:32:"455ca86512ef9de8a1bbe54f027d04ef";s:4:"path";a:6:{s:8:"original";s:32:"/UPLOAD/2018/10/23/adg-8-906.jpg";s:9:"sys_thumb";s:38:"/UPLOAD/2018/10/23/adg-8-906_80_80.jpg";s:5:"small";s:40:"/UPLOAD/2018/10/23/adg-8-906_100_100.jpg";s:7:"preview";s:40:"/UPLOAD/2018/10/23/adg-8-906_300_300.jpg";s:4:"view";s:40:"/UPLOAD/2018/10/23/adg-8-906_400_400.jpg";s:3:"big";s:42:"/UPLOAD/2018/10/23/adg-8-906_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}i:8;a:3:{s:2:"id";s:32:"3f8639725897b6a722a5491cf02622d5";s:4:"path";a:6:{s:8:"original";s:29:"/UPLOAD/2018/10/23/adg-15.jpg";s:9:"sys_thumb";s:35:"/UPLOAD/2018/10/23/adg-15_80_80.jpg";s:5:"small";s:37:"/UPLOAD/2018/10/23/adg-15_100_100.jpg";s:7:"preview";s:37:"/UPLOAD/2018/10/23/adg-15_300_300.jpg";s:4:"view";s:37:"/UPLOAD/2018/10/23/adg-15_400_400.jpg";s:3:"big";s:39:"/UPLOAD/2018/10/23/adg-15_1200_1200.jpg";}s:4:"text";s:98:"Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе";}}',
            'text'             => '',
            'active'           => 1,
            'stock'            => 1,
            'title_h1'         => 'Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе',
            'title_page'       => 'Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в кожухе',
        ],
        [
            'path_id'          => 188,
            'order'            => -2,
            'title'            => 4,
            'code'             => 'konteyner',
            'price_rub'        => 235000,
            'price_usd'        => 0,
            'price_eur'        => 0,
            'price_jpy'        => 0,
            'active_currency'  => 643,
            'image'            => 'a:9:{i:0;a:3:{s:2:"id";s:32:"3a01e8565ea263dd791f87bdca2a24fc";s:4:"path";a:6:{s:8:"original";s:51:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-5-50.jpg";s:9:"sys_thumb";s:57:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-5-50_80_80.jpg";s:5:"small";s:59:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-5-50_100_100.jpg";s:7:"preview";s:59:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-5-50_300_300.jpg";s:4:"view";s:59:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-5-50_400_400.jpg";s:3:"big";s:61:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-5-50_1200_1200.jpg";}s:4:"text";s:0:"";}i:1;a:3:{s:2:"id";s:32:"6bbce439f7fe53e9469e9cc323563d86";s:4:"path";a:6:{s:8:"original";s:52:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-3-184.jpg";s:9:"sys_thumb";s:58:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-3-184_80_80.jpg";s:5:"small";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-3-184_100_100.jpg";s:7:"preview";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-3-184_300_300.jpg";s:4:"view";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-3-184_400_400.jpg";s:3:"big";s:62:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-3-184_1200_1200.jpg";}s:4:"text";s:0:"";}i:2;a:3:{s:2:"id";s:32:"5744bfd3e52b40852ccd1b2c70f0f763";s:4:"path";a:6:{s:8:"original";s:51:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-4-94.jpg";s:9:"sys_thumb";s:57:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-4-94_80_80.jpg";s:5:"small";s:59:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-4-94_100_100.jpg";s:7:"preview";s:59:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-4-94_300_300.jpg";s:4:"view";s:59:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-4-94_400_400.jpg";s:3:"big";s:61:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-4-94_1200_1200.jpg";}s:4:"text";s:0:"";}i:3;a:3:{s:2:"id";s:32:"cffdb70db70cf2f3139297c812a7ba7c";s:4:"path";a:6:{s:8:"original";s:52:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-1-678.jpg";s:9:"sys_thumb";s:58:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-1-678_80_80.jpg";s:5:"small";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-1-678_100_100.jpg";s:7:"preview";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-1-678_300_300.jpg";s:4:"view";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-1-678_400_400.jpg";s:3:"big";s:62:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-1-678_1200_1200.jpg";}s:4:"text";s:0:"";}i:4;a:3:{s:2:"id";s:32:"38460d4425730a32aaa89aca2bd89c39";s:4:"path";a:6:{s:8:"original";s:52:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-2-954.jpg";s:9:"sys_thumb";s:58:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-2-954_80_80.jpg";s:5:"small";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-2-954_100_100.jpg";s:7:"preview";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-2-954_300_300.jpg";s:4:"view";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-2-954_400_400.jpg";s:3:"big";s:62:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-2-954_1200_1200.jpg";}s:4:"text";s:0:"";}i:5;a:3:{s:2:"id";s:32:"c8a3943d71e2e9dec0d343b8fb25a5eb";s:4:"path";a:6:{s:8:"original";s:52:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-6-460.jpg";s:9:"sys_thumb";s:58:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-6-460_80_80.jpg";s:5:"small";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-6-460_100_100.jpg";s:7:"preview";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-6-460_300_300.jpg";s:4:"view";s:60:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-6-460_400_400.jpg";s:3:"big";s:62:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-6-460_1200_1200.jpg";}s:4:"text";s:0:"";}i:6;a:3:{s:2:"id";s:32:"029372bed00dfa35dd2b03c38c4bfe06";s:4:"path";a:6:{s:8:"original";s:48:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-7.jpg";s:9:"sys_thumb";s:54:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-7_80_80.jpg";s:5:"small";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-7_100_100.jpg";s:7:"preview";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-7_300_300.jpg";s:4:"view";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-7_400_400.jpg";s:3:"big";s:58:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-7_1200_1200.jpg";}s:4:"text";s:0:"";}i:7;a:3:{s:2:"id";s:32:"e7a30669365b9f8798b85c2add911a83";s:4:"path";a:6:{s:8:"original";s:48:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-8.jpg";s:9:"sys_thumb";s:54:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-8_80_80.jpg";s:5:"small";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-8_100_100.jpg";s:7:"preview";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-8_300_300.jpg";s:4:"view";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-8_400_400.jpg";s:3:"big";s:58:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-8_1200_1200.jpg";}s:4:"text";s:0:"";}i:8;a:3:{s:2:"id";s:32:"1e7f1b5b1e1a157e02caf8632850c1bb";s:4:"path";a:6:{s:8:"original";s:48:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-9.jpg";s:9:"sys_thumb";s:54:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-9_80_80.jpg";s:5:"small";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-9_100_100.jpg";s:7:"preview";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-9_300_300.jpg";s:4:"view";s:56:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-9_400_400.jpg";s:3:"big";s:58:"/UPLOAD/2018/11/28/konteyner-sever-dgu-des-9_1200_1200.jpg";}s:4:"text";s:0:"";}}',
            'text'             => '',
            'active'           => 1,
            'stock'            => 1,
            'title_h1'         => 'Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в контейнере',
            'title_page'       => 'Дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт в контейнере',
        ],
        [
            'path_id'          => 188,
            'order'            => -1,
            'title'            => 2,
            'code'             => 'peredvezhnoj',
            'price_rub'        => 0,
            'price_usd'        => 0,
            'price_eur'        => 0,
            'price_jpy'        => 0,
            'active_currency'  => 643,
            'image'            => 'a:6:{i:0;a:3:{s:2:"id";s:32:"3447332ba1b756456bf05483befa11ba";s:4:"path";a:6:{s:8:"original";s:29:"/UPLOAD/2018/11/28/38-171.jpg";s:9:"sys_thumb";s:35:"/UPLOAD/2018/11/28/38-171_80_80.jpg";s:5:"small";s:37:"/UPLOAD/2018/11/28/38-171_100_100.jpg";s:7:"preview";s:37:"/UPLOAD/2018/11/28/38-171_300_300.jpg";s:4:"view";s:37:"/UPLOAD/2018/11/28/38-171_400_400.jpg";s:3:"big";s:39:"/UPLOAD/2018/11/28/38-171_1200_1200.jpg";}s:4:"text";s:0:"";}i:1;a:3:{s:2:"id";s:32:"1ed054e8bda2c923705c4e528bd994e1";s:4:"path";a:6:{s:8:"original";s:42:"/UPLOAD/2018/11/28/peredvizhnaya-1-722.jpg";s:9:"sys_thumb";s:48:"/UPLOAD/2018/11/28/peredvizhnaya-1-722_80_80.jpg";s:5:"small";s:50:"/UPLOAD/2018/11/28/peredvizhnaya-1-722_100_100.jpg";s:7:"preview";s:50:"/UPLOAD/2018/11/28/peredvizhnaya-1-722_300_300.jpg";s:4:"view";s:50:"/UPLOAD/2018/11/28/peredvizhnaya-1-722_400_400.jpg";s:3:"big";s:52:"/UPLOAD/2018/11/28/peredvizhnaya-1-722_1200_1200.jpg";}s:4:"text";s:0:"";}i:2;a:3:{s:2:"id";s:32:"3b0e10b5111035d7ed02121edd981113";s:4:"path";a:6:{s:8:"original";s:54:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-1.jpg";s:9:"sys_thumb";s:60:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-1_80_80.jpg";s:5:"small";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-1_100_100.jpg";s:7:"preview";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-1_300_300.jpg";s:4:"view";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-1_400_400.jpg";s:3:"big";s:64:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-1_1200_1200.jpg";}s:4:"text";s:0:"";}i:3;a:3:{s:2:"id";s:32:"30ec99a44084a8e2fede51570ff71709";s:4:"path";a:6:{s:8:"original";s:54:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-2.jpg";s:9:"sys_thumb";s:60:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-2_80_80.jpg";s:5:"small";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-2_100_100.jpg";s:7:"preview";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-2_300_300.jpg";s:4:"view";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-2_400_400.jpg";s:3:"big";s:64:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-2_1200_1200.jpg";}s:4:"text";s:0:"";}i:4;a:3:{s:2:"id";s:32:"c81c64aeebc0842c5e966f7fca1f9f10";s:4:"path";a:6:{s:8:"original";s:54:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-3.jpg";s:9:"sys_thumb";s:60:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-3_80_80.jpg";s:5:"small";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-3_100_100.jpg";s:7:"preview";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-3_300_300.jpg";s:4:"view";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-3_400_400.jpg";s:3:"big";s:64:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-3_1200_1200.jpg";}s:4:"text";s:0:"";}i:5;a:3:{s:2:"id";s:32:"287b477c6d9379ba0113d37dee3ad350";s:4:"path";a:6:{s:8:"original";s:54:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-4.jpg";s:9:"sys_thumb";s:60:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-4_80_80.jpg";s:5:"small";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-4_100_100.jpg";s:7:"preview";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-4_300_300.jpg";s:4:"view";s:62:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-4_400_400.jpg";s:3:"big";s:64:"/UPLOAD/2018/11/28/peredvizhnaya-electrostanciya-4_1200_1200.jpg";}s:4:"text";s:0:"";}}',
            'text'             => '',
            'active'           => 1,
            'stock'            => 1,
            'title_h1'         => 'Передвижная дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт',
            'title_page'       => 'Передвижная дизельная электростанция ADG-ENERGY АД-10-Т400 10 кВт',
        ]
    ];

    private $optionalPriceIds = [
        9, 10, 11, 12
    ];

    public function up()
    {
        $query = new MSTable(PRFX . 'power_stations_articles');
        $query->setFields(['*']);
        $query->setFilter('`level` = 1');
        $query->setFilter('`id` NOT IN (12, 13)');
        $articles = $query->getItems();

        foreach ($articles as $article) {
            foreach ($this->insertItems as $key => $items) {
                $items['parent']  = $article['id'];
                $items['parents'] = implode(',', [$article['parent'], $article['id']]);

                MSCore::db()->insert(
                    PRFX . 'power_stations_items',
                    $items
                );

                $item_id = MSCore::db()->nextID(PRFX . 'power_stations_items') - 1;

                $query = new MSTable(PRFX . 'optional_eq_prices');
                $query->setFields(['*']);
                $query->setFilter('`item_id` = ' . $this->optionalPriceIds[$key]);
                $optionalPrices = $query->getItems();

                foreach ($optionalPrices as $optionalPrice) {
                    unset($optionalPrice['id']);
                    $optionalPrice['item_id'] = $item_id;

                    MSCore::db()->insert(
                        PRFX . 'optional_eq_prices',
                        $optionalPrice
                    );
                }
            }
        }
    }

    public function down()
    {
    }
}
