<?php

class DublicateStandartComplectation extends Ruckusing_Migration_Base
{
    public function up()
    {
        $query = new MSTable(PRFX . 'power_stations_articles');
        $query->setFields(['*']);
        $query->setFilter('`level` = 1');
        $query->setFilter('`id` NOT IN (12)');
        $articles = $query->getItems();

        $query = new MSTable(PRFX . 'power_stations_equipment');
        $query->setFields(['*']);
        $query->setFilter('`id` = 4');
        $complectation = $query->getItem();

        unset($complectation['id']);
        unset($complectation['order']);
        unset($complectation['item_id']);

        foreach ($articles as $key => $article) {
            $complectation['item_id'] = $article['id'];
            $complectation['order'] = $key + 1;

            MSCore::db()->insert(
                PRFX . 'power_stations_equipment',
                $complectation
            );
        }
    }

    public function down()
    {

    }
}
