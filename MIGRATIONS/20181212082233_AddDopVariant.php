<?php

class AddDopVariant extends Ruckusing_Migration_Base
{
    public function up()
    {
        MSCore::db()->insert(
            PRFX . 'optional_equipment_articles',
            [
                'path_id'         => 190,
                'parent'          => 0,
                'name'            => 'Варинты передвижения',
                'code'            => 'varinty-peredvizheniya',
                'image'           => 'a:0:{}',
                'visible'         => 1,
                'parents_visible' => 1,
                'level'           => 0,
            ]
        );

        $id = MSCore::db()->nextID(PRFX . 'optional_equipment_articles') - 1;

        MSCore::db()->insert(
            PRFX . 'optional_equipment_items',
            [
                'path_id'         => 190,
                'parent'          => $id,
                'title'           => 'Тракторных шасси для кожуха',
                'code'            => 'traktornyh-shassi-dlya-kozhuha',
                'image'           => 'a:0:{}',
                'price_rub'       => 0,
                'price_usd'       => 0,
                'price_eur'       => 0,
                'price_jpy'       => 0,
                'active_currency' => 643,
                'active'          => 1,
                'parents'         => $id
            ]
        );

        $id = MSCore::db()->nextID(PRFX . 'optional_equipment_items') - 1;

        $query = new MSTable(PRFX . 'power_stations_items');
        $query->setFields(['*']);
        $query->setFilter('`title` = 2');
        $items = $query->getItems();

        foreach ($items as $item) {
            MSCore::db()->insert(
                PRFX . 'optional_eq_prices',
                [
                    'item_id'         => $item['id'],
                    'path_id'         => 188,
                    'title'           => $id,
                    'price_rub'       => 11,
                    'active_currency' => 643,
                    'individual'      => 0,
                    'active'          => 1,
                ]
            );
        }

    }

    public function down()
    {

    }
}
