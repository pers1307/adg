<?php

class ReBind extends Ruckusing_Migration_Base
{
    public function up()
    {
        /**
         * кидаем апдейт для перепривязки и удаляем не нужные категории
         */
        $this->execute("
          UPDATE mp_optional_eq_prices SET `title` = 101 WHERE `title` = '115'
        ");

        $this->execute("
          DELETE FROM mp_optional_equipment_items WHERE id = 115
        ");

        $this->execute("
          DELETE FROM mp_optional_equipment_articles WHERE id = 23
        ");
    }//up()

    public function down()
    {

    }//down()
}
