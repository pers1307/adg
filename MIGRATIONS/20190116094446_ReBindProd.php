<?php

class ReBindProd extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("
          UPDATE mp_optional_eq_prices SET `title` = 101 WHERE `title` = '114'
        ");

        $this->execute("
          DELETE FROM mp_optional_equipment_items WHERE id = 114
        ");

        $this->execute("
          DELETE FROM mp_optional_equipment_articles WHERE id = 22
        ");
    }//up()

    public function down()
    {
    }//down()
}
