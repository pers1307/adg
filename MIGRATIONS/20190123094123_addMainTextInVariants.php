<?php

class addMainTextInVariants extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("
          ALTER TABLE mp_variants_articles ADD `main_text` TEXT NULL
        ");
    }//up()

    public function down()
    {

    }//down()
}
