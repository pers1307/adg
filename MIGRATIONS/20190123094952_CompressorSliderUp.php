<?php

class CompressorSliderUp extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("
          UPDATE mp_power_stations_articles SET `inSlider` = 1 WHERE `level` = 1
        ");
    }//up()

    public function down()
    {
    }//down()
}
