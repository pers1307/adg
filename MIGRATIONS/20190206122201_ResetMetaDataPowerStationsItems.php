<?php

class ResetMetaDataPowerStationsItems extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("
          UPDATE mp_power_stations_items
          SET title_h1 = '', title_page = '', meta_description = '', meta_keywords = ''
        ");

        /**
         * сбрасываем алт у картинок
         */
        $query = new MSTable(PRFX . 'power_stations_items');
        $query->setFields(['*']);
        $items = $query->getItems();

        foreach ($items as $item) {
            $images = unserialize($item['image']);

            foreach ($images as &$image) {
                $image['text'] = '';
            }

            MSCore::db()->update(
                PRFX . 'power_stations_items',
                [
                    'image' => serialize($images)
                ],
                '`id` = ' . $item['id']
            );
        }
    }//up()

    public function down()
    {

    }//down()
}
