<?php

class AddContactEmailAndCode extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("
          ALTER TABLE mp_address ADD code VARCHAR(255) DEFAULT '' NULL;
        ");
        $this->execute("
          ALTER TABLE mp_address ADD email VARCHAR(255) DEFAULT '' NULL;
        ");
    }//up()

    public function down()
    {

    }//down()
}
