<?php

class AddCodeFiels extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("
          ALTER TABLE mp_manufacture ADD code VARCHAR(255) DEFAULT '' NULL;
        ");

        $this->execute("
          ALTER TABLE mp_delivery_prices ADD code VARCHAR(255) DEFAULT '' NULL;
        ");
    }//up()

    public function down()
    {
    }//down()
}
