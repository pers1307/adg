<?php

class ServicesMeta extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("
          ALTER TABLE mp_services ADD `title_h1` VARCHAR(255) DEFAULT '' NULL;
        ");

        $this->execute("
          ALTER TABLE mp_services ADD `title_page` VARCHAR(255) DEFAULT '' NULL;
        ");

        $this->execute("
          ALTER TABLE mp_services ADD `meta_description` VARCHAR(255) DEFAULT '' NULL;
        ");

        $this->execute("
          ALTER TABLE mp_services ADD `meta_keywords` VARCHAR(255) DEFAULT '' NULL;
        ");
    }//up()

    public function down()
    {
    }//down()
}
