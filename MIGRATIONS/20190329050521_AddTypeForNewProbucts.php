<?php

class AddTypeForNewProbucts extends Ruckusing_Migration_Base
{
    protected $productsName = [
        'AD-650MS',
        'AD-750MS',
        'AD-1000MS',
        'AD-1250MS',
        'AD-1375MS',
        'AD-1500MS',
        'AD-1700MS',
        'AD-1875MS',
        'AD-2000MS',
        'AD-2250MS',
        'AD-450MT',
        'AD-500MT',
        'AD-600MT',
        'AD-655MT',
        'AD-660MT',
        'AD-800MT',
        'AD-925MT',
        'AD-1000MT',
        'AD-1125MT',
        'AD-1250MT',
        'AD-1500MT',
        'AD-1640MT',
        'AD-1800MT',
        'AD-2050MT',
        'AD-2250MT',
        'AD-2500MT',
        'AD-2750MT',
        'AD-3000MT',
        'AD-30IS',
        'AD-45IS',
        'AD-50IS',
        'AD-60IS',
        'AD-75IS',
        'AD-80IS',
        'AD-100IS',
        'AD-120IS',
        'AD-125IS',
        'AD-130IS',
        'AD-150IS',
        'AD-160IS',
        'AD-170IS',
        'AD-200IS',
        'AD-250IS',
        'AD-300IS',
        'AD-350IS',
        'AD-400IS',
        'AD-440IS',
        'AD-500IS',
        'AD-600IS',
        'AD-7J',
        'AD-9J',
        'AD-12J',
        'AD-15J',
        'AD-20J',
        'AD-22J',
        'AD-30J',
        'AD-35J'
    ];

    public function up()
    {
        $query = new MSTable(PRFX . 'power_stations_items');
        $query->setFields(['*']);
        $query->setFilter('`parent` = 12');
        $items = $query->getItems();

        foreach ($items as &$item) {
            unset($item['id']);
            unset($item['parent']);
            unset($item['image']);
            unset($item['parents']);
        }

        foreach ($this->productsName as $productName) {
            $query = new MSTable(PRFX . 'power_stations_articles');
            $query->setFields(['*']);
            $query->setFilter('`name` = "' . $productName . '"');
            $article = $query->getItem();

            foreach ($items as &$item) {
                $item['parent'] = $article['id'];

                MSCore::db()->insert(
                    PRFX . 'power_stations_items',
                    $item
                );
            }
        }
    }//up()

    public function down()
    {
    }//down()
}
