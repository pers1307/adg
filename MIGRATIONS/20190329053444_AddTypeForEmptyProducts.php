<?php

class AddTypeForEmptyProducts extends Ruckusing_Migration_Base
{
    public function up()
    {
        $query = new MSTable(PRFX . 'power_stations_items');
        $query->setFields(['*']);
        $query->setFilter('`parent` = 12');
        $items = $query->getItems();

        foreach ($items as &$item) {
            unset($item['id']);
            unset($item['parent']);
            unset($item['image']);
            unset($item['parents']);
        }

        $query = new MSTable(PRFX . 'power_stations_articles');
        $query->setFields(['*']);
        $query->setFilter('`level` = 1');
        $articles = $query->getItems();

        foreach ($articles as $article) {
            $query = new MSTable(PRFX . 'power_stations_items');
            $query->setFields(['*']);
            $query->setFilter('`parent` = ' . $article['id']);
            $articleItems = $query->getItems();

            if (!empty($articleItems)) {
                continue;
            }

            foreach ($items as &$item) {
                $item['parent'] = $article['id'];

                MSCore::db()->insert(
                    PRFX . 'power_stations_items',
                    $item
                );
            }
        }
    }//up()

    public function down()
    {
    }//down()
}
