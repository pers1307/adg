<?php

class AddOptionalEqPrices extends Ruckusing_Migration_Base
{
    private $optionalPriceIds = [
        'open'         => 9,
        'kozhuh'       => 10,
        'konteyner'    => 11,
        'peredvezhnoj' => 12
    ];

    public function up()
    {
        $query = new MSTable(PRFX . 'power_stations_items');
        $query->setFields(['*']);
        $powerStationItems = $query->getItems();

        foreach ($powerStationItems as $powerStationItem) {
            $query = new MSTable(PRFX . 'optional_eq_prices');
            $query->setFields(['*']);
            $query->setFilter('`item_id` = ' . $powerStationItem['id']);
            $optionalPrices = $query->getItems();

            if (!empty($optionalPrices)) {
                continue;
            }

            /**
             * Иначе нужно раскопировать элементы
             */
            $query = new MSTable(PRFX . 'optional_eq_prices');
            $query->setFields(['*']);
            $query->setFilter('`item_id` = ' . $this->optionalPriceIds[$powerStationItem['code']]);
            $optionalPrices = $query->getItems();

            foreach ($optionalPrices as $optionalPrice) {
                unset($optionalPrice['id']);
                $optionalPrice['item_id'] = $powerStationItem['id'];

                MSCore::db()->insert(
                    PRFX . 'optional_eq_prices',
                    $optionalPrice
                );
            }
        }
    }//up()

    public function down()
    {
    }//down()
}
