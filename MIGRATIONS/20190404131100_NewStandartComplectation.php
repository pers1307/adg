<?php

class NewStandartComplectation extends Ruckusing_Migration_Base
{
    public function up()
    {
        $query = new MSTable(PRFX . 'power_stations_equipment');
        $query->setFields(['text']);
        $query->setFilter('`item_id` = 12');
        $comlectationText = $query->getItemOne();

        MSCore::db()->update(
            PRFX . 'power_stations_equipment',
            [
                'text' => $comlectationText
            ]
        );
    }//up()

    public function down()
    {
    }//down()
}
