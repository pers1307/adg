<?php

class UpdateTypeIdInTableRedirects2 extends Ruckusing_Migration_Base
{
    public function up()
    {
	    $this->execute("ALTER TABLE mp_redirects drop index `defsel`;");
        $this->execute("ALTER TABLE mp_redirects MODIFY `id` INT NOT NULL auto_increment;");
        $this->execute("ALTER TABLE mp_redirects MODIFY `from` varchar(300) NULL;");
        $this->execute("CREATE INDEX `defsel` on mp_redirects(`from`, `active`);");

    }//up()

    public function down()
    {
    }//down()
}
