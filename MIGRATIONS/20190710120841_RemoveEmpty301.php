<?php
class RemoveEmpty301 extends Ruckusing_Migration_Base
{

	//up()
	public function up()
	{
		$this->execute("
            DELETE FROM `mp_redirects` 
            WHERE `from` = '' AND `to` = ''  
        ");
	}

	//down()
	public function down(){}
}