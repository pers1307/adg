<?php

class RemoveMetadataEngines extends Ruckusing_Migration_Base
{
    public function up()
    {
        $this->execute("UPDATE mp_engines_items SET title_page='', meta_description='', meta_keywords='';");
    }//up()

    public function down()
    {
    }//down()
}
