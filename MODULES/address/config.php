<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'address';
    $module_caption = 'Адреса';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(
            'items' => array(
                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'title' => array(
                        'caption' => 'Тип',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'schedule' => array(
                        'caption' => 'График работы',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'phone' => array(
                        'caption' => 'Телефон',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'email' => array(
                        'caption' => 'Email',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'address' => array(
                        'caption' => 'Адрес',
                        'value' => '',
                        'height' => 260,
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;