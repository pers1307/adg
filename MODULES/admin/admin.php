<?php
header('Content-type: text/html; charset=utf-8');

switch (MSCore::urls()->vars[1]) {

    case 'fileLoader':

        $MSFiles = new MSFiles;
        $MSFiles->uploadItem();

        break;

    case 'fileQueue':
        $moduleName = htmlspecialchars($_POST['moduleName']);
        $configName = htmlspecialchars($_POST['configName']);
        $tableName = htmlspecialchars($_POST['tableName']);

        $pathId = intval($_POST['pathId']);
        $parent = intval($_POST['parent']);

        $settings = array();
        $upload = array();

        if(is_file(MODULES_DIR.'/'.$moduleName.'/config.php')) {
            $settings = include MODULES_DIR.'/'.$moduleName.'/config.php';
            $upload = (isset($settings['upload'])) ? $settings['upload'] : array();
        }

        if($upload) {
            $MSFiles = new MSFiles;
            $uploadDir = $MSFiles::getUploadFolder();
            $result = $MSFiles->uploadFile($uploadDir, array(
                'allowedExtensions' => isset($upload['settings']['allowed-extensions']) ? explode(',',$upload['settings']['allowed-extensions']) : null,
                'sizeLimit' => isset($upload['settings']['size-limit']) ? $upload['settings']['size-limit'] : null,
            ));

            if (!empty($result['success'])) {
                $sourceFile = $uploadDir . DS . $result['uploadName'];
                $id = md5($result['uploadName']);

                /** делаем миниатюры, если это картинки */
                if (isset($upload['thumbs']) && !is_null($upload['thumbs']))
                {
                    $path = array(
                        'original' => MSFiles::getFileRootPath($sourceFile),
                        'sys_thumb' => MSFiles::makeImageThumb($sourceFile, array(80, 80, true))
                    );

                    $thumbs = MSFiles::makeImageThumbs($sourceFile, $upload['thumbs']);
                    $path = array_merge($path, $thumbs);

                    $item = array(
                        'id' => $id,
                        'path' => $path,
                        'text' => ''
                    );
                } else {
                    $item = array(
                        'id' => $id,
                        'fileName' => $result['uploadName'],
                        'fileSize' => MSFiles::fileSizeFormat($result['fileSize']),
                        'path' => MSFiles::getFileRootPath($sourceFile),
                    );
                }

                if($item) {
                    $params = array(
                        $configName => serialize(array($item)),
                        'path_id' => $pathId
                    );

                    if(!empty($parent)) {
                        $params['parent'] = $parent;
                    }

                    MSCore::db()->insert(PRFX.$tableName, $params);

                    if(isset($settings['tables']['items']['order_field'])) {
                        $order = explode(' ', $settings['tables']['items']['order_field']);
                        $order = reset($order);
                        $order = trim($order, '`');

                        if($order == 'order') {
                            $inserted_id = MSCore::db()->id;
                            if($inserted_id) {
                                MSCore::db()->update(PRFX.$tableName,array('order' => $inserted_id), '`id`='.$inserted_id);
                            }
                        }
                    }
                }
            }

            header("Content-Type: text/plain");
            echo json_encode($result);
            exit;

        }
        break;

    case 'geocoder':

        $result = array(
            'items' => array()
        );

        $type = (isset($_POST['type'])) ? htmlspecialchars($_POST['type']) : null;

        switch($type) {
            // List items
            case 'list': {

                $query = (isset($_POST['q'])) ? htmlspecialchars($_POST['q']) : '';
                $addresses = curlRequest('http://geocode-maps.yandex.ru/1.x/?format=json&geocode='. $query);
                $addresses = json_decode($addresses, true);
                $addresses = $addresses['response']['GeoObjectCollection']['featureMember'];
                foreach($addresses as $address) {
                    $tmp['text'] = $address['GeoObject']['metaDataProperty']['GeocoderMetaData']['text'];
                    $tmp['id'] = json_encode(array('text' => $tmp['text'], 'coords' => str_replace(' ',':',$address['GeoObject']['Point']['pos'])));
                    $result['items'][] = $tmp;
                }
            }
                break;
        }

        header('Content-Type:application/json; charset=' . SITE_ENCODING);
        echo json_encode($result);
        exit;
        break;

    case 'autocomplete':
        $result = array(
            'total_count' => 0,
            'items' => array()
        );

        $type = (isset($_POST['type'])) ? htmlspecialchars($_POST['type']) : null;
        $table = (isset($_POST['table'])) ? htmlspecialchars($_POST['table']) : '';
        $order = (isset($_POST['order'])) ? htmlspecialchars($_POST['order']) : '';
        $where = (isset($_POST['where'])) ? htmlspecialchars($_POST['where']) : '';
        $keyField = (isset($_POST['key-field'])) ? htmlspecialchars($_POST['key-field']) : '';
        $nameField = (isset($_POST['name-field'])) ? htmlspecialchars($_POST['name-field']) : '';

        switch($type) {
            // Initial value
            case 'init': {
                $ids = (is_array($_POST['items']) && !empty($_POST['items'])) ? array_map('intval', $_POST['items']) : array();
                if($ids) {
                    $items = MSCore::db()->getAll('SELECT ' . $keyField . ' as id, ' . $nameField . ' as text FROM ' . $table . ' WHERE '.$keyField.' IN ('.implode(',',$ids).') ' . ($where != "" ? ' AND '.$where : '' ) . ' ' . ($order != "" ? 'ORDER BY ' . $order . '' : '') . '');
                    $result['total_count'] = count($items);
                    $result['items'] = $items;
                }
            }
                break;

            // List items
            case 'list': {
                $query = (isset($_POST['q'])) ? htmlspecialchars($_POST['q']) : '';
                $page = (isset($_POST['page'])) ? intval($_POST['page']) : 1;
                $limit = 10;
                if($query) {
                    $items = array();
                    $count = MSCore::db()->getOne('SELECT COUNT(*) FROM ' . $table . ' WHERE '.$nameField.' LIKE \'%'.$query.'%\' ' . ($where != "" ? ' AND '.$where : '' ) . ' ' . ($order != "" ? 'ORDER BY ' . $order . '' : '') . '');
                    if($count) {
                        $items = MSCore::db()->getAll('SELECT ' . $keyField . ' as id, ' . $nameField . ' as text FROM ' . $table . ' WHERE '.$nameField.' LIKE \'%'.$query.'%\' ' . ($where != "" ? ' AND '.$where : '' ) . ' ' . ($order != "" ? 'ORDER BY ' . $order . '' : '') . ' LIMIT '.(($page - 1) * $limit).', 10');
                    }
                    $result['total_count'] = $count;
                    $result['items'] = $items;
                }
            }
                break;
        }

        header('Content-Type:application/json; charset=' . SITE_ENCODING);
        echo json_encode($result);
        exit;
        break;

    case 'metrika_fetch':
        $result = array('code' => '');
        $result['code'] = HelperMetrika::getCode();
        echo json_encode($result);
        exit;
        break;

    case 'metrika_delegate':
        $result = array('error' => '', 'login' => '');
        $login = !empty($_POST['login']) ? trim(htmlspecialchars($_POST['login'])) : '';
        if(empty($login)) {
            $result['error'] = 'Логин не может быть пустым';
        } else {
            $result = HelperMetrika::delegateUser($login);
        }
        echo json_encode($result);
        exit;
        break;

    case'phpinfo':
        if (AUTH_IS_ROOT) {
            echo template('phpinfo');
        }
        break;

    case'serv_info':
        echo template('serv_info', array());
        break;

    case 'import_csv':
        $module_name = (isset(MSCore::urls()->vars[2])) ? MSCore::urls()->vars[2] : '';
        $db_name = (isset(MSCore::urls()->vars[3])) ? MSCore::urls()->vars[3] : '';
        $path_id = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;


        if (!isset($_FILES['csv']) && $module_name != "") {
            echo '
				<h3>Импорт данных</h3>
				<div style="padding: 10px;">			
				<form action="#" method="POST" id="import_csv" enctype="multipart/form-data">
				<div><b>Файл CSV</b></div>
				<input type="file" value="" name="csv">
				</form>
				</div>
				
				<SCRIPT type="text/javascript">
				dialogAddButtons(
					new Array(
						new Array("doLoad(getObj(\'import_csv\'),\'/' . ROOT_PLACE . '/admin/import_csv/' . $module_name . '/' . $db_name . '/' . (int)$path_id . '/\', \'\'); closeDialog();","Импортировать")
					)
				);
				</SCRIPT>			
			';
        } elseif (isset($_FILES['csv']) && $module_name != "") {
            $file = $_FILES['csv'];

            if (is_file($file['tmp_name'])) {
                $data = file($file['tmp_name']);
                foreach ($data as $num => $d) {
                    $data[$num] = trim($d);
                }


                $CUR_CONFIG = $CONFIG;
                include(MODULES_DIR . DS . $module_name . '/config.php');
                $mod_config = $CONFIG;
                $CONFIG = $CUR_CONFIG;
                unset($CUR_CONFIG);


                $main_table_config = 0;
                $table_name = '';
                $ok = false;
                foreach ($mod_config as $mparams => $data_config) {
                    if ($mparams == 'tables') {
                        $ok = true;
                        break;
                    }
                }

                if ($ok) {
                    foreach ($data_config as $tparams => $table_params_config) {
                        if (isset($table_params_config['db_name']) && $table_params_config['db_name'] == $db_name) {
                            $table_name = $tparams;
                            $main_table_config = $table_params_config;

                            break;
                        }
                    }


                    MSCore::db()->execute('TRUNCATE ' . PRFX . $db_name . '');

                    $sql = array();
                    foreach ($data as $dnum => $line) {
                        $line_values = explode(';', $line);

                        $num_f = 0;

                        $asql = array();
                        foreach ($main_table_config['config'] as $field => $field_config) {
                            if (isset($line_values[$num_f]) && $line_values[$num_f] != "") {
                                $asql[] = '`' . $field . '` = "' . MSCore::db()->pre(htmlspecialchars($line_values[$num_f], ENT_QUOTES)) . '"';
                            }

                            $num_f++;
                        }

                        MSCore::db()->execute('INSERT INTO ' . PRFX . $db_name . ' SET path_id=' . (int)$path_id . ', ' . implode(', ', $asql));
                    }
                }
            }
        } else {
            debug('error import cvs', 0, 1, 'a+');
        }
        break;

    case 'clear_linkpath':
        $module = (isset(MSCore::urls()->vars[2])) ? MSCore::urls()->vars[2] : '';
        $output = (isset(MSCore::urls()->vars[3])) ? MSCore::urls()->vars[3] : '';

        $_SESSION['link_path'] = array();

        $fastcall = MSCore::modules()->info[$module]['fastcall'];

        $_RESULT = array('fastcall' => $fastcall, 'output' => $output);
        die(json_encode($_RESULT));
        break;

    /* Каскадно перемещение вперед по модулям */
    case 'link2module':
    {
        $to_module = (isset(MSCore::urls()->vars[2])) ? MSCore::urls()->vars[2] : '';
        $from_module = (isset(MSCore::urls()->vars[3])) ? MSCore::urls()->vars[3] : '';
        $from_id = (isset(MSCore::urls()->vars[4])) ? MSCore::urls()->vars[4] : '';
        $to_key = (isset(MSCore::urls()->vars[5])) ? MSCore::urls()->vars[5] : '';
        $from_key = (isset(MSCore::urls()->vars[6])) ? MSCore::urls()->vars[6] : '';
        $path_id = (isset(MSCore::urls()->vars[7])) ? MSCore::urls()->vars[7] : '';
        $from_output = (isset(MSCore::urls()->vars[8])) ? MSCore::urls()->vars[8] : '';

        $imod = MSCore::modules()->info[$to_module];

        $link_path = isset($_SESSION['link_path']) ? $_SESSION['link_path'] : array();

//		$to_output = str_Replace($from_module,$to_module,$from_output);
        $to_output = mb_substr($to_module, 0, mb_strlen($from_module)) != $from_module ? str_replace($from_module, $to_module, $from_output) : $from_output;

        $array = array(
            'inmodule' => $to_module,
            'back2module' => $from_module,
            'from_id' => $from_id,
            'from_key' => $from_key,
            'to_key' => $to_key,
            'path_id' => $path_id,
            'form_output' => (empty($link_path)) ? 'fast_table_'.$from_module : $from_output,
            'to_output' => (empty($link_path)) ? 'fast_table_'.$to_module.'_linked' : $to_output . '_linked',
        );

        $link_path[] = $array;
        $_SESSION['link_path'] = $link_path;

        $_RESULT = array('content' => '&nbsp;',
            'fromModule' => $from_module,
            'toModule' => $to_module,
            'fastcall' => $imod['fastcall'] . (int)$path_id . '/'
        );
        die(json_encode($_RESULT));
    }
        break;

    /* Каскадно перемещение назад по открытым модулям */
    case 'back2module':
        $to_module = (isset(MSCore::urls()->vars[2])) ? MSCore::urls()->vars[2] : '';
        $from_module = (isset(MSCore::urls()->vars[3])) ? MSCore::urls()->vars[3] : '';

        $array = $_SESSION['link_path'];

        count($array) or die(json_encode(array()));

        $link_path = $array[sizeof($array) - 1];
        unset($array[sizeof($array) - 1]);
        $_SESSION['link_path'] = $array;

        $imod = MSCore::modules()->info[$to_module];

        $path_id = '';
        if (isset($link_path['path_id'])) {
            $path_id = (int)$link_path['path_id'] . '/';
        }

        $_RESULT = array('content' => '&nbsp;',
            'fromModule' => $from_module,
            'toModule' => $to_module,
            'fastcall' => $imod['fastcall'] . $path_id
        );
        die(json_encode($_RESULT));
        break;


    case 'passwd':
        echo template('passwd', array());
        break;


    /** Какие либо действия отдельного конфигуратора через главный модуль Админ */
    case 'mod_action':
        $form_item = (isset(MSCore::urls()->vars[2])) ? MSCore::urls()->vars[2] : '';
        $params = MSCore::urls()->vars;
        unset($params[0]);
        unset($params[1]);

        $tmp_params = array();
        foreach ($params as $num => $par) {
            $tmp_params[] = $par;
        }
        $params = $tmp_params;

        $file = $_SERVER['DOCUMENT_ROOT'] . '/CORE/forms/' . $form_item . '/action.php';

        if ($form_item != "" && is_file($file)) {
            require_once($file);
        }
        break;

    /**
     * Редактирование настроек страницы
     */
    case 'page_options':
        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;

        $vars = array();
        $vars['info'] = ($path_id !== false) ? MSCore::urls()->tree[MSCore::urls()->ids[$path_id]] : false;
        $vars['path_id'] = $path_id;
        $vars['title_menu'] = MSCore::urls()->tree[MSCore::urls()->ids[$path_id]]['title_menu'];

        /* Конфиг для редактирования TDK страницы */
        $config = array(
            'path_id' => array(
                'value' => $path_id,
                'type' => 'hidden',
            ),
            'parent' => array(
                'value' => $vars['info']['parent'],
                'type' => 'hidden',
            ),
            'www_type' => array(
                'value' => $vars['info']['www_type'],
                'type' => 'hidden',
            ),
            'title_menu' => array(
                'caption' => 'Заголовок в меню',
                'value' => html_entity_decode($vars['info']['title_menu']),
                'type' => 'string',
            ),
            'header' => array(
                'caption' => 'Заголовок страницы',
                'value' => html_entity_decode($vars['info']['header']),
                'type' => 'string',
            ),
            'title_page' => array(
                'caption' => 'Тег TITLE',
                'value' => html_entity_decode($vars['info']['title_page']),
                'type' => 'string',
            ),
            'description' => array(
                'caption' => 'Описание страницы (DESCRIPTION)',
                'value' => $vars['info']['meta_description'],
                'type' => 'string',
            ),
            'keywords' => array(
                'caption' => 'Ключевые слова страницы (KEYWORDS)',
                'value' => $vars['info']['meta_keywords'],
                'type' => 'string',
            ),

        );

        foreach ($GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $data) {
            $config[$field] = $data;
            $config[$field]['value'] = $vars['info'][$field];
        }
        $vars['form_tdk'] = write_tdk_Form(MSCore::forms()->make($config, '', FormConfigs::FORM_ID_TDK_FORM), array(
            'parent' => $vars['info']['parent'],
            'www_type' => $vars['info']['www_type']

        ));


        if (AUTH_IS_ROOT) {
            /* Верстка страницы */

            $layouts = array();
            $excludedFiles = array(
                'captcha.php',
                '404.php',
                '403.php',
                'api.php'
            );
            $sysGroups = array('ADMIN', 'CONTROL');

            $groupsFiles = GetDirsFromDir(DES_DIR);

            foreach ($groupsFiles as $group) {

                if (!(AUTH_IS_ROOT && SHOW_SYSTEM_PAGES) && in_array($group, $sysGroups)) {
                    continue;
                }

                $files = GetFilesFromDir(DES_DIR . DS . $group . DS . 'layout' . DS, true, DES_DIR);
                array_walk($files, 'arrayWalkReplace', array(DS, '|'));
                $files = array_flip($files);

                if (!SHOW_SYSTEM_PAGES) {
                    $files = array_diff($files, $excludedFiles);
                }

                empty($files) or $layouts[$group] = $files;
            }

            $config_template = array(
                'html' => array(
                    'caption' => 'Верстка страницы',
                    'value' => $vars['info']['main_template'] != '' ? $vars['info']['main_template'] : 'inner.php',
                    'values' => $layouts,
                    'type' => 'select'
                ),
                'path_id' => array(
                    'value' => $path_id,
                    'type' => 'hidden',
                )
            );
            $vars['template_form'] = MSCore::forms()->make($config_template, '', FormConfigs::FORM_ID_TEMPLATE);
        } else {
            $vars['template_form'] = '';
        }

        if (AUTH_IS_ROOT) {
            /* Сохранение блоков как шаблон (зона контент исключается) */
            $config_tpl = array(
                'caption' => array(
                    'caption' => 'Название шаблона',
                    'value' => '',
                    'type' => 'string',
                ),
                'path_id' => array(
                    'value' => $path_id,
                    'type' => 'hidden',
                )
            );
            $vars['tpl_form'] = MSCore::forms()->make($config_tpl, '', FormConfigs::FORM_ID_SAVETPL);
        } else {
            $vars['tpl_form'] = '';
        }


        /* Конфиг для редактирования SYSNAME страницы */
        $path_cur_page = MSCore::db()->getOne('SELECT path FROM '.PRFX.'www WHERE path_id="' . (int)$path_id . '"');
        if ($path_cur_page != "") {
            $path_cur_page = trim($path_cur_page, '/');
            $path_cur_page = explode('/', str_replace("//", "/", $path_cur_page));
            $path_cur_page = end($path_cur_page);
        }
        $config_pagepath = array(
            'page_path' => array(
                'caption' => 'Системное имя страницы',
                'value' => $path_cur_page,
                'height' => 100,
                'type' => 'string',
            ),
            'path_id' => array(
                'value' => $path_id,
                'type' => 'hidden',
            )
        );
        $vars['pagepath_form'] = MSCore::forms()->make($config_pagepath, '', FormConfigs::FORM_ID_PAGEPATH_FORM);


        /* Конфиг для редактирования RDR страницы */
        $vac_out = generateTreeSelectArray();
        $config_rdr = array(
            'rdr_path_id' => array(
                'caption' => 'Перевод на страницу сайта',
                'value' => $vars['info']['rdr_path_id'],
                'values' => $vac_out,
                'height' => 100,
                'type' => 'select',
            ),
            'rdr_url' => array(
                'caption' => 'Или на другой URL',
                'value' => $vars['info']['rdr_url'],
                'height' => 100,
                'type' => 'string',
            ),
            'path_id' => array(
                'value' => $path_id,
                'type' => 'hidden',
            )
        );
        $vars['redirect_form'] = MSCore::forms()->make($config_rdr, '', FormConfigs::FORM_ID_REDIRECT);

        echo template('page_options', $vars);
        break;


    /** Показ главной страницы функционала по копированию разделов*/
    case 'www_pages':
        $vars = array();

        $vars['tree_from'] = getTypesNode_templates(true, 'from');
        $vars['tree_to'] = getTypesNode_templates(true, 'to');

        echo template('www_pages', $vars);
        break;


    /**
     * Визивик на весь экран
     */
    case 'fullscreen_w':
        $sysname = urldecode((isset(MSCore::urls()->vars[2])) ? MSCore::urls()->vars[2] : "");
        $height = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : '200';

        $vars = array();
        $vars['sysname'] = $sysname;
        $vars['height'] = $height;

        echo template('fullscreen_w', $vars);

        break;

    case 'zone_wysiwyg_message':

        $zone = urldecode((isset(MSCore::urls()->vars[2])) ? MSCore::urls()->vars[2] : "");
        $path_id = urldecode((isset(MSCore::urls()->vars[3])) ? MSCore::urls()->vars[3] : 0);

        $vars = array();
        $vars['zone'] = $zone;
        $vars['path_id'] = $path_id;
        $vars['height'] = '100%';

        $content = MSCore::db()->getOne('SELECT content FROM `' . PRFX . 'zones_content` WHERE zone_id="' . $zone . '" AND path_id="' . (int)$path_id . '"');

        $vars['content'] = html_entity_decode($content, ENT_QUOTES);

        echo template('zone_wysiwyg', $vars);
        break;

    case 'zone_wysiwyg_save2':
        $_RESULT = array('content' => 'Сохранено');
        die(json_encode($_RESULT));
        break;

    case 'zone_wysiwyg_save':
        $zone = urldecode((isset(MSCore::urls()->vars[2])) ? MSCore::urls()->vars[2] : "");
        $path_id = urldecode((isset(MSCore::urls()->vars[3])) ? MSCore::urls()->vars[3] : "");

        $id = MSCore::db()->getOne('SELECT id FROM `' . PRFX . 'zones_content` WHERE  zone_id="' . $zone . '" AND path_id="' . $path_id . '"');

        if ($_REQUEST['content' . $zone] == '<br type="_moz" />') {
            $_REQUEST['content' . $zone] = '';
        }
        $allowed = array('b' => array(),
            'em' => array('class' => 1, 'style' => 1,),
            'i' => array('class' => 1, 'style' => 1,),
            'a' => array('class' => 1, 'style' => 1, 'href' => 1, 'title' => 1, 'alt' => 1),
            'p' => array('class' => 1, 'style' => 1, 'align' => 1),
            'br' => array('class' => 1, 'style' => 1,),
            'h1' => array('class' => 1, 'style' => 1,),
            'h2' => array('class' => 1, 'style' => 1,),
            'h3' => array('class' => 1, 'style' => 1,),
            'h4' => array('class' => 1, 'style' => 1,),
            'h5' => array('class' => 1, 'style' => 1,),
            'h6' => array('class' => 1, 'style' => 1,),
            'h7' => array('class' => 1, 'style' => 1,),
            'iframe' => array('class' => 1,
                'style' => 1,
                'width' => 1,
                'height' => 1,
                'frameborder' => 1,
                'scrolling' => 1,
                'marginheight' => 1,
                'marginwidth' => 1,
                'src' => 1,
            ),
            'small' => array('class' => 1, 'style' => 1,),
            'strong' => array('class' => 1, 'style' => 1,),
            'img' => array('class' => 1,
                'style' => 1,
                'title' => 1,
                'alt' => 1,
                'border' => 1,
                'width' => 1,
                'height' => 1,
                'src' => 1,
                'align' => 1
            ),
            'table' => array('class' => 1,
                'style' => 1,
                'title' => 1,
                'alt' => 1,
                'border' => 1,
                'width' => 1,
                'height' => 1,
                'cellpadding' => 1,
                'cellspacing' => 1
            ),
            'thead' => array('class' => 1,
                'style' => 1,
                'title' => 1,
                'alt' => 1,
                'border' => 1,
                'width' => 1,
                'height' => 1,
            ),
            'tbody' => array('class' => 1,
                'style' => 1,
                'title' => 1,
                'alt' => 1,
                'border' => 1,
                'width' => 1,
                'height' => 1,
            ),
            'tr' => array('style' => 1,
                'title' => 1,
                'alt' => 1,
                'border' => 1,
                'width' => 1,
                'height' => 1,
                'class' => 1,
                'colspan' => 1,
                'rowspan' => 1,
                'valign' => 1,
                'align' => 1,
            ),
            'td' => array('style' => 1,
                'title' => 1,
                'alt' => 1,
                'border' => 1,
                'width' => 1,
                'height' => 1,
                'class' => 1,
                'colspan' => 1,
                'rowspan' => 1,
                'valign' => 1,
                'align' => 1,
            ),
            'div' => array('style' => 1, 'class' => 1, 'width' => 1, 'align' => 1),
            'span' => array('style' => 1, 'class' => 1, 'width' => 1, 'align' => 1),
            'ul' => array('class' => 1, 'style' => 1, 'width' => 1, 'align' => 1),
            'li' => array('class' => 1, 'style' => 1, 'width' => 1, 'align' => 1),
            'ol' => array('class' => 1, 'style' => 1, 'width' => 1, 'align' => 1),
            'u' => array('class' => 1, 'style' => 1, 'width' => 1, 'align' => 1),
            'strike' => array('class' => 1, 'style' => 1, 'width' => 1, 'align' => 1),
        );

//		$_REQUEST['content'.$zone]=cleanTextFromWord($_REQUEST['content'.$zone]);
//		$_REQUEST['content'.$zone]=kses($_REQUEST['content'.$zone],$allowed);

        if ($id > 0) {
            MSCore::db()->execute('UPDATE `' . PRFX . 'zones_content` SET content="' . htmlspecialchars($_REQUEST['content' . $zone], ENT_QUOTES) . '" WHERE id="' . $id . '"');
        } else {
            MSCore::db()->execute('INSERT INTO `' . PRFX . 'zones_content` SET content="' . htmlspecialchars($_REQUEST['content' . $zone], ENT_QUOTES) . '", zone_id="' . $zone . '", path_id="' . $path_id . '"');
        }

        //$_RESULT = array('content' => ' Текст сохранен');

        $res_text = strip_tags($_REQUEST['content' . $zone], '<img><p><h1><h2><h3><h4><h5><h6><span><div><br><iframe><b><i><small><strong><table><tr><td><tbody><thead><ol><ul><li>');
        $_RESULT = array('content' => $res_text);
        die(json_encode($_RESULT));


        break;

#######################################################################################################################

    /**
     * Изменение видимости раздела
     */
    case 'toggle_vis':
        ob_clean();

        $path_id = (int)MSCore::urls()->vars[2];
        $isMovable = (bool)MSCore::urls()->vars[3];

        if (isset(MSCore::urls()->tree[MSCore::urls()->ids[$path_id]])) {
            $visible = MSCore::urls()->tree[MSCore::urls()->ids[$path_id]]['visible'] == 0 ? 1 : 0;
            $sql = "UPDATE `" . PRFX . "www` SET `visible` = '" . (int)$visible . "' WHERE `path_id` = " . (int)$path_id;
            MSCore::db()->execute($sql);
        }

        $PagesTree = new PagesTree();

        $_RESULT['content'] = $PagesTree->makeTree($path_id)->render(true, $isMovable ? 2 : 1);
        die(json_encode($_RESULT));
        break;

    /**
     * Отдаем содержание конфигуратора exporer и далее все данные отдает модуль Ajax
     */
    case 'explorer':
        ob_clean();

        $path = str_replace('//', '/', EndSlash(request('path', '')));
        $basepath = str_replace('//', '/', EndSlash(request('basepath', '')));

        //if (mb_strpos($path,$basepath) === false) die('Попытка взлома');

        /** Создадим ссыку для кнопки назад*/
        $backlink = explode('/', trim($path, '/'));
        if (sizeof($backlink) > 1) {
            $backlink = array_reverse($backlink);
            unset($backlink[key($backlink)]);
            $backlink = array_reverse($backlink);
        }
        $backlink = '/' . ROOT_PLACE . '/ajax/explorer/?path=' . EndSlash('/' . implode('/', $backlink)) . '&basepath=' . $basepath . '&path_id=' . request('path_id', '') . '&zone=' . request('zone', '') . '&conf=' . request('conf', '');

        $vars['path'] = $path;
        $vars['folders'] = cmsGetFoldersAndFiles(DOC_ROOT . DS . $vars['path']);
        $vars['backlink'] = $backlink;


        die(template('explorer', $vars));

        break;

    /**
     * Работа со структурой сайта
     */
    case 'structure':
        /** Проверка на вызов дочернего пути */
        if (isset(MSCore::urls()->vars[2])) {

            switch (MSCore::urls()->vars[2]) {
                /**
                 * Создание нового пути
                 */
                case 'add_path':
                    ob_clean();
                    $data_path_id = str_replace('node', '', MSCore::urls()->vars[3]);
                    $data_path_id = explode('_', $data_path_id);
                    list($path_id, $type_id) = $data_path_id;


                    $sql = 'SELECT `id`, `title` FROM `' . PRFX . 'www_types` WHERE `id`=' . (int)$type_id;
                    $cur_type = MSCore::db()->getRow($sql);

                    $config['static_www_type']['caption'] = 'Тип страницы';
                    $config['static_www_type']['value'] = $cur_type['title'];
                    $config['static_www_type']['type'] = 'static';

                    $config['header']['caption'] = 'Заголовок страницы';
                    $config['header']['value'] = '';
                    $config['header']['type'] = 'string';
                    $config['header']['to_code'] = true;

                    $config['code']['caption'] = 'Системное имя страницы';
                    $config['code']['value'] = '';
                    $config['code']['type'] = 'code';

                    $config['title_menu']['caption'] = 'Заголовок в меню:';
                    $config['title_menu']['value'] = '';
                    $config['title_menu']['type'] = 'string';

                    $config['title']['caption'] = 'Тег TITLE:';
                    $config['title']['value'] = '';
                    $config['title']['type'] = 'string';

                    $tpl = array();
                    $sql = "SELECT `id`,`caption` FROM `" . PRFX . "tpl`";

                    foreach (MSCore::db()->getAll($sql) as $info) {
                        $tpl[$info['id']] = $info['caption'];
                    }

                    if (count($tpl) > 1) {
                        $config['html'] = array(
                            'caption' => 'Шаблон:',
                            'value' => 0,
                            'values' => $tpl,
                            'type' => 'select',
                        );
                    } else {
                        $keys = array_keys($tpl);
                        $config['html'] = array(
                            'value' => reset($keys),
                            'type' => 'hidden',
                        );
                    }

                    $config['www_type']['value'] = (isset($type_id)) ? $type_id : 0;
                    $config['www_type']['type'] = 'hidden';

                    $config['root']['value'] = (isset($path_id)) ? $path_id : 0;
                    $config['root']['type'] = 'hidden';


                    foreach ($GLOBALS['PAGE_CONFIG']['pages']['config'] as $field => $data) {
                        $config[$field] = $data;
                    }

                    $vars['__FORM__'] = MSCore::forms()->make($config);

                    die(template('add_path', $vars));
                    break;

                /**
                 * Запросили редактирование структуры страницы выдаем все что можно
                 */
                case 'edit_path':
                    ob_clean();

                    $path_id = (isset(MSCore::urls()->vars[3])) ? MSCore::urls()->vars[3] : false;
                    $vars['info'] = ($path_id !== false) ? MSCore::urls()->tree[MSCore::urls()->ids[$path_id]] : false;
                    $vars['path_id'] = $path_id;

                    $_SESSION['link_path'] = array();
                    $_SESSION['PagesTreeActive'] = $path_id;

                    die(template('edit_path', $vars));
                    break;


                /**
                 * Изменение титла страницы из дерева
                 */
                case 'rename':
                    ob_clean();
                    if ((int)MSCore::urls()->vars[3] && trim(MSCore::urls()->vars[4])) {
                        $id = (int)MSCore::urls()->vars[3];
                        $name = trim(MSCore::urls()->vars[4]);
                        ob_clean();
                        $inf = MSCore::db()->getRow("SELECT * FROM `" . PRFX . "www` WHERE `path_id`='" . $id . "'");

                        if (sizeof($inf)) {
                            MSCore::db()->execute("UPDATE `" . PRFX . "www` SET `title_menu` = '" . MSCore::db()->pre($name) . "' WHERE `path_id`='" . $inf['path_id'] . "'") or die("NOT OK");
                            die("OK");
                        }
                        die("NOT OK");
                    }
                    break;


                /**
                 * Удаление прикрепленного блока/модуля из зоны страницы
                 */
                case 'delete_cont':
                    $path_id = (int)MSCore::urls()->vars[3];
                    $info = ($path_id !== 0) ? MSCore::urls()->tree[MSCore::urls()->ids[$path_id]] : false;
                    $zone = MSCore::urls()->vars[4];
                    $script = (int)MSCore::urls()->vars[5];
                    if (isset($info['config'][$zone][$script])) {
                        unset($info['config'][$zone][$script]);
                        $sql = "
							UPDATE `" . PRFX . "www` SET `config` = '" . MSCore::db()->pre(serialize($info['config'])) . "'
							WHERE `path_id` = '" . MSCore::db()->pre($path_id) . "'
						";
                        MSCore::db()->execute($sql);
                    }
                    Redirects::go(current_path('/admin/structure/edit_path/' . $path_id . '/'));
                    break;

                /**
                 * Добавление модуля на страницу
                 */
                case 'add_module':
                    ob_clean();
                    $vars = array();

                    $path_id = (int)MSCore::urls()->vars[3];
                    $zone = MSCore::urls()->vars[4];

                    $modules = array();

                    foreach (MSCore::modules()->info as $dir => $info) {
                        if (array_search($dir, MSCore::modules()->sys_modules)) {
                            continue;
                        }
                        $modules[$dir] = $info['module_caption'];
                    }

                    $config['module'] = array(
                        'caption' => 'Выберите модуль:',
                        'value' => '',
                        'values' => $modules,
                        'type' => 'select',
                        'size' => 15,
                    );

                    /*$config['get'] = array(
                        'caption' => 'Параметры в виде GET запроса: ',
                        'value' => ''
                    );*/

                    $config['path_id'] = array(
                        'value' => $path_id,
                        'type' => 'hidden',
                    );

                    $config['zone'] = array(
                        'value' => $zone,
                        'type' => 'hidden',
                    );

                    $vars['_FORM_'] = MSCore::forms()->make($config);

                    die(template('add_module', $vars));

                    break;

                /**
                 * Добавление блока на страницу
                 */
                case 'add_block':
                    ob_clean();
                    $vars = array();

                    $vars['path_id'] = (int)MSCore::urls()->vars[3];
                    $vars['zone'] = MSCore::urls()->vars[4];
                    $designName = MSCore::urls()->vars[5];
                    /**
                     * Пробегаемся по всем папкам в DESIGN и ищем папку BLOCKS с файлом config.php
                     */
                    $blocks = GetBlocks(DES_DIR, $designName);

                    foreach ($blocks as $key => $info) {
                        if ($info['caption']) {
                            $name = ($info['module'] && isset(MSCore::modules()->info[$info['module']])) ? MSCore::modules()->info[$info['module']]['module_caption'] : 'Другие';
                            $vars['result'][$name][] = $info;
                        }
                    }
                    @asort($vars['result']);
                    die(template('add_block', $vars));
                    break;
            }
            return;
        }
        break;

    case 'robotstxt':
        die(template('robotstxt', array()));
        break;
}

die();