<?php
    $CONFIG = array(
        'version' => '1.0.0.0',
        'module_name' => 'Admin',
        'module_caption' => 'Дерево страниц сайта',

        'pages' => array(
            'config' => array()
        ),
    );

    if (defined('AUTH_IS_ROOT') && AUTH_IS_ROOT)
    {
        $CONFIG['pages']['config'] = array_merge(
            array(
                'nodel' => array(
                    'caption' => 'Не удалять',
                    'value' => '0',
                    'type' => 'checkbox',
                    'in_list' => 1,
                    'filter' => 0,
                ),

                'noadd' => array(
                    'caption' => 'Не добавлять подразделы',
                    'value' => '0',
                    'type' => 'checkbox',
                    'in_list' => 1,
                    'filter' => 0,
                ),

                'noview' => array(
                    'caption' => 'Не менять видимость',
                    'value' => '0',
                    'type' => 'checkbox',
                    'in_list' => 1,
                    'filter' => 0,
                ),

                'notfound' => array(
                    'caption' => 'Страница 404',
                    'value' => '0',
                    'type' => 'checkbox',
                    'in_list' => 1,
                    'filter' => 0,
                ),

                'getvars' => array(
                    'caption' => 'Разрешенные GET переменные',
                    'value' => '',
                    'type' => 'string',
                    'in_list' => 0,
                    'filter' => 0,
                )
            ),

            $CONFIG['pages']['config']);
    }

    $CONFIG['pages']['config']['image'] = [
	    'caption' => 'Изображение',
	    'value' => '',
	    'type' => 'loader',
	    'thumbs' => array(
		    'view' => array(343, 285),
		    'min' => array(149, 139),
	    ),
	    'settings' => array(
		    'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
		    'size-limit' => 5 * 1024 * 1024,
		    'limit' => 1,
		    'module-name' => 'Admin'
	    ),
    ];

    $GLOBALS['PAGE_CONFIG'] = $CONFIG;

    return $CONFIG;