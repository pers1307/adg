<?php
    /**
     * @var array $info
     */

    $des_file = "";
    $file = DES_DIR . DS . str_replace('|', DS, $info['main_template']);

    $pathArray = explode('|', trim($info['main_template'], '|'));

    if (file_exists($file) && is_file($file))
    {
        $des_file = file_get_contents($file);
    }

    preg_match_all('#\$(_[A-Z0-9]+_)#', $des_file, $exzone);

    //--------получаем имя зоны данной верстки-----------
    if (isset($exzone[1]) && sizeof($exzone[1]))
    {
        $exzone = $exzone[1];
    }
    else
    {
        $exzone = false;
    }

    $assigned_modules = array();
    if (sizeof($info['config']) && is_array($info['config']))
    {
        foreach ($info['config'] as $zone_config)
        {
            foreach ($zone_config as $value)
            {
                if ($value['module'] && isset(MSCore::modules()->info[$value['module']]) && MSModuleController::blockOnPage($value['module']))
                {
                    $assigned_modules[MSCore::modules()->info[$value['module']]['module_caption']] = array(
                        MSCore::modules()->info[$value['module']]['fastcall'],
                        MSCore::modules()->info[$value['module']]['module_name']
                    );
                }
            }
        }
    }

    $tabs = array();

    if (isset($assigned_modules))
    {
        foreach ($assigned_modules as $name => $value)
        {

            $tabs[] = array(
                'title' => $name,
                'content' => '<div id="fast_table_' . $value[1] . '" class="moduleWrap"></div>'
            );
        }
    }

    $temp = assoc('value', MSCore::page()->allZones);
    if ($exzone && count($exzone))
    {
        $exzone = array_unique($exzone);
        foreach ($exzone as $zone)
        {
            if (isset($temp[$zone]))
            {
                $tabs[] = array(
                    'title' => $temp[$zone]['caption'],
                    'content' => template('admin/zoneContent', array(
                        'zone' => $zone,
                        'info' => $info,
                        'pathArray' => $pathArray
                    ))
                );
            }
        }
    }

    $TABS = new Tabs('admin/tabs');

    foreach ($tabs as $tabId => $tab)
    {

        $TABS->addTab($tab['title'], 'tab' . $tabId, array(
            'content' => $tab['content'],
            'attributes' => array(
                'title' => ''
            )
        ));
    }

    echo $TABS;

    if (!empty($assigned_modules))
    {
        ?>
        <script>
            <? foreach($assigned_modules as $name => $link){?>
            doLoad('', '<?=$link[0].$info['path_id'].'/1/'?>', 'fast_table_<?=$link[1]?>');
            <?}?>
        </script>
    <?
    }