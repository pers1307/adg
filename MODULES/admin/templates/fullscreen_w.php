
<!-- TinyMCE -->
<script type="text/javascript">
	tinyMCE.init({
		// General options
		mode : "exact",
		elements : "fsw_<?=$sysname;?>",
		theme : "advanced",
		plugins : "safari,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",

		// Theme options
		theme_advanced_buttons1 : "code,fullscreen, mpsave, print,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,formatselect,|,forecolor,backcolor",
		theme_advanced_buttons2 : "cut,copy,paste,|,search,replace,|,bullist,numlist,|,outdent,indent,|,undo,redo,|,link,unlink,anchor,image,media,|,hr,charmap,|,sub,sup",
		theme_advanced_buttons3 : "tablecontrols,|,visualaid",

		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
				
		file_browser_callback : "ajaxfilemanager",
		paste_use_dialog : false,

		
		theme_advanced_resizing : false,
		theme_advanced_resize_horizontal : false,

		apply_source_formatting : true,
		force_br_newlines : true,
		force_p_newlines : false,	
		relative_urls : true,
		language : "ru",


		// Example word content CSS (should be your site CSS) this one removes paragraph margins
		content_css : "css/word.css",

		// Drop lists for link/image/media/template dialogs
		template_external_list_url : "lists/template_list.js",
		external_link_list_url : "lists/link_list.js",
		external_image_list_url : "lists/image_list.js",
		media_external_list_url : "lists/media_list.js",


		setup : function(ed) {
			ed.addButton('mpsave', {
				title : 'Сохранить',
				image : '/DESIGN/ADMIN/js/mce/themes/advanced/img/save.gif',
				onclick : function() {				
					wisywigDoBackText('<?=$sysname;?>');
					alert('Сохранено');
				}
			});
		},
		// Replace values for the template plugin
		template_replace_values : {
			username : "Some User",
			staffid : "991234"
		}		
	});
	

function ajaxfilemanager(field_name, url, type, win) {
			var ajaxfilemanagerurl = "/DESIGN/ADMIN/js/mce/plugins/ajaxfilemanager/ajaxfilemanager.php";
			switch (type) {
				case "image":
					break;
				case "media":
					break;
				case "flash": 
					break;
				case "file":
					break;
				default:
					return false;
			}
            tinyMCE.activeEditor.windowManager.open({
                url: "/DESIGN/ADMIN/js/mce/plugins/ajaxfilemanager/ajaxfilemanager.php",
                width: 782,
                height: 440,
                inline : "yes",
                close_previous : "no"
            },{
                window : win,
                input : field_name
            });
            
/*            return false;			
			var fileBrowserWindow = new Array();
			fileBrowserWindow["file"] = ajaxfilemanagerurl;
			fileBrowserWindow["title"] = "Ajax File Manager";
			fileBrowserWindow["width"] = "782";
			fileBrowserWindow["height"] = "440";
			fileBrowserWindow["close_previous"] = "no";
			tinyMCE.openWindow(fileBrowserWindow, {
			  window : win,
			  input : field_name,
			  resizable : "yes",
			  inline : "yes",
			  editor_id : tinyMCE.getWindowArg("editor_id")
			});
			
			return false;*/
		}
	
	
	
</script>
<!-- /TinyMCE -->
<textarea name="fsw_<?=$sysname;?>" id="fsw_<?=$sysname;?>" class="wisywig"></textarea>

<script type="text/javascript">
	dialogAddButtons(
		new Array(
			new Array("wisywigDoBackText('<?=$sysname;?>');alert('Сохранено');", "Сохранить")
		)
	);
document.getElementById('fsw_<?=$sysname;?>').innerHTML = document.getElementById('<?=$sysname;?>').innerHTML;
</script>