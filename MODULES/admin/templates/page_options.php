<div class="form-wrap pageSettings">
    <?if ($form_tdk != "") { ?>
        <fieldset>
            <legend>Основные параметры страницы</legend>
            <div id="tdk_form_div">
                <?=$form_tdk;?>
            </div>
        </fieldset>
    <? }?>

    <?if ($template_form != "") { ?>
        <fieldset>
            <legend>Верстка страницы</legend>

            <form method="post" id="template_form" enctype="multipart/form-data">
                <?=$template_form;?>
                <div class="table">
                    <div>
                        <button type="button" onclick="doLoad(document.getElementById('template_form'), '/<?= ROOT_PLACE; ?>/ajax/update_template/','template_report','post','rewrite');">Сохранить</button>
                    </div>
                    <div id="template_report" class="msg-wrap"></div>
                </div>
            </form>
        </fieldset>
    <? }?>

    <?if ($tpl_form != "") { ?>
        <fieldset>
            <legend>Сохранение шаблона страницы</legend>
            <form method="post" id="tpl_form" enctype="multipart/form-data">
                <?=$tpl_form;?>
                <div class="table">
                    <div>
                        <button type="button" onclick="doLoad(document.getElementById('tpl_form'), '/<?= ROOT_PLACE; ?>/ajax/save_tpl_page/','tpl_report');">Сохранить</button>
                    </div>
                    <div id="tpl_report" class="msg-wrap"></div>
                </div>
            </form>
        </fieldset>
    <? }?>
    <?if ($pagepath_form != "") { ?>
        <fieldset>
            <legend>Системное имя страницы</legend>
            <form method="post" id="pagepath_form" enctype="multipart/form-data">
                <?=$pagepath_form;?>
                <div class="table">
                    <div>
                        <button type="button" onclick="doLoad(document.getElementById('pagepath_form'), '/<?= ROOT_PLACE; ?>/ajax/save_pagepath_page/','pagepath_report');">Сохранить</button>
                    </div>
                    <div id="pagepath_report" class="msg-wrap"></div>
                </div>
            </form>
        </fieldset>
    <? }?>

    <?if ($redirect_form != "") { ?>
        <fieldset>
            <legend>Перенаправления</legend>
            <form method="post" id="rdr_form" enctype="multipart/form-data">
                <?=$redirect_form;?>
                <div class="table">
                    <div>
                        <button type="button" onclick="doLoad(document.getElementById('rdr_form'), '/<?= ROOT_PLACE; ?>/ajax/update_rdr/', 'rdr_report');">Сохранить</button>
                    </div>
                    <div id="rdr_report" class="msg-wrap"></div>
                </div>
            </form>
        </fieldset>
    <? }?>
</div>