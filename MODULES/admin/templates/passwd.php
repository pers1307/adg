<?
    global $_USER;
?>
<div class="form-wrap">
    <form id="passwd_form">
        <fieldset>
            <legend>Изменение пароля пользователя <?= $_USER['username']; ?></legend>

            <div class="table">
                <div>Текущий пароль</div>
                <div><input type="password" name="oldpass"></div>
            </div>
            <div class="table">
                <div>Новый пароль</div>
                <div><input type="password" name="newpass"></div>
            </div>
            <div class="table">
                <div>Подтверждение нового пароля</div>
                <div><input type="password" name="newpass2"></div>
            </div>
            <div id="passwd_report" class="msg-wrap">&nbsp;</div>
        </fieldset>
    </form>
</div>
<div class="form-bottom-controls">
    <ul>
        <li>
            <button onclick="doLoad(document.getElementById('passwd_form'), '/<?= ROOT_PLACE; ?>/ajax/passwd_change/','passwd_report','post','rewrite');">
                Сохранить
            </button>
        </li>
        <li>
            <button onclick="Site.modal.close()" class="grey">Отменить</button>
        </li>
    </ul>
</div>