<?php
    /*
    родительский элемент и у разделов и у элементов называется parent
    управление видимостью - поле visible (только у разделов)
    для элементов управление видимостью можно сделать отдельным полем
    */

    $module_name = 'catalog';
    $module_caption = 'Каталог товаров';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '2.0.0.0',

        'tables' => array(
            /*
                Названия таблиц "articles" и "items" обязательны, эти имена используются в различных функциях
            */


            //------------------------------Разделы-------------------------------------------------------
            'articles' => array(

                'db_name' => $module_name . '_articles',
                'dialog' => array('width' => 650, 'height' => 400),
                'onpage' => 20,

                //-------если поставить true то нельзя будет добавлять разделы первого уровня
                'noAddArticles' => false,

                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`name`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => false,

                //------масимальное количество уровней вложенности----------
                'max_levels' => 1,

                //------поле, которое будет выводиться в списке
                'title_field' => 'name',
                'is_order' => true, // Ручная сортировка

                'config' => array(
                    'name' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 1,
                        'value' => '',
                    ),
					'title' => array(
						'caption' => 'Тип товара',
						'value' => '',
						'type' => 'string',
						'in_list' => 0,
						'filter' => 0,
					),
	                'image' => array(
		                'caption' => 'Изображение',
		                'value' => '',
		                'type' => 'loader',
		                'thumbs' => array(
			                'view' => array(343, 285),
			                'min' => array(149, 139),
		                ),
		                'settings' => array(
			                'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
			                'size-limit' => 5 * 1024 * 1024,
			                'limit' => 1,
			                'module-name' => $module_name
		                ),
		                'in_list' => 0,
	                ),
	                'char_group' => array(
		                'caption' => 'Группа характеристик',
		                'value' => '',
		                'type' => 'select',
		                'from' => [
			                'table_name' => PRFX . 'char_other_articles',
			                'key_field' => 'id',
			                'name_field' => 'name',
			                'where' => '`visible` = 1',
		                ],
		                'in_list' => 0,
		                'filter' => 0,
	                ),
                    'text' => array(
                        'caption' => 'Текст',
                        'value' => '',
                        'type' => 'wysiwyg',
                    ),
					'textBottom' => array(
						'caption' => 'Нижний текст',
						'value' => '',
						'type' => 'wysiwyg',
					),
                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'CEO блок',
                        'output' => 'open',
                    ),
                    'title_h1' => array(
                        'caption' => 'Заголовок h1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),
                ),
            ),
            //------------------------------Разделы-------------------------------------------------------

            //------------------------------Товары-------------------------------------------------------
            'items' => array(

                'db_name' => $module_name . '_items',
                'dialog' => array('width' => 650, 'height' => 500),
                'onpage' => 20,
                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`title`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => true,

                'config' => array(
                    'title' => array(
                        'caption' => 'Заголовок',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),
					'image' => array(
						'caption' => 'Картинка',
						'value' => '',
						'type' => 'loader',
						'thumbs' => array(
							'view' => array(343, 285),
							'min' => array(149, 139),
						),
						'settings' => array(
							'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
							'size-limit' => 5 * 1024 * 1024,
							'limit' => 1,
							'module-name' => $module_name
						),
						'in_list' => 1,
						'filter' => 0,
					),
	                'price' => array(
		                'caption' => 'Цена',
		                'value' => '',
		                'type' => 'string',
	                ),
	                'own' => array(
		                'caption' => 'Собственное производство',
		                'value' => 1,
		                'type' => 'checkbox',
		                'in_list' => 0,
	                ),
	                array(
		                'type' => 'divider',
	                ),
	                array(
		                'type' => 'section',
		                'caption' => 'Завод изготовитель',
		                'output' => 'open',
	                ),
	                'fabric' => array(
		                'caption' => 'Название',
		                'value' => '',
		                'type' => 'string',
		                'in_list' => 0,
		                'filter' => 0,
	                ),
	                'fabricIcon' => array(
		                'caption' => 'Иконка',
		                'value' => '',
		                'type' => 'loader',
		                'thumbs' => array(
			                'view' => array(0, 0)
		                ),
		                'settings' => array(
			                'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
			                'size-limit' => 5 * 1024 * 1024,
			                'limit' => 1,
			                'module-name' => $module_name
		                ),
		                'in_list' => 0,
		                'filter' => 0,
	                ),
	                array(
		                'type' => 'section',
		                'output' => 'close',
	                ),
	                array(
		                'type' => 'divider',
	                ),
                    'text' => array(
                        'caption' => 'Описание',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),

	                array(
		                'type' => 'divider',
	                ),
	                array(
		                'type' => 'section',
		                'caption' => 'Связи каталогов',
		                'output' => 'open',
	                ),
	                'spare' => array(
		                'caption' => 'Запчасти',
		                'value' => '',
		                'empty' => true,
		                'from' => array(
			                'table_name' => PRFX . 'spare_articles',
			                'key_field' => 'id',
			                'name_field' => 'name',
			                'where' => 'visible = 1 and level =2 AND CONCAT(",",parents,",") LIKE "%,2,%"',
		                ),
		                'type' => 'select',
		                'in_list' => 0,
	                ),
	                array(
		                'type' => 'section',
		                'output' => 'close',
	                ),
	                array(
		                'type' => 'divider',
	                ),

                    'active' => array(
                        'caption' => 'Опубликовано',
                        'value' => 1,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 1,
                    ),
                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'CEO блок',
                        'output' => 'open',
                    ),
                    'title_h1' => array(
                        'caption' => 'Заголовок h1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),
                ),
            ),
            //------------------------------Товары-------------------------------------------------------
        )
    );

    return $CONFIG;