<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'delivery_prices';
    $module_caption = 'Доставка в регионы';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(
            'items' => array(
                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`title` ASC',
                'onpage' => 20,
                'config' => array(
                    'title' => array(
                        'caption' => 'Регион',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'price' => array(
                        'caption' => 'Мин. стоимость',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'time' => array(
                        'caption' => 'Срок',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;