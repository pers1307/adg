<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'descriptions';
    $module_caption = 'Описание (descriptions)';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(

                    'url' => array(
                        'caption' => 'Маска URLа (% - любая замена)',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'type_insert' => array(
                        'caption' => 'Место вставки',
                        'type' => 'select',
                        'values' => array(
                            0 => 'вместо',
                            1 => 'до оригинала',
                            2 => 'после оригинала',
                            3 => 'до результата',
                            4 => 'после результата',
                            5 => 'вместо (если не указано значение)',
                        ),
                        'in_list' => 1,

                    ),
                    'text' => array(
                        'caption' => 'Текст',
                        'value' => '',
                        'height' => 70,
                        'type' => 'memo',
                        'filter' => 0,
                        'in_list' => 1,
                    ),
                    'sort' => array(
                        'caption' => 'Сила',
                        'type' => 'string',
                        'value' => 0,
                        'in_list' => 1,
                    ),
                    'active' => array(
                        'caption' => 'Включено',
                        'type' => 'boolean',
                        'value' => 1,
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                ),
            ),
        ),
    );

    return $CONFIG;