<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'download_files';
    $module_caption = 'Файлы для скачивания';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(
            'items' => array(
                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'id' => array(
                        'caption' => 'ID',
                        'value' => '',
                        'type' => 'static',
                        'in_list' => 1,
                    ),
                    'title' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'file' => array(
                        'caption' => 'Файл',
                        'value' => '',
                        'type' => 'loader',
                        'settings' => array(
                            'allowed-extensions' => 'pdf, doc, docx, xls, xlsx, zip, rar, gz, txt, trf',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                    ),
                    /*'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    )*/
                ),
            ),
        ),

        'settings' => array(
            'questionnaireId' => array(
                'caption' => 'ID файла опросный лист',
                'value' => 1,
                'type' => 'string'
            ),
            'presentationId' => array(
                'caption' => 'ID файла презентация',
                'value' => 1,
                'type' => 'string'
            ),
        ),
    );

    return $CONFIG;