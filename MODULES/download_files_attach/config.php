<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'download_files_attach';
    $module_caption = 'Список документов';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(
            'items' => array(
                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'attach_file' => array(
                        'caption' => 'Файл',
                        'value' => '',
                        'type' => 'select',
                        'in_list' => 1,
                        'from' => array(
                            'table_name' => PRFX . 'download_files',
                            'key_field' => 'id',
                            'name_field' => 'title',
                            'order' => 'title',
                        ),
                    ),
                ),
            ),
        ),
    );

    return $CONFIG;