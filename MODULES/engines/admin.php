<?php

$Catalog = new MSCatalogControl();

header('Content-type: text/html; charset=utf-8');
global $CONFIG;
ob_clean();

$Catalog->loadConfig();

$table_articles = $CONFIG['tables']['articles']['db_name'];
$table_items = $CONFIG['tables']['items']['db_name'];

$output = MSModuleController::blockOnPage($CONFIG['module_name']);
$output_id = ($output > 0 ? 'fast_table_' . $CONFIG['module_name'] : 'center');

//list($articles_isorder,$CONFIG['tables']['articles'])=moduleOrderField($CONFIG['tables']['articles']);
$articles_isorder = true;

//list($items_isorder,$CONFIG['tables']['items'])=moduleOrderField($CONFIG['tables']['items']);

$Catalog->checkModuleIntegrity();

switch (MSCore::urls()->vars[1])
{
    case 'config':
        ob_clean();
        $config = MSCore::modules()->by_dir(MSCore::urls()->vars[0]);

        $config['config'] = array();
        foreach (MSCore::page()->allZones as $_zone)
        {
            $config['config']['mod_' . $_zone['value']] = array(
                'caption' => $_zone['value'],
                'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
                'module' => MSCore::urls()->vars[0],
                'zone' => $_zone['value'],
                'type' => 'explorer',
            );
        }

        $vars['_FORM_'] = MSCore::forms()->make($config['config']);
        $vars['mod'] = MSCore::urls()->vars[0];

        die (template('module_config', $vars));
        break;

    case 'fastview':
        $path_id = isset(MSCore::urls()->vars[2]) ? MSCore::urls()->vars[2] : 0;
        $sel_page = isset(MSCore::urls()->vars[3]) ? MSCore::urls()->vars[3] : 0;

        $articles_vars = $Catalog->articles_generateVars();

        $vars = array();
        $vars['CONFIG'] = $CONFIG;
        $vars['path_id'] = $path_id;
        $vars['page'] = 1;
        $vars['articles'] = template('moduleCatalog/articles', $articles_vars);

        $_RESULT = array('content' => template('moduleCatalog/fast', $vars));
        //debug($_RESULT);
        die(json_encode($_RESULT));
        break;


    case 'swap_article':
        $path_id = isset(MSCore::urls()->vars[2]) ? MSCore::urls()->vars[2] : 0;
        $action = (isset(MSCore::urls()->vars[3]) && MSCore::urls()->vars[3] == 'up') ? 1 : 0;
        $article_id = isset(MSCore::urls()->vars[4]) ? MSCore::urls()->vars[4] : 0;

        $Catalog->setSwapItemsOrder($CONFIG['tables']['articles']['db_name'], $article_id, $action, true, 'article');

        $articles_vars = $Catalog->articles_generateVars();

        $_RESULT = array('content' => template('moduleCatalog/articles', $articles_vars));
        die(json_encode($_RESULT));
        break;

    case 'toggle_vis_article':
        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
        $article_id = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;

        $status = MSCore::db()->getOne('SELECT `visible` FROM ' . PRFX . $CONFIG['tables']['articles']['db_name'] . ' WHERE id=' . (int)$article_id);
        $status = (intval($status) == 0 ? 1 : 0);

        MSNestedSets::updateNodeVisibility($article_id, $status, $CONFIG['tables']['articles']['db_name']);

        $articles_vars = $Catalog->articles_generateVars();
        $_RESULT = array('content' => template('moduleCatalog/articles', $articles_vars));
        die(json_encode($_RESULT));
        break;

    case 'add_article':
        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
        $article_id = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;
        $parent = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;
        $level = (isset(MSCore::urls()->vars[5])) ? (int)MSCore::urls()->vars[5] : 0;

        $CONFIG = $Catalog->articles_generateConfigValues($article_id, $parent, $path_id);

        if (isset($_REQUEST['conf']))
        {

            //защита от дублирования поля code {start}
            if(!empty($_REQUEST['conf'][1]['code'])) {
                $id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : 0;
                $tableName = $CONFIG['tables']['articles']['db_name'];
                $codeItem = $_REQUEST['conf'][1]['code'];
                if (MSCore::db()->getOne('SELECT `id` FROM `' . PRFX . $tableName . '` WHERE' . ($id != 0 ? ' `id`!=' . $id . ' AND' : '') . ' `parent` = "' . (int)($parent) . '" AND `path_id` = "' . getInt($path_id) . '" AND `code` = "' . MSCore::db()->pre($codeItem) . '"')) {
                    $_REQUEST['conf'][1]['code'] = $codeItem . '-' . date('dHis');
                }
            }
            //защита от дублирования поля code {end}


            if ($inserted_id = $Catalog->articles_saveItem(false))
            {
                $articles_vars = $Catalog->articles_generateVars();

                $articles_vars['apply'] = isset(MSCore::urls()->vars[5]) && MSCore::urls()->vars[5] > 0 ? 1 : 0;

                $inserted_id = '<input id="inserted_id" type="hidden" value="' . $inserted_id . '" name="id"/>';

                $_RESULT = array('content' => array(template('moduleCatalog/articles', $articles_vars), $inserted_id));
                die(json_encode($_RESULT));
            }
            else
            {
                echo '<i style="display:none">Fatal error: </i>Введенный "Символьный код" уже занят';

                /**
                 * TODO: Сейчас на все ошибки одна причина, исправить :)
                 */
            }
        }
        else
        {
            // Удаление полей, скрытых на данном уровне
            foreach($CONFIG['tables']['articles']['config'] as $k => $v){
                if(isset($v['level']) && !in_array($level, $v['level'])) {
                    unset($CONFIG['tables']['articles']['config'][$k]);
                }
            }
            $vars['_FORM_'] = MSCore::forms()->make($CONFIG['tables']['articles']['config']);
            $vars['CONFIG'] = $CONFIG;

            $vars['id'] = (int)$article_id;
            $vars['path_id'] = (int)$path_id;
            $vars['output_id'] = $output_id;

            echo template('moduleCatalog/add_article', $vars);
        }
        break;

    case 'del_article':

        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
        $parent = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;

        if ($parent != 0)
        {
            $Catalog->deleteArticle($parent);
        }

        $articles_vars = $Catalog->articles_generateVars();

        $_RESULT = array('content' => template('moduleCatalog/articles', $articles_vars));
        die(json_encode($_RESULT));
        break;

####################################################################################################################################################
####################################################################################################################################################
########################################							ИТЕМЫ						####################################################
####################################################################################################################################################
####################################################################################################################################################

    case 'filter':
        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
        $sel_page = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;
        $parent = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;

        $sel_page = !isset($sel_page) ? 1 : (int)$sel_page;
        if(!empty($_REQUEST['filters']) && is_array($_REQUEST['filters'])) {
            $_REQUEST['filters'] = array_map('trim', $_REQUEST['filters']);
        }
        $_SESSION['filters'][$CONFIG['tables']['items']['db_name']] = serialize($_REQUEST['filters']);

        $items_vars = $Catalog->showItems($CONFIG, $parent, $sel_page);

        $_RESULT = array('content' => template('moduleCatalog/items', $items_vars));
        die(json_encode($_RESULT));
        break;


    case 'clear_filter':

        $Catalog->clearFilters($CONFIG['tables']['items']['db_name']);

    case 'show_items':
        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
        $sel_page = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;
        $parent = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;

        $sel_page = !isset($sel_page) ? 1 : (int)$sel_page;

        $_SESSION['moduleCatalogActiveArticle'] = $parent;

        $items_vars = $Catalog->showItems($CONFIG, $parent, $sel_page);

        $_RESULT = array('content' => template('moduleCatalog/items', $items_vars));
        die(json_encode($_RESULT));
        break;

    case 'swap_item':
        $action = (isset(MSCore::urls()->vars[2]) && MSCore::urls()->vars[2] == 'up') ? 1 : 0;
        $item_id = isset(MSCore::urls()->vars[3]) ? MSCore::urls()->vars[3] : 0;
        $sel_page = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;
        $parent = (isset(MSCore::urls()->vars[5])) ? (int)MSCore::urls()->vars[5] : 0;
        $path_id = (isset(MSCore::urls()->vars[6])) ? (int)MSCore::urls()->vars[6] : 0;

        $Catalog->setSwapItemsOrder($CONFIG['tables']['items']['db_name'], $item_id, $action, false);

        $items_vars = $Catalog->showItems($CONFIG, $parent, $sel_page);

        $_RESULT = array('content' => template('moduleCatalog/items', $items_vars));
        die(json_encode($_RESULT));
        break;

    case 'add_item':
        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
        $parent = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;
        $sel_page = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;
        $new = (isset(MSCore::urls()->vars[5])) ? (int)MSCore::urls()->vars[5] : 0;

        $order = isset($CONFIG['tables']['items']['order_field']) ? $CONFIG['tables']['items']['order_field'] : '`order`';

        if ($new > 0)
        {
            $values = [];
            $list = MSCore::db()->getRow("SELECT * FROM `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` WHERE `id`=" . $new);
            foreach ($CONFIG['tables']['items']['config'] as $k => $v)
            {
                if ($k != 'order' && !is_numeric($k))
                {
                    $CONFIG['tables']['items']['config'][$k]['value'] = $list[$k];
                }

                if (isset($v['in'])) {
                    $values[$v['in']][$k] = $v;
                }
            }

            foreach ($values as $k => $v) {
                if (isset($list[$k]) && !is_array($list[$k])) {
                    $list[$k] = json_decode($list[$k], true);
                }

                foreach ($v as $key => $val) {
                    if (isset($list[$k][$val['name']])) {
                        $CONFIG['tables']['items']['config'][$key]['value'] = $list[$k][$val['name']];
                    }
                }

            }
        }

        $CONFIG['tables']['items']['config']['parent']['caption'] = '';
        $CONFIG['tables']['items']['config']['parent']['value'] = $parent;
        $CONFIG['tables']['items']['config']['parent']['type'] = 'hidden';


        if (isset($_REQUEST['conf']))
        {

            //защита от дублирования поля code {start}
            if(!empty($_REQUEST['conf'][1]['code'])) {
                $id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : 0;
                $tableName = $CONFIG['tables']['items']['db_name'];
                $codeItem = $_REQUEST['conf'][1]['code'];
                if (MSCore::db()->getOne('SELECT `id` FROM `' . PRFX . $tableName . '` WHERE' . ($id != 0 ? ' `id`!=' . $id . ' AND' : '') . ' `parent` = "' . (int)($parent) . '" AND `path_id` = "' . getInt($path_id) . '" AND `code` = "' . MSCore::db()->pre($codeItem) . '"')) {
                    $_REQUEST['conf'][1]['code'] = $codeItem . '-' . date('dHis');
                }
            }
            //защита от дублирования поля code {end}

            $save_CONFIG['items']['config'] = MSCore::forms()->save($CONFIG['tables']['items']['config']);

            $sql_ = array("`path_id` = '" . $path_id . "'");

            $properties = [];
            foreach ($save_CONFIG['items']['config'] as $field => $info)
            {

                if (isset($info['in'])) {
                    $properties[$info['in']][$info['name']] = $info['value'];
                    if (empty($info['value'])) {
                        unset($properties[$info['in']][$info['name']]);
                    }
                }

                if (is_numeric($field)) {
                    continue;
                }

                if (isset($info['value']))
                {

                    if (!empty($info['type']) && $info['type'] == 'code' && empty($info['value']))
                    {

                        $info['value'] = "NULL";

                    }
                    else
                    {

                        $info['value'] = "'" . MSCore::db()->pre($info['value']) . "'";

                    }

                    $sql_[] = "`" . $field . "` = " . $info['value'];

                }

            }

            if ($_REQUEST['id'] > 0)
            {
                $sql = "UPDATE `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` SET " . implode(', ', $sql_) . " WHERE `id` = " . (int)$_REQUEST['id'];
            }
            else
            {

                if (!isset($CONFIG['tables']['items']['order_field']))
                {

                    //-----------уккажем сортировку в зависимости от того что указано в конфиге
                    if ($CONFIG['tables']['items']['add_new_on_top'])
                    {
                        $order = MSCore::db()->GetOne("SELECT MIN(`order`) FROM `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` WHERE `parent`=" . (int)$CONFIG['tables']['items']['config']['parent']['value']);
                        $sql_[] = "`order`=" . ((int)$order - 1);
                    }
                    else
                    {
                        $order = MSCore::db()->GetOne("SELECT MAX(`order`) FROM `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` WHERE `parent`=" . (int)$CONFIG['tables']['items']['config']['parent']['value']);
                        $sql_[] = "`order`=" . ((int)$order + 1);
                    }
                }

                $sql = "INSERT INTO `" . PRFX . $CONFIG['tables']['items']['db_name'] . "` SET " . implode(', ', $sql_);
            }

            if (MSCore::db()->execute($sql, false))
            {

                $inserted_id = ($_REQUEST['id']) ? intval($_REQUEST['id']) : MSCore::db()->id;
                MSNestedSets::updateNodeMaterializedPath($parent, $inserted_id, $table_articles, $table_items, true);

                $items_vars = $Catalog->showItems($CONFIG, $parent, $sel_page);
                $items_vars['apply'] = (isset(MSCore::urls()->vars[6])) ? (int)MSCore::urls()->vars[6] : 0;

                $inserted_id = '<input id="inserted_id" type="hidden" value="' . $inserted_id . '" name="id"/>';

                $_RESULT = array('content' => array(template('moduleCatalog/items', $items_vars), $inserted_id));
                die(json_encode($_RESULT));

            }
            else
            {

                echo '<i style="display:none">Fatal error: </i>Введенный "Символьный код" уже занят';

            }

        }
        else
        {
            $vars['CONFIG'] = $CONFIG;
            $vars['_FORM_'] = MSCore::forms()->make($CONFIG['tables']['items']['config']);
            $vars['id'] = (int)$new;
            $vars['path_id'] = $path_id;
            $vars['parent'] = $parent;
            $vars['page'] = $sel_page;

            $vars['output'] = $output;

            echo template('moduleCatalog/add_item', $vars);
        }
        break;

    case 'delete_item':
        $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
        $art_id = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;
        $sel_page = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;
        $item_id = (isset(MSCore::urls()->vars[5])) ? (int)MSCore::urls()->vars[5] : 0;

        $Catalog->deleteAbstractItem('items', $item_id);

        /* CREATE VIEW */
        $filter = $Catalog->generateFilterWhereSQL();

        $items_vars = $Catalog->showItems($CONFIG, $art_id, $sel_page);
        $_RESULT = array('content' => template('moduleCatalog/items', $items_vars));
        die(json_encode($_RESULT));
        break;
}

die();