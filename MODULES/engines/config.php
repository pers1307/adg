<?php
    /*
    родительский элемент и у разделов и у элементов называется parent
    управление видимостью - поле visible (только у разделов)
    для элементов управление видимостью можно сделать отдельным полем
    */

    $module_name = 'engines';
    $module_caption = 'Дизельные двигатели';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '2.0.0.0',

        'tables' => array(
            /*
                Названия таблиц "articles" и "items" обязательны, эти имена используются в различных функциях
            */

            //------------------------------Разделы-------------------------------------------------------
            'articles' => array(

                'db_name' => $module_name . '_articles',
                'dialog' => array('width' => 650, 'height' => 400),
                'onpage' => 20,

                //-------если поставить true то нельзя будет добавлять разделы первого уровня
                'noAddArticles' => false,

                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`name`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => false,

                //------масимальное количество уровней вложенности----------
                'max_levels' => 1,

                //------поле, которое будет выводиться в списке
                'title_field' => 'name',
                'is_order' => true, // Ручная сортировка

                'config' => array(
                    'name' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 1,
                        'value' => '',
                    ),
	                'image' => array(
		                'caption' => 'Логотип двигателя',
		                'value' => '',
		                'type' => 'loader',
		                'thumbs' => array(
			                'min' => array(200, 200, true),
			                'list' => array(200, 80, true),
		                ),
		                'settings' => array(
			                'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
			                'size-limit' => 5 * 1024 * 1024,
			                'limit' => 1,
			                'module-name' => $module_name
		                ),
		                'in_list' => 0,
	                ),
                    'text' => array(
                        'caption' => 'Текст',
                        'value' => '',
                        'type' => 'wysiwyg',
                    ),
					'textBottom' => array(
						'caption' => 'Нижний текст',
						'value' => '',
						'type' => 'wysiwyg',
					),
                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'CEO блок',
                        'output' => 'open',
                    ),
                    'title_h1' => array(
                        'caption' => 'Заголовок h1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),
                ),
            ),
            //------------------------------Разделы-------------------------------------------------------

            //------------------------------Товары-------------------------------------------------------
            'items' => array(

                'db_name' => $module_name . '_items',
                'dialog' => array('width' => 650, 'height' => 500),
                'onpage' => 20,
                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`title`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => true,

                'config' => array(
                    'title' => array(
                        'caption' => 'Заголовок',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),
					'image' => array(
						'caption' => 'Картинка',
						'value' => '',
						'type' => 'loader',
						'thumbs' => array(
							'preview' => array(180, 180),
							'view' => array(300, 300),
							'big' => array(1200, 1200),
						),
						'settings' => array(
							'allowed-extensions' => 'jpg,jpeg,bmp,gif,png',
							'size-limit' => 5 * 1024 * 1024,
							'limit' => 3,
							'module-name' => $module_name
						),
						'in_list' => 1,
						'filter' => 0,
					),
                    'text' => array(
                        'caption' => 'Описание',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Опубликовано',
                        'value' => 1,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 1,
                    ),

                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'Характеристики',
                        'output' => 'open',
                        'toggle' => 'hide',
                    ),

                    [
                        'name' => 'manufacturer',
                        'caption' => 'Производитель',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'type',
                        'caption' => 'Тип',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    'mainPowerKw' => [
                        'caption' => 'Основная мощность, кВт',
                        'value' => '',
                        'type' => 'string',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'reservedPowerKw',
                        'caption' => 'Резервная мощность, кВт',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'fuelType',
                        'caption' => 'Тип топлива',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'construction',
                        'caption' => 'Конструкция',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'cylindersCount',
                        'caption' => 'Число цилиндров',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'volumeW',
                        'caption' => 'Рабочий объем, л',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'cylindersOperation',
                        'caption' => 'Порядок работы цилиндров',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'boost',
                        'caption' => 'Наддув',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'compression',
                        'caption' => 'Степень сжатия',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'cylinderDiameter',
                        'caption' => 'Диаметр цилиндра, мм',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'motion',
                        'caption' => 'Ход поршня, мм',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'rotationFreq',
                        'caption' => 'Частота вращения коленчатого вала, об/мин',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'speedRegulator',
                        'caption' => 'Регулятор оборотов',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'volumeLub',
                        'caption' => 'Объем системы смазки, л',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'coolSystem',
                        'caption' => 'Система охлаждения',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'volumeCool',
                        'caption' => 'Объем системы охлаждения, л',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'fuel50',
                        'caption' => 'Расход топлива при 50% нагрузке, л/ч',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'fuel75',
                        'caption' => 'Расход топлива при 75% нагрузке, л/ч',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'fuel100',
                        'caption' => 'Расход топлива при 100% нагрузке, л/ч',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'volt',
                        'caption' => 'Напряжение в системе электрооборудования',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'start',
                        'caption' => 'Система запуска',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'totalWorkTime',
                        'caption' => 'Рабочий ресурс двигателя, моточасов',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'dimensions',
                        'caption' => 'Габаритные размеры (ДхШхВ), мм',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],
                    [
                        'name' => 'weight',
                        'caption' => 'Масса двигателя, кг',
                        'value' => '',
                        'type' => 'string',
                        'in' => 'properties',
                        'group' => 'characteristics',
                    ],

                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),

                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'CEO блок',
                        'output' => 'open',
                    ),
                    'title_h1' => array(
                        'caption' => 'Заголовок h1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),
                ),
            ),
            //------------------------------Товары-------------------------------------------------------
        ),

        'settings' => array(
            'onPage' => array(
                'caption' => 'Количество позиций на странице',
                'value' => 10,
                'type' => 'string'
            ),
            'menuTitle' => array(
                'caption' => 'Заголовк меню',
                'value' => '',
                'type' => 'string'
            ),
            array(
                'type' => 'divider',
            ),
            array(
                'type' => 'section',
                'caption' => 'Шаблонные параметры для страницы запчастей',
                'output' => 'open',
            ),
            'parts_title_h1' => array(
                'caption' => 'Заголовок h1',
                'value' => 'template %name%',
                'type' => 'string',
                'help' => 'Для вставки названия двигателя используйте %name%',
            ),
            'parts_title_page' => array(
                'caption' => 'Page title',
                'value' => 'template %name%',
                'type' => 'string',
                'help' => 'Для вставки названия двигателя используйте %name%',
            ),
            'parts_meta_description' => array(
                'caption' => 'Meta description',
                'value' => 'template %name%',
                'type' => 'string',
                'help' => 'Для вставки названия двигателя используйте %name%',
            ),
            'parts_meta_keywords' => array(
                'caption' => 'Meta keywords',
                'value' => 'template %name%',
                'type' => 'string',
                'help' => 'Для вставки названия двигателя используйте %name%',
            ),
            array(
                'type' => 'section',
                'output' => 'close',
            ),
            array(
                'type' => 'divider',
            ),
        ),
    );

    return $CONFIG;