<?php

    $CONFIG = MSCore::modules()->getModuleConfig($module['module_name']);

    MSCore::db()->moduleType = 'catalog';

    if (isset($CONFIG['tables']['articles']))
    {
        $tableName = PRFX . $CONFIG['tables']['articles']['db_name'];

        MSCore::db()->afterSqls[] = "ALTER TABLE {$tableName}
            ADD INDEX `parent` USING BTREE (`parent`, `path_id`),
            ADD INDEX `lft` USING BTREE (`lft`) ,
            ADD INDEX `rgt` USING BTREE (`rgt`) ,
            ADD INDEX `lft_2` USING BTREE (`lft`, `rgt`) ,
            ADD INDEX `visible` USING BTREE (`path_id`, `visible`, `parents_visible`, `lft`, `rgt`) ,
            ADD INDEX `path_id` USING BTREE (`path_id`, `parent`, `order`);";

        $additionalFields = array(
            '`visible`  tinyint(1) NOT NULL DEFAULT 0',
            '`parents_visible`  tinyint(1) NULL DEFAULT NULL',
            '`lft`  int(11) NULL DEFAULT NULL',
            '`rgt`  int(11) NULL DEFAULT NULL',
            '`level`  int(11) NULL DEFAULT NULL',
            '`parents`  varchar(255) NULL DEFAULT NULL',
        );

        MSCore::db()->createModuleTable($CONFIG['tables']['articles'], $additionalFields);
    }


    if (isset($CONFIG['tables']['items']))
    {
        $additionalFields = array(
            '`parents`  varchar(255) NULL DEFAULT NULL',
        );
        MSCore::db()->createModuleTable($CONFIG['tables']['items'], $additionalFields);
    }

    unset($CONFIG);
    unset($module_name);