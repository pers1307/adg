<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'galleries';
    $module_caption = 'Галереи';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(
            'items' => array(
                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'id' => array(
                        'caption' => 'ID',
                        'value' => '',
                        'type' => 'static',
                        'in_list' => 1,
                        'filter' => 1,
                    ),
                    'title' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'images' => array(
                        'caption' => 'Изображения',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'view' => array(200, 200),
                            'big' => array(1200, 1200),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 15,
                            'module-name' => $module_name
                        ),
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;