<?php
/**
ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
это обычный модуль спискового типа, т.е. без разделов.
 */

$module_name = 'industry';
$module_caption = 'Отраслевые решения';

$CONFIG = array(

    'module_name' => $module_name,
    'module_caption' => $module_caption,
    'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
    'version' => '1.1.0.0',
    'tables' => array(
        'items' => array(
            'db_name' => $module_name,
            'dialog' => array('width' => 660, 'height' => 410),
            'key_field' => 'id',
            'order_field' => '`order` ASC',
            'onpage' => 20,
            'config' => array(
                'title' => array(
                    'caption' => 'Заголовок',
                    'value' => '',
                    'type' => 'string',
                    'in_list' => 1,
                    'filter' => 1,
                    'to_code' => true,
                ),
                'code' => array(
                    'type' => 'code',
                    'caption' => 'Символьный код',
                    'in_list' => 0,
                    'value' => '',
                ),
                'icon' => array(
                    'caption' => 'Иконка',
                    'value' => '',
                    'type' => 'loader',
                    'thumbs' => array(
                        'origin' => array(0, 0),
                    ),
                    'settings' => array(
                        'allowed-extensions' => 'jpg,jpeg,png',
                        'size-limit' => 5 * 1024 * 1024,
                        'limit' => 1,
                        'module-name' => $module_name
                    ),
                    'in_list' => 1,
                ),
                'text' => array(
                    'caption' => 'Текст',
                    'value' => '',
                    'height' => 260,
                    'type' => 'wysiwyg',
                    'in_list' => 0,
                ),
                'text_bottom' => array(
                    'caption' => 'Нижний текст',
                    'value' => '',
                    'height' => 260,
                    'type' => 'wysiwyg',
                    'in_list' => 0,
                ),
                'list' => array(
                    'caption' => 'Список генераторов',
                    'value' => '',
                    'from' => array(
                        'table_name' => PRFX . 'power_stations_articles',
                        'key_field' => 'id',
                        'name_field' => 'name',
                        'where' => 'level=1',
                        'order' => '`parent`, `order`',
                    ),
                    'type' => 'group_checkbox',
                    'in_list' => 0,
                ),

                'active' => array(
                    'caption' => 'Активность',
                    'value' => '1',
                    'type' => 'checkbox',
                    'in_list' => 1,
                ),

                array(
                    'type' => 'divider',
                ),
                array(
                    'type' => 'section',
                    'caption' => 'CEO блок',
                    'output' => 'open',
                ),
                'title_h1' => array(
                    'caption' => 'Заголовок h1',
                    'value' => '',
                    'type' => 'string',
                ),
                'title_page' => array(
                    'caption' => 'Page title',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_description' => array(
                    'caption' => 'Meta description',
                    'value' => '',
                    'type' => 'string',
                ),
                'meta_keywords' => array(
                    'caption' => 'Meta keywords',
                    'value' => '',
                    'type' => 'string',
                ),
                array(
                    'type' => 'section',
                    'output' => 'close',
                ),
                array(
                    'type' => 'divider',
                ),
            ),
        ),
    ),

    /*'settings' => array(
        'onPage' => array(
            'caption' => 'Количество позиций на странице',
            'value' => 10,
            'type' => 'string'
        ),
    ),*/
);

return $CONFIG;