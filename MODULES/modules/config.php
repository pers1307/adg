<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'modules';
    $module_caption = 'Управление модулями';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '6.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'module_id',
                'order_field' => '`title`',
                'onpage' => 20,
                'config' => array(

                    'module_caption' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                    ),

                    'type' => array(
                        'caption' => 'Тип модуля',
                        'value' => '',
                        'values' => array(
                            0 => 'Нет значения'
                        ),
                        'from' => array(
                            'table_name' => PRFX . 'modules_types',
                            'key_field' => 'id',
                            'name_field' => 'title',
                            'order' => '`order`',
                            'where' => '',
                        ),
                        'type' => 'select',
                        'in_list' => 1,
                        'filter' => 1,
                    ),

                    'version' => array(
                        'caption' => 'Версия',
                        'value' => '',
                        'type' => 'static',
                        'in_list' => 1,
                        'filter' => 1,
                    ),
                ),
            ),
        ),
    );

    return $CONFIG;