<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'optional_eq_prices';
    $module_caption = 'Доп. оборудование (цены)';

    //Equipment values
    $OEModel = new OptionalEquipmentModel([
            'pathId' => OPTIONAL_EQUIPMENT,
            'onlyHaveItems' => true,
            'itemsOrder' => '`order` ASC',
        ]
    );
    $values = $allValues = [];
    foreach ($OEModel->fullTree as $art) {
        $OEModel->_currentArticle = $art;
        $data = $OEModel->getItems(['id', 'title', 'parent', 'code'], false);
        foreach ($data['items'] as $item) {
            $values[$art['name']][$item['id']] = $allValues[$item['id']] = $item['title'];
        }
    }

    unset($OEModel, $data);

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'show' => false,
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'title' => array(
                        'caption' => 'Оборудование',
                        'value' => '',
                        'type' => 'select',
                        'in_list' => 1,
                        'values' => $values,
                        'allValues' => $allValues,
                    ),
                    /*'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),*/
                    /*'text' => array(
                        'caption' => 'Подробное описание',
                        'value' => '',
                        'height' => 260,
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),*/

                    'individual' => array(
                        'caption' => 'Рассчитывается индивидуально',
                        'value' => '',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    )
                ),
            ),
        ),
    );

$CONFIG['tables']['items']['config'] =
    array_slice($CONFIG['tables']['items']['config'], 0, 1) +
    CurrencyHelper::currenciesConfig() +
    array_slice($CONFIG['tables']['items']['config'], 1, null);

return $CONFIG;