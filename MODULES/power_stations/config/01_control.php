<?php

return [
    [
        'type' => 'divider',
        'level' => [1],
    ],
    [
        'type' => 'section',
        'caption' => 'Система управления',
        'output' => 'open',
        'toggle' => 'hide',
        'level' => [1],
    ],

    [
        'name' => 'controller',
        'caption' => 'Пульт управления',
        'value' => '',
        'type' => 'string',
        'in' => 'control',
        'level' => [1],
    ],
    [
        'name' => 'parallel',
        'caption' => 'Параллельная работа',
        'value' => '',
        'type' => 'string',
        'in' => 'control',
        'level' => [1],
    ],
    [
        'name' => 'remote',
        'caption' => 'Удаленный мониторинг и управление',
        'value' => '',
        'type' => 'string',
        'in' => 'control',
        'level' => [1],
    ],
    [
        'name' => 'automatic',
        'caption' => 'Автоматический ввод резерва (АВР)',
        'value' => '',
        'type' => 'string',
        'in' => 'control',
        'level' => [1],
    ],
    [
        'name' => 'powerIntegration',
        'caption' => 'Интеграция с источником бесперебойного питания (ИБП)',
        'value' => '',
        'type' => 'string',
        'in' => 'control',
        'level' => [1],
    ],
    [
        'name' => 'neutral',
        'caption' => 'Работа в сети с «глухозаземлённой» / «изолированной» нейтралью',
        'value' => '',
        'type' => 'string',
        'in' => 'control',
        'level' => [1],
    ],

    [
        'type' => 'section',
        'output' => 'close',
        'level' => [1],
    ],
    [
        'type' => 'divider',
        'level' => [1],
    ],
];