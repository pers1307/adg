<?php

return [
    [
        'type' => 'divider',
        'level' => [1],
    ],
    [
        'type' => 'section',
        'caption' => 'Система электрооборудования',
        'output' => 'open',
        'toggle' => 'hide',
        'level' => [1],
    ],

    [
        'name' => 'voltage',
        'caption' => 'Напряжение в системе электрооборудования',
        'value' => '',
        'type' => 'string',
        'in' => 'electrical',
        'level' => [1],
    ],

    [
        'type' => 'section',
        'output' => 'close',
        'level' => [1],
    ],
    [
        'type' => 'divider',
        'level' => [1],
    ],
];