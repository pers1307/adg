<?php

return [
    [
        'type' => 'divider',
        'level' => [1],
    ],
    [
        'type' => 'section',
        'caption' => 'Двигатель',
        'output' => 'open',
        'toggle' => 'hide',
        'level' => [1],
    ],
    'engine' => [
        'caption' => 'Двигатель',
        'value' => '',
        'from' => [
            'table_name' => PRFX . 'engines_items',
            'key_field' => 'id',
            'name_field' => 'title',
            'order' => 'title',
        ],
        'type' => 'select',
        'empty' => true,
        'in_list' => 0,
        'filter' => 0,
        'level' => [1],
    ],
    [
        'type' => 'section',
        'output' => 'close',
        'level' => [1],
    ],
    [
        'type' => 'divider',
        'level' => [1],
    ],
];