<?php

return [
    [
        'type' => 'divider',
        'level' => [1],
    ],
    [
        'type' => 'section',
        'caption' => 'Генератор',
        'output' => 'open',
        'toggle' => 'hide',
        'level' => [1],
    ],

    [
        'name' => 'model',
        'caption' => 'Модель генератора',
        'value' => '',
        'type' => 'string',
        'in' => 'generator',
        'level' => [1],
    ],
    [
        'name' => 'type',
        'caption' => 'Тип генератора',
        'value' => '',
        'type' => 'string',
        'in' => 'generator',
        'level' => [1],
    ],
    [
        'name' => 'voltage',
        'caption' => 'Напряжение генератора, В',
        'value' => '',
        'type' => 'string',
        'in' => 'generator',
        'level' => [1],
    ],
    [
        'name' => 'regulator',
        'caption' => 'Регулятор напряжения генератора',
        'value' => '',
        'type' => 'string',
        'in' => 'generator',
        'level' => [1],
    ],
    [
        'name' => 'insulation',
        'caption' => 'Класс изоляции',
        'value' => '',
        'type' => 'string',
        'in' => 'generator',
        'level' => [1],
    ],
    [
        'name' => 'protection',
        'caption' => 'Степень защиты',
        'value' => '',
        'type' => 'string',
        'in' => 'generator',
        'level' => [1],
    ],
    [
        'name' => 'life',
        'caption' => 'Рабочий ресурс генератора, часов',
        'value' => '',
        'type' => 'string',
        'in' => 'generator',
        'level' => [1],
    ],

    [
        'type' => 'section',
        'output' => 'close',
        'level' => [1],
    ],
    [
        'type' => 'divider',
        'level' => [1],
    ],
];