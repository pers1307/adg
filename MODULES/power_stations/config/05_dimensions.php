<?php

return [
    [
        'type' => 'divider',
        'level' => [1],
    ],
    [
        'type' => 'section',
        'caption' => 'Размеры и вес',
        'output' => 'open',
        'toggle' => 'hide',
        'level' => [1],
    ],

    [
        'name' => 'dimensions',
        'caption' => 'Габаритные размеры открытого исполнения (ДхШхВ), мм',
        'value' => '',
        'type' => 'string',
        'in' => 'dimensions',
        'level' => [1],
    ],
    [
        'name' => 'weight',
        'caption' => 'Вес установки, кг',
        'value' => '',
        'type' => 'string',
        'in' => 'dimensions',
        'level' => [1],
    ],
    [
        'name' => 'dimensionsCasing',
        'caption' => 'Габаритные размеры исполнения в звукоизолирующем кожухе (ДхШхВ), мм',
        'value' => '',
        'type' => 'string',
        'in' => 'dimensions',
        'level' => [1],
    ],
    [
        'name' => 'weightFull',
        'caption' => 'Полный вес установки в кожухе, кг',
        'value' => '',
        'type' => 'string',
        'in' => 'dimensions',
        'level' => [1],
    ],

    [
        'type' => 'section',
        'output' => 'close',
        'level' => [1],
    ],
    [
        'type' => 'divider',
        'level' => [1],
    ],
];