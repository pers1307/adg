<?php

return [
    [
        'type' => 'divider',
        'level' => [1],
    ],
    [
        'type' => 'section',
        'caption' => 'Дополнительная информация',
        'output' => 'open',
        'toggle' => 'hide',
        'level' => [1],
    ],

    [
        'name' => 'country',
        'caption' => 'Страна происхождения оборудования',
        'value' => '',
        'type' => 'string',
        'in' => 'information',
        'level' => [1],
    ],
    [
        'name' => 'service',
        'caption' => 'Межсервисный интервал, моточасов',
        'value' => '',
        'type' => 'string',
        'in' => 'information',
        'level' => [1],
    ],
    [
        'name' => 'warranty',
        'caption' => 'Гарантия',
        'value' => '',
        'type' => 'string',
        'in' => 'information',
        'level' => [1],
    ],

    [
        'type' => 'section',
        'output' => 'close',
        'level' => [1],
    ],
    [
        'type' => 'divider',
        'level' => [1],
    ],
];