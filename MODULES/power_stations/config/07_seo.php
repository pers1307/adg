<?php

return [
    array(
        'type' => 'divider',
    ),
    array(
        'type' => 'section',
        'caption' => 'CEO блок',
        'output' => 'open',
    ),
    'title_h1' => array(
        'caption' => 'Заголовок h1',
        'value' => '',
        'type' => 'string',
    ),
    'title_page' => array(
        'caption' => 'Page title',
        'value' => '',
        'type' => 'string',
    ),
    'meta_description' => array(
        'caption' => 'Meta description',
        'value' => '',
        'type' => 'string',
    ),
    'meta_keywords' => array(
        'caption' => 'Meta keywords',
        'value' => '',
        'type' => 'string',
    ),
    array(
        'type' => 'section',
        'output' => 'close',
    ),
    array(
        'type' => 'divider',
    ),
];