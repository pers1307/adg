<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'power_stations_equipment';
    $module_caption = 'Стандартная комплектация';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(
            'items' => array(
                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'title' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'item_id' => array(
                        'caption' => 'Генератор',
                        'from' => array(
                            'table_name' => PRFX . 'power_stations_articles',
                            'key_field' => 'id',
                            'name_field' => 'name',
                            'where' => '`level`=1'
                        ),
                        'type' => 'select',
                        'in_list' => 1,
                    ),
                    'image' => array(
                        'caption' => 'Изображение',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'min' => array(150, 150),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 0,
                    ),
                    'text' => array(
                        'caption' => 'Текст',
                        'value' => '',
                        'type' => 'wysiwyg',
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;