<?php

    $module_name = 'settings';
    $module_caption = 'Настройки';

    return $CONFIG = array(
        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'version' => '6.0',

        'settings' => array(

            array(
                'type' => 'divider',
            ),
            array(
                'type' => 'section',
                'caption' => 'Курсы валют',
                'output' => 'open',
            ),
            'currency_eur' => array(
                'caption' => 'Euro',
                'value' => '',
                'type' => 'float'
            ),
            'currency_usd' => array(
                'caption' => 'USD',
                'value' => '',
                'type' => 'float'
            ),
            'currency_jpy' => array(
                'caption' => 'JPY',
                'value' => '',
                'type' => 'float'
            ),
            array(
                'type' => 'section',
                'output' => 'close',
            ),
            array(
                'type' => 'divider',
            ),

            array(
                'type' => 'divider',
            ),
            array(
                'type' => 'section',
                'caption' => 'Метрика',
                'output' => 'open',
            ),
            'metrika' => array(
                'caption' => 'Код',
                'value' => '',
                'type' => 'metrika'
            ),
            array(
                'type' => 'section',
                'output' => 'close',
            ),
            array(
                'type' => 'divider',
            ),

           /* array(
                'type' => 'divider',
            ),
            array(
                'type' => 'section',
                'caption' => 'Настройки сайта',
                'output' => 'open',
            ),
            'footer_title' => array(
                'caption' => 'Название компании в нижней части сайта',
                'value' => '',
                'type' => 'string'
            ),
            array(
                'type' => 'section',
                'output' => 'close',
            ),
            array(
                'type' => 'divider',
            ),*/
        )
    );