<?php
    /*
    родительский элемент и у разделов и у элементов называется parent
    управление видимостью - поле visible (только у разделов)
    для элементов управление видимостью можно сделать отдельным полем
    */

    $module_name = 'spare_parts';
    $module_caption = 'Запасные части для двигателей';

    $CONFIG = array(
        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '2.0.0.0',

        'tables' => array(
            /*
                Названия таблиц "articles" и "items" обязательны, эти имена используются в различных функциях
            */
            //------------------------------Разделы-------------------------------------------------------
            'articles' => array(

                'db_name' => $module_name . '_articles',
                'dialog' => array('width' => 650, 'height' => 400),
                'onpage' => 20,

                //-------если поставить true то нельзя будет добавлять разделы первого уровня
                'noAddArticles' => false,

                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`name`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => false,

                //------масимальное количество уровней вложенности----------
                'max_levels' => 1,

                //------поле, которое будет выводиться в списке
                'title_field' => 'name',
                'is_order' => true, // Ручная сортировка

                'config' => array(
                    'name' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'to_code' => true,
                    ),
//                    'code' => array(
//                        'type' => 'code',
//                        'caption' => 'Символьный код',
//                        'in_list' => 1,
//                        'value' => '',
//                    ),
                    'active' => array(
                        'caption' => 'Опубликовано',
                        'value' => 1,
                        'type' => 'checkbox',
                        'in_list' => 1,
                        'filter' => 1,
                    ),
                ),
            ),
            //------------------------------Разделы-------------------------------------------------------

            //------------------------------Товары-------------------------------------------------------
            'items' => array(

                'db_name' => $module_name . '_items',
                'dialog' => array('width' => 650, 'height' => 500),
                'onpage' => 20,
                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`title`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => true,

                'config' => array(
                    'title' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => true,
                    ),
//                    'code' => array(
//                        'type' => 'code',
//                        'caption' => 'Символьный код',
//                        'in_list' => 0,
//                        'value' => '',
//                    ),
	                'available' => array(
		                'caption' => 'Наличие',
		                'value' => '',
                        'values' => (new ModelSpareParts())->available,
		                'type' => 'select',
		                'in_list' => 0,
	                ),
                    'active' => array(
                        'caption' => 'Опубликовано',
                        'value' => 1,
                        'type' => 'checkbox',
                        'in_list' => 1,
                        'filter' => 1,
                    ),
                ),
            ),
            //------------------------------Товары-------------------------------------------------------
        )
    );

    return $CONFIG;