<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'spares';
    $module_caption = 'Запчасти';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'title' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'to_code' => true
                    ),
                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),
                    'image' => array(
                        'caption' => 'Картинка',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'preview' => array(310, 300),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                    ),
                    'announce' => array(
                        'caption' => 'Краткое описание',
                        'value' => '',
                        'height' => 260,
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'text' => array(
                        'caption' => 'Подробное описание',
                        'value' => '',
                        'height' => 260,
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    ),

                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'CEO блок',
                        'output' => 'open',
                    ),
                    'title_h1' => array(
                        'caption' => 'Заголовок h1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),
                ),
            ),
        ),

        'settings' => array(
            'onPage' => array(
                'caption' => 'Количество позиций на странице',
                'value' => 10,
                'type' => 'string'
            ),
        ),
    );

    return $CONFIG;