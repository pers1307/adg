<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'staff';
    $module_caption = 'Сотрудники';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'tables' => array(
            'items' => array(
                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'title' => array(
                        'caption' => 'Направление',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'image' => array(
                        'caption' => 'Фотография',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'origin' => array(0, 0),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 0,
                    ),
                    'name' => array(
                        'caption' => 'ФИО',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'post' => array(
                        'caption' => 'Должность',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                    ),
                    'phone' => array(
                        'caption' => 'Телефон',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'phone2' => array(
                        'caption' => 'Доп. телефон',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'email' => array(
                        'caption' => 'Email',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;