<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'tpl';
    $module_caption = 'Шаблоны страниц';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '6.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`id`',
                'onpage' => 20,
                'config' => array(

                    'caption' => array(
                        'caption' => 'Название шаблона',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 0,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;