<?php

    $Catalog = new MSCatalogControl();

    header('Content-type: text/html; charset=utf-8');
    global $CONFIG;
    ob_clean();

    $Catalog->loadConfig();

    $output = MSModuleController::blockOnPage($CONFIG['module_name']);
    $output_id = ($output > 0 ? 'fast_table_' . $CONFIG['module_name'] : 'center');

    list($tree_isorder, $CONFIG['tables']['tree']) = $Catalog->moduleOrderField($CONFIG['tables']['tree']);
//$tree_isorder=true;

    $Catalog->checkModuleIntegrity();

    switch (MSCore::urls()->vars[1])
    {

        case 'config':
            ob_clean();
            $config = MSCore::modules()->by_dir(MSCore::urls()->vars[0]);

            $config['config'] = array();

            foreach (MSCore::page()->allZones as $_zone)
            {
                $config['config']['mod_' . $_zone['value']] = array(
                    'caption' => $_zone['value'],
                    'value' => (isset($config['output'][$_zone['value']]) ? $config['output'][$_zone['value']] : ''),
                    'module' => MSCore::urls()->vars[0],
                    'zone' => $_zone['value'],
                    'type' => 'explorer',
                );
            }

            $vars['_FORM_'] = MSCore::forms()->make($config['config']);
            $vars['mod'] = MSCore::urls()->vars[0];

            die (template('module_config', $vars));
            break;

        case 'fastview':
            $path_id = isset(MSCore::urls()->vars[2]) ? MSCore::urls()->vars[2] : 0;
            $sel_page = isset(MSCore::urls()->vars[3]) ? MSCore::urls()->vars[3] : 0;

            $tmp = $CONFIG['module_name'] . '_tree_generateVars';
            $tree_vars = $tmp();

            $vars = array();
            $vars['CONFIG'] = $CONFIG;
            $vars['path_id'] = $path_id;
            $vars['page'] = 1;
            $vars['tree'] = template('tree', $tree_vars);

            $_RESULT = array('content' => template('fast', $vars));
            die(json_encode($_RESULT));
            break;


        case 'swap_tree':
            $path_id = isset(MSCore::urls()->vars[2]) ? MSCore::urls()->vars[2] : 0;
            $action = (isset(MSCore::urls()->vars[3]) && MSCore::urls()->vars[3] == 'up') ? 1 : 0;
            $tree_id = isset(MSCore::urls()->vars[4]) ? MSCore::urls()->vars[4] : 0;

            $tmp = $CONFIG['module_name'] . '_setSwapItemsOrder';
            $tmp($CONFIG['tables']['tree']['db_name'], $tree_id, $action);

            $tmp = $CONFIG['module_name'] . '_tree_generateVars';
            $tree_vars = $tmp();

            $_RESULT = array('content' => template('tree', $tree_vars));
            die(json_encode($_RESULT));
            break;

        case 'toggle_vis_tree':
            $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
            $tree_id = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;

            $active = MSCore::db()->getOne('SELECT `visible` FROM ' . PRFX . $CONFIG['tables']['tree']['db_name'] . ' WHERE id=' . (int)$tree_id);

            MSCore::db()->execute('UPDATE ' . PRFX . $CONFIG['tables']['tree']['db_name'] . ' SET `visible`=' . (int)!(boolean)$active . ' WHERE id=' . (int)$tree_id);

            $_RESULT = array('content' => '<img border="0" src="/DESIGN/ADMIN/images/' . ((int)!(boolean)$active == 0 ? 'invis' : 'vis') . '.gif" alt=""/>');
            die(json_encode($_RESULT));
            break;

        case 'add':
            $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
            $tree_id = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;
            $parent = (isset(MSCore::urls()->vars[4])) ? (int)MSCore::urls()->vars[4] : 0;
            $level = (isset(MSCore::urls()->vars[5])) ? (int)MSCore::urls()->vars[5] : 0;

            $tmp = $CONFIG['module_name'] . '_tree_generateConfigValues';
            $CONFIG = $tmp($tree_id, $parent, $path_id);

            if (isset($_REQUEST['conf']))
            {

                $tmp = $CONFIG['module_name'] . '_tree_saveItem';

                if ($inserted_id = $tmp(false))
                {

                    $tmp = $CONFIG['module_name'] . '_tree_generateVars';
                    $tree_vars = $tmp();

                    $tree_vars['apply'] = isset(MSCore::urls()->vars[6]) && MSCore::urls()->vars[6] > 0 ? 1 : 0;

                    $inserted_id = '<input id="inserted_id" type="hidden" value="' . $inserted_id . '" name="id"/>';

                    $_RESULT = array('content' => array(template('tree', $tree_vars), $inserted_id));
                    die(json_encode($_RESULT));

                }
                else
                {

                    echo '<i style="display:none">Fatal error: </i>Введенный "Символьный код" уже занят';

                    /**
                     * TODO: Сейчас на все ошибки одна причина, исправить :)
                     */

                }

            }
            else
            {

                foreach ($CONFIG['tables']['tree']['config'] as $k => $v)
                {

                    $v['where'] = empty($v['where']) ? 0 : $v['where'];

                    switch ($v['where'])
                    {

                        case 0:
                            # code...
                            break;

                        case 1:
                            if ($level == $CONFIG['tables']['tree']['max_levels'] - 1)
                            {
                                unset($CONFIG['tables']['tree']['config'][$k]);
                            }
                            break;

                        case 2:

                            if ($level < $CONFIG['tables']['tree']['max_levels'] - 1)
                            {
                                unset($CONFIG['tables']['tree']['config'][$k]);
                            }

                            break;

                    }

                }

                $vars['_FORM_'] = MSCore::forms()->make($CONFIG['tables']['tree']['config']);
                $vars['CONFIG'] = $CONFIG;

                $vars['id'] = (int)$tree_id;
                $vars['path_id'] = (int)$path_id;
                $vars['output_id'] = $output_id;

                echo template('add', $vars);
            }
            break;

        case 'del_tree':

            $path_id = (isset(MSCore::urls()->vars[2])) ? (int)MSCore::urls()->vars[2] : 0;
            $parent = (isset(MSCore::urls()->vars[3])) ? (int)MSCore::urls()->vars[3] : 0;

            if ($parent != 0)
            {
                $tmp = $CONFIG['module_name'] . '_deleteTree';
                $tmp($parent);
            }

            $tmp = $CONFIG['module_name'] . '_tree_generateVars';
            $tree_vars = $tmp();

            $_RESULT = array('content' => template('tree', $tree_vars));
            die(json_encode($_RESULT));
            break;

    }

    die();