<div class="tapeModuleWrap">

<?php

$d_tree_width = $CONFIG['tables']['tree']['dialog']['width'];
$d_tree_height = $CONFIG['tables']['tree']['dialog']['height'];

echo MSCore::forms()->writeModule_LinksPath();

if (AUTH_IS_ROOT || auth_Access($CONFIG['module_name'], 'add')){?>

    <button onclick="displayMessage('/<?=ROOT_PLACE;?>/<?=$CONFIG['module_name'];?>/add/<?=(int)$path_id?>/0/', <?=$d_tree_width;?>, <?=$d_tree_height;?>)">Добавить раздел</button>

<?}?>


<table cellpadding="0" cellspacing="0" border="0" align="center" class="catalog_table">
	<tr>
		<th style="border-right:0">

			<table cellpadding="0" cellspacing="0" border="0" width="100%" class="no_border">
				<tr>
					<td style="color: white;"><b></b></td>
				</tr>
			</table>
		
		</th>
	</tr>
	<tr>
		<td class="tree" id="tree">

			<?=$tree;?>
			
		</td>
	</tr>
</table>

</div>
