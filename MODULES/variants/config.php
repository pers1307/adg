<?php
    /*
    родительский элемент и у разделов и у элементов называется parent
    управление видимостью - поле visible (только у разделов)
    для элементов управление видимостью можно сделать отдельным полем
    */

    $module_name = 'variants';
    $module_caption = 'Варианты исполнения 2.0';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '2.0.0.0',

        'tables' => array(
            /*
                Названия таблиц "articles" и "items" обязательны, эти имена используются в различных функциях
            */


            //------------------------------Разделы-------------------------------------------------------
            'articles' => array(

                'db_name' => $module_name . '_articles',
                'dialog' => array('width' => 650, 'height' => 400),
                'onpage' => 20,

                //-------если поставить true то нельзя будет добавлять разделы первого уровня
                'noAddArticles' => false,

                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`name`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => false,

                //------масимальное количество уровней вложенности----------
                'max_levels' => 1,

                //------поле, которое будет выводиться в списке
                'title_field' => 'name',
                'is_order' => true, // Ручная сортировка

                'config' => array(

                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'Название',
                        'output' => 'open',
                    ),
                    'name' => array(
                        'caption' => 'Общее',
                        'value' => '',
                        'type' => 'string',
                        'to_code' => true,
                    ),
                    'titleName' => array(
                        'caption' => 'В карточке (конечная стр.)',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'titleGenerator' => array(
                        'caption' => 'В навигационной цепочке',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    'titleGenerator1' => array(
                        'caption' => 'В карточке генератора',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 0,
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),

                    'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 1,
                        'value' => '',
                    ),
                    'image' => array(
                        'caption' => 'Изображение',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'preview' => array(310, 300),
                            'view' => array(400, 400),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 1,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                    ),
                    'announce' => array(
                        'caption' => 'Краткое описание',
                        'value' => '',
                        'height' => 260,
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'main_text' => array(
                        'caption' => 'Основной текст',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0
                    ),
                    /*'text' => array(
                        'caption' => 'Подробное описание',
                        'value' => '',
                        'height' => 260,
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),*/

                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'CEO блок',
                        'output' => 'open',
                    ),
                    'title_h1' => array(
                        'caption' => 'Заголовок h1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),
                ),
            ),
            //------------------------------Разделы-------------------------------------------------------

            //------------------------------Товары-------------------------------------------------------
            'items' => array(

                'db_name' => $module_name . '_items',
                'dialog' => array('width' => 650, 'height' => 500),
                'onpage' => 20,
                //------если ордер не указан то сортировка будет вручную
//			'order_field' => '`title`',

                //---------указывает куда добавлять новые элементы, вверх или вниз по сортировке--------
                'add_new_on_top' => true,

                'config' => array(
                    'title' => array(
                        'caption' => 'Заголовок',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 1,
                        'to_code' => false,
                    ),
                    /*'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),*/
                    'text' => array(
                        'caption' => 'Описание',
                        'value' => '',
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    'gallery' => array(
                        'caption' => 'Галерея',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'min' => array(100, 100),
                            'preview' => array(310, 300),
                            'view' => array(1920, 1080),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 5,
                            'module-name' => $module_name
                        ),
                        'in_list' => 1,
                    ),
                    'show_in_station' => array(
                        'caption' => 'Отображать в эл-ции',
                        'value' => 1,
                        'type' => 'boolean',
                    ),
                    'active' => array(
                        'caption' => 'Опубликовано',
                        'value' => 1,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 1,
                    ),
                    array(
                        'type' => 'divider',
                    ),
                    array(
                        'type' => 'section',
                        'caption' => 'CEO блок',
                        'output' => 'open',
                    ),
                    'title_h1' => array(
                        'caption' => 'Заголовок h1',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'title_page' => array(
                        'caption' => 'Page title',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_description' => array(
                        'caption' => 'Meta description',
                        'value' => '',
                        'type' => 'string',
                    ),
                    'meta_keywords' => array(
                        'caption' => 'Meta keywords',
                        'value' => '',
                        'type' => 'string',
                    ),
                    array(
                        'type' => 'section',
                        'output' => 'close',
                    ),
                    array(
                        'type' => 'divider',
                    ),
                ),

                'links' => array(
                    array(
                        'target_module' => 'variants_item_list',
                        'target_key' => 'item_id',
                        'main_key' => 'id',
                    ),
                ),
            ),
            //------------------------------Товары-------------------------------------------------------
        )
    );

    return $CONFIG;