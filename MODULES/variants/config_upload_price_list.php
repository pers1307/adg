<?php
/*
родительский элемент и у разделов и у элементов называется parent
управление видимостью - поле visible (только у разделов)
для элементов управление видимостью можно сделать отдельным полем
*/

$module_name = 'variants';
$module_caption = 'Каталог';

$CONFIG = array(
    'module_name' => $module_name,
    'module_caption' => $module_caption,
    'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
    'version' => '1.1.0.0',
    'tables' => array(
        'items' => array(
            'db_name' => $module_name,
            'dialog' => array('width' => 660, 'height' => 410),
            'config' => array(
                'file' => array(
                    'caption' => 'Загрузка прайс листа',
                    'value' => '',
                    'type' => 'loader',
                    'settings' => array(
                        'allowed-extensions' => 'xls',
                        'size-limit' => 5 * 1024 * 1024,
                        'limit' => 1,
                        'module-name' => $module_name
                    ),
                    'in_list' => 1,
                    'filter' => 0,
                ),
            ),
        ),
    ),
);

return $CONFIG;