<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'variants_item_list';
    $module_caption = 'Список опции';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '1.1.0.0',
        'show' => false,
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order` ASC',
                'onpage' => 20,
                'config' => array(
                    'title' => array(
                        'caption' => 'Название',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'to_code' => false
                    ),
                    /*'code' => array(
                        'type' => 'code',
                        'caption' => 'Символьный код',
                        'in_list' => 0,
                        'value' => '',
                    ),*/
                    'text' => array(
                        'caption' => 'Подробное описание',
                        'value' => '',
                        'height' => 260,
                        'type' => 'wysiwyg',
                        'in_list' => 0,
                    ),
                    /*'price' => array(
                        'type' => 'string',
                        'caption' => 'Цена',
                        'in_list' => 1,
                        'value' => '',
                    ),*/
                    'gallery' => array(
                        'caption' => 'Галерея',
                        'value' => '',
                        'type' => 'loader',
                        'thumbs' => array(
                            'min' => array(90, 70),
                            'preview' => array(300, 250),
                            'view' => array(1920, 1080),
                        ),
                        'settings' => array(
                            'allowed-extensions' => 'jpg,jpeg,png',
                            'size-limit' => 5 * 1024 * 1024,
                            'limit' => 5,
                            'module-name' => $module_name
                        ),
                        'in_list' => 0,
                    ),
                    'active' => array(
                        'caption' => 'Активность',
                        'value' => '1',
                        'type' => 'checkbox',
                        'in_list' => 1,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;