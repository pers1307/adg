<?php
    /**
    ТИП МОДУЛЯ: СПИСОКОВЫЙ МОДУЛЬ (аля новости, статьи)
    это обычный модуль спискового типа, т.е. без разделов.
     */

    $module_name = 'www_types';
    $module_caption = 'Типы страниц';

    $CONFIG = array(

        'module_name' => $module_name,
        'module_caption' => $module_caption,
        'fastcall' => '/' . ROOT_PLACE . '/' . $module_name . '/fastview/',
        'version' => '6.0',
        'tables' => array(

            'items' => array(

                'db_name' => $module_name,
                'dialog' => array('width' => 660, 'height' => 410),
                'key_field' => 'id',
                'order_field' => '`order`',
                'onpage' => 20,
                'config' => array(

                    'title' => array(
                        'caption' => 'Название типа',
                        'value' => '',
                        'type' => 'string',
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'flag_addroot' => array(
                        'caption' => 'Возможность добавлять в корень',
                        'value' => 1,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 0,
                    ),
                    'flag_addsub' => array(
                        'caption' => 'Возможность добавлять под-разделы',
                        'value' => 1,
                        'type' => 'boolean',
                        'in_list' => 1,
                        'filter' => 0,
                    )
                ),
            ),
        ),
    );

    return $CONFIG;