<?php

    return [
		'catalog' => [
			'className' => 'ModelCatalog',
			'itemsTableName' => '{table_items}',
			'articlesTableName' => '{table_articles}',
			'pathId' => 0,
			'vars' => MSCore::urls()->vars,
			'articlesTreeFields' => [
				'title_h1',
				'title_page',
				'meta_description',
				'meta_keywords'
			],
			'onlyHaveItems' => false,
			'itemsOrder' => '`order` ASC',
			'pagination' => [
			'onPage' => 10
			]
		],
    ];