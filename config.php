<?php
    define('NOW_TIME', time());

    /** Установка внутренней кодировки */
    defined('SITE_ENCODING') or define('SITE_ENCODING', 'UTF-8');
    mb_internal_encoding(SITE_ENCODING);

    /** Определяем системные директории:
     * DS — альяс для константы DIRECTORY_SEPARATOR
     * DOC_ROOT — корень сайта
     * CORE_DIR — папка с ядром системы
     * MODULES_DIR — папка с модулями
     * DES_DIR — папка с дизайном
     * BLOCKS_DIR — папка с блоками вывода
     * FILES_DIR — папка с загружаемыми файлами
     * TMP_DIR — временная папка
     */

    define('DS',  DIRECTORY_SEPARATOR);
    define('DOC_ROOT', dirname(__FILE__));
    define('CORE_DIR', DOC_ROOT . DS . 'CORE');
    define('MODULES_DIR', DOC_ROOT . DS . 'MODULES');
    define('DES_DIR', DOC_ROOT . DS . 'DESIGN');
    define('BLOCKS_DIR', DES_DIR . DS . 'SITE' . DS . 'blocks');
    define('FILES_DIR', DOC_ROOT . DS . 'UPLOAD');
    define('TMP_DIR', DOC_ROOT . DS . 'TMP');
    define('VENDOR_DIR', DOC_ROOT . DS . 'VENDOR');
    define('CONFIG_DIR', CORE_DIR . DS . 'configs');
    define('INPUT_PRICE_LIST_DIR',  DOC_ROOT . DS . 'CRON' . DS . 'INPUT' . DS);
    define('OUTPUT_PRICE_LIST_DIR', DOC_ROOT . DS . 'CRON' . DS . 'OUTPUT' . DS);
    define('PRICE_LIST_DOWNLOAD_DIR', DOC_ROOT . DS . 'UPLOAD' . DS . 'price_lists' . DS . 'download' . DS);
    define('PRICE_LIST_DOWNLOAD_URL', DS . 'UPLOAD' . DS . 'price_lists' . DS . 'download' . DS);
    define('PRICE_LIST_HANDLED_DIR', DOC_ROOT . DS . 'UPLOAD' . DS . 'price_lists' . DS . 'handled' . DS);

    /** Проверяем, есть ли файл с локальными настройками. Если есть, то подключаем его */
    if(is_file($localConfig = DOC_ROOT . DS . 'config.local.php')){
        require_once $localConfig;
    }

    /** Определяем откуда пользователь (из МС или нет) */
    defined('MS') or define('MS', (isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], array('82.193.139.18'))));

    /** Имя сайта (домен) */
    $domain = isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : 'cms.dev';
    define('DOMAIN', str_replace('www.', '', $domain));
    define('HTTP_HOST', $domain);

    /** Имя сессии которое будет установлено в куку */
    define('SESS_NAME', 'mp6' . str_replace('.', '', DOMAIN));

    define('REMOTE_ADDR', isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 'unknown');

    /** Реквизиты для базы данных (продакшн) */
    defined('CONFIG_DBHOST') or define('CONFIG_DBHOST', 'localhost');
    defined('CONFIG_DBUSER') or define('CONFIG_DBUSER', 'test');
    defined('CONFIG_DBPASS') or define('CONFIG_DBPASS', 'test');
    defined('CONFIG_DBNAME') or define('CONFIG_DBNAME', 'test');

    /** Уровень ошибок */
    defined('ERROR_REPORTING') or define('ERROR_REPORTING', 0);
    error_reporting(defined(ERROR_REPORTING) ? constant(ERROR_REPORTING) : ERROR_REPORTING);

    /** Префикс для таблиц БД */
    define('PRFX', 'mp_', true);

    /** Сжимать JS */
    defined('COMPRESS_JS') or define('COMPRESS_JS', false);

    /** Сжимать CSS */
    defined('COMPRESS_CSS') or define('COMPRESS_CSS', false);

    /** Делать редирект c domain.ru на www.domain.ru */
    defined('WWW_REDIRECT') or define('WWW_REDIRECT', true);

    /** БЕЗОПАСНОСТЬ */
    /** Авторизация для уникального айпи */
    define('CHECK_UNIQ_IP', 0);
    define('ROOT_PLACE', 'admin');
    /** Дефолтный доступ 0 - запрещено все, 1 - разрешено все */
    define('DEFAULT_ACCESS', 0);
    /** Отображать системные страницы для ROOT */
    define('SHOW_SYSTEM_PAGES', false);
    defined('HTTPS_REDIRECT') or define('HTTPS_REDIRECT', false);

    /** Включить дебаг функцию */
    defined('MS_DEBUG') or define('MS_DEBUG', false);

    /** Настройки cookie*/
    defined('COOKIE_SALT') or define('COOKIE_SALT', '3292a11b524a41aac4993659fc1a98cd');
    defined('COOKIE_LIFETIME') or define('COOKIE_LIFETIME', '31556926');

    define('IS_WIN', strtoupper(substr(PHP_OS, 0, 3)) === 'WIN');

    /** Пути до подключаемых файлов стилей и скриптов */
    $GLOBALS['staticFiles'] = array(

        /** Скрипты и стили для сайта */
        'site-default-scripts' => array(
            '//DESIGN/SITE/js/libs/modernizr-2.5.3.min.js',
        ),
        'site-async-scripts' => [
            '//DESIGN/SITE/js/libs/jquery-1.9.1.min.js',
            '//DESIGN/SITE/js/libs/fancybox.js',
            '//DESIGN/SITE/js/libs/slick.js',
            '//DESIGN/SITE/js/libs/maskedInput.js',
            '//DESIGN/SITE/js/libs/hammer.js',
            '//DESIGN/SITE/js/libs/sly.js',
            '//DESIGN/SITE/js/libs/tooltipster.bundle.min.js',
            '//DESIGN/SITE/js/maps.js',
            '//DESIGN/SITE/js/script.js',
        ],
        'site-default-styles' => array(
            '//DESIGN/SITE/css/content.css',
            '//DESIGN/SITE/css/style.css',
	        '//DESIGN/SITE/css/custom.css'
        ),
        /** Скрипты и стили для панели администратора */
        'admin-general-scripts' => array(
            '//DESIGN/CONTROL/js/jquery-1.9.1.min.js',
            '//DESIGN/CONTROL/js/jquery-ui-1.10.3.custom.min.js',
            '//DESIGN/CONTROL/js/jquery.ui.datepicker-ru.js',
            '//DESIGN/CONTROL/js/jquery-migrate.js',
            '//DESIGN/CONTROL/js/jquery.cookie.js',
            '//DESIGN/CONTROL/js/jquery.sortable.js',
            '//DESIGN/CONTROL/js/timepicker/jquery-ui-timepicker-addon.js',
            '//DESIGN/CONTROL/js/timepicker/jquery-ui-timepicker-ru.js',
            '//DESIGN/CONTROL/js/utils.js',
            '//DESIGN/CONTROL/js/site.js',
            '//DESIGN/CONTROL/js/numberMask.js',
            '//DESIGN/CONTROL/js/jquery.placeholder.min.js',
            '//DESIGN/CONTROL/js/jquery.jrumble.min.js',
            '//DESIGN/CONTROL/js/baron.js',
            '//DESIGN/CONTROL/js/select2/select2.min.js',
            '//DESIGN/CONTROL/js/select2/select2_locale_ru.js',
            '//DESIGN/CONTROL/js/tinymce/jquery.tinymce.min.js',
            '//DESIGN/CONTROL/js/functions.js',
            '//DESIGN/CONTROL/js/tabs.js',
            '//DESIGN/CONTROL/js/treeMenu.js',
            '//DESIGN/CONTROL/js/loginForm.js',
            '//DESIGN/CONTROL/js/pagesTree.js',
            '//DESIGN/CONTROL/js/moduleControls.js',
            '//DESIGN/CONTROL/js/generalModules.js',
            '//DESIGN/CONTROL/js/modalWindow.js',
            '//DESIGN/CONTROL/js/customElements.js',
            '//DESIGN/CONTROL/js/baronScrollerController.js',
            '//DESIGN/CONTROL/js/scrollableTable.js',
            '//DESIGN/CONTROL/js/jquery.fineuploader-3.6.4.min.js',
            '//DESIGN/CONTROL/js/fileLoader.js',
            '//DESIGN/CONTROL/js/general.js',
            '//DESIGN/CONTROL/js/colorPicker/spectrum.js',
        ),
        'admin-general-styles' => array(
            '//DESIGN/CONTROL/css/reset.css',
            '//DESIGN/CONTROL/css/fonts.css',
            '//DESIGN/CONTROL/css/font-awesome.min.css',
            '//DESIGN/CONTROL/css/jquery-ui/ms-theme/jquery-ui-1.10.3.custom.min.css',
            '//DESIGN/CONTROL/js/timepicker/jquery-ui-timepicker-addon.css',
            '//DESIGN/CONTROL/css/general.css',
            '//DESIGN/CONTROL/js/select2/select2.css',
            '//DESIGN/CONTROL/css/main.css',
            '//DESIGN/CONTROL/css/auth.css',
            '//DESIGN/CONTROL/css/tabs.css',
            '//DESIGN/CONTROL/css/moduleControls.css',
            '//DESIGN/CONTROL/css/moduleCatalog.css',
            '//DESIGN/CONTROL/css/moduleTape.css',
            '//DESIGN/CONTROL/css/scrollableTable.css',
            '//DESIGN/CONTROL/css/modalWindow.css',
            '//DESIGN/CONTROL/css/form.css',
            '//DESIGN/CONTROL/css/treeMenu.css',
            '//DESIGN/CONTROL/css/baronScroller.css',
            '//DESIGN/CONTROL/css/fileLoader.css',
            '//DESIGN/CONTROL/css/spectrum.css',
        )

    );

    define('POWER_STATION_PATH_ID', 188);
    define('VARIANTS_PATH_ID', 194);
    define('SPARES_PATH_ID', 192);
    define('OPTIONAL_EQUIPMENT', 190);
    define('POLICY', 186);
    define('CONSENT', 185);
