<?php

require_once 'console.php';

require_once VENDOR_DIR  . '/phpoffice/phpexcel/Classes/PHPExcel.php';

// Создаем объект класса PHPExcel
$xls = new PHPExcel();
// Устанавливаем индекс активного листа
$xls->setActiveSheetIndex(0);
// Получаем активный лист
$sheet = $xls->getActiveSheet();
// Подписываем лист
$sheet->setTitle('Выгрузка');

$items = MSCore::db()->getAll('
    SELECT
      power_station.id,
      power_station.article,
      power_station.station,
      power_station.name,
      power_station.price_rub,
      power_station.price_usd,
      power_station.price_eur,
      power_station.price_jpy,
      power_station.active_currency,
      optional.title options
    FROM (
      SELECT
      mp_power_stations_items.id,
      psa2.name as article,
      psa.name as station,
      mp_variants_articles.name,
      mp_power_stations_items.price_rub,
      mp_power_stations_items.price_usd,
      mp_power_stations_items.price_eur,
      mp_power_stations_items.price_jpy,
      mp_power_stations_items.active_currency
    FROM mp_power_stations_items
    JOIN mp_power_stations_articles psa ON mp_power_stations_items.parent = psa.id
    JOIN mp_power_stations_articles psa2 ON psa.parent = psa2.id
    JOIN mp_variants_articles ON mp_power_stations_items.title = mp_variants_articles.id
    ) as power_station
    LEFT JOIN (
      SELECT
      mp_optional_eq_prices.id,
      mp_optional_eq_prices.item_id,
      mp_optional_equipment_items.title
    FROM mp_optional_eq_prices
    JOIN mp_optional_equipment_items ON mp_optional_eq_prices.title = mp_optional_equipment_items.id
    WHERE mp_optional_equipment_items.id = 82
    ) as optional
    ON power_station.id = optional.item_id;
');

foreach ($items as &$item) {
    $item['active_currency'] =
        (CurrencyHelper::getCurrencyByCode($item['active_currency']))->isoName;
}

$sheet->setCellValueByColumnAndRow(
    0,
    1,
    'id'
);
$sheet->setCellValueByColumnAndRow(
    1,
    1,
    'Мощность'
);
$sheet->setCellValueByColumnAndRow(
    2,
    1,
    'Генератор'
);
$sheet->setCellValueByColumnAndRow(
    3,
    1,
    'Модификация'
);
$sheet->setCellValueByColumnAndRow(
    4,
    1,
    'Цена в руб'
);
$sheet->setCellValueByColumnAndRow(
    5,
    1,
    'Цена в usd'
);
$sheet->setCellValueByColumnAndRow(
    6,
    1,
    'Цена в eur'
);
$sheet->setCellValueByColumnAndRow(
    7,
    1,
    'Цена в jpy'
);
$sheet->setCellValueByColumnAndRow(
    8,
    1,
    'Выбранная валюта'
);
$sheet->setCellValueByColumnAndRow(
    9,
    1,
    'Шкаф'
);

foreach ($items as $key => $item) {
    $count = 0;
    foreach ($item as $itemKey => $param) {
        $sheet->setCellValueByColumnAndRow(
            $count,
            $key + 2,
            $param
        );

        // Применяем выравнивание
        $sheet->getStyleByColumnAndRow($count, $key + 2)->getAlignment()->
        setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        ++$count;
    }
}

$objWriter = new \PHPExcel_Writer_Excel5($xls);
$objWriter->save('prices.xls');