<?php

require_once 'console.php';

require_once VENDOR_DIR  . '/phpoffice/phpexcel/Classes/PHPExcel.php';

// Создаем объект класса PHPExcel
$xls = new PHPExcel();
// Устанавливаем индекс активного листа
$xls->setActiveSheetIndex(0);
// Получаем активный лист
$sheet = $xls->getActiveSheet();
// Подписываем лист
$sheet->setTitle('Выгрузка');

$items = MSCore::db()->getAll('
    SELECT
      mp_optional_eq_prices.id,
      psa2.name as article,
      psa.name as station,
      mp_variants_articles.name,
      mp_optional_eq_prices.price_rub,
      mp_optional_eq_prices.price_usd,
      mp_optional_eq_prices.price_eur,
      mp_optional_eq_prices.price_jpy,
      mp_optional_eq_prices.active_currency
    FROM mp_power_stations_items
    JOIN mp_power_stations_articles psa ON mp_power_stations_items.parent = psa.id
    JOIN mp_power_stations_articles psa2 ON psa.parent = psa2.id
    JOIN mp_variants_articles ON mp_power_stations_items.title = mp_variants_articles.id
    JOIN mp_optional_eq_prices ON mp_optional_eq_prices.item_id = mp_power_stations_items.id
    WHERE mp_optional_eq_prices.title = \'101\' AND mp_power_stations_items.title = \'2\'
    ORDER BY article ASC
');

foreach ($items as &$item) {
    $item['active_currency'] =
        (CurrencyHelper::getCurrencyByCode($item['active_currency']))->isoName;
}

$sheet->setCellValueByColumnAndRow(
    0,
    1,
    'id'
);
$sheet->setCellValueByColumnAndRow(
    1,
    1,
    'Мощность'
);
$sheet->setCellValueByColumnAndRow(
    2,
    1,
    'Генератор'
);
$sheet->setCellValueByColumnAndRow(
    3,
    1,
    'Модификация'
);
$sheet->setCellValueByColumnAndRow(
    4,
    1,
    'Цена в руб'
);
$sheet->setCellValueByColumnAndRow(
    5,
    1,
    'Цена в usd'
);
$sheet->setCellValueByColumnAndRow(
    6,
    1,
    'Цена в eur'
);
$sheet->setCellValueByColumnAndRow(
    7,
    1,
    'Цена в jpy'
);
$sheet->setCellValueByColumnAndRow(
    8,
    1,
    'Выбранная валюта'
);

foreach ($items as $key => $item) {
    $count = 0;
    foreach ($item as $itemKey => $param) {
        $sheet->setCellValueByColumnAndRow(
            $count,
            $key + 2,
            $param
        );

        // Применяем выравнивание
        $sheet->getStyleByColumnAndRow($count, $key + 2)->getAlignment()->
        setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);

        ++$count;
    }
}

$objWriter = new \PHPExcel_Writer_Excel5($xls);
$objWriter->save('prices_chassi.xls');