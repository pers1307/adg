<?php

require_once 'console.php';

$items = MSCore::db()->getAll('
    SELECT *
    FROM mp_engines_items
');

foreach ($items as $item) {
    $properties = json_decode($item['properties'], true);

    foreach ($properties as $key => $property) {
        if ($key == 'boost') {
            $properties[$key] = str_replace("охдаждением", "охлаждением", $property);
        }

        if ($key == 'speedRegulator') {
            $properties[$key] = str_replace("эленктронный", "электронный", $property);
        }
    }

    MSCore::db()->update(
        'mp_engines_items',
        [
            'properties' => json_encode($properties)
        ],
        '`id` = ' . $item['id']
    );
}