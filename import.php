<?php

require_once 'console.php';
require_once VENDOR_DIR  . '/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

$objPHPExcel = \PHPExcel_IOFactory::load('price_import.xls');
$objPHPExcel->setActiveSheetIndex(0);
$aSheet = $objPHPExcel->getActiveSheet();

//этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
$array = array();
//получим итератор строки и пройдемся по нему циклом
foreach($aSheet->getRowIterator() as $row){
    //получим итератор ячеек текущей строки
    $cellIterator = $row->getCellIterator();
    //пройдемся циклом по ячейкам строки
    //этот массив будет содержать значения каждой отдельной строки
    $item = array();
    foreach($cellIterator as $cell){
        //заносим значения ячеек одной строки в отдельный массив
        //array_push($item, iconv('utf-8', 'cp1251', $cell->getCalculatedValue()));
        array_push($item, $cell->getCalculatedValue());
    }
    //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
    array_push($array, $item);
}

/**
 * todo: делаем map типов
 */
$typeMap = [
    'Открытый тип'    => 1,
    'В кожухе'        => 3,
    'В контейнере'    => 4,
    'Передвижной тип' => 2,
];

foreach ($array as $key => $item) {
    if ($key == 0) {
        continue;
    }

    /**
     * Ищем по названию товара
     */
    $idPowerStation = MSCore::db()->getOne('
            SELECT id
            FROM mp_power_stations_articles
            WHERE `name` LIKE "%' . $item[2] . '%"
        ');

    if (empty($idPowerStation)) {
        echo  $item[2];
    }

    /**
     * потом ищем непосредственно модификацию
     */
    $idModification = MSCore::db()->getOne('
            SELECT id
            FROM mp_power_stations_items
            WHERE `title` = "' . $typeMap[$item[3]] . '"
            AND parent = ' . $idPowerStation . '
        ');

    if (empty($idModification)) {
        echo  $item[2] . ' ' . $item[9];
    }

    if ($item[8] == 'RUB') {
        MSCore::db()->update(
            PRFX . 'power_stations_items',
            [
                'price_rub' => $item[4],
                'price_usd' => 0,
                'price_eur' => 0,
                'price_jpy' => 0,
                'active_currency' => 643,
            ],
            '`id` = ' . $idModification
        );
    }

    if ($item[8] == 'USD') {
        MSCore::db()->update(
            PRFX . 'power_stations_items',
            [
                'price_rub' => 0,
                'price_usd' => $item[5],
                'price_eur' => 0,
                'price_jpy' => 0,
                'active_currency' => 978,
            ],
            '`id` = ' . $idModification
        );
    }

    if ($item[8] == 'EUR') {
        MSCore::db()->update(
            PRFX . 'power_stations_items',
            [
                'price_rub' => 0,
                'price_usd' => 0,
                'price_eur' => $item[6],
                'price_jpy' => 0,
                'active_currency' => 840,
            ],
            '`id` = ' . $idModification
        );
    }

    if (!empty($item[9])) {
        $id = MSCore::db()->getOne('
            SELECT id
            FROM mp_optional_equipment_items
            WHERE `title` LIKE "%' . $item[9] . '%"
        ');

        // 82
        if (!empty($id)) {
//            if ($item[3] == 'Открытый тип') {
//                --$item[0];
//            }

            MSCore::db()->update(
                PRFX . 'optional_eq_prices',
                [
                    'title' => $id
                ],
                '`title` = "82" AND `item_id` = ' . $item[0]
            );
        }
    }
}