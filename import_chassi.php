<?php

require_once 'console.php';

/**
 * Парсим файлик
 */
require_once VENDOR_DIR  . '/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

$objPHPExcel = \PHPExcel_IOFactory::load('prices_chassi.xls');
$objPHPExcel->setActiveSheetIndex(0);
$aSheet = $objPHPExcel->getActiveSheet();

//этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
$array = array();
//получим итератор строки и пройдемся по нему циклом
foreach($aSheet->getRowIterator() as $row){
    //получим итератор ячеек текущей строки
    $cellIterator = $row->getCellIterator();
    //пройдемся циклом по ячейкам строки
    //этот массив будет содержать значения каждой отдельной строки
    $item = array();
    foreach($cellIterator as $cell){
        //заносим значения ячеек одной строки в отдельный массив
        //array_push($item, iconv('utf-8', 'cp1251', $cell->getCalculatedValue()));
        array_push($item, $cell->getCalculatedValue());
    }
    //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
    array_push($array, $item);
}

foreach ($array as $key => $item) {
    if ($key == 0) {
        continue;
    }

    if (!empty($item[0])) {
        MSCore::db()->update(
            PRFX . 'optional_eq_prices',
            [
                'price_rub' => $item[4]
            ],
            '`id` = ' . $item[0]
        );
    } else {
        /**
         * по названию найти передвижной тип и его шасси
         */
        $idOptionalEqPrices = MSCore::db()->getOne('
            SELECT mp_optional_eq_prices.id
            FROM mp_power_stations_articles
            JOIN mp_power_stations_items ON mp_power_stations_items.parent = mp_power_stations_articles.id
            JOIN mp_optional_eq_prices ON mp_optional_eq_prices.item_id = mp_power_stations_items.id
            WHERE
              mp_power_stations_articles.name LIKE "' . $item[2] . '"
              AND mp_power_stations_items.code = \'peredvezhnoj\'
        ');

        MSCore::db()->update(
            PRFX . 'optional_eq_prices',
            [
                'price_rub' => $item[4]
            ],
            '`id` = ' . $idOptionalEqPrices
        );
    }
}