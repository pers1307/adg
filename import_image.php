<?php

require_once 'console.php';

/**
 * Достать все нужные элементы
 */
$items = MSCore::db()->getAll('
    SELECT *
    FROM mp_power_stations_items
    WHERE title = \'4\'
    ORDER BY `id` ASC
');

/**
 * Пройтись по их картинкам и удалить их
 */
foreach ($items as $item) {
    $images = unserialize($item['image']);

    foreach ($images as $image) {

        foreach ($image['path'] as $path) {
            $file = __DIR__ . $path;

            if (file_exists($file)) {
                unlink($file);
            }
        }
    }
}

$dir    = '/UPLOAD/container/';
$files = scandir(DOC_ROOT . $dir, SCANDIR_SORT_ASCENDING);

/**
 * todo: поменять файлы местами
 */
$name10 = $files[3];
unset($files[3]);
$files[] = $name10;

$smallConf   = array(100, 100);
$previewConf = array(300, 300);
$viewConf    = array(400, 400);
$bigConf     = array(1200, 1200);

//'small' => array(100, 100),
//'preview' => array(300, 300),
//'view' => array(400, 400),
//'big' => array(1200, 1200),

foreach ($items as $item) {
    $galleryNew = [];

    foreach ($files as $file) {
        if ($file == '.' || $file == '..') {
            continue;
        }

        $newFileName = md5($item['id']) . $file;
        $uploadDir = MSFiles::getUploadFolder() . '/';
        copy(DOC_ROOT . $dir . $file, $uploadDir . $newFileName);

        $path = [
            'sys_thumb' => MSFiles::makeImageThumb($uploadDir . $newFileName, [80, 80]),
            'small'     => MSFiles::makeImageThumb($uploadDir . $newFileName, $smallConf),
            'preview'   => MSFiles::makeImageThumb($uploadDir . $newFileName, $previewConf),
            'view'      => MSFiles::makeImageThumb($uploadDir . $newFileName, $viewConf),
            'big'       => MSFiles::makeImageThumb($uploadDir . $newFileName, $bigConf),
            'original'  => $uploadDir . $newFileName
        ];

        /**
         * todo: тут нужно добавить определенные параметры
         */
        $galleryNew[] = [
            'id'   => md5($item['id']),
            'path' => $path,
            'text' => ''
        ];
    }

    /**
     * кинуть апдейт на это дело
     */
    MSCore::db()->update(
        PRFX . 'power_stations_items',
        [
            'image' => serialize($galleryNew)
        ],
        '`id` = ' . $item['id']
    );
}