<?php

require_once 'console.php';

$paths = [
    [
        'id' => [4],
        'path' => '/UPLOAD/cummins/',
    ],
    [
        'id' => [8],
        'path' => '/UPLOAD/DEUTZ/',
    ],
    [
        'id' => [10],
        'path' => '/UPLOAD/SDEC/',
    ],
    [
        'id' => [9],
        'path' => '/UPLOAD/Doosan/',
    ],
    [
        'id' => [5],
        'path' => '/UPLOAD/Perkins/',
    ],
    [
        'id' => [3],
        'path' => '/UPLOAD/RICARDO/',
    ],
    [
        'id' => [11],
        'path' => '/UPLOAD/Scania/',
    ],
    [
        'id' => [6,7],
        'path' => '/UPLOAD/Weichai/',
    ],
];

foreach ($paths as $path) {
    process($path['id'], $path['path']);
}

function process($ids, $path)
{
    foreach ($ids as $id) {
        rendering($id, 1, $path . 'open/');
        rendering($id, 3, $path . 'casing/');
    }
}

function rendering($engineId, $typeId, $fullPath)
{
    /**
     * Достать все нужные элементы
     */
    $items = MSCore::db()->getAll('
        SELECT
          mp_power_stations_items.id,
          mp_power_stations_items.code,
          mp_power_stations_articles.name,
          mp_power_stations_items.image,
          mp_engines_articles.name
        FROM mp_power_stations_articles
        JOIN mp_engines_items ON mp_power_stations_articles.engine = mp_engines_items.id
        JOIN mp_engines_articles ON mp_engines_articles.id = mp_engines_items.parent
        JOIN mp_power_stations_items ON mp_power_stations_articles.id = mp_power_stations_items.parent
        WHERE mp_power_stations_items.title = ' . $typeId . '
        AND mp_engines_articles.id = ' . $engineId . ';
    ');

    /**
     * Пройтись по их картинкам и удалить их
     */
    foreach ($items as $item) {
        $images = unserialize($item['image']);

        foreach ($images as $image) {

            foreach ($image['path'] as $path) {
                $file = __DIR__ . $path;

                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

    $dir   = $fullPath;
    $files = scandir(DOC_ROOT . $dir, SCANDIR_SORT_ASCENDING);
    sort($files, SORT_NUMERIC);

    $smallConf   = array(100, 100);
    $previewConf = array(300, 300);
    $viewConf    = array(400, 400);
    $bigConf     = array(1200, 1200);

    //'small' => array(100, 100),
    //'preview' => array(300, 300),
    //'view' => array(400, 400),
    //'big' => array(1200, 1200),

    foreach ($items as $item) {
        $galleryNew = [];

        foreach ($files as $file) {
            if ($file == '.' || $file == '..') {
                continue;
            }

            $newFileName = md5($item['id']) . $file;
            $uploadDir = MSFiles::getUploadFolder() . '/';
            copy(DOC_ROOT . $dir . $file, $uploadDir . $newFileName);

            $path = [
                'sys_thumb' => MSFiles::makeImageThumb($uploadDir . $newFileName, [80, 80]),
                'small'     => MSFiles::makeImageThumb($uploadDir . $newFileName, $smallConf),
                'preview'   => MSFiles::makeImageThumb($uploadDir . $newFileName, $previewConf),
                'view'      => MSFiles::makeImageThumb($uploadDir . $newFileName, $viewConf),
                'big'       => MSFiles::makeImageThumb($uploadDir . $newFileName, $bigConf),
                'original'  => $uploadDir . $newFileName
            ];

            /**
             * todo: тут нужно добавить определенные параметры
             */
            $galleryNew[] = [
                'id'   => md5($item['id']),
                'path' => $path,
                'text' => ''
            ];
        }

        /**
         * кинуть апдейт на это дело
         */
        MSCore::db()->update(
            PRFX . 'power_stations_items',
            [
                'image' => serialize($galleryNew)
            ],
            '`id` = ' . $item['id']
        );
    }
}