<?php

require_once 'console.php';

$products = [
    'AD-650MS',
    'AD-750MS',
    'AD-1000MS',
    'AD-1250MS',
    'AD-1375MS',
    'AD-1500MS',
    'AD-1700MS',
    'AD-1875MS',
    'AD-2000MS',
    'AD-2250MS',
    'AD-450MT',
    'AD-500MT',
    'AD-600MT',
    'AD-655MT',
    'AD-660MT',
    'AD-800MT',
    'AD-925MT',
    'AD-1000MT',
    'AD-1125MT',
    'AD-1250MT',
    'AD-1500MT',
    'AD-1640MT',
    'AD-1800MT',
    'AD-2050MT',
    'AD-2250MT',
    'AD-2500MT',
    'AD-2750MT',
    'AD-3000MT',
    'AD-30IS',
    'AD-45IS',
    'AD-50IS',
    'AD-60IS',
    'AD-75IS',
    'AD-80IS',
    'AD-100IS',
    'AD-120IS',
    'AD-125IS',
    'AD-130IS',
    'AD-150IS',
    'AD-160IS',
    'AD-170IS',
    'AD-200IS',
    'AD-250IS',
    'AD-300IS',
    'AD-350IS',
    'AD-400IS',
    'AD-440IS',
    'AD-500IS',
    'AD-600IS',
    'AD-7J',
    'AD-9J',
    'AD-12J',
    'AD-15J',
    'AD-20J',
    'AD-22J',
    'AD-30J',
    'AD-35J',
    'AD-2250C',
    'AD-2500C',
    'AD-2750C',
    'AD-3000C',
    'AD-3000C',
];

$paths = [
    [
        'id' => 14,
        'path' => '/UPLOAD/images/open/IVECO-MOTORS/',
    ],
    [
        'id' => 15,
        'path' => '/UPLOAD/images/open/kubota/',
    ],
    [
        'id' => 12,
        'path' => '/UPLOAD/images/open/Mitsubishi/',
    ],
    [
        'id' => 13,
        'path' => '/UPLOAD/images/open/MTU/',
    ],
];

$pathOthers = [
    'kozhuh'       => '/UPLOAD/images/casing/',
    'konteyner'    => '/UPLOAD/images/container/',
    'peredvezhnoj' => '/UPLOAD/images/mobile/',
];

foreach ($products as $productName) {
    $items = MSCore::db()->getAll('
        SELECT
          mp_power_stations_items.*,
          mp_engines_articles.id engineId
        FROM mp_power_stations_articles
        JOIN mp_engines_items ON mp_power_stations_articles.engine = mp_engines_items.id
        JOIN mp_engines_articles ON mp_engines_articles.id = mp_engines_items.parent
        JOIN mp_power_stations_items ON mp_power_stations_articles.id = mp_power_stations_items.parent
        WHERE mp_power_stations_articles.name LIKE "%' . $productName . '%"
    ');

    foreach ($items as $item) {
        if ($item['code'] == 'open') {
            foreach ($paths as $path) {
                if ($item['engineId'] == $path['id']) {
                    rendering2($item, $path['path']);
                }
            }
        } else {
            rendering2($item, $pathOthers[$item['code']]);
        }
    }


}

function rendering2($item, $path)
{
    $images = unserialize($item['image']);

    if (!empty($images)) {
        foreach ($images as $image) {

            foreach ($image['path'] as $pathImage) {
                $file = __DIR__ . $pathImage;

                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

    $dir   = $path;
    $files = scandir(DOC_ROOT . $dir, SCANDIR_SORT_ASCENDING);
    sort($files, SORT_NUMERIC);

    $smallConf   = array(100, 100);
    $previewConf = array(300, 300);
    $viewConf    = array(400, 400);
    $bigConf     = array(1200, 1200);

    $galleryNew = [];

    foreach ($files as $file) {
        if ($file == '.' || $file == '..') {
            continue;
        }

        $newFileName = md5($item['id']) . $file;
        $uploadDir = MSFiles::getUploadFolder() . '/';
        copy(DOC_ROOT . $dir . $file, $uploadDir . $newFileName);

        $path = [
            'sys_thumb' => MSFiles::makeImageThumb($uploadDir . $newFileName, [80, 80]),
            'small'     => MSFiles::makeImageThumb($uploadDir . $newFileName, $smallConf),
            'preview'   => MSFiles::makeImageThumb($uploadDir . $newFileName, $previewConf),
            'view'      => MSFiles::makeImageThumb($uploadDir . $newFileName, $viewConf),
            'big'       => MSFiles::makeImageThumb($uploadDir . $newFileName, $bigConf),
            'original'  => $uploadDir . $newFileName
        ];

        $galleryNew[] = [
            'id'   => md5($item['id']),
            'path' => $path,
            'text' => ''
        ];
    }

    MSCore::db()->update(
        PRFX . 'power_stations_items',
        [
            'image' => serialize($galleryNew)
        ],
        '`id` = ' . $item['id']
    );
}