<?php

require_once 'console.php';

/**
 * Парсим файлик
 */

require_once VENDOR_DIR  . '/phpoffice/phpexcel/Classes/PHPExcel/IOFactory.php';

$objPHPExcel = \PHPExcel_IOFactory::load('parts2.ods');
$objPHPExcel->setActiveSheetIndex(0);
$aSheet = $objPHPExcel->getActiveSheet();

//этот массив будет содержать массивы содержащие в себе значения ячеек каждой строки
$array = array();
//получим итератор строки и пройдемся по нему циклом
foreach($aSheet->getRowIterator() as $row){
    //получим итератор ячеек текущей строки
    $cellIterator = $row->getCellIterator();
    //пройдемся циклом по ячейкам строки
    //этот массив будет содержать значения каждой отдельной строки
    $item = array();
    foreach($cellIterator as $cell){
        //заносим значения ячеек одной строки в отдельный массив
        //array_push($item, iconv('utf-8', 'cp1251', $cell->getCalculatedValue()));
        array_push($item, $cell->getCalculatedValue());
    }
    //заносим массив со значениями ячеек отдельной строки в "общий массв строк"
    array_push($array, $item);
}

$articleId = 0;

foreach ($array as $key => $item) {
    if (is_null($item[0])) {
        continue;
    }

    if (is_null($item[1])) {
        $query = new MSTable(PRFX . 'spare_parts_articles');
        $query->setFields(['id']);
        $query->setFilter('`name` = "' . $item[0] . '"');
        $articleId = $query->getItemOne();

        continue;
    }

    // $table, array $items,
    MSCore::db()->insert(
        PRFX . 'spare_parts_items',
        [
            'path_id'   => 0,
            'parent'    => $articleId,
            'order'     => 0,
            'title'     => $item[0],
            'code'      => translitUrl($item[0]),
            'available' => ($item[1] == 'в наличии') ? 1 : 0,
        ]
    );
}