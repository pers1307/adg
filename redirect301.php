<?php
require_once 'console.php';

require_once VENDOR_DIR  . '/phpoffice/phpexcel/Classes/PHPExcel.php';

date_default_timezone_set('Europe/Moscow');

function parse_excel_file( $filename ){
    // подключаем библиотеку


    $result = array();

    // получаем тип файла (xls, xlsx), чтобы правильно его обработать
    $file_type = \PHPExcel_IOFactory::identify( $filename );
    // создаем объект для чтения
    $objReader = \PHPExcel_IOFactory::createReader( $file_type );
    $objPHPExcel = $objReader->load( $filename ); // загружаем данные файла в объект
    $result = $objPHPExcel->getActiveSheet()->toArray(); // выгружаем данные из объекта в массив

    return $result;
}

$root = $_SERVER['DOCUMENT_ROOT'];
$filename = $root . 'redirects301.xlsx';
$res = parse_excel_file( $filename );
$count = 0;
foreach ($res as $key) {
    if ($count === 0) {
        $count++;
        continue;
    }
    MSCore::db()->insert("mp_redirects", [
        'path_id' => 0,
        'from' => $key[0],
        'to' => $key[1],
        'active' => 1
    ]);
}
